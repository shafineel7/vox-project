# -*- coding: utf-8 -*-
from odoo import models, fields, api
class ProductProduct(models.Model):
    _inherit = 'product.product'

    is_gate_pass = fields.Boolean(string="Gate Pass Expense")

