# -*- coding: utf-8 -*-
from odoo import models, fields, api, Command, _,exceptions
from odoo.exceptions import ValidationError
# from babel.dates import format_date
from datetime import date


class GatePassRequest(models.Model):
    _name = 'gate.pass.request'
    _description = 'gate Pass Request'
    _inherit = ['mail.thread', 'mail.activity.mixin']


    def _get_default_groups(self):
        staff_op = self.env.ref('department_groups.group_staff_operations_super').id
        con_op = self.env.ref('department_groups.group_construction_operations_super').id
        soft_op = self.env.ref('department_groups.group_soft_ser_operations_super').id
        con_co = self.env.ref('department_groups.group_construction_operations_coord').id
        staff_co = self.env.ref('department_groups.group_staff_operations_coord').id
        soft_co = self.env.ref('department_groups.group_soft_ser_operations_coord').id
        return [Command.link(staff_op),Command.link(con_op),Command.link(soft_op),Command.link(con_co),Command.link(staff_co),Command.link(soft_co)]

    name = fields.Char('Name', copy=False)
    gate_pass_type_id = fields.Many2one('gate.pass.type',string="gate Pass Type")
    employee_id = fields.Many2one('hr.employee', string='Employee')
    employee_sequence = fields.Char('Employee ID',related='employee_id.employee_sequence',store=True)
    start_date = fields.Date("Start Date", default=fields.Date.today)
    end_date = fields.Date("End Date",default=fields.Date.today)
    cancellation_date = fields.Date(string="Cancellation Date")
    duration = fields.Integer(String="Duration",compute='_compute_duration',store=True)
    gate_pass_amount = fields.Float(string="Gate Pass Amount")
    gate_pass_attachment = fields.Binary(string="Gate Pass Attachment")
    status = fields.Selection([('new', 'New'), ('hod_approval', 'HOD Approval'),
                               ('finance_mgr_approval', 'Finance Manager Approval'),
                               ('accounts_approval', 'Accounts Approval'),
                               ('closed', 'Closed'), ('reject', 'Rejected')], default='new', tracking=True)

    gate_pass_state = fields.Selection([('active', 'Active'), ('inactive', 'Inactive'),
                              ],tracking=True)
    dept_head_employee_ids = fields.Many2many('hr.employee', related='employee_id.department_id.department_head_ids')
    dept_head_user_ids = fields.Many2many('res.users', 'user_gate_pass_rel', 'user_id', 'gate_pass_req_id',
                                          compute='_compute_dept_head_user_ids', store=True)
    approver_user_id = fields.Many2one('res.users',string="Approver User")

    is_self_assign = fields.Boolean(string="Self Assign")
    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    partner_id = fields.Many2one('res.partner', string='Paid To' )

    approval_hod_id = fields.Many2one('res.users', string="Approval HOD")
    approval_finance_id = fields.Many2one('res.users', string="Approval Finance")
    approval_accounts_id = fields.Many2one('res.users', string="Approval Accounts")
    is_department_head = fields.Boolean(string="Department Head",compute='_compute_is_department_head')
    move_id = fields.Many2one('account.move', string='Journal Entry')
    gate_loss_amount = fields.Float(String="Gate Loss Amount",store=True)
    reject_reason = fields.Text(string="Reject Reason")



    @api.depends('start_date','end_date')
    def _compute_duration(self):
        for rec in self:
            rec.duration= (rec.end_date - rec.start_date).days + 1

    @api.constrains('start_date', 'end_date', 'employee_id')
    def _check_date(self):

        for record in self:
            domain = [
                ('id', '!=', record.id),  # Exclude the current record
                ('start_date', '<=', record.end_date),
                ('end_date', '>=', record.start_date),
                ('employee_id', 'in', record.employee_id.ids),
                ('gate_pass_type_id', '=', record.gate_pass_type_id.id),
            ]
            conflicting_records = self.search_count(domain)
            if conflicting_records:
                raise ValidationError("Conflicting dates found!")

        # all_employees = self.employee_id
        # all_leaves = self.search([
        #     ('start_date', '<', max(self.mapped('end_date'))),
        #     ('end_date', '>', min(self.mapped('start_date'))),
        #     ('employee_id', 'in', all_employees.ids),
        #     ('id', 'not in', self.ids),
        # ])
        # for holiday in self:
        #     domain = [
        #         ('start_date', '<', holiday.end_date),
        #         ('end_date', '>', holiday.start_date),
        #         ('id', '!=', holiday.id),
        #     ]
        #
        #     employee_ids = holiday.employee_id.ids
        #     search_domain = domain + [('employee_id', 'in', employee_ids)]
        #     conflicting_holidays = all_leaves.filtered_domain(search_domain)
        #
        #     if conflicting_holidays:
        #         conflicting_holidays_list = []
        #         # Do not display the name of the employee if the conflicting holidays have an employee_id.user_id equivalent to the user id
        #         holidays_only_have_uid = bool(holiday.employee_id)
        #         for conflicting_holiday in conflicting_holidays:
        #             conflicting_holiday_data = {}
        #             conflicting_holiday_data['start_date'] = format_date(self.env,
        #                                                                 min(conflicting_holiday.mapped('start_date')))
        #             conflicting_holiday_data['end_date'] = format_date(self.env,
        #                                                               min(conflicting_holiday.mapped('end_date')))
        #             if conflicting_holiday.employee_id.user_id.id != self.env.uid:
        #                 holidays_only_have_uid = False
        #             if conflicting_holiday_data not in conflicting_holidays_list:
        #                 conflicting_holidays_list.append(conflicting_holiday_data)
        #         if not conflicting_holidays_list:
        #             return
        #         conflicting_holidays_strings = []



    def _compute_is_department_head(self):
        for record in self:
            if self.env.user.id in record.employee_id.department_id.department_head_ids.user_id.ids and self.env.user.id in record.assigned_user_ids.ids:
                record.is_department_head = True
            else:
                record.is_department_head = False

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if vals.get('name', _('New')) == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('gate.pass.request')
        return super().create(vals_list)


    @api.depends('employee_id', 'employee_id.department_id','employee_id.department_id.department_head_ids', 'employee_id.type')
    def _compute_dept_head_user_ids(self):
        user_list = []
        for rec in self:
            if rec.employee_id:
                if rec.dept_head_employee_ids:
                    for head in rec.dept_head_employee_ids:
                        if head.user_id:
                            user_list.append(head.user_id.id)
            rec.dept_head_user_ids = user_list

    @api.model
    def default_get(self, fields):
        if self.user_has_groups(
                'base.group_system,'
                'department_groups.group_staff_operations_super,'
                'department_groups.group_construction_operations_super,'
                'department_groups.group_soft_ser_operations_super,'
                'department_groups.group_construction_operations_coord,'
                'department_groups.group_staff_operations_coord,'
                'department_groups.group_soft_ser_operations_coord'

        ):
            return super(GatePassRequest, self).default_get(fields)
        else:
            raise exceptions.ValidationError("You cannot Create Gate pass Request")
    def action_submit(self):
        for rec in self:
            if self.user_has_groups(
                    'department_groups.group_staff_operations_super,'
                    'department_groups.group_construction_operations_super,'
                    'department_groups.group_soft_ser_operations_super,'
                    'department_groups.group_construction_operations_coord,'
                    'department_groups.group_staff_operations_coord,'
                    'department_groups.group_soft_ser_operations_coord'

            ):
                if rec.gate_pass_amount==0:
                    raise ValidationError("Enter The Gate Pass Amount")
                rec.status = 'hod_approval'
                rec.assigned_user_ids = [Command.link(user.id) for user in rec.dept_head_user_ids]
                user = self.env['res.users'].browse(rec.assigned_user_ids.ids)
                user_groups = user.groups_id
                for group in user_groups:
                    rec.assigned_group_ids = [Command.link(group.id)]
                email_template = self.env.ref('gate_pass_request.email_template_gate_pass',
                                              raise_if_not_found=False)
                hr_manager_emails = [manager.login for manager in rec.assigned_user_ids]
                if hr_manager_emails:
                    email_values = {'email_to': ','.join(hr_manager_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_hod(self):
        for rec in self:
            if self.env.user.id in rec.dept_head_user_ids.ids:
                rec.sudo().status = 'finance_mgr_approval'
                rec.sudo().is_self_assign = False
                fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                fin_manager_users = self.env['res.users'].search([('groups_id', 'in', fin_manager_group.id)])
                rec.assigned_user_ids = [Command.link(user.id) for user in fin_manager_users]
                rec.assigned_group_ids = [Command.link(fin_manager_group.id)]
                email_template = self.env.ref('gate_pass_request.email_template_gate_pass',
                                              raise_if_not_found=False)
                fin_manager_emails = [manager.login for manager in fin_manager_users]
                if fin_manager_emails:
                    email_values = {'email_to': ','.join(fin_manager_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_finance_manager(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_finance_mgr'):
                rec.status = 'accounts_approval'
                rec.sudo().is_self_assign = False
                acc_manager_group = self.env.ref('account.group_account_manager')
                acc_user_group = self.env.ref('account.group_account_user')
                acc_billing_group = self.env.ref('account.group_account_invoice')
                fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                acc_payroll_group = self.env.ref('department_groups.group_finance_accountant_payroll')
                asst_mgr_accountant = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                acc_fin_accountant = self.env.ref('department_groups.group_finance_accountant')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [fin_manager_group.id, acc_manager_group.id,
                                                                 acc_user_group.id, acc_billing_group.id,
                                                                 acc_payroll_group.id, asst_mgr_accountant.id,
                                                                 acc_fin_accountant.id])])
                acc_team_groups = self.env['res.groups'].browse(
                    (fin_manager_group.id, acc_manager_group.id,acc_user_group.id, acc_billing_group.id,acc_payroll_group.id,
                     asst_mgr_accountant.id,acc_fin_accountant.id))
                rec.assigned_user_ids = [Command.link(user.id) for user in acc_team_users]
                rec.assigned_group_ids = [Command.link(group.id) for group in acc_team_groups]

                email_template = self.env.ref('gate_pass_request.email_template_gate_pass',
                                              raise_if_not_found=False)
                acc_team_emails = [manager.login for manager in acc_team_users]
                if acc_team_emails:
                    email_values = {'email_to': ','.join(acc_team_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_account_team(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_finance_mgr') or rec.env.user.has_group(
                    'department_groups.group_finance_accountant') or rec.env.user.has_group(
                'department_groups.group_finance_accountant_payroll') or rec.env.user.has_group(
                'account.group_account_manager') or rec.env.user.has_group(
                'account.group_account_user') or rec.env.user.has_group(
                'department_groups.group_finance_asst_mgr_accountant') or rec.env.user.has_group(
                'account.group_account_invoice'):
                gate_pass_expense = self.env['product.product'].search([('is_gate_pass', '=', True)],limit=1)
                if not gate_pass_expense:
                    raise ValidationError("Please add The Expense")

                if not rec.partner_id:
                    raise ValidationError("Please select the Partner")
                pass_amount = 0
                if rec.gate_pass_amount:
                    pass_amount = rec.gate_pass_amount
                else:
                    pass_amount = gate_pass_expense.standard_price



                move = self.env['account.move'].create({
                    'move_type': 'entry',
                    'date': fields.Date.today(),
                    'ref': rec.name,
                    'line_ids': [
                        Command.create({
                            'debit': pass_amount,
                            'credit': 0.0,
                            'account_id': gate_pass_expense.property_account_expense_id.id,
                            'partner_id': rec.partner_id.id,
                            'name': rec.name,
                        }),
                        Command.create({
                            'debit': 0.0,
                            'credit': pass_amount,
                            'account_id': rec.partner_id.property_account_payable_id.id,
                            'partner_id': rec.partner_id.id,
                            'name': rec.name,
                            # 'name': '/'
                        }),
                    ],
                })
                rec.move_id = move.id
                # rec.move_id._post()
                rec.sudo().status = 'closed'
                rec.sudo().gate_pass_state = 'active'
                rec.sudo().is_self_assign = False

    def action_inactivate(self):
        for rec in self:
            rec.sudo().gate_pass_state = 'inactive'
            rec.sudo().cancellation_date = date.today()
            duration = (rec.end_date - rec.start_date).days + 1
            if rec.cancellation_date:
                # if rec.start_date > rec.cancellation_date > rec.end_date:
                if rec.start_date > rec.cancellation_date or rec.cancellation_date > rec.end_date:
                    raise ValidationError("Cancellation Date is not Valid")
            loss_days = (rec.end_date - rec.cancellation_date).days
            loss_amount = (rec.gate_pass_amount/duration) * loss_days
            rec.sudo().gate_loss_amount = loss_amount
            message = _(
                "Your Gate pass is inactive for %s" % rec.gate_pass_amount)
            warning = {
                'message': message
            }
            return {'warning': warning}

    def action_self_assign(self):
        for rec in self:
            if self.env.user.id in rec.dept_head_user_ids.ids:
                rec.assigned_user_ids = [Command.unlink(user.id) for user in rec.dept_head_user_ids]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_hod_id': rec.env.user.id})
            elif rec.status == 'finance_mgr_approval':
                fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                fin_manager_users = self.env['res.users'].search([('groups_id', 'in', fin_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in fin_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_finance_id': rec.env.user.id})
            else:
                fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                acc_payroll_group = self.env.ref('department_groups.group_finance_accountant_payroll')
                asst_mgr_accountant = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                acc_fin_accountant = self.env.ref('department_groups.group_finance_accountant')
                acc_payroll_asst = self.env.ref('account.group_account_manager')
                acc_pur_exe = self.env.ref('account.group_account_user')
                acc_store_keep = self.env.ref('account.group_account_invoice')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [fin_manager_group.id, acc_payroll_group.id,
                                                                 asst_mgr_accountant.id, acc_fin_accountant.id,
                                                                 acc_payroll_asst.id, acc_pur_exe.id,
                                                                 acc_store_keep.id, acc_pur_exe.id])])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in acc_team_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.assigned_user_ids = [Command.link(rec.approval_finance_id.id)]
                rec.write({'is_self_assign': True, 'approval_accounts_id': rec.env.user.id})


    def action_rejected(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

