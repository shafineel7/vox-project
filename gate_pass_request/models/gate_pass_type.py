# -*- coding: utf-8 -*-

from odoo import models, fields


class GatePassType(models.Model):
    _name = 'gate.pass.type'
    _description = 'Gate Pass Type'

    name = fields.Char(string="Gate Pass Type")
