# -*- coding: utf-8 -*-
{
    'name': ' Gate Pass request ',
    'version': '17.0.1.0.0',
    'category': 'Human Resources',
    'author': 'Hilsha P H',
    'summary': ' Gate Pass request ',
    'description': """
       Gate pass request
    """,
    'depends': ['employee','reject_reason',],
    'data': [
        'security/gate_pass_request_security.xml',
        'security/ir.model.access.csv',
        'data/gate_pass_request_data.xml',
        'data/mail_template_data.xml',
        'views/product_product_views.xml',
        'views/gate_pass_type_views.xml',
        'views/gate_pass_request_views.xml',
        'views/gate_pass_request_menus.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
