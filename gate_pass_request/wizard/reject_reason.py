from odoo import models
from odoo.exceptions import ValidationError


class RejectReason(models.TransientModel):
    _inherit = "reject.reason"

    def confirm_action(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model == 'gate.pass.request' and active_id:
            gate_pass = self.env[active_model].browse(active_id)
            for rec in self:
                if rec.env.user.id in gate_pass.dept_head_user_ids.ids:
                    gate_pass.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_hod_id': rec.env.user.id})
                elif rec.env.user.has_group('department_groups.group_finance_mgr'):
                    gate_pass.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_finance_id': rec.env.user.id})
                else:
                    gate_pass.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_accounts_id': rec.env.user.id})
        return super().confirm_action()


