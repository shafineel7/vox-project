{
    'name': 'Crm Contract Addendum Docx ',
    'author': 'aswin',
    'summary': 'Crm Contract Addendum reports as docx.',
    'version': '17.0.1.0.0',
    'website': 'https://www.voxtronme.com',
    'depends': ["crm_contracts"],
    'description': """This module is for downloading word document of Contract Addendum reports.""",
    'data': [
        'views/crm_lead_views.xml'
    ],
    'external_dependencies': {
        'python': [
            'docxtpl',
        ],
    },

    'installable': True,
    'auto_install': False,
    'application': True,
}
