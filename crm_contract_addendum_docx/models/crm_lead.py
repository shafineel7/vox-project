from odoo import models, fields, Command
from datetime import datetime
from docxtpl import DocxTemplate, RichText
import base64
import os


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    effective_date = fields.Date(string="Effective Date")

    def generate_contract_addendum(self):
        module_path = os.path.dirname(os.path.abspath(__file__))
        template_path = os.path.join(module_path, '..', 'crm_contract_addendum_docx_template',
                                     'Contract Addendum Trumax.docx')

        document = DocxTemplate(template_path)

        formatted_creation_date = self.create_date.strftime('%Y-%m-%d')
        if self.contract_id and self.contract_id.create_date:
            formatted_contract_creation_date = self.contract_id.create_date.strftime('%Y-%m-%d')
        else:
            formatted_contract_creation_date = ''

        context = {
            'id': self.contract_id.code,
            'creation_date': formatted_creation_date,
            'contract_create_date': formatted_contract_creation_date,
            'company_name': self.company_id.name,
            'postcode': self.company_id.zip,
            'state': self.company_id.state_id.name,
            'country': self.company_id.country_id.name,
            'client_opportunity': self.partner_id.name,
            'client_postcode': self.partner_id.zip,
            'client_state': self.partner_id.state_id.name,
            'client_country': self.partner_id.country_id.name,
            'services': enumerate(self.lead_estimation_ids),
            'approval': self.md_id.name or '',
            'contact_person': self.contact_name or '',
            'job_position': self.function or '',
            'mb': self.mobile or '',
            'effective_date': self.effective_date,
        }

        document.render(context)

        temp_dir = os.path.join(module_path, '..', 'temp')
        os.makedirs(temp_dir, exist_ok=True)
        temp_file_path = os.path.join(temp_dir, f'{self.name}.docx')

        document.save(temp_file_path)

        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()

        docx_base64 = base64.b64encode(docx_content)

        attachment = self.env['ir.attachment'].sudo().create({
            'name': f"{self.display_name} Contract_Addendum.docx",
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'crm.lead',
            'res_id': self.id,
            'store_fname': f"{self.display_name}.doc"
        })

        os.remove(temp_file_path)

        return attachment

    def action_download_contract_addendum(self):
        attachment = self.generate_contract_addendum()

        url = '/download_ca/docx/%d' % attachment.id

        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self'
        }

    def submit_quote(self):
        res = super(CrmLead, self).submit_quote()
        if self.lead_type != 'new':
            if 'context' in res:
                ctx = res['context']
                attachment = self.generate_contract_addendum()

                if attachment:
                    ctx['default_attachment_ids'] = [Command.link(attachment.id)]
                    res['context'] = ctx
            return res
        return res
