# -*- coding: utf-8 -*-

{

    'name': 'Employee',
    'author': 'Abhilash.c',
    'version': '17.0.1.0.2',
    'website': 'https://www.voxtronme.com',
    'summary': 'Employee information.',
    'category': 'Human Resources/Employees',
    'depends': ['hr', 'hr_payroll_account'],
    'description': 'In employee module added extra fields, tabs as master.'
                   'Added menus designation, agency, expense, medical center, biometric center in '
                   'configuration as masters.'
                   'In agency we can create an agent and agent can create multiple customers '
                   'agent can be converted into user also',
    'data': [
            'security/ir.model.access.csv',
            'data/employee_sequence_data.xml',
            'views/employee_views.xml',
            'views/hr_department_views.xml',
            'views/agent_agent_views.xml',
            'views/res_users_views.xml',
            'views/designation_views.xml',
            'views/salary_structure_views.xml',
            'views/medical_center_views.xml',
            'views/tawjee_center_views.xml',
            'views/biometric_center_views.xml',
            'views/qualification_views.xml',
            'views/employee_menus.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,

}
