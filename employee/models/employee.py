# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime


class Employee(models.Model):
    _inherit = "hr.employee"

    dob = fields.Date(string='Date Of Birth', required=True)
    employee_sequence = fields.Char(string='Employee ID', readonly=True, copy=False)
    type_of_employee = fields.Selection([
        ('direct_billable', 'Direct Billable'),
        ('direct_non_billable', 'Direct Non Billable'),
        ('indirect_non_billable', 'Indirect Non Billable'),
    ], default='direct_billable', string='Employee Type', required=True)
    date_of_joining = fields.Date(string='Date Of Joining', required=True)
    eid_number = fields.Char(string='EID Number', required=True)
    eid_expiry = fields.Date(string='EID Expiry', required=True)
    passport_exp = fields.Date(string='Passport Expiry', required=True)
    labour_card_no = fields.Char(string='Labour Card Number', required=True)
    labour_card_exp = fields.Date(string='Labour Card Expiry', required=True)
    temporary_labour_card_exp = fields.Date(string='Temporary Labour Card Expiry')
    mol_offer_letter_date = fields.Date(string='MOL Offer Letter Date')
    remarks = fields.Char(string='Remarks')
    st_document = fields.Binary(string='ST Document', attachment=True)
    mb_document = fields.Binary(string='MB Document', attachment=True)
    mol_offer_letter = fields.Binary(string='MOL Offer Letter')
    mol_status = fields.Selection([
        ('applied', 'Applied'),
        ('on_hold', 'On Hold'),
        ('rejected', 'Rejected'),
        ('approved', 'Approved'),
    ], string='MOL Status')
    e_visa_date = fields.Date(string='E Visa Date')
    e_visa_copy = fields.Binary(string='E Visa Copy')
    visa_stamping_status = fields.Selection([
        ('done', 'Done'),
        ('not_done', 'Not Done'),
    ], string='Visa Stamping Status')
    visa_status = fields.Selection([
        ('applied', 'Applied'),
        ('on_hold', 'On Hold'),
        ('rejected', 'Rejected'),
        ('approved', 'Approved'),
    ], string='Visa Status')
    medical_application_no = fields.Char(string='Medical Application Number')
    medical_center_id = fields.Many2one('medical.center', string='Medical Center')
    medical_application_date = fields.Date(string='Medical Application Date')
    receipt_attachment = fields.Binary(string='Receipt Attachment', attachment=True)
    medical_completed_date = fields.Date(string='Medical Completed Date')
    remark = fields.Char(string='Remarks')
    medical_status = fields.Selection([
        ('done', 'Done'),
        ('not_done', 'Not Done'),
    ], string='Medical Status')
    tawjeeh_application_no = fields.Char(string='Tawjeeh Application Number')
    tawjeeh_appointment_date = fields.Date(string='Tawjeeh Appointment Date')
    tawjeeh_receipt_attachment = fields.Binary(string='Receipt Attachment')
    tawjeeh_status = fields.Selection([
        ('done', 'Done'),
        ('not_done', 'Not Done'),
    ], string='Tawjeeh Status')
    tawjeeh_center_id = fields.Many2one('tawjeeh.center', string='Tawjeeh Center')
    eid_application_no = fields.Char(string='EID Application Number')
    eid_application_copy = fields.Binary(string='EID Application Copy', attachment=True)
    eid_status = fields.Selection(
        [('eid_applied', 'EID Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('eid_approved', 'EID Approved'), ], string='EID status')
    biometric_date = fields.Date(string='Biometric Date')
    biometric_center_id = fields.Many2one('biometric.center', string='Biometric Center')
    biometric_status = fields.Selection([
        ('yes', 'Yes'), ('no', 'NO'),
    ], string='Biometric Status')
    health_insurance_status = fields.Selection([
        ('insurance_applied', 'Insurance Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
        ('insurance_approved', 'Insurance Approved')
    ], string='Health Insurance Status')
    health_insurance_date = fields.Date(string='Health Insurance Application Date')
    health_insurance_no = fields.Char(string='Health Insurance Number')
    group_code = fields.Char(string='Group Code')
    insurance_authority = fields.Char(string='Insurance Authority ID')
    premium_amount = fields.Float(string='Premium Amount')
    insurance_card = fields.Binary(string='Insurance Card', attachment=True)
    insurance_document = fields.Binary(string='Insurance Document', attachment=True)
    insurance_application_cancel_date = fields.Date(string='Insurance Application Cancellation Date')
    insurance_cancel_date = fields.Date(string='Insurance Cancellation Date')
    date_of_journey = fields.Date(string='Date Of Journey')
    date_time_of_landing = fields.Date(string='Date And Time Of Landing')
    destination_airport_id = fields.Many2one('airport.destination', string='Destination Airport')
    terminal_id = fields.Many2one('terminal.terminal', string='Terminal')
    flight_no = fields.Char(string='Flight Number')
    ticket_copy = fields.Binary(string='Ticket Copy')
    camp_assignment_date = fields.Date(string='Date Of Assignment To Camp')
    camp_assigned = fields.Char(string='Camp Assigned')
    camp_location = fields.Char(string='Camp Location')
    camp_manager_name_id = fields.Many2one('hr.employee', string='Camp Manager Name')
    adv_amt_location = fields.Float(string='Advance amount issued in this location ')
    passport_custody_status = fields.Selection([
        ('with_employee', 'With Employee'),
        ('in_custody', 'In Custody'),
    ], string='Passport Custody Status')
    collection_date = fields.Date(string='Collection Date')
    request_status = fields.Selection([
        ('open', 'Open'),
        ('closed', 'Closed'),
    ], string='Request Status')
    issue_date = fields.Date(string='Issue Date')
    reason = fields.Char(string='Reason')
    planned_return_date = fields.Date(string='Planned Return Date')
    actual_return_date = fields.Date(string='Actual Return Date')
    cv = fields.Binary(string='CV',  attachment=True, required=True)
    branches_ids = fields.Many2many('res.company', string='Branches', domain="[('parent_id', '=', company_id)]")
    home_country_mobile = fields.Char(string='Home Country Mobile')
    designation_id = fields.Many2one('designation', string='Designation')
    type = fields.Selection([
        ('direct', 'Direct'),
        ('indirect', 'Indirect'),
    ], string='Type', required=True)
    age = fields.Char(string='Age', compute='_compute_age')
    is_room_allocation = fields.Boolean(string='Room Allocation')
    accommodation_status = fields.Selection([('live_in', 'Live In'), ('live_out', 'Live Out')],
                                            string='Accommodation Status')

    @api.onchange('department_id')
    def _onchange_department_id(self):
        for rec in self:
            if rec.department_id:
                rec.branches_ids = [(6, 0, rec.department_id.branches_ids.ids)]

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('employee_sequence') or vals['employee_sequence'] == _('New'):
                vals['employee_sequence'] = self.env['ir.sequence'].next_by_code('hr.employee') or _('New')
        return super().create(vals_list)

    @api.depends('name')
    def _compute_display_name(self):
        for record in self:
            if record.employee_sequence and record.name:
                record.display_name = record.name + "(" + record.employee_sequence + ")"
            else:
                record.display_name = record.name

    @api.depends('birthday')
    def _compute_age(self):
        for record in self:
            if record.birthday:
                dob_date = fields.Date.from_string(record.birthday)
                today = fields.Date.today()
                age = today.year - dob_date.year - ((today.month, today.day) < (dob_date.month, dob_date.day))
                record.age = str(age)
            else:
                record.age = '0'

    @api.model
    def _name_search(self, name, domain=None, operator='ilike', limit=None, order=None, args=None):
        args = list(args or [])
        if name:
            args += ['|', ('employee_sequence', operator, name), ('name', operator, name)]

        return self._search(args, limit=limit)
