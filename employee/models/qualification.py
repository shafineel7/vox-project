# -*- coding: utf-8 -*-
from odoo import models, fields


class Qualification(models.Model):
    _name = 'qualification'
    _description = 'Qualification'

    name = fields.Char(string='Name', required=True)
