# -*- coding: utf-8 -*-

from odoo import models, fields


class TawjeeCenter(models.Model):
    _name = 'tawjeeh.center'
    _description = 'Tawjeeh center'

    name = fields.Char(string="Tawjeeh Center")
    city = fields.Char(string="City")
    country_id = fields.Many2one('res.country', string='Country')
