# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ResUsers(models.Model):
    _inherit = 'res.users'

    agent_id = fields.Many2one('agent.agent', string='Agent')
    agent_ids = fields.Many2many('agent.agent','agent_user_rel','agent_id','user_id', string='Agents')

    @api.onchange('name', 'phone', 'mobile', 'login')
    def _onchange_update_agent_details(self):
        for user in self:
            if user.agent_id:
                user.agent_id.write({
                    'name': user.name,
                    'contact_number': user.phone,
                    'mobile_number': user.mobile,
                    'email_address': user.login,
                })

    @api.model_create_multi
    def create(self, vals_list):
        res = super(ResUsers, self).create(vals_list)
        for user in res:
            if user.agent_id:
                user.agent_id.user_id = user
                user.agent_id.partner_id = user.partner_id
        return res

