# -*- coding: utf-8 -*-

from odoo import fields, models


class AirportDestination(models.Model):
    _name = "airport.destination"
    _description = "Airport Destination"

    name = fields.Char(string='Name')
    terminal = fields.Char(string='Terminal')
