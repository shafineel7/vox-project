# -*- coding: utf-8 -*-

from odoo import fields, models


class Terminal(models.Model):
    _name = "terminal.terminal"
    _description = "Terminal"

    name = fields.Char(string='Name')
