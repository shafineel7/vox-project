# -*- coding: utf-8 -*-

from . import employee
from . import airport_destination
from . import terminal
from . import hr_department
from . import medical_center
from . import tawjee_center
from . import agent_agent
from . import res_users
from . import designation
from . import salary_structure
from . import biometric_center
from . import qualification
