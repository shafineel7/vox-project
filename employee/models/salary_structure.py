# -*- coding: utf-8 -*-

from odoo import models, fields


class SalaryStructure(models.Model):
    _inherit = 'hr.payroll.structure'

    designation_id = fields.Many2one('designation', string="Designation")
