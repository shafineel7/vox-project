# -*- coding: utf-8 -*-

from odoo import models, fields


class Designation(models.Model):
    _name = 'designation'
    _description = 'Designation'

    name = fields.Char(string="Designation", required=True)
    salary_structure_id = fields.Many2one('hr.payroll.structure', string="Salary Structure")

    # food_allowance = fields.Float('Food Allowance')
    # site_allowance = fields.Float('Site Allowance')
    # supervisory_allowance = fields.Float('Supervisory Allowance')
    # normal_ot_allowance = fields.Float('Normal OT Allowance')
    # friday_ot_allowance = fields.Float('Friday OT Allowance')
    # holiday_ot_allowance = fields.Float('Holiday OT Allowance')
    # ramadan_ot_allowance = fields.Float('Ramadan OT Allowance')
    # fixed_ot_allowance = fields.Float('Fixed OT Allowance')
    # housing_allowance = fields.Float('Housing Allowance')
    # transportation_allowance = fields.Float('Transportation Allowance')
    # other_allowance = fields.Float('Other Allowance')
    wage = fields.Float('Wage')
    gross_salary = fields.Float('Gross Salary')

