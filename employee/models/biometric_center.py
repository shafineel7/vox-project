# -*- coding: utf-8 -*-

from odoo import models, fields


class BiometricCenter(models.Model):
    _name = 'biometric.center'
    _description = "Biometric center for Biometric process "

    name = fields.Char(string="Biometric Center")
    city = fields.Char(string="City")
    country_id = fields.Many2one('res.country', string='Country')
