# -*- coding: utf-8 -*-

from odoo import models, fields


class MedicalCenter(models.Model):
    _name = 'medical.center'
    _description = 'Medical center'

    name = fields.Char(string="Medical Center")
    city = fields.Char(string="City")
    country_id = fields.Many2one('res.country', string='Country')
