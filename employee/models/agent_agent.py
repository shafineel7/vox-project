# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class AgentAgent(models.Model):
    _name = 'agent.agent'
    _description = 'Agent'

    name = fields.Char(string='Name', required=True)
    license_no = fields.Char(string='License No.')
    license_expiry = fields.Date(string='License Expiry')
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict',
                               domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    country_code = fields.Char(related='country_id.code', string="Country Code")
    contact_person = fields.Char(string='Contact Person')
    designation_id = fields.Many2one('designation', string='Designation')
    contact_number = fields.Char(string='Phone')
    mobile_number = fields.Char(string='Mobile')
    email_address = fields.Char(string='Email')
    security_deposit = fields.Float(string='Security Deposit')
    outstanding_payments = fields.Float(string='Outstanding Payments')
    internal_notes = fields.Text(string='Internal Notes')
    parent_id = fields.Many2one('agent.agent', string='Related Company')
    child_ids = fields.One2many('agent.agent', 'parent_id', string='Contact')
    user_id = fields.Many2one('res.users', 'User', readonly=False)
    is_contact = fields.Boolean(string='Is Contact', default=False)
    partner_id = fields.Many2one('res.partner', string="Related Customer")

    def action_create_user(self):
        self.ensure_one()
        res_user = self.env['res.users'].search([('agent_id', '=', self.id)])
        print(res_user)
        if res_user:
            raise ValidationError(_("This already an user."))
        return {
            'name': _('Create User'),
            'type': 'ir.actions.act_window',
            'res_model': 'res.users',
            'view_mode': 'form',
            'view_id': self.env.ref('hr.view_users_simple_form').id,
            'target': 'new',
            'context': dict(self._context, **{
                'default_agent_id': self.id,
                'default_name': self.name,
                'default_phone': self.contact_number,
                'default_mobile': self.mobile_number,
                'default_login': self.email_address,
            })
        }

    @api.depends('parent_id')
    def _compute_display_name(self):
        """ Return the categories' display name, including their direct
            parent by default.
        """
        for category in self:
            names = []
            current = category
            while current:
                names.append(current.name or "")
                current = current.parent_id
            category.display_name = ' / '.join(reversed(names))

    def action_agent_transactions(self):
        action = self.env["ir.actions.actions"]._for_xml_id("account.action_account_moves_all_tree")
        action['context'] = {
            'search_default_partner_id': self.partner_id.id,
            'default_partner_id': self.partner_id.id,
            'search_default_posted': 1,
            'search_default_trade_payable': 1,
            'search_default_trade_receivable': 1,
        }
        return action

    def action_agent_exit(self):
        return {

        }

    def action_agent_documents(self):
        return{

        }

    def open_agent_partner(self):
        self.ensure_one()
        return {
            'name': _('Contracts'),
            'view_mode': 'form',
            'res_model': 'res.partner',
            'res_id': self.partner_id.id,
            'view_id': self.env.ref('base.view_partner_form').id,
            'type': 'ir.actions.act_window',
        }


