# -*- coding: utf-8 -*-

from odoo import fields, models


class Department(models.Model):
    _inherit = "hr.department"

    branches_ids = fields.Many2many('res.company', string='Branches', domain="[('parent_id', '=', company_id)]")
    department_head_ids = fields.Many2many(
        'hr.employee', 'hr_department_hr_employee_rel','hr_department_id',
        'hr_employee_id', string='Department Head')
    operations_supervisor_ids = fields.Many2many(
        'hr.employee', 'hr_department_operations_supervisor_rel', 'hr_department_id',
        'operations_supervisor_id', string='Operations Supervisor',
        check_company=True)
    operations_coordinator_ids = fields.Many2many(
        'hr.employee', 'hr_department_operations_coordinator_rel', 'hr_department_id',
        'operations_coordinator_id', string='Operations Coordinator',
        check_company=True)
    assistant_operations_supervisor_ids = fields.Many2many(
        'hr.employee', 'hr_department_assistant_operations_supervisor_rel', 'hr_department_id',
        'assistant_operations_supervisor_id', string='Assistant Operations Supervisor',
        check_company=True)
