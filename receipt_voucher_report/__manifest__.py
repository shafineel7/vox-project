# -*- coding: utf-8 -*-
{
    'name': 'Receipt Voucher Document',
    'version': '17.0.1.0.0',
    'category': 'Accounting/Accounting',
    'summary': 'Module for Downloading Receipt Voucher PDF Report',
    'author': 'Anoopa',
    'website': 'https://www.voxtronme.com',
    'depends': ['account_accountant', 'cheque_tracker_status'],
    'description': """This module is for creating Receipt Voucher PDF report.""",
    'data': [
        'report/receipt_voucher_header.xml',
        'report/receipt_voucher_report.xml',
        'report/receipt_voucher_report_template.xml',
        'views/account_payment_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
