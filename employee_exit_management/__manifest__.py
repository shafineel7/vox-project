# -*- coding: utf-8 -*-
{
    'name': 'Employee Exit management',
    'version': '17.0.1.0.1',
    'category': 'Human Resources',
    'author': 'Hilsha P H, Arya I R',
    'summary': 'Employee Exit management',
    'description': """
        Employee exit refers to the process by which an employee leaves an organization, either
        voluntarily or involuntarily. It encompasses various steps and procedures that need to be
        followed to ensure a smooth transition f or both the departing employee and the organization
    """,
    'depends': ['employee', 'recruitment', 'department_groups', 'reject_reason', 'account', 'material_requisition',
                'employee_recruitment_onboarding', 'expense_management', 'hr_employee_warning', 'hr_gratuity', 'insurance_deletion', 'airline_ticket'],
    'data': [
        'security/employee_exit_management_security.xml',
        'security/ir.model.access.csv',
        'data/exit_reason_data.xml',
        'data/product_data.xml',
        'data/ir_sequence_data.xml',
        'data/mail_template_data.xml',
        'data/employee_exit_cron.xml',
        'wizard/update_camp_exit_status_views.xml',
        'wizard/medically_unfit_views.xml',
        'wizard/employee_retirement_views.xml',
        'views/hr_employee_views.xml',
        'views/exit_reasons_views.xml',
        'views/insurance_views.xml',
        'views/airline_ticket_views.xml',
        'views/product_views.xml',
        'views/employee_exit_management_views.xml',
        'views/employee_exit_management_menus.xml',
        'views/account_move_views.xml',
        'views/expense_booking_views.xml',
        'views/hr_expense_views.xml',
        'views/res_config_settings_views.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
