from odoo import models, fields, Command, _


class MedicallyUnfit(models.TransientModel):
    _name = "medically.unfit"
    _description = "Medically Unfit"

    name = fields.Text(string='Reason', required=True)
    medically_unfit_document = fields.Binary(string="Fitness Document")
    medically_unfit_document_name = fields.Char()

    def confirm_action(self):
        self.ensure_one()
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')
        exit_reason = self.env['exit.reasons'].search([('type', '=', 'medically_unfit')], limit=1)

        if active_model == 'hr.employee' and active_id:
            employee_id = self.env[active_model].browse(active_id)
            allowed_groups = []
            if self.env.ref(
                    'department_groups.group_construction_operations_coord') in self.env.user.groups_id:
                allowed_groups.append(
                    self.env.ref('department_groups.group_construction_operations_coord').id)
            if self.env.ref(
                    'department_groups.group_staff_operations_coord') in self.env.user.groups_id:
                allowed_groups.append(self.env.ref('department_groups.group_staff_operations_coord').id)
            if self.env.ref(
                    'department_groups.group_soft_ser_operations_coord') in self.env.user.groups_id:
                allowed_groups.append(self.env.ref('department_groups.group_soft_ser_operations_coord').id)
            if self.env.ref(
                    'department_groups.group_hr_asst') in self.env.user.groups_id:
                allowed_groups.append(self.env.ref('department_groups.group_hr_asst').id)
            if self.env.ref(
                    'department_groups.group_hr_welfare_officer') in self.env.user.groups_id:
                allowed_groups.append(self.env.ref('department_groups.group_hr_welfare_officer').id)
            assigned_group = [Command.link(group) for group in allowed_groups]
            exit_id = self.env['employee.exit.management'].create({
                'employee_id': active_id,
                'exit_reason_id': exit_reason.id,
                'ticket_eligibility': exit_reason.ticket_eligibility,
                'penalty_amount': exit_reason.penalty_amount,
                'assigned_group_ids': assigned_group,
                'is_auto_created': True,
                'medically_unfit_reason': self.name,
                'medically_unfit_document': self.medically_unfit_document,
                'medically_unfit_document_name': self.medically_unfit_document_name
            })
            view_id = self.env.ref('employee_exit_management.employee_exit_management_view_form')
            ctx = self.env.context.copy()
            return {
                'name': _('Employee Exit Management'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'employee.exit.management',
                'views': [(view_id.id, 'form')],
                'view_id': view_id.id,
                'target': 'main',
                'res_id': exit_id.id,
                'context': ctx
            }
