from odoo import models, fields, Command, _


class EmployeeRetirement(models.TransientModel):
    _name = "employee.retirement"
    _description = "Employee Retirement"

    decision = fields.Selection([('continue', 'Continue'), ('not_continue', 'Not Continue')], required=True,
                                string="Please select")

    def confirm_action(self):
        self.ensure_one()
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')
        exit_reason = self.env['exit.reasons'].search([('type', '=', 'retirement')], limit=1)

        if active_model == 'hr.employee' and active_id and self.decision == 'not_continue':
            employee_id = self.env[active_model].browse(active_id)
            group = self.env.ref("department_groups.group_hr_mgr").id
            assigned_group = [Command.link(group)]
            exit_id = self.env['employee.exit.management'].create({
                'employee_id': active_id,
                'exit_reason_id': exit_reason.id,
                'ticket_eligibility': exit_reason.ticket_eligibility,
                'assigned_group_ids': assigned_group,
                'is_auto_created': True,
            })
            view_id = self.env.ref('employee_exit_management.employee_exit_management_view_form')
            ctx = self.env.context.copy()
            return {
                'name': _('Employee Exit Management'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'employee.exit.management',
                'views': [(view_id.id, 'form')],
                'view_id': view_id.id,
                'target': 'main',
                'res_id': exit_id.id,
                'context': ctx
            }
