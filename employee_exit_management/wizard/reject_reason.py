from odoo import models
from odoo.exceptions import ValidationError


class RejectReason(models.TransientModel):
    _inherit = "reject.reason"

    def confirm_action(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model == 'employee.exit.management' and active_id:
            employee_exit_obj = self.env[active_model].browse(active_id)
            if employee_exit_obj.penalty_move_id and employee_exit_obj.penalty_move_id.state != 'cancel':
                if employee_exit_obj.penalty_move_id.state == 'draft':
                    employee_exit_obj.penalty_move_id.remarks = employee_exit_obj.name + " - Rejected : " + self.name
                    employee_exit_obj.penalty_move_id.button_cancel()
                else:
                    journal_id = employee_exit_obj.penalty_move_id.journal_id
                    reversal = self.env['account.move.reversal'].create({
                        'move_ids': [(6, 0, [employee_exit_obj.penalty_move_id.id])],
                        'reason': employee_exit_obj.name + " - Rejected : " + self.name,
                        'company_id': employee_exit_obj.company_id.id,
                        'journal_id': journal_id.id
                    })
                    reversal.reverse_moves()
            for rec in self:
                if rec.env.user.has_group('department_groups.group_hr_mgr'):
                    employee_exit_obj.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject'})
                elif rec.env.user.has_group('department_groups.group_mngmt_md'):
                    employee_exit_obj.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject'})

                else:
                    employee_exit_obj.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject'})
                employee_exit_obj.employee_id.exit_request_created = False
                employee_exit_obj.employee_id.is_medically_unfit = False
                template_id = self.env.ref('employee_exit_management.email_template_employee_exit_rejection',
                                           raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(employee_exit_obj.id, force_send=True)
                else:
                    raise ValidationError('Email template not found')
        return super().confirm_action()
