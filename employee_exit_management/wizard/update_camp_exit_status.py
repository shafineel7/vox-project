from odoo import models, fields, _


class UpdateCampExitStatus(models.TransientModel):
    _name = "update.camp.exit.status"
    _description = "Update Camp Exit Status"

    camp_exit_status = fields.Selection([('live_out', 'Live out')], string="Camp Exit Status", default='live_out', required=True)

    def confirm_action(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model == 'employee.exit.management' and active_id:
            employee_exit_obj = self.env[active_model].browse(active_id)
            for rec in self:
                employee_exit_obj.camp_exit_status = rec.camp_exit_status
                employee_exit_obj.employee_id.accommodation_status = 'live_out'
                groups = [self.env.ref('department_groups.group_hr_mgr').id, self.env.ref('department_groups.group_hr_asst').id]
                res_users = self.env['res.users'].search([('groups_id', 'in', groups)])
                template_id = self.env.ref('employee_exit_management.email_template_camp_exit_status_updated',
                                           raise_if_not_found=False)
                emails = [user.partner_id.email for user in res_users]
                if emails and self.env.user.partner_id.email:
                    email_values = {'email_to': ','.join(emails),
                                    'email_from': self.env.user.partner_id.email}
                    template_id.send_mail(employee_exit_obj.id, email_values=email_values, force_send=True)
                employee_exit_obj.camp_supervisor_user_id = self.env.user.id
            if self.env.user not in employee_exit_obj.sudo().assigned_user_ids:
                tree_view = {
                    'name': _('Employee Exit Management'),
                    'view_mode': 'tree',
                    'view_id': False,
                    'res_model': 'employee.exit.management',
                    'type': 'ir.actions.act_window',
                    'target': 'main',
                }
                return tree_view
