# -*- coding: utf-8 -*-

from odoo import api, fields, Command, models, _


class HrExpense(models.Model):
    _inherit = "hr.expense"

    employee_exit_management_id = fields.Many2one('employee.exit.management', 'Employee Exit Ref',
                                                  copy=False, readonly=True)
    on_behalf_of_employee_id = fields.Many2one('hr.employee', string="On Behalf of")
