# -*- coding: utf-8 -*-

from odoo import models, fields, _


class Company(models.Model):
    _inherit = 'res.company'

    direct_employee_resignation_month = fields.Integer(string="Months to be completed for direct employee resignation",
                                                       default=6)
    direct_employee_refuse_to_work_month = fields.Integer(string="Month Limit for Direct employee refuse to work",
                                                          default=6)
    contract_expiry_month = fields.Integer(string="Contract Expiration Check (Month)", default=2)
    amount_disbursal_debit_ac_id = fields.Many2one(
        comodel_name='account.account', string="Disbursal Debit Account", check_company=True,
        domain="[('deprecated', '=', False),('account_type', '=', 'expense')]")
    amount_disbursal_credit_ac_id = fields.Many2one(
        comodel_name='account.account', string="Disbursal Credit Account", check_company=True,
        domain="[('deprecated', '=', False),('account_type', 'in', ['liability_payable', 'liability_current'])]")
    amount_disbursal_journal_id = fields.Many2one(
        'account.journal', string="Disbursal Journal", check_company=True,
        domain="[('type', '=', 'general')]")
    retirement_age = fields.Integer(string="Retirement Age")
    retirement_notice_period = fields.Integer(string="Retirement Notice Period (Month)")
