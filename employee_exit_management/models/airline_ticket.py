# -*- coding: utf-8 -*-
from odoo import models, fields, api, _


class AirlineTicket(models.Model):
    _inherit = 'airline.ticket'

    employee_exit_management_id = fields.Many2one('employee.exit.management', string='Employee Exit Request', copy=False)
    exit_reason_id = fields.Many2one('exit.reasons', related='employee_exit_management_id.exit_reason_id', copy=False)
