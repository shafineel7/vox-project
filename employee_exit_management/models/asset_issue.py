# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class AssetIssue(models.Model):
    _inherit = "asset.issue"

    is_asset_recollected = fields.Boolean(string='Is Asset Recollected?', default=False, copy=False)
