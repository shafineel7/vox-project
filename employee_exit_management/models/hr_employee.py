# -*- coding: utf-8 -*-
from odoo import models, fields, api, Command, _
from odoo.exceptions import UserError, ValidationError
from dateutil.relativedelta import relativedelta


class HrEmployeePrivate(models.Model):
    _inherit = "hr.employee"

    notice_period = fields.Integer(string="Notice Period(Days)", required=True, default=60)
    exit_request_created = fields.Boolean(default=False, copy=False)
    is_operation_coordinator = fields.Boolean(compute="_compute_is_operation_coordinator")
    is_medically_unfit = fields.Boolean(default=False, copy=False)
    medically_unfit_reason = fields.Char(string="Medically Unfit Reason")
    medically_unfit_document = fields.Binary(string="Fitness Document")
    medically_unfit_document_name = fields.Char()
    is_user_allowed_to_create_medically_unfit = fields.Boolean(compute="_compute_is_user_allowed_to_create_medically_unfit")
    is_medically_unfit_possible = fields.Boolean(compute="_compute_is_medically_unfit_possible")
    retirement_notice_date = fields.Date(compute="_compute_retirement_notice_date", store=True)
    retirement_notification_send = fields.Boolean(default=False)
    is_ready_to_retire = fields.Boolean(default=False)

    @api.depends('birthday', 'company_id.retirement_notice_period', 'company_id.retirement_age')
    def _compute_retirement_notice_date(self):
        for record in self:
            retirement_age = record.company_id.retirement_age
            retirement_age_as_months = retirement_age * 12
            if record.birthday and record.company_id.retirement_notice_period:
                months = retirement_age_as_months - record.company_id.retirement_notice_period
                record.retirement_notice_date = record.birthday + relativedelta(months=months)
            else:
                record.retirement_notice_date = False

    def _compute_is_medically_unfit_possible(self):
        month = int(self.env['ir.config_parameter'].sudo().get_param(
            'employee_exit_management.direct_employee_medically_unfit_month'))
        for record in self:
            if record.date_of_joining and record.date_of_joining + relativedelta(months=month) > fields.Date.today():
                record.is_medically_unfit_possible = True
            else:
                record.is_medically_unfit_possible = False

    def _compute_is_user_allowed_to_create_medically_unfit(self):
        for record in self:
            if self.user_has_groups('department_groups.group_hr_welfare_officer,department_groups.group_hr_asst') or self.env.user in record.department_id.operations_coordinator_ids.mapped('user_id'):
                record.is_user_allowed_to_create_medically_unfit = True
            else:
                record.is_user_allowed_to_create_medically_unfit = False

    def _compute_is_operation_coordinator(self):
        for record in self:
            if self.env.user in record.department_id.operations_coordinator_ids.mapped('user_id'):
                record.is_operation_coordinator = True
            else:
                record.is_operation_coordinator = False

    def action_create_termination(self):
        exit_reason = self.env['exit.reasons'].search([('type', '=', 'termination')], limit=1)
        for record in self:
            if not record.type:
                raise UserError(_("Please define employee type in employee master"))
            if record.exit_request_created:
                raise UserError(_("Exit requests exist already."))
            if not record.sudo().contract_id:
                raise UserError(_("No running contract found for the selected employee"))
            if record.type == 'indirect':
                assigned_group = [Command.link(self.env.ref('department_groups.group_hr_mgr').id)]
            else:
                allowed_groups = []
                if self.env.ref(
                        'department_groups.group_construction_operations_coord') in self.env.user.groups_id:
                    allowed_groups.append(
                        self.env.ref('department_groups.group_construction_operations_coord').id)
                if self.env.ref(
                        'department_groups.group_staff_operations_coord') in self.env.user.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_staff_operations_coord').id)
                if self.env.ref(
                        'department_groups.group_soft_ser_operations_coord') in self.env.user.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_soft_ser_operations_coord').id)
                assigned_group = [Command.link(group) for group in allowed_groups]
            exit_id = self.env['employee.exit.management'].create({
                'employee_id': record.id,
                'exit_reason_id': exit_reason.id,
                'ticket_eligibility': exit_reason.ticket_eligibility,
                'assigned_group_ids': assigned_group,
                'is_auto_created': True,
            })
            view_id = self.env.ref('employee_exit_management.employee_exit_management_view_form')
            ctx = self.env.context.copy()
            return {
                'name': _('Employee Exit Management'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'employee.exit.management',
                'views': [(view_id.id, 'form')],
                'view_id': view_id.id,
                'target': 'main',
                'res_id': exit_id.id,
                'context': ctx
            }

    def action_create_medically_unfit(self):
        self.ensure_one()
        month = int(self.env['ir.config_parameter'].sudo().get_param(
            'employee_exit_management.direct_employee_medically_unfit_month'))
        if self.date_of_joining + relativedelta(months=month) <= fields.Date.today():
            raise ValidationError(
                _("Medically unfit is only possible if the exit creation date is within %s months of the joining date.",
                  month))
        if not self.contract_id:
            raise UserError(_("No running contract found for the selected employee"))
        return {
            'name': 'Medically Unfit',
            'type': 'ir.actions.act_window',
            'res_model': 'medically.unfit',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_create_employee_retirement(self):
        self.ensure_one()
        if not self.type:
            raise UserError(_("Please define employee type."))
        if not self.contract_id:
            raise UserError(_("No running contract found for the selected employee"))
        return {
            'name': 'Employee Retirment',
            'type': 'ir.actions.act_window',
            'res_model': 'employee.retirement',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def _send_retirement_alert(self):
        employees = self.search([('retirement_notification_send', '=', False), ('retirement_notice_date', '=', fields.Date.today()),
            ('exit_request_created', '=', False), ('active', '=', True)], limit=100)
        for employee in employees:
            employee.is_ready_to_retire = True
            groups = [self.env.ref('department_groups.group_hr_mgr').id]
            users = self.env['res.users'].search(
                [('groups_id', 'in', groups)])
            email_template = self.env.ref('employee_exit_management.email_template_retirement_alert',
                                          raise_if_not_found=False)
            email_to = [user.partner_id.email for user in users]
            email_values = {'email_to': ','.join(email_to)}
            if email_template and email_to:
                email_template.send_mail(employee.id, email_values=email_values, force_send=True)
                employee.retirement_notification_send = True
        if len(employees) == 100:  # assumes there are more whenever search hits limit
            self.env.ref('employee_exit_management.ir_cron_employee_retirement_alert')._trigger()
