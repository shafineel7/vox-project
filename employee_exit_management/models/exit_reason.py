# -*- coding: utf-8 -*-
from odoo import models, fields, api, _


class ExitReasons(models.Model):
    _name = 'exit.reasons'
    _description = 'Exit Reasons'

    name = fields.Char(string='Name', required=True)
    ticket_eligibility = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Ticket Eligibility', required=True)
    type = fields.Selection([
        ('resignation', 'Resignation'), ('termination', 'Termination'), ('contract_expiry', 'Contract Expiry'),
        ('abscond', 'Abscond'), ('refuse_to_work', 'Refuse to Work'), ('medically_unfit', 'Medically Unfit'),
        ('refuse_to_travel', 'Refuse to Travel'), ('demise', 'Demise'), ('retirement', 'Retirement')
    ], required=True)
    departure_reason_id = fields.Many2one('hr.departure.reason', string="Departure Reason")
    penalty_amount = fields.Float(string="Penalty Amount")
