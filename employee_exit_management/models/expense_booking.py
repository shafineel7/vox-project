# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ExpenseBooking(models.Model):
    _inherit = 'expense.booking'

    employee_exit_management_id = fields.Many2one('employee.exit.management', 'Employee Exit Ref', copy=False)
