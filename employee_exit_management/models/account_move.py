# -*- coding: utf-8 -*-
from odoo import models, fields


class AccountMove(models.Model):
    _inherit = 'account.move'

    employee_exit_management_id = fields.Many2one('employee.exit.management', 'Employee Exit Ref', copy=False)
    remarks = fields.Char(string="Remarks", copy=False)
