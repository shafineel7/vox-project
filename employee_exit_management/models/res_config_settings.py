# -*- coding: utf-8 -*-

from odoo import models, fields, _


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    direct_employee_resignation_month = fields.Integer(related="company_id.direct_employee_resignation_month",
                                                       readonly=False)
    direct_employee_refuse_to_work_month = fields.Integer(related='company_id.direct_employee_refuse_to_work_month',
                                                          readonly=False)
    contract_expiry_month = fields.Integer(related="company_id.contract_expiry_month", readonly=False)
    direct_employee_medically_unfit_month = fields.Integer(
        config_parameter='employee_exit_management.direct_employee_medically_unfit_month', default=2)
    ticket_not_eligible_camp_status_update_reminder_days = fields.Integer(
        config_parameter='employee_exit_management.ticket_not_eligible_camp_status_update_reminder_days', default=7)
    ticket_eligible_camp_status_update_reminder_days = fields.Integer(
        config_parameter='employee_exit_management.ticket_eligible_camp_status_update_reminder_days', default=2)
    insurance_deletion_grace_period = fields.Integer(
        config_parameter='employee_exit_management.insurance_deletion_grace_period')
    retirement_age = fields.Integer(related='company_id.retirement_age', readonly=False)
    retirement_notice_period = fields.Integer(related='company_id.retirement_notice_period', readonly=False)

    amount_disbursal_debit_ac_id = fields.Many2one(
        'account.account', related="company_id.amount_disbursal_debit_ac_id",
        string="Disbursal Debit Account", readonly=False, check_company=True,
        domain="[('deprecated', '=', False), ('account_type', '=', 'expense')]")
    amount_disbursal_credit_ac_id = fields.Many2one(
        'account.account', related="company_id.amount_disbursal_credit_ac_id",
        string="Disbursal Credit Account", readonly=False, check_company=True,
        domain="[('deprecated', '=', False), ('account_type', 'in', ['liability_payable', 'liability_current'])]")
    amount_disbursal_journal_id = fields.Many2one(
        'account.journal', related='company_id.amount_disbursal_journal_id', readonly=False,
        string="Disbursal Journal", check_company=True, domain="[('type', '=', 'general')]")
