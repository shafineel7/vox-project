# -*- coding: utf-8 -*-

from datetime import date
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError


class AdditionalDeduction(models.Model):
    _name = "additional.deduction"
    _description = "Additional Deduction"

    def _get_default_product_id(self):
        expense = self.env['product.product'].search([('is_additional_deduction', '=', True), ('can_be_expensed', '=', True)], limit=1)
        return expense.id

    name = fields.Char("Description", required=True)
    product_id = fields.Many2one(
        'product.product', string='Expense Category', required=True, default=_get_default_product_id,
        domain=[('is_additional_deduction', '=', True), ('can_be_expensed', '=', True)])
    unit_price = fields.Float(string='Amount', required=True, digits="Product Price")
    tax_ids = fields.Many2many('account.tax', 'additional_deduction_tax', 'additional_deduction_id', 'tax_id',
                               string='Tax', check_company=True, domain="[('id', 'in', suitable_tax_ids)]")
    suitable_tax_ids = fields.Many2many(
        'account.tax', 'additional_deduction_suitable_tax', 'additional_deduction_id', 'suitable_tax_id',
        compute='_compute_suitable_tax_ids',
    )
    total = fields.Monetary(string='Total Amount', compute='_compute_total', store=True)
    employee_exit_management_id = fields.Many2one(
        'employee.exit.management', string='Employee Exit Management ID', required=True, ondelete='cascade')
    company_id = fields.Many2one(related="employee_exit_management_id.company_id")
    currency_id = fields.Many2one(
        string='Company Currency',
        related='company_id.currency_id', readonly=True,
    )
    expense_paid_by = fields.Selection([('employee', 'Employee'), ('company', 'Company')], string="Paid By", required=True, default='employee')

    @api.depends('expense_paid_by', 'company_id')
    def _compute_suitable_tax_ids(self):
        for record in self:
            if record.expense_paid_by == 'employee':
                record.suitable_tax_ids = self.env['account.tax'].search([('type_tax_use', '=', 'sale'),
                                                                          ('company_id', '=', record.company_id.id)])
            else:
                record.suitable_tax_ids = self.env['account.tax'].search([('type_tax_use', '=', 'purchase'),
                                                                          ('company_id', '=', record.company_id.id)])

    @api.constrains('unit_price')
    def _check_unit_price(self):
        for record in self:
            if record.unit_price <= 0:
                raise ValidationError("Amount must be greater than zero.")

    @api.depends('unit_price', 'tax_ids', 'currency_id')
    def _compute_total(self):
        for record in self:
            if record.tax_ids:
                taxes_res = record.tax_ids.compute_all(
                    record.unit_price,
                    quantity=1,
                    currency=record.currency_id,
                )
                record.total = taxes_res['total_included']
            else:
                record.total = record.unit_price
