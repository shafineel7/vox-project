# -*- coding: utf-8 -*-
from odoo import models, fields, api, Command, _
from odoo.exceptions import ValidationError, UserError, AccessError
import datetime
from dateutil.relativedelta import relativedelta


class EmployeeExitManagement(models.Model):
    _name = 'employee.exit.management'
    _description = 'Employee Exit Management'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _get_default_groups(self):
        hr_user = self.env.ref('hr.group_hr_user').id
        hr_asst = self.env.ref('department_groups.group_hr_asst').id
        hr_manager = self.env.ref('department_groups.group_hr_mgr').id
        construction_operation_cod = self.env.ref('department_groups.group_construction_operations_coord').id
        staff_operation_cod = self.env.ref('department_groups.group_staff_operations_coord').id
        soft_service_operation_cod = self.env.ref('department_groups.group_soft_ser_operations_coord').id
        on_boarding = self.env.ref('department_groups.group_hr_asst_onboard').id
        welfare = self.env.ref('department_groups.group_hr_welfare_officer').id
        return [Command.link(hr_user), Command.link(hr_asst), Command.link(construction_operation_cod),
                Command.link(staff_operation_cod), Command.link(soft_service_operation_cod),
                Command.link(hr_manager), Command.link(on_boarding), Command.link(welfare)]

    def _get_default_cancellation_amount(self):
        expense_category = self.env['product.product'].search(
            [('is_visa_cancellation', '=', True), ('can_be_expensed', '=', True)], limit=1)
        return expense_category.standard_price

    name = fields.Char(string="Exit Reference", required=True, copy=False, readonly=False, index='trigram',
                       default=lambda self: _('New'))
    employee_id = fields.Many2one('hr.employee', string='Employee ID', required=True,
                                  domain="[('company_id', '=', company_id), ('exit_request_created', '=', False)]")
    employee_name = fields.Char(string='Employee Name', related="employee_id.name")
    designation_id = fields.Many2one('designation', string='Designation', related="employee_id.designation_id")
    allowed_exit_reason_ids = fields.Many2many('exit.reasons', compute="_compute_allowed_exit_reason_ids")
    exit_reason_id = fields.Many2one('exit.reasons', string="Exit Reasons", required=True,
                                     domain="[('id', 'in', allowed_exit_reason_ids)]")
    exit_reason_type = fields.Selection(related='exit_reason_id.type')
    resignation_letter = fields.Binary(string="Resignation Letter")
    resignation_letter_name = fields.Char()
    remarks = fields.Char(string="Remarks")
    ticket_eligibility = fields.Selection([('yes', 'Yes'), ('no', 'No'), ], string='Ticket Eligibility', required=True)
    relieving_date = fields.Date(string='Relieving Date', compute="_compute_relieving_date", store=True)
    actual_last_working_date = fields.Date(string='Actual last working date')
    status = fields.Selection([
        ('new', 'New'), ('approved', 'Approved'), ('eos', 'EOS'), ('visa_cancellation', 'Visa Cancellation'),
        ('air_ticket', 'Air Ticket'), ('on_hold', 'On Hold'), ('insurance_deletion', 'Insurance Deletion'),
        ('closed', 'Closed'), ('reject', 'Rejected')], default='new', tracking=True, string="Stage")
    previous_status = fields.Selection([
        ('new', 'New'), ('approved', 'Approved'), ('eos', 'EOS'), ('visa_cancellation', 'Visa Cancellation'),
        ('air_ticket', 'Air Ticket'), ('on_hold', 'On Hold'), ('insurance_deletion', 'Insurance Deletion'),
        ('closed', 'Closed'), ('reject', 'Rejected')])
    new_stage_status = fields.Selection([
        ('operation_supervisor_approval', 'Operation Supervisor Approval'),
        ('welfare_officer_approval', 'Welfare Officer Approval'),
        ('assistant_operation_supervisor_approval', 'Assistant Operation Supervisor Approval'),
        ('department_head_approval', 'Department Head Approval'), ('ta_approval', 'Talent Acquisition Approval'),
        ('hr_approval', 'HR Approval'), ('md_approval', 'MD Approval'), ('accounts_approval', 'Accounts Approval'),
        ('hr_asst_approval', 'HR Assistant Approval')], string="Status", tracking=True)
    type = fields.Selection([
        ('direct', 'Direct'),
        ('indirect', 'In Direct'),
    ], string='Type', related="employee_id.type", required=True)
    is_self_assign = fields.Boolean(string="Self Assign", default=False)
    reject_reason = fields.Char(string="Reject Reason")
    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user, tracking=True)
    is_self_assigned_user = fields.Boolean(compute="_compute_is_self_assigned_user")
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    company_id = fields.Many2one('res.company', required=True, default=lambda self: self.env.company)
    currency_id = fields.Many2one(
        string='Company Currency', related='company_id.currency_id', readonly=True, )
    employee_exit_date = fields.Date(default=lambda self: fields.Date.today(), required=True)
    is_department_head = fields.Boolean(compute="_compute_is_department_head")
    is_assist_ops = fields.Boolean(compute="_compute_is_assist_ops")
    is_ticket_eligibility_readable = fields.Boolean(compute="_compute_is_ticket_eligibility_readable")
    is_submitted = fields.Boolean(default=False)
    exit_interview_status = fields.Selection([('completed', 'Completed')], string="Exit Interview Status")
    is_auto_created = fields.Boolean(default=False)
    is_login_user_camp_manager = fields.Boolean(compute="_compute_is_login_user_camp_manager")
    # USERS
    dept_head_user_ids = fields.Many2many('res.users', 'hod_user_exit_rel', 'exit_id', 'user_id',
                                          compute='_compute_dept_head_user_ids', store=True)
    op_supervisor_user_ids = fields.Many2many('res.users', 'op_supervisor_user_exit_rel', 'exit_id', 'user_id',
                                              compute='_compute_op_supervisor_user_ids', store=True)
    op_coordinator_user_ids = fields.Many2many('res.users', 'op_coordinator_user_exit_rel', 'exit_id', 'user_id',
                                               compute='_compute_op_coordinator_user_ids', store=True)
    ast_op_supervisor_user_ids = fields.Many2many('res.users', 'ast_op_supervisor_user_exit_rel', 'exit_id', 'user_id',
                                                  compute='_compute_ast_op_supervisor_user_ids', store=True)
    is_dept_head = fields.Boolean(compute='_compute_is_dept_head')
    is_op_supervisor = fields.Boolean(compute='_compute_is_op_supervisor')
    is_op_coordinator = fields.Boolean(compute='_compute_is_op_coordinator')
    is_ast_op_supervisor = fields.Boolean(compute='_compute_is_ast_op_supervisor')

    # Refuse to Work
    is_refuse_to_work_penalty_readable = fields.Boolean(compute="_compute_is_refuse_to_work_penalty_readable")
    willing_to_pay_penalty = fields.Selection([('yes', 'Yes'), ('no', 'No')], string="Employee Willing to Pay Penalty")
    penalty_amount = fields.Monetary(string="Penalty Amount")
    penalty_move_id = fields.Many2one('account.move', string="Penalty Move")
    exit_approval_talent_acquisition_executive_id = fields.Many2one('res.users', string="Approval TA")

    # Refuse to Travel
    is_refuse_to_travel_penalty_readable = fields.Boolean(compute="_compute_is_refuse_to_travel_penalty_readable")
    exit_approval_accounts_team_id = fields.Many2one('res.users', string="Approval Accounts Team")
    refuse_to_travel_document = fields.Binary(string="Document")
    refuse_to_travel_document_name = fields.Char()

    # Medically Unfit
    medically_unfit_reason = fields.Char(string="Medically Unfit Reason")
    medically_unfit_document = fields.Binary(string="Fitness Document")
    medically_unfit_document_name = fields.Char()
    exit_approval_hr_assistant_id = fields.Many2one('res.users', string="Approval HR Assistant")
    is_actual_last_working_date_updated = fields.Boolean(default=False)
    is_medically_unfit_penalty_readable = fields.Boolean(compute="_compute_is_medically_unfit_penalty_readable")

    # Demise
    eos_temp_closed_hr_user_id = fields.Many2one('res.users')
    eos_temp_closed_md_user_id = fields.Many2one('res.users')
    eos_temp_closed_finance_manager_user_id = fields.Many2one('res.users')
    eos_temp_closed_accounts_user_id = fields.Many2one('res.users')
    eos_closed_hr_id = fields.Many2one('res.users')
    eos_demise_close_hr_user_id = fields.Many2one('res.users')
    disbursal_amount = fields.Monetary(string="Disbursal Amount")
    disbursal_status = fields.Selection([('disbursed', 'Disbursed')], string="Disbursal Status")
    is_fm_approved_eos_temp_close = fields.Boolean(default=False)
    disbursal_move_id = fields.Many2one('account.move', string="Disbursal JE")
    death_certificate = fields.Binary(string="Death Certificate")
    death_certificate_name = fields.Char()

    # Resignation approval user
    resignation_approval_hod_id = fields.Many2one('res.users', string="Approval HOD")
    resignation_approval_hr_id = fields.Many2one('res.users', string="Approval HR")
    resignation_approval_md_id = fields.Many2one('res.users', string="Approval MD")
    resignation_approval_operations_supervisor_id = fields.Many2one('res.users',
                                                                    string="Approval Operations Supervisor")
    resignation_approval_welfare_officer_id = fields.Many2one('res.users',
                                                              string="Approval Operations Welfare Officer")
    resignation_approval_assistant_operation_supervisor_id = fields.Many2one(
        'res.users', string="Approval Assistant Operation Supervisor Officer")

    # EOS Stage status
    eos_stage_status = fields.Selection([
        ('asset_collection', 'Asset Collection'), ('transport_deductions', 'Transport Deductions'),
        ('hr_verification', 'HR Verification'), ('full_and_final_settlement', 'Full and Final Settlement'),
        ('payroll_gratuity', 'Payroll & Gratuity Calculation'), ('accounts_approval', 'Accounts Approval'),
        ('hr_approval', 'HR Approval'), ('finance_manager_approval', 'Finance Manager Approval'),
        ('temp_closed', 'Temporarily Closed'), ('md_approval', 'MD Approval'),
        ('eos_temp_finance_manager_approval', 'Finance Manager Approval'), ('disbursal', 'Disbursal'),
        ('eos_temp_hr_verification', 'HR Verification')], string="EOS Status",
        tracking=True)

    asset_collection_ids = fields.One2many('asset.collection', 'employee_exit_management_id',
                                           string="Asset Collections")
    asset_collection_user_id = fields.Many2one('res.users', string="Asset Collection User")
    is_asset_collection_readable = fields.Boolean(compute="_compute_is_asset_collection_readable")
    scrapped_asset_count = fields.Integer(string="Scrapped Asset Count", compute='_get_scrapped_asset')
    scrapped_asset_ids = fields.Many2many(comodel_name='stock.scrap', string="Scraps", compute='_get_scrapped_asset',
                                          copy=False)
    # Transport deduction
    is_transport_deduction_readable = fields.Boolean(compute="_compute_is_transport_deduction_readable")
    transport_deduction_user_id = fields.Many2one('res.users', string="Transport Deduction User")
    transport_deduction_ids = fields.One2many('transport.deduction', 'employee_exit_management_id',
                                              string="Transport Deductions")
    transport_deduction_total = fields.Monetary(string='Grand Total', compute='_compute_transport_deduction_total',
                                                store=True)
    transport_deduction_expense_booking_id = fields.Many2many(
        'expense.booking', 'employee_exit_management_expense_booking_rel', 'employee_exit_management_id',
        'expense_booking_id', string='Expense Bookings')
    td_expense_booking_count = fields.Integer(string="TD Expense Booking Count",
                                              compute='_compute_td_expense_booking_count')

    # HR Verification
    eos_hr_verification_user_id = fields.Many2one('res.users', string="HR Verification User")

    # Final Settlement
    eos_final_settlement_user_id = fields.Many2one('res.users', string="Final Settlement User")
    is_system_calculated_gratuity = fields.Boolean(default=False, string="Is System Calculated Gratuity")
    system_calculated_gratuity = fields.Monetary(string="System Calculated Gratuity")
    employee_gratuity_years = fields.Float(string='Gratuity Calculation Years',
                                           readonly=True, store=True,
                                           help="Employee gratuity years")
    employee_basic_salary = fields.Float(string='Basic Salary',
                                         readonly=True,
                                         help="Employee's basic salary.")
    gratuity_to_be_disbursed = fields.Monetary(string="Gratuity to be Disbursed")
    gratuity_split_up = fields.Html()
    wps_status = fields.Selection([('uploaded', 'Uploaded')], string="WPS File Status")
    is_final_settlement_readable = fields.Boolean(compute="_compute_is_final_settlement_readable")
    final_settlement_date = fields.Date(string="Final Settlement Date")

    # PAYROLL and GRATUITY
    eos_payroll_gratuity_user_id = fields.Many2one('res.users', string="Payroll User")
    is_payroll_gratuity_readable = fields.Boolean(compute="_compute_is_payroll_gratuity_readable")
    payslip_status = fields.Selection([('generated', 'Generated')], string="Payslip Status")

    # Accounts Approval
    eos_accounts_approval_user_id = fields.Many2one('res.users', string="Accounts Approval User")
    is_additional_accounts_deduction_readable = fields.Boolean(
        compute="_compute_is_additional_accounts_deduction_readable")
    additional_deduction_expense_booking_id = fields.Many2many(
        'expense.booking', 'employee_exit_management_additional_expense_booking_rel',
        'employee_exit_management_id', 'additional_expense_booking_id', string='Additional Expense Bookings')
    additional_deduction_expense_booking_count = fields.Integer(
        string="Additional Deduction Expense Booking Count",
        compute='_compute_additional_deduction_expense_booking_count')
    additional_deduction_ids = fields.One2many('additional.deduction', 'employee_exit_management_id',
                                               string="Additional Deductions")
    additional_deduction_total = fields.Monetary(string='Grand Total', compute='_compute_additional_deduction_total',
                                                 store=True)
    # Finance manager approval
    eos_finance_manager_approval_user_id = fields.Many2one('res.users', string="Finance Manager Approval User")

    # HR Approval - DIRECT
    eos_hr_approval_user_id = fields.Many2one('res.users', string="HR Approval User")

    # VISA CANCELLATION
    visa_cancellation_stage_status = fields.Selection([
        ('new', 'New'),
        ('visa_cancellation', 'Visa Cancellation'),
        ('lc_immigration_cancellation', 'Labour Card and Immigration Cancellation')], string="Visa Cancellation Status",
        tracking=True)
    visa_cancellation_hr_assist_user_id = fields.Many2one('res.users', string="Visa Cancellation HR assistant")
    visa_cancellation_hr_assist_second_user_id = fields.Many2one('res.users',
                                                                 string="Visa Cancellation HR assistant Second User")
    visa_cancellation_pro_user_id = fields.Many2one('res.users', string="Visa Cancellation PRO")
    visa_cancellation_to_be_carried_out_date = fields.Date(string="Visa Cancellation to be Carried Out Date")
    cancellation_document = fields.Binary(string="Cancellation Document")
    cancellation_document_name = fields.Char()
    cancellation_document_signed_by_employee = fields.Binary(string="Signed Cancellation Document")
    cancellation_document_signed_by_employee_name = fields.Char()
    is_cancellation_document_updated_by_pro = fields.Boolean(default=False)
    is_visa_cancellation_readable = fields.Boolean(compute="_compute_is_visa_cancellation_readable")

    # Immigration Cancellation
    grace_period_expiry_date = fields.Date(string="Grace Period Expiry Date")
    visa_cancellation_amount = fields.Monetary(string="Visa Cancellation Amount",
                                               default=_get_default_cancellation_amount)
    visa_cancellation_date = fields.Date(string="Visa Cancellation Date")
    visa_cancellation_status = fields.Selection([('completed', 'Completed')], string="Visa Cancellation Status")
    immigration_cancellation_document = fields.Binary(string="Immigration Cancellation Document")
    immigration_cancellation_document_name = fields.Char()
    immigration_cancellation_pro_user_id = fields.Many2one('res.users', string="Immigration Cancellation PRO")
    is_immigration_cancellation_readable = fields.Boolean(compute="_compute_is_immigration_cancellation_readable")
    visa_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    visa_expense_id = fields.Many2one('hr.expense', string="Visa Expense", store=True, copy=False)

    # Insurance Deletion
    insurance_deletion_hr_assist_user_id = fields.Many2one('res.users', string="Insurance deletion HR Assistant")
    insurance_deletion_date = fields.Date(string="Insurance Deletion Date")
    insurance_deletion_request_id = fields.Many2one('insurance.deletion', string="Insurance Deletion Ref")
    insurance_deletion_status = fields.Selection(related='insurance_deletion_request_id.status',
                                                 string="Insurance Deletion Status")
    is_insurance_deletion_readable = fields.Boolean(compute="_compute_is_insurance_deletion_readable")

    # Air Ticket
    is_air_ticket_visible = fields.Boolean(default=False)
    is_air_ticket_readable = fields.Boolean(compute="_compute_is_air_ticket_readable")
    air_ticket_hr_assist_user_id = fields.Many2one('res.users')
    ticket_date = fields.Date(string="Ticket Date")
    flight_number = fields.Char(string="Flight Number")
    ticket_amount = fields.Monetary(string="Ticket Cost")
    ticket_copy = fields.Binary(string="Ticket Copy")
    ticket_copy_name = fields.Char()
    airline_ticket_id = fields.Many2one('airline.ticket', string="Air Ticket Request")

    # Camp Supervisor
    camp_exit_status = fields.Selection([('live_out', 'Live out')], string="Camp Exit Status")
    camp_supervisor_user_id = fields.Many2one('res.users')
    supervisor_reminder_send_date = fields.Date()

    def _compute_is_dept_head(self):
        for rec in self:
            if self._uid in rec.dept_head_user_ids.ids:
                rec.is_dept_head = True
            else:
                rec.is_dept_head = False

    def _compute_is_op_supervisor(self):
        for rec in self:
            if self._uid in rec.op_supervisor_user_ids.ids:
                rec.is_op_supervisor = True
            else:
                rec.is_op_supervisor = False

    def _compute_is_op_coordinator(self):
        for rec in self:
            if self._uid in rec.op_coordinator_user_ids.ids:
                rec.is_op_coordinator = True
            else:
                rec.is_op_coordinator = False

    def _compute_is_ast_op_supervisor(self):
        for rec in self:
            if self._uid in rec.ast_op_supervisor_user_ids.ids:
                rec.is_ast_op_supervisor = True
            else:
                rec.is_ast_op_supervisor = False

    @api.depends('employee_id', 'employee_id.department_id.department_head_ids')
    def _compute_dept_head_user_ids(self):
        user_list = []
        for record in self:
            if record.employee_id.department_id.department_head_ids:
                for emp in record.employee_id.department_id.department_head_ids:
                    if emp.user_id:
                        user_list.append(emp.user_id.id)
            record.dept_head_user_ids = user_list

    @api.depends('employee_id', 'employee_id.department_id.operations_supervisor_ids')
    def _compute_op_supervisor_user_ids(self):
        user_list = []
        for record in self:
            if record.employee_id.department_id.operations_supervisor_ids:
                for emp in record.employee_id.department_id.operations_supervisor_ids:
                    if emp.user_id:
                        user_list.append(emp.user_id.id)
            record.op_supervisor_user_ids = user_list

    @api.depends('employee_id', 'employee_id.department_id.operations_coordinator_ids')
    def _compute_op_coordinator_user_ids(self):
        user_list = []
        for record in self:
            if record.employee_id.department_id.operations_coordinator_ids:
                for emp in record.employee_id.department_id.operations_coordinator_ids:
                    if emp.user_id:
                        user_list.append(emp.user_id.id)
            record.op_coordinator_user_ids = user_list

    @api.depends('employee_id', 'employee_id.department_id.assistant_operations_supervisor_ids')
    def _compute_ast_op_supervisor_user_ids(self):
        user_list = []
        for record in self:
            if record.employee_id.department_id.assistant_operations_supervisor_ids:
                for emp in record.employee_id.department_id.assistant_operations_supervisor_ids:
                    if emp.user_id:
                        user_list.append(emp.user_id.id)
            record.ast_op_supervisor_user_ids = user_list

    @api.depends('additional_deduction_expense_booking_id')
    def _compute_additional_deduction_expense_booking_count(self):
        for record in self:
            if record.additional_deduction_expense_booking_id:
                record.additional_deduction_expense_booking_count = len(record.additional_deduction_expense_booking_id)
            else:
                record.additional_deduction_expense_booking_count = 0

    @api.depends('transport_deduction_expense_booking_id')
    def _compute_td_expense_booking_count(self):
        for record in self:
            if record.transport_deduction_expense_booking_id:
                record.td_expense_booking_count = len(record.transport_deduction_expense_booking_id)
            else:
                record.td_expense_booking_count = 0

    @api.depends('asset_collection_ids.stock_scrap_id')
    def _get_scrapped_asset(self):
        for record in self:
            scraps = record.asset_collection_ids.stock_scrap_id.filtered(
                lambda r: r.state == 'done')
            record.scrapped_asset_ids = scraps
            record.scrapped_asset_count = len(scraps)

    @api.depends('transport_deduction_ids.total')
    def _compute_transport_deduction_total(self):
        for record in self:
            if record.transport_deduction_ids:
                record.transport_deduction_total = sum(record.transport_deduction_ids.mapped('total'))
            else:
                record.transport_deduction_total = 0

    @api.depends('additional_deduction_ids.total')
    def _compute_additional_deduction_total(self):
        for record in self:
            if record.additional_deduction_ids:
                record.additional_deduction_total = sum(record.additional_deduction_ids.mapped('total'))
            else:
                record.additional_deduction_total = 0

    def _compute_is_self_assigned_user(self):
        for record in self:
            if self.env.user.id in record.assigned_user_ids.ids:
                record.is_self_assigned_user = True
            else:
                record.is_self_assigned_user = False

    def _compute_is_department_head(self):
        for record in self:
            if self.env.user.id in record.employee_id.department_id.department_head_ids.user_id.ids and self.env.user.id in record.assigned_user_ids.ids:
                record.is_department_head = True
            else:
                record.is_department_head = False

    def _compute_is_assist_ops(self):
        for record in self:
            if self.env.user.id in record.employee_id.department_id.assistant_operations_supervisor_ids.user_id.ids and self.env.user.id in record.assigned_user_ids.ids:
                record.is_assist_ops = True
            else:
                record.is_assist_ops = False

    def _compute_is_login_user_camp_manager(self):
        for record in self:
            if self.env.user.id == record.employee_id.camp_manager_name_id.user_id.id:
                record.is_login_user_camp_manager = True
            else:
                record.is_login_user_camp_manager = False

    def _compute_is_refuse_to_work_penalty_readable(self):
        for record in self:
            if record.new_stage_status == 'welfare_officer_approval' and record.is_self_assigned_user and record.is_self_assign:
                record.is_refuse_to_work_penalty_readable = True
            elif record.eos_stage_status == 'accounts_approval' and record.is_self_assigned_user and record.is_self_assign:
                record.is_refuse_to_work_penalty_readable = True
            else:
                record.is_refuse_to_work_penalty_readable = False

    @api.depends('new_stage_status', 'assigned_user_ids')
    def _compute_is_refuse_to_travel_penalty_readable(self):
        for record in self:
            if (record.user_has_groups('department_groups.group_hr_asst_onboard,'
                                       'department_groups.group_finance_asst_mgr_accountant,'
                                       'department_groups.group_finance_accountant,'
                                       'department_groups.group_finance_mgr')
                    and self.env.user.id in record.assigned_user_ids.ids):
                record.is_refuse_to_travel_penalty_readable = True
            else:
                record.is_refuse_to_travel_penalty_readable = False

    def _compute_is_medically_unfit_penalty_readable(self):
        for record in self:
            if record.is_submitted is False:
                record.is_medically_unfit_penalty_readable = True
            elif record.eos_stage_status == 'accounts_approval' and record.is_self_assigned_user and record.is_self_assign:
                record.is_medically_unfit_penalty_readable = True
            else:
                record.is_medically_unfit_penalty_readable = False

    def _compute_is_ticket_eligibility_readable(self):
        for record in self:
            if record.env.user.has_group(
                    'department_groups.group_hr_mgr') and self.env.user.id in record.assigned_user_ids.ids:
                record.is_ticket_eligibility_readable = True
            else:
                record.is_ticket_eligibility_readable = False

    def _compute_is_transport_deduction_readable(self):
        for record in self:
            if record.transport_deduction_user_id == self.env.user:
                record.is_transport_deduction_readable = True
            else:
                record.is_transport_deduction_readable = False

    def _compute_is_asset_collection_readable(self):
        for record in self:
            if record.asset_collection_user_id == self.env.user:
                record.is_asset_collection_readable = True
            else:
                record.is_asset_collection_readable = False

    def _compute_is_final_settlement_readable(self):
        for record in self:
            if record.eos_final_settlement_user_id == self.env.user:
                record.is_final_settlement_readable = True
            else:
                record.is_final_settlement_readable = False

    def _compute_is_payroll_gratuity_readable(self):
        for record in self:
            if record.eos_payroll_gratuity_user_id == self.env.user:
                record.is_payroll_gratuity_readable = True
            else:
                record.is_payroll_gratuity_readable = False

    def _compute_is_additional_accounts_deduction_readable(self):
        for record in self:
            if record.eos_accounts_approval_user_id == self.env.user:
                record.is_additional_accounts_deduction_readable = True
            else:
                record.is_additional_accounts_deduction_readable = False

    def _compute_is_visa_cancellation_readable(self):
        for record in self:
            if record.visa_cancellation_stage_status == 'new':
                if record.visa_cancellation_hr_assist_user_id == self.env.user:
                    record.is_visa_cancellation_readable = True
                else:
                    record.is_visa_cancellation_readable = False
            elif record.visa_cancellation_stage_status == 'visa_cancellation':
                if record.is_cancellation_document_updated_by_pro is False:
                    if record.visa_cancellation_pro_user_id == self.env.user:
                        record.is_visa_cancellation_readable = True
                    else:
                        record.is_visa_cancellation_readable = False
                else:
                    if record.visa_cancellation_hr_assist_second_user_id == self.env.user:
                        record.is_visa_cancellation_readable = True
                    else:
                        record.is_visa_cancellation_readable = False
            else:
                record.is_visa_cancellation_readable = False

    def _compute_is_immigration_cancellation_readable(self):
        for record in self:
            if record.immigration_cancellation_pro_user_id == self.env.user:
                record.is_immigration_cancellation_readable = True
            else:
                record.is_immigration_cancellation_readable = False

    def _compute_is_insurance_deletion_readable(self):
        for record in self:
            if record.insurance_deletion_hr_assist_user_id == self.env.user:
                record.is_insurance_deletion_readable = True
            else:
                record.is_insurance_deletion_readable = False

    def _compute_is_air_ticket_readable(self):
        for record in self:
            if record.air_ticket_hr_assist_user_id == self.env.user:
                record.is_air_ticket_readable = True
            else:
                record.is_air_ticket_readable = False

    @api.model
    def default_get(self, fields):
        if self.user_has_groups(
                'base.group_user,'
                'department_groups.group_hr_asst,'
                'hr.group_hr_user,department_groups.group_hr_mgr,'
                'department_groups.group_construction_operations_coord,'
                'department_groups.group_staff_operations_coord,'
                'department_groups.group_soft_ser_operations_coord,department_groups.group_hr_asst_onboard,'
                'department_groups.group_hr_welfare_officer,base.group_user'):
            return super(EmployeeExitManagement, self).default_get(fields)
        else:
            raise ValidationError(_("You cannot create Employee Exit Request"))

    @api.onchange('exit_reason_id')
    def get_default_ticket_eligibility(self):
        if self.exit_reason_id:
            self.ticket_eligibility = self.exit_reason_id.ticket_eligibility
            self.penalty_amount = self.exit_reason_id.penalty_amount

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        if self.employee_id:
            if not self.employee_id.type:
                raise UserError(_("Please define employee type in employee master"))
            if self.employee_id.exit_request_created:
                raise UserError(_("Exit requests exist already."))
            if not self.employee_id.sudo().contract_id:
                raise UserError(_("No running contract found for the selected employee"))

    @api.depends('employee_id')
    def _compute_allowed_exit_reason_ids(self):
        for record in self:
            if record.employee_id:
                if record.employee_id.type == 'indirect':
                    exit_reason_type = ['resignation', 'termination', 'contract_expiry', 'abscond', 'demise',
                                        'retirement']
                    record.allowed_exit_reason_ids = self.env['exit.reasons'].search([('type', 'in', exit_reason_type)])
                if record.employee_id.type == 'direct':
                    exit_reason_type = ['resignation', 'termination', 'contract_expiry', 'abscond', 'refuse_to_work',
                                        'refuse_to_travel', 'medically_unfit', 'demise', 'retirement']
                    record.allowed_exit_reason_ids = self.env['exit.reasons'].search([('type', 'in', exit_reason_type)])
            else:
                record.allowed_exit_reason_ids = False

    @api.depends('employee_exit_date', 'employee_id.notice_period', 'exit_reason_type', 'employee_id.contract_id',
                 'employee_id')
    def _compute_relieving_date(self):
        for record in self:
            if record.exit_reason_type in ('resignation', 'termination', 'refuse_to_work'):
                record.relieving_date = record.employee_exit_date + datetime.timedelta(
                    days=record.employee_id.notice_period)
            elif record.exit_reason_type == 'contract_expiry':
                record.relieving_date = record.employee_id.contract_id.date_end
            else:
                record.relieving_date = fields.Date.today()

    # Smart button

    def action_view_additional_expense_booking(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("expense_management.expense_booking_action")
        action['domain'] = [('id', 'in', self.additional_deduction_expense_booking_id.ids)]
        action['context'] = dict(self._context, create=False)
        return action

    def action_view_td_expense_booking(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("expense_management.expense_booking_action")
        action['domain'] = [('id', 'in', self.transport_deduction_expense_booking_id.ids)]
        action['context'] = dict(self._context, create=False)
        return action

    def action_view_scrapped_asset(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("stock.action_stock_scrap")
        action['domain'] = [('id', 'in', self.scrapped_asset_ids.ids)]
        action['context'] = dict(self._context, create=False)
        return action

    def action_view_penalty_move(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("account.action_move_out_invoice_type")
        action['domain'] = [('id', '=', self.penalty_move_id.id)]
        action['context'] = dict(self._context, create=False)
        return action

    def action_view_disbursal_move(self):
        domain = [('id', '=', self.disbursal_move_id.id)]
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.move',
            'name': _('Disbursal Move'),
            'view_mode': 'tree,form',
            'context': dict(self._context, create=False),
            'domain': domain,
        }

    @api.constrains('penalty_amount', 'willing_to_pay_penalty')
    def _check_penalty_amount(self):
        for record in self:
            if record.willing_to_pay_penalty == 'yes' and record.penalty_amount <= 0:
                raise ValidationError(_("Penalty amount must be greater than zero."))

    @api.constrains('employee_id', 'exit_reason_id')
    def _check_resignation_is_possible(self):
        for record in self:
            if record.employee_id and not record.employee_id.type:
                raise UserError(_("Please define employee type in employee master"))
            if record.employee_id.exit_request_created:
                raise UserError(_("Exit requests exist already."))
            if not record.employee_id.sudo().contract_id:
                raise UserError(_("No running contract found for the selected employee"))
            if not record.employee_id.date_of_joining:
                raise UserError(_("Please provide date of joining in employee master"))
            if record.exit_reason_id.type == 'resignation' and record.employee_id.type == 'direct':
                if record.employee_id.date_of_joining + relativedelta(
                        months=record.employee_id.company_id.direct_employee_resignation_month) > fields.Date.today():
                    raise ValidationError(_("%s need to complete %d months of employment", record.employee_id.name,
                                            record.employee_id.company_id.direct_employee_resignation_month))
            if record.exit_reason_id.type == 'contract_expiry':
                if not record.employee_id.sudo().contract_id:
                    raise UserError(_("No running contract found for the selected employee"))
                if not record.employee_id.sudo().contract_id.date_end:
                    raise UserError(_("Contract end date is not provided in the employee's contract."))
                condition_date = fields.Date.today() + relativedelta(
                    months=record.employee_id.company_id.contract_expiry_month)
                if record.employee_id.sudo().contract_id.date_end > condition_date:
                    raise ValidationError(
                        _("%s's contract expiration date is not within %d months. So you can't create a contract expiry against this employee as for now.",
                          record.employee_id.name,
                          record.employee_id.company_id.contract_expiry_month))
            if record.exit_reason_id.type == 'termination':
                if record.employee_id.sudo().warning_current_contract < 2:
                    raise ValidationError(
                        _("To terminate an employee, a minimum of two warning letters should be issued within the contract period."))
            if record.exit_reason_id.type == 'refuse_to_work':
                if record.employee_id.type == 'indirect':
                    raise ValidationError("Refuse to work is applicable for direct employees only")
                if record.employee_id.date_of_joining + relativedelta(
                        months=record.employee_id.company_id.direct_employee_refuse_to_work_month) < fields.Date.today():
                    raise ValidationError(
                        _("only employee's within %s months of joining can be selected for refuse to work",
                          record.employee_id.company_id.direct_employee_refuse_to_work_month))
            if record.exit_reason_id.type == 'medically_unfit':
                if record.employee_id.type == 'indirect':
                    raise ValidationError("Medically unfit is applicable for direct employees only")
                month = int(self.env['ir.config_parameter'].sudo().get_param(
                    'employee_exit_management.direct_employee_medically_unfit_month'))
                if record.employee_id.date_of_joining + relativedelta(months=month) <= fields.Date.today():
                    raise ValidationError(
                        _("Medically unfit is only possible if the exit creation date is within %s months of the joining date.",
                          month))
            if record.exit_reason_id.type == 'retirement':
                if not record.employee_id.birthday:
                    raise UserError(_("Please set date of birth of employee"))
                if not record.employee_id.retirement_notice_date or record.employee_id.retirement_notice_date > fields.Date.today():
                    raise ValidationError(
                        _("Please wait till employee reach retirement age."))

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if 'exit_reason_id' in vals and 'employee_id' in vals:
                exit_reason = self.env['exit.reasons'].browse(vals['exit_reason_id'])
                employee = self.env['hr.employee'].browse(vals['employee_id'])
                if exit_reason.type == 'resignation':
                    if employee.type == 'indirect':
                        if self.env.user.has_group('hr.group_hr_user') and not self.env.user.has_group(
                                'department_groups.group_hr_asst'):
                            if self.env.user != employee.user_id:
                                raise AccessError(
                                    _("You don't have the permission to create a resignation of indirect employee"))
                        if not self.env.user.has_group('hr.group_hr_user') and not self.env.user.has_group(
                                'department_groups.group_hr_asst'):
                            raise AccessError(
                                _("You don't have the permission to create a resignation of indirect employee"))
                    else:
                        allowed_user = []
                        for coord in employee.department_id.operations_coordinator_ids:
                            if coord.user_id:
                                allowed_user.append(coord.user_id.id)
                        if self.env.user.id not in allowed_user:
                            raise AccessError(
                                _("You don't have the permission to create a resignation of direct employee"))
                        if employee.date_of_joining + relativedelta(
                                months=employee.company_id.direct_employee_resignation_month) > fields.Date.today():
                            raise ValidationError(_("%s need to complete %d months of employment", employee.name,
                                                    employee.company_id.direct_employee_resignation_month))
                elif exit_reason.type == 'contract_expiry':
                    if not self.user_has_groups('department_groups.group_hr_asst,department_groups.group_hr_mgr'):
                        raise AccessError(
                            _("You don't have the permission to create a contract expiry"))
                elif exit_reason.type == 'termination':
                    if employee.type == 'indirect':
                        if not self.env.user.has_group('department_groups.group_hr_mgr'):
                            raise AccessError(
                                _("You don't have the access rights to create termination of indirect employee"))
                    else:
                        allowed_user = []
                        for coord in employee.department_id.operations_coordinator_ids:
                            if coord.user_id:
                                allowed_user.append(coord.user_id.id)
                        if self.env.user.id not in allowed_user:
                            raise AccessError(
                                _("You don't have the access rights to create termination of direct employee"))
                elif exit_reason.type == 'refuse_to_work':
                    if employee.type == 'indirect':
                        raise ValidationError("Refuse to work is applicable for direct employees only")
                    allowed_user = []
                    for coord in employee.department_id.operations_coordinator_ids:
                        if coord.user_id:
                            allowed_user.append(coord.user_id.id)
                    if self.env.user.id not in allowed_user:
                        raise AccessError(
                            _("You don't have the access rights to create refuse to work exit request for direct employees"))
                elif exit_reason.type == 'refuse_to_travel':
                    if employee.type == 'indirect':
                        raise ValidationError("Refuse to travel is applicable for direct employees only")
                    if not self.env.user.has_group('department_groups.group_hr_asst_onboard'):
                        raise AccessError(
                            _("You don't have the access rights to create refuse to travel exit request for direct employees"))
                elif exit_reason.type == 'medically_unfit':
                    if employee.type == 'indirect':
                        raise ValidationError("Medically unfit is applicable for direct employees only")
                    allowed_user = []
                    for coord in employee.department_id.operations_coordinator_ids:
                        if coord.user_id:
                            allowed_user.append(coord.user_id.id)
                    if (not self.user_has_groups('department_groups.group_hr_asst,'
                                                 'department_groups.group_hr_welfare_officer')
                            and self.env.user.id not in allowed_user):
                        raise AccessError(
                            _("You don't have the access rights to create medically unfit exit request for direct employees"))
                elif exit_reason.type == 'demise':
                    if not self.env.user.has_group('department_groups.group_hr_asst'):
                        raise AccessError(
                            _("You don't have the access rights to create demise exit request"))
                elif exit_reason.type == 'retirement':
                    if not self.env.user.has_group('department_groups.group_hr_mgr'):
                        raise AccessError(
                            _("You don't have the access rights to create retirement exit request"))

            if 'company_id' in vals:
                self = self.with_company(vals['company_id'])
            if vals.get('name', _("New")) == _("New"):
                vals['name'] = self.env['ir.sequence'].next_by_code('employee.exit') or _("New")
        res = super().create(vals_list)
        for record in res:
            record.employee_id.exit_request_created = True
            if record.exit_reason_type == 'medically_unfit':
                record.employee_id.write({
                    'medically_unfit_reason': record.medically_unfit_reason,
                    'is_medically_unfit': True,
                    'medically_unfit_document': record.medically_unfit_document,
                    'medically_unfit_document_name': record.medically_unfit_document_name
                })
                email_template = self.env.ref('employee_exit_management.email_template_medically_unfit_notification',
                                              raise_if_not_found=False)
                users = self.env['res.users']
                agent_user = self.env['res.users'].search(
                    [('agent_id', '=', record.employee_id.employee_onboarding_id.agent_id.id)])
                if agent_user:
                    users += agent_user
                if record.op_coordinator_user_ids:
                    users += record.op_coordinator_user_ids
                if record.ast_op_supervisor_user_ids:
                    users += record.ast_op_supervisor_user_ids
                emails = [user.partner_id.email for user in users]
                email_values = {'email_to': ','.join(emails),
                                'email_from': self.env.user.partner_id.email}
                if email_template and emails and self.env.user.partner_id.email:
                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
        return res

    def action_hold_exit_request(self):
        for record in self:
            record.previous_status = record.status
            record.status = 'on_hold'

    def action_resume_exit_request(self):
        for record in self:
            record.status = record.previous_status

    def unlink(self):
        for record in self:
            # if record.is_submitted is True:
            #     raise UserError(_("You can't delete submitted Exit request"))
            record.employee_id.exit_request_created = False
            record.employee_id.is_medically_unfit = False
        return super().unlink()

    def action_submit(self):
        for record in self:
            if record.exit_reason_type in ('resignation', 'retirement'):
                if record.exit_reason_type == 'retirement':
                    if not self.env.user.has_group('department_groups.group_hr_mgr'):
                        raise AccessError(_("You don't have access to submit employee exit request"))
                else:
                    if record.type == 'indirect':
                        if self.env.user.has_group('department_groups.group_hr_asst'):
                            if record.employee_id.user_id and record.employee_id.user_id != record.create_uid:
                                if self.env.user != record.create_uid:
                                    raise AccessError(_("You don't have permission to submit this exit request"))
                            if not record.employee_id.user_id and self.env.user != record.create_uid:
                                raise AccessError(_("You don't have permission to submit this exit request"))
                        if self.env.user.id != record.employee_id.user_id.id and not self.env.user.has_group(
                                'department_groups.group_hr_asst'):
                            raise AccessError(_("You don't have access to submit employee exit request"))
                    else:
                        if self.env.user.id not in record.op_coordinator_user_ids.ids:
                            raise AccessError(_("You don't have access to submit employee exit request"))

                record.is_submitted = True
                if record.type == 'indirect':
                    department_head_ids = record.employee_id.department_id.department_head_ids
                    email_template = self.env.ref('employee_exit_management.email_template_hod_approval',
                                                  raise_if_not_found=False)
                    hod_emails = [hod.user_id.partner_id.email for hod in department_head_ids]
                    email_values = {'email_to': ','.join(hod_emails), 'email_from': self.env.user.partner_id.email}
                    if email_template and hod_emails and self.env.user.partner_id.email:
                        email_template.send_mail(record.id, email_values=email_values, force_send=True)
                    record.assigned_user_ids = False
                    record.assigned_group_ids = False
                    record.assigned_user_ids = [Command.link(hod.user_id.id) for hod in department_head_ids]
                    allowed_groups = []
                    if self.env.ref(
                            'department_groups.group_construction_dept_head') in record.assigned_user_ids.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_construction_dept_head').id)
                    if self.env.ref(
                            'department_groups.group_staff_dept_head') in record.assigned_user_ids.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_staff_dept_head').id)
                    if self.env.ref(
                            'department_groups.group_soft_ser_dept_head') in record.assigned_user_ids.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_soft_ser_dept_head').id)
                    record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                    record.new_stage_status = 'department_head_approval'
                else:
                    operations_supervisor_users = []
                    for op_super in record.employee_id.department_id.operations_supervisor_ids:
                        if op_super.user_id:
                            operations_supervisor_users.append(Command.link(op_super.user_id.id))
                    if len(operations_supervisor_users) == 0:
                        raise ValidationError(
                            _("Please assign operation supervisor and related user to operation supervisor"))
                    record.assigned_user_ids = False
                    record.assigned_group_ids = False
                    record.assigned_user_ids = operations_supervisor_users
                    allowed_groups = []
                    if self.env.ref(
                            'department_groups.group_construction_operations_super') in record.assigned_user_ids.groups_id:
                        allowed_groups.append(
                            self.env.ref('department_groups.group_construction_operations_super').id)
                    if self.env.ref(
                            'department_groups.group_staff_operations_super') in record.assigned_user_ids.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_staff_operations_super').id)
                    if self.env.ref(
                            'department_groups.group_soft_ser_operations_super') in record.assigned_user_ids.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_soft_ser_operations_super').id)
                    record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                    operations_supervisor_emails = [op_super_employee.work_email for op_super_employee in
                                                    record.employee_id.department_id.operations_supervisor_ids]
                    email_values = {'email_to': ','.join(operations_supervisor_emails),
                                    'email_from': self.env.user.partner_id.email}
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_operations_supervisor_approval',
                        raise_if_not_found=False)
                    if email_template and operations_supervisor_emails and self.env.user.partner_id.email:
                        email_template.send_mail(record.id, email_values=email_values, force_send=True)
                    record.new_stage_status = 'operation_supervisor_approval'

    def action_submit_contract_expiry(self):
        for record in self:
            if not self.user_has_groups('department_groups.group_hr_asst,department_groups.group_hr_mgr'):
                raise ValidationError(_("You don't have permission to submit this exit request"))
            record.is_submitted = True
            if record.type == 'indirect':
                department_head_ids = record.employee_id.department_id.department_head_ids
                email_template = self.env.ref('employee_exit_management.email_template_hod_approval',
                                              raise_if_not_found=False)
                hod_emails = [hod.user_id.partner_id.email for hod in department_head_ids]
                email_values = {'email_to': ','.join(hod_emails),
                                'email_from': self.env.user.partner_id.email}
                if email_template and hod_emails and self.env.user.partner_id.email:
                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.assigned_user_ids = False
                record.assigned_group_ids = False
                record.assigned_user_ids = [Command.link(hod.user_id.id) for hod in department_head_ids]
                allowed_groups = []
                if self.env.ref(
                        'department_groups.group_construction_dept_head') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_construction_dept_head').id)
                if self.env.ref(
                        'department_groups.group_staff_dept_head') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_staff_dept_head').id)
                if self.env.ref(
                        'department_groups.group_soft_ser_dept_head') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_soft_ser_dept_head').id)
                record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                record.new_stage_status = 'department_head_approval'
            else:
                operations_supervisor_users = []
                for op_super in record.employee_id.department_id.operations_supervisor_ids:
                    if op_super.user_id:
                        operations_supervisor_users.append(Command.link(op_super.user_id.id))
                if len(operations_supervisor_users) == 0:
                    raise ValidationError(
                        _("Please assign operation supervisor and related user to operation supervisor"))
                record.assigned_user_ids = False
                record.assigned_group_ids = False
                record.assigned_user_ids = operations_supervisor_users
                allowed_groups = []
                if self.env.ref(
                        'department_groups.group_construction_operations_super') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(
                        self.env.ref('department_groups.group_construction_operations_super').id)
                if self.env.ref(
                        'department_groups.group_staff_operations_super') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_staff_operations_super').id)
                if self.env.ref(
                        'department_groups.group_soft_ser_operations_super') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_soft_ser_operations_super').id)
                record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                operations_supervisor_emails = [op_super_employee.work_email for op_super_employee in
                                                record.employee_id.department_id.operations_supervisor_ids]
                email_values = {'email_to': ','.join(operations_supervisor_emails),
                                'email_from': self.env.user.partner_id.email}
                email_template = self.env.ref(
                    'employee_exit_management.email_template_operations_supervisor_approval',
                    raise_if_not_found=False)
                if email_template and operations_supervisor_emails and self.env.user.partner_id.email:
                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.new_stage_status = 'operation_supervisor_approval'

    def action_submit_termination(self):
        for record in self:
            record.employee_id.exit_request_created = True
            if record.type == 'indirect':
                if not self.env.user.has_group('department_groups.group_hr_mgr'):
                    raise AccessError(_("You don't have permission to submit this exit request"))
                record.is_submitted = True
                record.resignation_approval_hr_id = self.env.user.id
                department_head_ids = record.employee_id.department_id.department_head_ids
                email_template = self.env.ref('employee_exit_management.email_template_hod_approval',
                                              raise_if_not_found=False)
                hod_emails = [hod.user_id.partner_id.email for hod in department_head_ids]
                email_values = {'email_to': ','.join(hod_emails),
                                'email_from': self.env.user.partner_id.email}
                if email_template and hod_emails and self.env.user.partner_id.email:
                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.assigned_user_ids = False
                record.assigned_group_ids = False
                record.assigned_user_ids = [Command.link(hod.user_id.id) for hod in department_head_ids]
                allowed_groups = []
                if self.env.ref(
                        'department_groups.group_construction_dept_head') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_construction_dept_head').id)
                if self.env.ref(
                        'department_groups.group_staff_dept_head') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_staff_dept_head').id)
                if self.env.ref(
                        'department_groups.group_soft_ser_dept_head') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_soft_ser_dept_head').id)
                record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                record.new_stage_status = 'department_head_approval'

            if record.type == 'direct':
                allowed_user = []
                for coord in record.employee_id.department_id.operations_coordinator_ids:
                    if coord.user_id:
                        allowed_user.append(coord.user_id.id)
                if self.env.user.id not in allowed_user:
                    raise AccessError(_("You don't have access to submit employee exit request"))
                record.is_submitted = True
                operations_supervisor_users = []
                for op_super in record.employee_id.department_id.operations_supervisor_ids:
                    if op_super.user_id:
                        operations_supervisor_users.append(Command.link(op_super.user_id.id))
                if len(operations_supervisor_users) == 0:
                    raise ValidationError(
                        _("Please assign operation supervisor and related user to operation supervisor"))
                record.assigned_user_ids = False
                record.assigned_group_ids = False
                record.assigned_user_ids = operations_supervisor_users
                allowed_groups = []
                if self.env.ref(
                        'department_groups.group_construction_operations_super') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(
                        self.env.ref('department_groups.group_construction_operations_super').id)
                if self.env.ref(
                        'department_groups.group_staff_operations_super') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_staff_operations_super').id)
                if self.env.ref(
                        'department_groups.group_soft_ser_operations_super') in record.assigned_user_ids.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_soft_ser_operations_super').id)
                record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                operations_supervisor_emails = [op_super_employee.work_email for op_super_employee in
                                                record.employee_id.department_id.operations_supervisor_ids]
                email_values = {'email_to': ','.join(operations_supervisor_emails),
                                'email_from': self.env.user.partner_id.email}
                email_template = self.env.ref(
                    'employee_exit_management.email_template_operations_supervisor_approval',
                    raise_if_not_found=False)
                if email_template and operations_supervisor_emails and self.env.user.partner_id.email:
                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.new_stage_status = 'operation_supervisor_approval'

    def action_submit_refuse_to_work(self):
        for record in self:
            record.employee_id.exit_request_created = True
            if record.type == 'direct':
                allowed_user = []
                for coord in record.employee_id.department_id.operations_coordinator_ids:
                    if coord.user_id:
                        allowed_user.append(coord.user_id.id)
                if self.env.user.id not in allowed_user:
                    raise AccessError(_("You don't have access to submit employee exit request"))
                group = self.env.ref('department_groups.group_hr_welfare_officer')
                users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                emails = [user.partner_id.email for user in users]
                record.assigned_user_ids = False
                record.assigned_group_ids = False
                record.assigned_user_ids = users
                record.assigned_group_ids = [Command.link(group.id)]
                record.is_submitted = True
                email_values = {'email_to': ','.join(emails),
                                'email_from': self.env.user.partner_id.email}
                email_template = self.env.ref(
                    'employee_exit_management.email_template_welfare_officer_approval',
                    raise_if_not_found=False)
                if email_template and emails and self.env.user.partner_id.email:
                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.new_stage_status = 'welfare_officer_approval'

    def action_submit_refuse_to_travel(self):
        for record in self:
            record.employee_id.exit_request_created = True
            if record.type == 'direct':
                allowed_user = []
                for coord in record.employee_id.department_id.operations_coordinator_ids:
                    if coord.user_id:
                        allowed_user.append(coord.user_id.id)
                if not self.user_has_groups('department_groups.group_hr_asst_onboard'):
                    raise AccessError(_("You don't have access to submit employee exit request"))
                record.is_submitted = True
                group = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
                users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                emails = [user.partner_id.email for user in users]
                record.assigned_user_ids = False
                record.assigned_group_ids = False
                record.assigned_user_ids = users
                record.assigned_group_ids = [Command.link(group.id)]
                email_values = {'email_to': ','.join(emails),
                                'email_from': self.env.user.partner_id.email}
                email_template = self.env.ref(
                    'employee_exit_management.email_template_talent_acquisition_approval',
                    raise_if_not_found=False)
                if email_template and emails and self.env.user.partner_id.email:
                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.new_stage_status = 'ta_approval'

    def action_submit_medically_unfit(self):
        for record in self:
            if record.type == 'direct':
                if not record.is_submitted:
                    if (not self.user_has_groups('department_groups.group_hr_asst,'
                                                 'department_groups.group_hr_welfare_officer')
                            and self.env.uid not in record.op_coordinator_user_ids.ids):
                        raise AccessError(_("You don't have access to submit employee exit request"))
                    record.is_submitted = True
                    record.employee_id.exit_request_created = True
                    email_template = self.env.ref('employee_exit_management.email_template_notify_hod',
                                                  raise_if_not_found=False)
                    hod_emails = [user.partner_id.email for user in record.dept_head_user_ids]
                    email_values = {'email_to': ','.join(hod_emails),
                                    'email_from': self.env.user.partner_id.email}
                    if email_template and hod_emails and self.env.user.partner_id.email:
                        email_template.send_mail(record.id, email_values=email_values, force_send=True)
                    record.assigned_user_ids = False
                    record.assigned_group_ids = False
                    record.assigned_user_ids = [Command.link(user.id) for user in record.dept_head_user_ids]
                    allowed_groups = []
                    if self.env.ref(
                            'department_groups.group_construction_dept_head') in record.dept_head_user_ids.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_construction_dept_head').id)
                    if self.env.ref(
                            'department_groups.group_staff_dept_head') in record.dept_head_user_ids.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_staff_dept_head').id)
                    if self.env.ref(
                            'department_groups.group_soft_ser_dept_head') in record.dept_head_user_ids.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_soft_ser_dept_head').id)
                    record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                else:
                    if not record.actual_last_working_date:
                        raise UserError(_("Please update actual last working date."))
                    record.is_actual_last_working_date_updated = True
                    if self.env.uid not in record.assigned_user_ids.ids:
                        raise AccessError(_("You don't have access to submit employee exit request"))
                    record.is_self_assign = False
                    group = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
                    users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                    emails = [user.partner_id.email for user in users]
                    record.assigned_user_ids = False
                    record.assigned_group_ids = False
                    record.assigned_user_ids = users
                    record.assigned_group_ids = [Command.link(group.id)]
                    email_values = {'email_to': ','.join(emails),
                                    'email_from': self.env.user.partner_id.email}
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_talent_acquisition_approval',
                        raise_if_not_found=False)
                    if email_template and emails and self.env.user.partner_id.email:
                        email_template.send_mail(record.id, email_values=email_values, force_send=True)
                    record.new_stage_status = 'ta_approval'

    def action_submit_demise(self):
        for record in self:
            record.employee_id.exit_request_created = True
            if not self.user_has_groups('department_groups.group_hr_asst'):
                raise AccessError(_("You don't have access to submit employee exit request"))
            if not record.relieving_date:
                raise UserError(_("Please set relieving"))
            record.is_submitted = True
            group = self.env.ref('department_groups.group_hr_mgr')
            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
            emails = [user.partner_id.email for user in users]
            record.assigned_user_ids = False
            record.assigned_group_ids = False
            record.assigned_user_ids = users
            record.assigned_group_ids = [Command.link(group.id)]
            email_values = {'email_to': ','.join(emails),
                            'email_from': self.env.user.partner_id.email}
            email_template = self.env.ref(
                'employee_exit_management.email_template_hr_manager_approval',
                raise_if_not_found=False)
            if email_template and emails and self.env.user.partner_id.email:
                email_template.send_mail(record.id, email_values=email_values, force_send=True)
            record.new_stage_status = 'hr_approval'

    def action_self_assign_medically_unfit_hod(self):
        for record in self:
            if record.exit_reason_type == 'medically_unfit' and record.is_submitted:
                if self.env.user in record.dept_head_user_ids:
                    record.write({'is_self_assign': True, 'resignation_approval_hod_id': record.env.user.id})
                    record.assigned_group_ids = False
                    allowed_groups = []
                    if self.env.ref(
                            'department_groups.group_construction_dept_head') in record.env.user.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_construction_dept_head').id)
                    if self.env.ref(
                            'department_groups.group_staff_dept_head') in record.env.user.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_staff_dept_head').id)
                    if self.env.ref(
                            'department_groups.group_soft_ser_dept_head') in record.env.user.groups_id:
                        allowed_groups.append(self.env.ref('department_groups.group_soft_ser_dept_head').id)
                    record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                    assigned_user = record.env.user.id
                    record.assigned_user_ids = False
                    record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                else:
                    raise ValidationError(_("You don't have access to self assign this exit request"))

    def action_self_assign(self):
        for record in self:
            if record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'refuse_to_travel',
                    'medically_unfit',
                    'demise', 'retirement') and not record.is_self_assign:
                if record.status == 'new':
                    if record.new_stage_status == 'department_head_approval':
                        if self.env.user in record.dept_head_user_ids:
                            record.write({'is_self_assign': True, 'resignation_approval_hod_id': record.env.user.id})
                            record.assigned_group_ids = False
                            allowed_groups = []
                            if self.env.ref(
                                    'department_groups.group_construction_dept_head') in record.env.user.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_construction_dept_head').id)
                            if self.env.ref(
                                    'department_groups.group_staff_dept_head') in record.env.user.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_staff_dept_head').id)
                            if self.env.ref(
                                    'department_groups.group_soft_ser_dept_head') in record.env.user.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_soft_ser_dept_head').id)
                            record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.new_stage_status == 'hr_approval':
                        if record.env.user.has_group('department_groups.group_hr_mgr'):
                            record.write({'is_self_assign': True, 'resignation_approval_hr_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.new_stage_status == 'md_approval':
                        if record.env.user.has_group('department_groups.group_mngmt_md'):
                            record.write({'is_self_assign': True, 'resignation_approval_md_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.new_stage_status == 'operation_supervisor_approval':
                        if self.env.user in record.op_supervisor_user_ids:
                            record.assigned_group_ids = False
                            record.write({'is_self_assign': True,
                                          'resignation_approval_operations_supervisor_id': record.env.user.id})
                            allowed_groups = []
                            if self.env.ref(
                                    'department_groups.group_construction_operations_super') in record.env.user.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_construction_operations_super').id)
                            if self.env.ref(
                                    'department_groups.group_staff_operations_super') in record.env.user.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_staff_operations_super').id)
                            if self.env.ref(
                                    'department_groups.group_soft_ser_operations_super') in record.env.user.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_soft_ser_operations_super').id)
                            record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.new_stage_status == 'welfare_officer_approval':
                        if record.env.user.has_group('department_groups.group_hr_welfare_officer'):
                            record.write(
                                {'is_self_assign': True, 'resignation_approval_welfare_officer_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.new_stage_status == 'assistant_operation_supervisor_approval':
                        if self.env.user in record.assigned_user_ids:
                            record.write({'is_self_assign': True,
                                          'resignation_approval_assistant_operation_supervisor_id': record.env.user.id})
                            record.assigned_group_ids = False
                            allowed_groups = []
                            if self.env.ref(
                                    'department_groups.group_construction_asst_operations_super') in record.env.user.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_construction_asst_operations_super').id)
                            if self.env.ref(
                                    'department_groups.group_staff_asst_operations_super') in record.env.user.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_staff_asst_operations_super').id)
                            if self.env.ref(
                                    'department_groups.group_soft_ser_asst_operations_super') in record.env.user.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_soft_ser_asst_operations_super').id)
                            record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.new_stage_status == 'ta_approval':
                        if self.env.user in record.assigned_user_ids:
                            record.write({'is_self_assign': True,
                                          'exit_approval_talent_acquisition_executive_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.new_stage_status == 'accounts_approval':
                        if self.env.user in record.assigned_user_ids:
                            record.write({'is_self_assign': True,
                                          'exit_approval_accounts_team_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.new_stage_status == 'hr_asst_approval':
                        if self.env.user in record.assigned_user_ids:
                            record.write({'is_self_assign': True,
                                          'exit_approval_hr_assistant_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))

                elif record.status == 'eos':
                    if record.eos_stage_status == 'asset_collection':
                        if record.type == 'indirect':
                            if (self.env.user in record.assigned_user_ids and
                                    self.user_has_groups('department_groups.group_finance_asst_mgr_accountant,'
                                                         'department_groups.group_finance_accountant,'
                                                         'department_groups.group_finance_mgr')):
                                record.assigned_group_ids = False
                                record.write({'is_self_assign': True, 'asset_collection_user_id': record.env.user.id})
                                allowed_groups = []
                                if self.env.ref(
                                        'department_groups.group_finance_asst_mgr_accountant') in record.env.user.groups_id:
                                    allowed_groups.append(
                                        self.env.ref('department_groups.group_finance_asst_mgr_accountant').id)
                                if self.env.ref(
                                        'department_groups.group_finance_accountant') in record.env.user.groups_id:
                                    allowed_groups.append(self.env.ref('department_groups.group_finance_accountant').id)
                                if self.env.ref(
                                        'department_groups.group_finance_mgr') in record.env.user.groups_id:
                                    allowed_groups.append(
                                        self.env.ref('department_groups.group_finance_mgr').id)
                                record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                                assigned_user = record.env.user.id
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                            else:
                                raise ValidationError(_("You don't have access to self assign this exit request"))
                        else:
                            if (self.env.user in record.assigned_user_ids and
                                    self.env.user.has_group('department_groups.group_finance_store_keeper')):
                                record.write({'is_self_assign': True, 'asset_collection_user_id': record.env.user.id})
                                assigned_user = record.env.user.id
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                            else:
                                raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'transport_deductions':
                        if record.type == 'indirect':
                            if (self.env.user in record.assigned_user_ids and
                                    self.user_has_groups('department_groups.group_finance_asst_mgr_accountant,'
                                                         'department_groups.group_finance_accountant,'
                                                         'department_groups.group_finance_mgr')):
                                record.assigned_group_ids = False
                                record.write(
                                    {'is_self_assign': True, 'transport_deduction_user_id': record.env.user.id})
                                allowed_groups = []
                                if self.env.ref(
                                        'department_groups.group_finance_asst_mgr_accountant') in record.env.user.groups_id:
                                    allowed_groups.append(
                                        self.env.ref('department_groups.group_finance_asst_mgr_accountant').id)
                                if self.env.ref(
                                        'department_groups.group_finance_accountant') in record.env.user.groups_id:
                                    allowed_groups.append(self.env.ref('department_groups.group_finance_accountant').id)
                                if self.env.ref(
                                        'department_groups.group_finance_mgr') in record.env.user.groups_id:
                                    allowed_groups.append(
                                        self.env.ref('department_groups.group_finance_mgr').id)
                                record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                                assigned_user = record.env.user.id
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                            else:
                                raise ValidationError(_("You don't have access to self assign this exit request"))
                        else:
                            if (self.env.user in record.assigned_user_ids and
                                    self.env.user.has_group('department_groups.group_fleet_manager')):
                                record.write(
                                    {'is_self_assign': True, 'transport_deduction_user_id': record.env.user.id})
                                assigned_user = record.env.user.id
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                            else:
                                raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'hr_verification':
                        if self.env.user in record.assigned_user_ids and self.env.user.has_group(
                                'department_groups.group_hr_mgr'):
                            record.write({'is_self_assign': True, 'eos_hr_verification_user_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'full_and_final_settlement':
                        if record.type == 'indirect':
                            if (self.env.user in record.assigned_user_ids and
                                    self.env.user.has_group('department_groups.group_finance_mgr')):
                                record.write(
                                    {'is_self_assign': True, 'eos_final_settlement_user_id': record.env.user.id})
                                assigned_user = record.env.user.id
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                            else:
                                raise ValidationError(_("You don't have access to self assign this exit request"))
                        else:
                            if (self.env.user in record.assigned_user_ids and
                                    self.env.user.has_group('department_groups.group_finance_asst_mgr_accountant')):
                                record.write(
                                    {'is_self_assign': True, 'eos_final_settlement_user_id': record.env.user.id})
                                assigned_user = record.env.user.id
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                            else:
                                raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'payroll_gratuity':
                        if (self.env.user in record.assigned_user_ids and
                                self.user_has_groups('department_groups.group_finance_accountant_payroll,'
                                                     'department_groups.group_finance_payroll_asst')):
                            record.write({'is_self_assign': True, 'eos_payroll_gratuity_user_id': record.env.user.id})
                            record.assigned_group_ids = False
                            allowed_groups = []
                            if self.env.user.has_group('department_groups.group_finance_accountant_payroll'):
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_finance_accountant_payroll').id)
                            if self.env.user.has_group('department_groups.group_finance_payroll_asst'):
                                allowed_groups.append(self.env.ref('department_groups.group_finance_payroll_asst').id)
                            record.assigned_group_ids = [
                                Command.link(group) for group in allowed_groups]
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'accounts_approval':
                        if (self.env.user in record.assigned_user_ids and
                                self.env.user.has_group('department_groups.group_finance_asst_mgr_accountant')):
                            record.write({'is_self_assign': True, 'eos_accounts_approval_user_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'hr_approval':
                        if self.env.user in record.assigned_user_ids and self.env.user.has_group(
                                'department_groups.group_hr_mgr'):
                            record.write({'is_self_assign': True, 'eos_hr_approval_user_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'finance_manager_approval':
                        if self.env.user in record.assigned_user_ids and self.env.user.has_group(
                                'department_groups.group_finance_mgr'):
                            record.write(
                                {'is_self_assign': True, 'eos_finance_manager_approval_user_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'temp_closed':
                        if self.env.user in record.assigned_user_ids and self.env.user.has_group(
                                'department_groups.group_hr_mgr'):
                            record.write({'is_self_assign': True, 'eos_temp_closed_hr_user_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'md_approval':
                        if record.env.user.has_group('department_groups.group_mngmt_md'):
                            record.write({'is_self_assign': True, 'eos_temp_closed_md_user_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'eos_temp_finance_manager_approval':
                        if self.env.user in record.assigned_user_ids and self.env.user.has_group(
                                'department_groups.group_finance_mgr'):
                            record.write(
                                {'is_self_assign': True, 'eos_temp_closed_finance_manager_user_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'disbursal':
                        if self.env.user in record.assigned_user_ids and self.user_has_groups(
                                'department_groups.group_finance_asst_mgr_accountant,'
                                'department_groups.group_finance_accountant,'
                                'department_groups.group_finance_mgr'):
                            record.write(
                                {'is_self_assign': True, 'eos_temp_closed_accounts_user_id': record.env.user.id})
                            record.assigned_group_ids = False
                            allowed_groups = []
                            if self.env.ref(
                                    'department_groups.group_finance_asst_mgr_accountant') in record.env.user.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_finance_asst_mgr_accountant').id)
                            if self.env.ref(
                                    'department_groups.group_finance_accountant') in record.env.user.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_finance_accountant').id)
                            if self.env.ref(
                                    'department_groups.group_finance_mgr') in record.env.user.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_finance_mgr').id)
                            record.assigned_group_ids = [Command.link(group) for group in allowed_groups]
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.eos_stage_status == 'eos_temp_hr_verification':
                        if self.env.user in record.assigned_user_ids and self.env.user.has_group(
                                'department_groups.group_hr_mgr'):
                            record.write({'is_self_assign': True, 'eos_demise_close_hr_user_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))

                elif record.status == 'visa_cancellation':
                    if record.visa_cancellation_stage_status == 'new':
                        if (self.env.user in record.assigned_user_ids and
                                self.env.user.has_group('department_groups.group_hr_asst')):
                            record.write(
                                {'is_self_assign': True, 'visa_cancellation_hr_assist_user_id': record.env.user.id})
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.visa_cancellation_stage_status == 'visa_cancellation':
                        if record.is_cancellation_document_updated_by_pro is False:
                            if (self.env.user in record.assigned_user_ids and
                                    self.user_has_groups('department_groups.group_pro_mgr,'
                                                         'department_groups.group_pro_asst')):
                                record.write(
                                    {'is_self_assign': True, 'visa_cancellation_pro_user_id': record.env.user.id})
                                record.assigned_group_ids = False
                                groups = []
                                if self.env.user.has_group('department_groups.group_pro_mgr'):
                                    groups.append(self.env.ref('department_groups.group_pro_mgr').id)
                                if self.env.user.has_group('department_groups.group_pro_asst'):
                                    groups.append(self.env.ref('department_groups.group_pro_asst').id)

                                record.assigned_group_ids = [
                                    Command.link(group) for group in groups]
                                assigned_user = record.env.user.id
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                            else:
                                raise ValidationError(_("You don't have access to self assign this exit request"))
                        else:
                            if self.env.user in record.assigned_user_ids and self.env.user.has_group(
                                    'department_groups.group_hr_asst'):
                                record.write(
                                    {'is_self_assign': True,
                                     'visa_cancellation_hr_assist_second_user_id': record.env.user.id})
                                assigned_user = record.env.user.id
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                            else:
                                raise ValidationError(_("You don't have access to self assign this exit request"))
                    elif record.visa_cancellation_stage_status == 'lc_immigration_cancellation':
                        if (self.env.user in record.assigned_user_ids and
                                self.user_has_groups('department_groups.group_pro_mgr,'
                                                     'department_groups.group_pro_asst')):
                            record.write(
                                {'is_self_assign': True, 'immigration_cancellation_pro_user_id': record.env.user.id})
                            record.assigned_group_ids = False
                            groups = []
                            if self.env.user.has_group('department_groups.group_pro_mgr'):
                                groups.append(self.env.ref('department_groups.group_pro_mgr').id)
                            if self.env.user.has_group('department_groups.group_pro_asst'):
                                groups.append(self.env.ref('department_groups.group_pro_asst').id)

                            record.assigned_group_ids = [
                                Command.link(group) for group in groups]
                            assigned_user = record.env.user.id
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                        else:
                            raise ValidationError(_("You don't have access to self assign this exit request"))
                elif record.status == 'insurance_deletion':
                    if self.env.user in record.assigned_user_ids and self.env.user.has_group(
                            'department_groups.group_hr_asst'):
                        record.write(
                            {'is_self_assign': True,
                             'insurance_deletion_hr_assist_user_id': record.env.user.id})
                        assigned_user = record.env.user.id
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                    else:
                        raise ValidationError(_("You don't have access to self assign this exit request"))
                elif record.status == 'air_ticket':
                    if self.env.user in record.assigned_user_ids and self.env.user.has_group(
                            'department_groups.group_hr_asst'):
                        record.write(
                            {'is_self_assign': True,
                             'air_ticket_hr_assist_user_id': record.env.user.id})
                        assigned_user = record.env.user.id
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(assigned_user)]
                    else:
                        raise ValidationError(_("You don't have access to self assign this exit request"))

    def action_approve(self):
        for record in self:
            current_user = self.env.user
            if record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'refuse_to_travel',
                    'medically_unfit',
                    'demise', 'retirement'):
                if record.status == 'new':
                    if record.new_stage_status == 'department_head_approval':
                        if self.env.user != record.resignation_approval_hod_id:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                        if not record.actual_last_working_date:
                            raise ValidationError(_("Please set actual last working date."))
                        if record.exit_reason_type in ('resignation', 'contract_expiry', 'retirement'):
                            record.new_stage_status = 'hr_approval'
                            record.is_self_assign = False
                            record.assigned_group_ids = False
                            hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                            hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                            record.assigned_group_ids = [Command.link(hr_manager_group.id)]
                            email_template = self.env.ref('employee_exit_management.email_template_hr_manager_approval',
                                                          raise_if_not_found=False)
                            hr_manager_emails = [manager.partner_id.email for manager in hr_manager_users]
                            if hr_manager_emails and record.resignation_approval_hod_id.partner_id.email:
                                email_values = {'email_to': ','.join(hr_manager_emails),
                                                'email_from': record.resignation_approval_hod_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                        elif record.exit_reason_type == 'termination':
                            if record.type == 'indirect':
                                record.new_stage_status = 'md_approval'
                                record.is_self_assign = False
                                md_group = self.env.ref('department_groups.group_mngmt_md')
                                md_users = self.env['res.users'].search([('groups_id', 'in', md_group.id)])
                                record.assigned_group_ids = False
                                record.assigned_group_ids = [Command.link(md_group.id)]
                                email_template = self.env.ref('employee_exit_management.email_template_md_approval',
                                                              raise_if_not_found=False)
                                md_emails = [manager.partner_id.email for manager in md_users]
                                if md_emails and record.resignation_approval_hr_id.partner_id.email:
                                    email_values = {'email_to': ','.join(md_emails),
                                                    'email_from': record.resignation_approval_hr_id.partner_id.email}
                                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(user.id) for user in md_users]
                            else:
                                record.new_stage_status = 'hr_approval'
                                record.is_self_assign = False
                                record.assigned_group_ids = False
                                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                                hr_manager_users = self.env['res.users'].search(
                                    [('groups_id', 'in', hr_manager_group.id)])
                                record.assigned_group_ids = [Command.link(hr_manager_group.id)]
                                email_template = self.env.ref(
                                    'employee_exit_management.email_template_hr_manager_approval',
                                    raise_if_not_found=False)
                                hr_manager_emails = [manager.partner_id.email for manager in hr_manager_users]
                                if hr_manager_emails and record.resignation_approval_hod_id.partner_id.email:
                                    email_values = {'email_to': ','.join(hr_manager_emails),
                                                    'email_from': record.resignation_approval_hod_id.partner_id.email}
                                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                    elif record.new_stage_status == 'hr_approval':
                        if self.env.user != record.resignation_approval_hr_id:
                            raise AccessError(_("You don't have access to approve this exit request"))
                        record.is_self_assign = False
                        if record.exit_reason_type in ('resignation', 'retirement'):
                            if record.type == 'indirect':
                                record.new_stage_status = 'md_approval'
                                md_group = self.env.ref('department_groups.group_mngmt_md')
                                md_users = self.env['res.users'].search([('groups_id', 'in', md_group.id)])
                                record.assigned_group_ids = False
                                record.assigned_group_ids = [Command.link(md_group.id)]
                                email_template = self.env.ref('employee_exit_management.email_template_md_approval',
                                                              raise_if_not_found=False)
                                md_emails = [manager.partner_id.email for manager in md_users]
                                if md_emails and record.resignation_approval_hr_id.partner_id.email:
                                    email_values = {'email_to': ','.join(md_emails),
                                                    'email_from': record.resignation_approval_hr_id.partner_id.email}
                                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(user.id) for user in md_users]
                            else:
                                record.status = 'approved'
                                email_template = self.env.ref(
                                    'employee_exit_management.email_template_employee_exit_approved',
                                    raise_if_not_found=False)
                                email_to = []
                                for employee in record.employee_id.department_id.department_head_ids:
                                    if employee.work_email:
                                        email_to.append(employee.work_email)
                                if email_to and record.resignation_approval_hr_id.partner_id.email:
                                    email_values = {'email_to': ','.join(email_to),
                                                    'email_from': record.resignation_approval_hr_id.partner_id.email}
                                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        if record.exit_reason_type == 'contract_expiry':
                            record.new_stage_status = 'md_approval'
                            md_group = self.env.ref('department_groups.group_mngmt_md')
                            md_users = self.env['res.users'].search([('groups_id', 'in', md_group.id)])
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [Command.link(md_group.id)]
                            email_template = self.env.ref('employee_exit_management.email_template_md_approval',
                                                          raise_if_not_found=False)
                            md_emails = [manager.partner_id.email for manager in md_users]
                            if md_emails and record.resignation_approval_hr_id.partner_id.email:
                                email_values = {'email_to': ','.join(md_emails),
                                                'email_from': record.resignation_approval_hr_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in md_users]
                        if record.exit_reason_type in ('termination', 'refuse_to_work', 'medically_unfit'):
                            record.status = 'approved'
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_employee_exit_approved',
                                raise_if_not_found=False)
                            email_to = []
                            for employee in record.employee_id.department_id.department_head_ids:
                                if employee.work_email:
                                    email_to.append(employee.work_email)
                            if email_to and record.resignation_approval_hr_id.partner_id.email:
                                email_values = {'email_to': ','.join(email_to),
                                                'email_from': record.resignation_approval_hr_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        if record.exit_reason_type == 'refuse_to_travel':
                            record.new_stage_status = 'accounts_approval'
                            asst_mgr_group = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                            finance_act_group = self.env.ref('department_groups.group_finance_accountant')
                            finance_group = self.env.ref('department_groups.group_finance_mgr')
                            account_users = self.env['res.users'].search(
                                [('groups_id', 'in', [asst_mgr_group.id, finance_act_group.id, finance_group.id])])
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [Command.link(asst_mgr_group.id),
                                                         Command.link(finance_act_group.id),
                                                         Command.link(finance_group.id)]
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_new_accounts_approval',
                                raise_if_not_found=False)
                            email_to = [user.partner_id.email for user in account_users]
                            if email_to and record.resignation_approval_hr_id.partner_id.email:
                                email_values = {'email_to': ','.join(email_to),
                                                'email_from': record.resignation_approval_hr_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in account_users]
                        if record.exit_reason_type == 'demise':
                            if not record.actual_last_working_date:
                                raise ValidationError(_("Please set actual last working date."))
                            record.is_actual_last_working_date_updated = True
                            record.status = 'approved'
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_employee_exit_approved',
                                raise_if_not_found=False)
                            email_to = []
                            for employee in record.employee_id.department_id.department_head_ids:
                                if employee.work_email:
                                    email_to.append(employee.work_email)
                            if email_to and record.resignation_approval_hr_id.partner_id.email:
                                email_values = {'email_to': ','.join(email_to),
                                                'email_from': record.resignation_approval_hr_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.status = 'eos'
                            record.eos_stage_status = 'asset_collection'
                            asset_issued = self.env['asset.issue'].search([('employee_id', '=', record.employee_id.id),
                                                                           ('is_asset_recollected', '=', False)])
                            asset_collection = []
                            for asset in asset_issued:
                                asset_collection.append((0, 0, {
                                    'product_id': asset.product_id.id,
                                    'quantity': asset.quantity,
                                    'issued_date': asset.date,
                                    'from_location_id': asset.from_location_id.id,
                                    'asset_issue_id': asset.id,
                                }))
                            if asset_collection:
                                record.asset_collection_ids = asset_collection
                            if record.type == 'indirect':
                                asst_mgr_group = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                                finance_act_group = self.env.ref('department_groups.group_finance_accountant')
                                payroll_act_group = self.env.ref('department_groups.group_finance_mgr')
                                account_users = self.env['res.users'].search(
                                    [('groups_id', 'in',
                                      [asst_mgr_group.id, finance_act_group.id, payroll_act_group.id])])
                                email_template = self.env.ref(
                                    'employee_exit_management.email_template_asset_collection',
                                    raise_if_not_found=False)
                                email_to = [user.partner_id.email for user in account_users]
                                email_values = {'email_to': ','.join(email_to)}
                                if email_template and email_to:
                                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                                record.assigned_group_ids = False
                                record.assigned_group_ids = [Command.link(asst_mgr_group.id),
                                                             Command.link(finance_act_group.id),
                                                             Command.link(payroll_act_group.id)]
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(user.id) for user in account_users]
                            else:
                                store_keeper_group = self.env.ref('department_groups.group_finance_store_keeper')
                                users = self.env['res.users'].search(
                                    [('groups_id', 'in', [store_keeper_group.id])])
                                email_template = self.env.ref(
                                    'employee_exit_management.email_template_asset_collection',
                                    raise_if_not_found=False)
                                email_to = [user.partner_id.email for user in users]
                                email_values = {'email_to': ','.join(email_to)}
                                if email_template and email_to:
                                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                                record.assigned_group_ids = False
                                record.assigned_group_ids = [Command.link(store_keeper_group.id)]
                                record.assigned_user_ids = False
                                record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                    elif record.new_stage_status == 'md_approval':
                        if self.env.user == record.resignation_approval_md_id:
                            record.status = 'approved'
                            record.is_self_assign = False
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_employee_exit_approved',
                                raise_if_not_found=False)
                            email_to = []
                            for hod in record.dept_head_user_ids:
                                if hod.partner_id.email not in email_to:
                                    email_to.append(hod.partner_id.email)
                            hr_users = self.env['res.users'].search([('groups_id', 'in', [self.env.ref('department_groups.group_hr_mgr').id])])
                            for hr in hr_users:
                                if hr.partner_id.email not in email_to:
                                    email_to.append(hr.partner_id.email)
                            if email_to and record.resignation_approval_md_id.partner_id.email:
                                email_values = {'email_to': ','.join(email_to),
                                                'email_from': record.resignation_approval_md_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        else:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                    elif record.new_stage_status == 'operation_supervisor_approval':
                        if self.env.user != record.resignation_approval_operations_supervisor_id:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                        record.is_self_assign = False
                        if record.exit_reason_type in (
                                'resignation', 'termination', 'retirement') and record.type == 'direct':
                            record.new_stage_status = 'welfare_officer_approval'
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_welfare_officer_approval',
                                raise_if_not_found=False)
                            group = self.env.ref('department_groups.group_hr_welfare_officer')
                            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                            emails = [user.partner_id.email for user in users]
                            if emails and record.resignation_approval_operations_supervisor_id.partner_id.email:
                                email_values = {'email_to': ','.join(emails),
                                                'email_from': record.resignation_approval_operations_supervisor_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [Command.link(group.id)]
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                        if record.exit_reason_type == 'contract_expiry' and record.type == 'direct':
                            record.new_stage_status = 'assistant_operation_supervisor_approval'
                            assistant_operations_supervisor_users = []
                            for op_super in record.employee_id.department_id.assistant_operations_supervisor_ids:
                                if op_super.user_id:
                                    assistant_operations_supervisor_users.append(Command.link(op_super.user_id.id))
                            if len(assistant_operations_supervisor_users) == 0:
                                raise ValidationError(
                                    _("Please assign assistant operation supervisor and related user to assistant operation supervisor."))
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_assistant_operation_supervisor_approval',
                                raise_if_not_found=False)
                            emails = [op_super_employee.work_email for op_super_employee in
                                      record.employee_id.department_id.assistant_operations_supervisor_ids]
                            if emails and record.resignation_approval_operations_supervisor_id.partner_id.email:
                                email_values = {'email_to': ','.join(emails),
                                                'email_from': record.resignation_approval_operations_supervisor_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_group_ids = False
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = assistant_operations_supervisor_users
                            allowed_groups = []
                            if self.env.ref(
                                    'department_groups.group_construction_asst_operations_super') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_construction_asst_operations_super').id)
                            if self.env.ref(
                                    'department_groups.group_staff_asst_operations_super') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_staff_asst_operations_super').id)
                            if self.env.ref(
                                    'department_groups.group_soft_ser_asst_operations_super') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_soft_ser_asst_operations_super').id)
                            record.sudo().assigned_group_ids = [Command.link(group) for group in allowed_groups]
                    elif record.new_stage_status == 'welfare_officer_approval':
                        if self.env.user != record.resignation_approval_welfare_officer_id:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                        if record.exit_reason_type in (
                                'resignation', 'termination', 'refuse_to_work',
                                'retirement') and record.type == 'direct':
                            if record.exit_reason_type in (
                                    'resignation', 'retirement') and not record.exit_interview_status:
                                raise UserError(_("Please select exit interview status"))
                            if record.exit_reason_type == 'refuse_to_work' and not record.willing_to_pay_penalty:
                                raise UserError(_("Please select employee willing to pay penalty"))
                            record.is_self_assign = False
                            record.new_stage_status = 'assistant_operation_supervisor_approval'
                            assistant_operations_supervisor_users = []
                            for op_super in record.employee_id.department_id.assistant_operations_supervisor_ids:
                                if op_super.user_id:
                                    assistant_operations_supervisor_users.append(Command.link(op_super.user_id.id))
                            if len(assistant_operations_supervisor_users) == 0:
                                raise ValidationError(
                                    _("Please assign assistant operation supervisor and related user to assistant operation supervisor."))
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_assistant_operation_supervisor_approval',
                                raise_if_not_found=False)
                            emails = [op_super_employee.work_email for op_super_employee in
                                      record.employee_id.department_id.assistant_operations_supervisor_ids]
                            if emails and record.resignation_approval_welfare_officer_id.partner_id.email:
                                email_values = {'email_to': ','.join(emails),
                                                'email_from': record.resignation_approval_welfare_officer_id.partner_id.email}
                                email_template.send_mail(record.sudo().id, email_values=email_values, force_send=True)
                            record.assigned_group_ids = False
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = assistant_operations_supervisor_users
                            allowed_groups = []
                            if self.env.ref(
                                    'department_groups.group_construction_asst_operations_super') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_construction_asst_operations_super').id)
                            if self.env.ref(
                                    'department_groups.group_staff_asst_operations_super') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_staff_asst_operations_super').id)
                            if self.env.ref(
                                    'department_groups.group_soft_ser_asst_operations_super') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(
                                    self.env.ref('department_groups.group_soft_ser_asst_operations_super').id)
                            record.sudo().assigned_group_ids = [Command.link(group) for group in allowed_groups]
                        if record.exit_reason_type == 'contract_expiry' and record.type == 'direct':
                            record.is_self_assign = False
                            record.new_stage_status = 'department_head_approval'
                            users = []
                            for hod in record.employee_id.department_id.department_head_ids:
                                if hod.user_id:
                                    users.append(Command.link(hod.user_id.id))
                            if len(users) == 0:
                                raise ValidationError(
                                    _("Please assign department head and related user to department head"))
                            email_template = self.env.ref('employee_exit_management.email_template_hod_approval',
                                                          raise_if_not_found=False)
                            hod_emails = [hod.work_email for hod in
                                          record.employee_id.department_id.department_head_ids]
                            email_values = {'email_to': ','.join(hod_emails),
                                            'email_from': self.env.user.partner_id.email}
                            if email_template and hod_emails and self.env.user.partner_id.email:
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_group_ids = False
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = users
                            allowed_groups = []
                            if self.env.ref(
                                    'department_groups.group_construction_dept_head') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_construction_dept_head').id)
                            if self.env.ref(
                                    'department_groups.group_staff_dept_head') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_staff_dept_head').id)
                            if self.env.ref(
                                    'department_groups.group_soft_ser_dept_head') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_soft_ser_dept_head').id)
                            record.sudo().assigned_group_ids = [Command.link(group) for group in allowed_groups]
                    elif record.new_stage_status == 'assistant_operation_supervisor_approval':
                        if self.env.user != record.resignation_approval_assistant_operation_supervisor_id:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                        record.is_self_assign = False
                        if record.exit_reason_type in (
                                'resignation', 'termination', 'retirement') and record.type == 'direct':
                            record.new_stage_status = 'department_head_approval'
                            users = []
                            for hod in record.employee_id.department_id.department_head_ids:
                                if hod.user_id:
                                    users.append(Command.link(hod.user_id.id))
                            if len(users) == 0:
                                raise ValidationError(
                                    _("Please assign department head and related user to department head"))
                            email_template = self.env.ref('employee_exit_management.email_template_hod_approval',
                                                          raise_if_not_found=False)
                            hod_emails = [hod.work_email for hod in
                                          record.employee_id.department_id.department_head_ids]
                            email_values = {'email_to': ','.join(hod_emails),
                                            'email_from': self.env.user.partner_id.email}
                            if email_template and hod_emails and self.env.user.partner_id.email:
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_group_ids = False
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = users
                            allowed_groups = []
                            if self.env.ref(
                                    'department_groups.group_construction_dept_head') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_construction_dept_head').id)
                            if self.env.ref(
                                    'department_groups.group_staff_dept_head') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_staff_dept_head').id)
                            if self.env.ref(
                                    'department_groups.group_soft_ser_dept_head') in record.sudo().assigned_user_ids.groups_id:
                                allowed_groups.append(self.env.ref('department_groups.group_soft_ser_dept_head').id)
                            record.sudo().assigned_group_ids = [Command.link(group) for group in allowed_groups]
                        if record.exit_reason_type == 'contract_expiry' and record.type == 'direct':
                            record.new_stage_status = 'welfare_officer_approval'
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_welfare_officer_approval',
                                raise_if_not_found=False)
                            group = self.env.ref('department_groups.group_hr_welfare_officer')
                            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                            emails = [user.partner_id.email for user in users]
                            if emails and record.resignation_approval_assistant_operation_supervisor_id.partner_id.email:
                                email_values = {'email_to': ','.join(emails),
                                                'email_from': record.resignation_approval_assistant_operation_supervisor_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [Command.link(group.id)]
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                        if record.exit_reason_type == 'refuse_to_work' and record.type == 'direct':
                            if not record.actual_last_working_date:
                                raise ValidationError(_("Please set actual last working date."))
                            record.new_stage_status = 'ta_approval'
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_talent_acquisition_approval',
                                raise_if_not_found=False)
                            group = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
                            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                            emails = [user.partner_id.email for user in users]
                            if emails and record.resignation_approval_assistant_operation_supervisor_id.partner_id.email:
                                email_values = {'email_to': ','.join(emails),
                                                'email_from': record.resignation_approval_assistant_operation_supervisor_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [Command.link(group.id)]
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                    elif record.new_stage_status == 'ta_approval':
                        if self.env.user != record.exit_approval_talent_acquisition_executive_id:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                        if record.exit_reason_type == 'medically_unfit':
                            record.new_stage_status = 'hr_asst_approval'
                            record.is_self_assign = False
                            record.assigned_group_ids = False
                            group = self.env.ref('department_groups.group_hr_asst')
                            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                            record.assigned_group_ids = [Command.link(group.id)]
                            email_template = self.env.ref('employee_exit_management.email_template_hr_asst_approval',
                                                          raise_if_not_found=False)
                            emails = [manager.partner_id.email for manager in users]
                            if emails and record.exit_approval_talent_acquisition_executive_id.partner_id.email:
                                email_values = {'email_to': ','.join(emails),
                                                'email_from': record.exit_approval_talent_acquisition_executive_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                        else:
                            record.new_stage_status = 'hr_approval'
                            record.is_self_assign = False
                            record.assigned_group_ids = False
                            hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                            hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                            record.assigned_group_ids = [Command.link(hr_manager_group.id)]
                            email_template = self.env.ref('employee_exit_management.email_template_hr_manager_approval',
                                                          raise_if_not_found=False)
                            hr_manager_emails = [manager.partner_id.email for manager in hr_manager_users]
                            if hr_manager_emails and record.exit_approval_talent_acquisition_executive_id.partner_id.email:
                                email_values = {'email_to': ','.join(hr_manager_emails),
                                                'email_from': record.exit_approval_talent_acquisition_executive_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                    elif record.new_stage_status == 'accounts_approval':
                        if self.env.user != record.exit_approval_accounts_team_id:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                        if record.exit_reason_type == 'refuse_to_travel':
                            if not record.exit_reason_id.departure_reason_id:
                                raise UserError(_("Please provide departure reason in exit reason"))
                            penalty_product = int(self.env.ref('employee_exit_management.product_exit_penalty').id)
                            if record.record.penalty_amount > 0:
                                if record.willing_to_pay_penalty == 'yes':
                                    partner_id = record.employee_id.work_contact_id.id
                                else:
                                    if record.employee_id.employee_onboarding_id.agent_id:
                                        agent_user = self.env['res.users'].search(
                                            [('agent_id', '=', record.employee_id.employee_onboarding_id.agent_id.id)])
                                        partner_id = agent_user.partner_id.id
                                    else:
                                        partner_id = record.employee_id.work_contact_id.id
                                invoice = self.env['account.move'].create({
                                    'ref': record.name,
                                    'move_type': 'out_invoice',
                                    'company_id': record.company_id.id,
                                    'invoice_date': fields.Date.today(),
                                    'partner_id': partner_id,
                                    'employee_exit_management_id': record.id
                                })
                                invoice.invoice_line_ids = [(0, 0, {
                                    'product_id': penalty_product,
                                    'price_unit': record.penalty_amount,
                                    'tax_ids': False,
                                    'display_type': 'product',
                                })]
                                record.penalty_move_id = invoice.id
                            record.is_self_assign = False
                            record.status = 'closed'
                            emails = [record.employee_id.work_email, record.create_uid.partner_id.email]
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_exit_closed',
                                raise_if_not_found=False)
                            if emails and record.exit_approval_accounts_team_id.partner_id.email:
                                email_values = {'email_to': ','.join(emails),
                                                'email_from': record.exit_approval_accounts_team_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            departure = self.env['hr.departure.wizard'].create({
                                'departure_date': fields.Date.today(),
                                'employee_id': record.employee_id.id,
                                'departure_reason_id': record.exit_reason_id.departure_reason_id.id,
                            })
                            departure.with_context(toggle_active=True).action_register_departure()
                    elif record.new_stage_status == 'hr_asst_approval':
                        if self.env.user != record.exit_approval_hr_assistant_id:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                        record.is_self_assign = False
                        record.new_stage_status = 'hr_approval'
                        hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                        hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                        record.assigned_group_ids = [Command.link(hr_manager_group.id)]
                        email_template = self.env.ref('employee_exit_management.email_template_hr_manager_approval',
                                                      raise_if_not_found=False)
                        hr_manager_emails = [manager.partner_id.email for manager in hr_manager_users]
                        if hr_manager_emails and record.exit_approval_hr_assistant_id.partner_id.email:
                            email_values = {'email_to': ','.join(hr_manager_emails),
                                            'email_from': record.exit_approval_hr_assistant_id.partner_id.email}
                            email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                elif record.status == 'eos':
                    if record.eos_stage_status == 'hr_verification':
                        if self.env.user == record.eos_hr_verification_user_id:
                            record.eos_stage_status = 'full_and_final_settlement'
                            record.is_self_assign = False
                            group = self.env.ref('department_groups.group_finance_mgr')
                            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_eos_finance_manager_gratuity_notification',
                                raise_if_not_found=False)
                            email_to = []
                            for user in users:
                                email_to.append(user.partner_id.email)
                            if email_to and record.eos_hr_verification_user_id.partner_id.email:
                                email_values = {'email_to': ','.join(email_to),
                                                'email_from': record.eos_hr_verification_user_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [
                                Command.link(self.env.ref('department_groups.group_finance_mgr').id)]
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                        else:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                    elif record.eos_stage_status == 'hr_approval':
                        if self.env.user == record.eos_hr_approval_user_id:
                            record.eos_stage_status = 'finance_manager_approval'
                            record.is_self_assign = False
                            group = self.env.ref('department_groups.group_finance_mgr')
                            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_request_for_finance_manager_approval',
                                raise_if_not_found=False)
                            email_to = []
                            for user in users:
                                email_to.append(user.partner_id.email)
                            if email_to and record.eos_hr_approval_user_id.partner_id.email:
                                email_values = {'email_to': ','.join(email_to),
                                                'email_from': record.eos_hr_approval_user_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)

                            record.assigned_group_ids = False
                            record.assigned_group_ids = [
                                Command.link(self.env.ref('department_groups.group_finance_mgr').id)]
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                        else:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                    elif record.eos_stage_status == 'finance_manager_approval':
                        if self.env.user == record.eos_finance_manager_approval_user_id:
                            record.eos_stage_status = 'full_and_final_settlement'
                            record.is_self_assign = False
                            group = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_request_for_accounts_assistant_manager_approval',
                                raise_if_not_found=False)
                            email_to = []
                            for user in users:
                                email_to.append(user.partner_id.email)
                            if email_to and record.eos_finance_manager_approval_user_id.partner_id.email:
                                email_values = {'email_to': ','.join(email_to),
                                                'email_from': record.eos_finance_manager_approval_user_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [
                                Command.link(self.env.ref('department_groups.group_finance_asst_mgr_accountant').id)]
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                        else:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                    elif record.eos_stage_status == 'md_approval':
                        if self.env.user == record.eos_temp_closed_md_user_id:
                            record.eos_stage_status = 'eos_temp_finance_manager_approval'
                            record.is_self_assign = False
                            group = self.env.ref('department_groups.group_finance_mgr')
                            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_request_for_finance_manager_approval',
                                raise_if_not_found=False)
                            email_to = []
                            for user in users:
                                email_to.append(user.partner_id.email)
                            if email_to and record.eos_temp_closed_md_user_id.partner_id.email:
                                email_values = {'email_to': ','.join(email_to),
                                                'email_from': record.eos_temp_closed_md_user_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)

                            record.assigned_group_ids = False
                            record.assigned_group_ids = [
                                Command.link(self.env.ref('department_groups.group_finance_mgr').id)]
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                        else:
                            raise ValidationError(_("You don't have access to approve this exit request"))
                    elif record.eos_stage_status == 'eos_temp_finance_manager_approval':
                        if self.env.user == record.eos_temp_closed_finance_manager_user_id:
                            record.eos_stage_status = 'disbursal'
                            record.is_self_assign = False
                            record.is_fm_approved_eos_temp_close = True
                            asst_mgr_group = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                            finance_act_group = self.env.ref('department_groups.group_finance_accountant')
                            finance_group = self.env.ref('department_groups.group_finance_mgr')
                            users = self.env['res.users'].search(
                                [('groups_id', 'in', [asst_mgr_group.id, finance_act_group.id, finance_group.id])])

                            email_template = self.env.ref(
                                'employee_exit_management.email_template_eos_temp_close_disbursal',
                                raise_if_not_found=False)
                            email_to = []
                            for user in users:
                                email_to.append(user.partner_id.email)
                            if email_to and record.eos_temp_closed_finance_manager_user_id.partner_id.email:
                                email_values = {'email_to': ','.join(email_to),
                                                'email_from': record.eos_temp_closed_finance_manager_user_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)

                            record.assigned_group_ids = False
                            record.assigned_group_ids = [Command.link(asst_mgr_group.id),
                                                         Command.link(finance_act_group.id),
                                                         Command.link(finance_group.id)]
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                        else:
                            raise ValidationError(_("You don't have access to approve this exit request"))
            if current_user not in record.sudo().assigned_user_ids:
                tree_view = {
                    'name': _('Employee Exit Management'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'view_id': False,
                    'res_model': 'employee.exit.management',
                    'type': 'ir.actions.act_window',
                    'target': 'main',
                }
                return tree_view

    def action_reject(self):
        self.ensure_one()
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_asset_collection_submit(self):
        for record in self:
            if record.status == 'eos' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                current_user = self.env.user
                if record.eos_stage_status == 'asset_collection':
                    if self.env.user == record.asset_collection_user_id:
                        if any(line.recollection_status == 'no' or not line.recollection_status for line in
                               record.asset_collection_ids):
                            raise UserError(_("Please select recollection status as yes"))
                        if any(line.recollected_date is False for line in
                               record.asset_collection_ids.filtered(lambda l: l.recollection_status == 'yes')):
                            raise UserError(_("Please enter recollected date"))

                        for line in record.asset_collection_ids:
                            line.asset_issue_id.is_asset_recollected = True
                            if line.product_id and line.quantity and line.from_location_id:
                                stock_quant = self.env['stock.quant'].search([
                                    ('product_id', '=', line.product_id.id),
                                    ('location_id', '=', line.from_location_id.id)
                                ], limit=1)
                                if stock_quant:
                                    new_qty_available = stock_quant.quantity + line.quantity
                                    stock_quant.write({'quantity': new_qty_available})
                            if line.scrap_qty:
                                scrap_order = self.env['stock.scrap'].create({
                                    'product_id': line.product_id.id,
                                    'scrap_qty': line.scrap_qty,
                                    'location_id': line.from_location_id.id,
                                    'origin': record.name,
                                })
                                scrap_order.action_validate()
                                line.stock_scrap_id = scrap_order.id
                        record.eos_stage_status = 'transport_deductions'
                        record.is_self_assign = False
                        if record.type == 'indirect':
                            asst_mgr_group = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                            finance_act_group = self.env.ref('department_groups.group_finance_accountant')
                            payroll_act_group = self.env.ref('department_groups.group_finance_mgr')
                            account_users = self.env['res.users'].search(
                                [('groups_id', 'in', [asst_mgr_group.id, finance_act_group.id, payroll_act_group.id])])
                            email_template = self.env.ref('employee_exit_management.email_template_transport_deduction',
                                                          raise_if_not_found=False)
                            email_to = [user.partner_id.email for user in account_users]
                            email_values = {'email_to': ','.join(email_to),
                                            'email_from': record.asset_collection_user_id.partner_id.email}
                            if email_template and email_to and record.asset_collection_user_id.partner_id.email:
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.write(
                                {'assigned_user_ids': False, 'assigned_group_ids': False})
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in account_users]
                            record.sudo().assigned_group_ids = [Command.link(asst_mgr_group.id),
                                                                Command.link(finance_act_group.id),
                                                                Command.link(payroll_act_group.id)]
                        else:
                            group = self.env.ref('department_groups.group_fleet_manager')
                            users = self.env['res.users'].search(
                                [('groups_id', 'in', [group.id])])
                            email_template = self.env.ref('employee_exit_management.email_template_transport_deduction',
                                                          raise_if_not_found=False)
                            email_to = [user.partner_id.email for user in users]
                            email_values = {'email_to': ','.join(email_to),
                                            'email_from': record.asset_collection_user_id.partner_id.email}
                            if email_template and email_to and record.asset_collection_user_id.partner_id.email:
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.write(
                                {'assigned_user_ids': False, 'assigned_group_ids': False})
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                            record.sudo().assigned_group_ids = [Command.link(group.id)]
                    else:
                        raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_transport_deduction_submit(self):
        for record in self:
            if record.status == 'eos' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                current_user = self.env.user
                if record.eos_stage_status == 'transport_deductions':
                    if self.env.user == record.transport_deduction_user_id:
                        company_expense_booking = []
                        partner_expense_booking = []
                        for line in record.transport_deduction_ids.filtered(lambda l: l.transport_paid_by == 'company'):
                            company_expense_booking.append(
                                (0, 0, {
                                    'employee_id': record.employee_id.id,
                                    'employee_name': record.employee_id.name,
                                    'department_id': record.employee_id.department_id.id,
                                    'quantity': 1,
                                    'product_id': line.product_id.id,
                                    'unit_price': line.unit_price,
                                    'tax_ids': line.tax_ids,
                                    'name': line.name,
                                })
                            )
                        if len(company_expense_booking) > 0:
                            expense_booking = self.env['expense.booking'].create({
                                'date': fields.Date.today(),
                                'expense_paid_by': 'company',
                                'expense_details_ids': company_expense_booking,
                                'partner_id': record.company_id.partner_id.id,
                                'company_id': record.company_id.id,
                                'employee_exit_management_id': record.id,
                            })
                            res = expense_booking.action_confirm()
                            record.transport_deduction_expense_booking_id = [(4, expense_booking.id)]
                        for line in record.transport_deduction_ids.filtered(
                                lambda l: l.transport_paid_by == 'employee'):
                            partner_expense_booking.append(
                                (0, 0, {
                                    'employee_id': record.employee_id.id,
                                    'employee_name': record.employee_id.name,
                                    'department_id': record.employee_id.department_id.id,
                                    'quantity': 1,
                                    'product_id': line.product_id.id,
                                    'unit_price': line.unit_price,
                                    'tax_ids': line.tax_ids,
                                    'name': line.name,
                                })
                            )
                        if len(partner_expense_booking) > 0:
                            expense_booking = self.env['expense.booking'].create({
                                'date': fields.Date.today(),
                                'expense_paid_by': 'customer',
                                'expense_details_ids': partner_expense_booking,
                                'partner_id': record.employee_id.work_contact_id.id,
                                'company_id': record.company_id.id,
                                'employee_exit_management_id': record.id,
                            })
                            res = expense_booking.action_confirm()
                            record.transport_deduction_expense_booking_id = [(4, expense_booking.id)]
                        if record.type == 'indirect':
                            record.eos_stage_status = 'hr_verification'
                            record.is_self_assign = False
                            hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                            hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [Command.link(hr_manager_group.id)]
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_eos_hr_manager_approval',
                                raise_if_not_found=False)
                            hr_manager_emails = [manager.partner_id.email for manager in hr_manager_users]
                            if hr_manager_emails and record.transport_deduction_user_id.partner_id.email:
                                email_values = {'email_to': ','.join(hr_manager_emails),
                                                'email_from': record.transport_deduction_user_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                        else:
                            record.eos_stage_status = 'payroll_gratuity'
                            record.is_self_assign = False
                            groups = [self.env.ref('department_groups.group_finance_accountant_payroll').id,
                                      self.env.ref('department_groups.group_finance_payroll_asst').id]
                            users = self.env['res.users'].search([('groups_id', 'in', groups)])
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [Command.link(group) for group in groups]
                            email_template = self.env.ref(
                                'employee_exit_management.email_template_eos_payroll_gratuity',
                                raise_if_not_found=False)
                            to_emails = [manager.partner_id.email for manager in users]
                            if to_emails and record.transport_deduction_user_id.partner_id.email:
                                email_values = {'email_to': ','.join(to_emails),
                                                'email_from': record.transport_deduction_user_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                    else:
                        raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_calculate_gratuity(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'eos' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                if not record.designation_id:
                    raise UserError(_("Please assign designation to employee"))
                if record.designation_id and not record.designation_id.gratuity_configuration_id:
                    raise UserError(_("Please assign gratuity configuration to employee designation"))
                if record.type == 'indirect':
                    if record.eos_stage_status == 'full_and_final_settlement':
                        if self.env.user != record.eos_final_settlement_user_id:
                            raise ValidationError(_("You are not allowed to calculate Gratuity"))
                else:
                    if record.eos_stage_status == 'payroll_gratuity':
                        if self.env.user != record.eos_payroll_gratuity_user_id:
                            raise ValidationError(_("You are not allowed to calculate Gratuity"))
                employee_working_days = (record.actual_last_working_date - record.employee_id.date_of_joining).days
                employee_gratuity_years = employee_working_days / 365
                record.employee_gratuity_years = employee_gratuity_years
                gratuity_split_up, employee_basic_salary, employee_gratuity_amount = record.designation_id.gratuity_configuration_id.calculate_gratuity(
                    record.employee_id, employee_gratuity_years, record.actual_last_working_date)
                record.is_system_calculated_gratuity = True
                record.system_calculated_gratuity = employee_gratuity_amount
                record.gratuity_to_be_disbursed = employee_gratuity_amount
                record.employee_basic_salary = employee_basic_salary
                record.gratuity_split_up = gratuity_split_up
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_submit_payroll_gratuity(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'eos' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                if record.eos_stage_status == 'payroll_gratuity':
                    if self.env.user == record.eos_payroll_gratuity_user_id:
                        if not record.is_system_calculated_gratuity:
                            raise UserError(_("Please calculate gratuity"))
                        if not record.payslip_status:
                            raise UserError(_("Please update payslip status"))
                        record.eos_stage_status = 'accounts_approval'
                        record.is_self_assign = False
                        group = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                        users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                        record.assigned_group_ids = False
                        record.assigned_group_ids = [
                            Command.link(self.env.ref('department_groups.group_finance_asst_mgr_accountant').id)]
                        email_template = self.env.ref(
                            'employee_exit_management.email_template_eos_accounts_approval',
                            raise_if_not_found=False)
                        emails = [user.partner_id.email for user in users]
                        if emails and record.eos_payroll_gratuity_user_id.partner_id.email:
                            email_values = {'email_to': ','.join(emails),
                                            'email_from': record.eos_payroll_gratuity_user_id.partner_id.email}
                            email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                    else:
                        raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_approve_eos_accounts_approval(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'eos' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                if record.eos_stage_status == 'accounts_approval':
                    if self.env.user == record.eos_accounts_approval_user_id:
                        if record.exit_reason_type in ('refuse_to_work', 'medically_unfit'):
                            penalty_product = int(self.env.ref('employee_exit_management.product_exit_penalty').id)
                            if record.penalty_amount > 0:
                                if record.willing_to_pay_penalty == 'yes':
                                    partner_id = record.employee_id.work_contact_id.id
                                else:
                                    if record.employee_id.employee_onboarding_id.agent_id:
                                        agent_user = self.env['res.users'].search(
                                            [('agent_id', '=', record.employee_id.employee_onboarding_id.agent_id.id)])
                                        partner_id = agent_user.partner_id.id
                                    else:
                                        partner_id = record.employee_id.work_contact_id.id
                                invoice = self.env['account.move'].create({
                                    'ref': record.name,
                                    'move_type': 'out_invoice',
                                    'company_id': record.company_id.id,
                                    'invoice_date': fields.Date.today(),
                                    'partner_id': partner_id,
                                    'employee_exit_management_id': record.id
                                })
                                invoice.invoice_line_ids = [(0, 0, {
                                    'product_id': penalty_product,
                                    'price_unit': record.penalty_amount,
                                    'tax_ids': False,
                                    'display_type': 'product',
                                })]
                                record.penalty_move_id = invoice.id
                        company_expense_booking = []
                        partner_expense_booking = []
                        for line in record.additional_deduction_ids.filtered(lambda l: l.expense_paid_by == 'company'):
                            company_expense_booking.append(
                                (0, 0, {
                                    'employee_id': record.employee_id.id,
                                    'employee_name': record.employee_id.name,
                                    'department_id': record.employee_id.department_id.id,
                                    'name': line.name,
                                    'quantity': 1,
                                    'product_id': line.product_id.id,
                                    'unit_price': line.unit_price,
                                    'tax_ids': line.tax_ids,
                                })
                            )
                        if len(company_expense_booking) > 0:
                            expense_booking = self.env['expense.booking'].create({
                                'date': fields.Date.today(),
                                'expense_paid_by': 'company',
                                'expense_details_ids': company_expense_booking,
                                'partner_id': record.company_id.partner_id.id,
                                'company_id': record.company_id.id,
                                'employee_exit_management_id': record.id,
                            })
                            res = expense_booking.action_confirm()
                            record.additional_deduction_expense_booking_id = [(4, expense_booking.id)]
                        for line in record.additional_deduction_ids.filtered(
                                lambda l: l.expense_paid_by == 'employee'):
                            partner_expense_booking.append(
                                (0, 0, {
                                    'employee_id': record.employee_id.id,
                                    'employee_name': record.employee_id.name,
                                    'department_id': record.employee_id.department_id.id,
                                    'name': line.name,
                                    'quantity': 1,
                                    'product_id': line.product_id.id,
                                    'unit_price': line.unit_price,
                                    'tax_ids': line.tax_ids,
                                })
                            )
                        if len(partner_expense_booking) > 0:
                            expense_booking = self.env['expense.booking'].create({
                                'date': fields.Date.today(),
                                'expense_paid_by': 'customer',
                                'expense_details_ids': partner_expense_booking,
                                'partner_id': record.employee_id.work_contact_id.id,
                                'company_id': record.company_id.id,
                                'employee_exit_management_id': record.id,
                            })
                            res = expense_booking.action_confirm()
                            record.additional_deduction_expense_booking_id = [(4, expense_booking.id)]
                        record.eos_stage_status = 'hr_approval'
                        record.is_self_assign = False
                        hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                        hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                        record.assigned_group_ids = False
                        record.assigned_group_ids = [Command.link(hr_manager_group.id)]
                        email_template = self.env.ref('employee_exit_management.email_template_eos_hr_manager_approval',
                                                      raise_if_not_found=False)
                        hr_manager_emails = [manager.partner_id.email for manager in hr_manager_users]
                        if hr_manager_emails and record.eos_accounts_approval_user_id.partner_id.email:
                            email_values = {'email_to': ','.join(hr_manager_emails),
                                            'email_from': record.eos_accounts_approval_user_id.partner_id.email}
                            email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                    else:
                        raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_process_final_settlement(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'eos' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                if record.eos_stage_status == 'full_and_final_settlement':
                    if record.type == 'indirect':
                        if self.env.user == record.eos_final_settlement_user_id:
                            if not record.is_system_calculated_gratuity:
                                raise UserError(_("Please calculate the gratuity before submission."))

                        else:
                            raise ValidationError(_("You don't have access to submit this exit request."))
                        if not record.wps_status and record.exit_reason_type != 'demise':
                            raise UserError(_("Please update WPS status."))
                        record.status = 'visa_cancellation'
                        record.visa_cancellation_stage_status = 'new'
                        record.is_self_assign = False
                        group = self.env.ref('department_groups.group_hr_asst')
                        users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                        record.assigned_group_ids = False
                        record.assigned_group_ids = [Command.link(self.env.ref('department_groups.group_hr_asst').id)]
                        email_template = self.env.ref(
                            'employee_exit_management.email_template_visa_cancellation_hr_asst_approval',
                            raise_if_not_found=False)
                        emails = [user.partner_id.email for user in users]
                        if emails and record.eos_final_settlement_user_id.partner_id.email:
                            email_values = {'email_to': ','.join(emails),
                                            'email_from': record.eos_final_settlement_user_id.partner_id.email}
                            email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                    else:
                        if self.env.user == record.eos_final_settlement_user_id:
                            if not record.wps_status and record.exit_reason_type != 'demise':
                                raise UserError(_("Please update WPS status."))
                            if record.ticket_eligibility == 'yes':
                                record.status = 'air_ticket'
                                record.is_air_ticket_visible = True
                                email_template = self.env.ref(
                                    'employee_exit_management.email_template_issue_air_ticket_hr_asst_approval',
                                    raise_if_not_found=False)
                            else:
                                record.status = 'visa_cancellation'
                                record.visa_cancellation_stage_status = 'new'
                                email_template = self.env.ref(
                                    'employee_exit_management.email_template_visa_cancellation_hr_asst_approval',
                                    raise_if_not_found=False)
                            record.is_self_assign = False
                            record.final_settlement_date = fields.Date.today()
                            group = self.env.ref('department_groups.group_hr_asst')
                            users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                            record.assigned_group_ids = False
                            record.assigned_group_ids = [
                                Command.link(self.env.ref('department_groups.group_hr_asst').id)]

                            emails = [user.partner_id.email for user in users]
                            if emails and record.eos_final_settlement_user_id.partner_id.email:
                                email_values = {'email_to': ','.join(emails),
                                                'email_from': record.eos_final_settlement_user_id.partner_id.email}
                                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                            record.assigned_user_ids = False
                            record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                        else:
                            raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_proceed_to_payment(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'eos' and record.exit_reason_type == 'demise':
                if record.eos_stage_status == 'temp_closed':
                    if self.env.user != record.eos_temp_closed_hr_user_id:
                        raise AccessError(_("You don't have access to proceed this exit request."))
                    record.write({
                        'eos_stage_status': 'md_approval',
                        'is_self_assign': False
                    })
                    group = self.env.ref('department_groups.group_mngmt_md')
                    users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                    record.assigned_group_ids = False
                    record.assigned_group_ids = [Command.link(self.env.ref('department_groups.group_mngmt_md').id)]
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_eos_temp_close_md_approval',
                        raise_if_not_found=False)
                    emails = [user.partner_id.email for user in users]
                    if emails and record.eos_temp_closed_hr_user_id.partner_id.email:
                        email_values = {'email_to': ','.join(emails),
                                        'email_from': record.eos_temp_closed_hr_user_id.partner_id.email}
                        email_template.send_mail(record.id, email_values=email_values, force_send=True)
                    record.assigned_user_ids = False
                    record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_submit_eos_disbursal(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'eos' and record.exit_reason_type == 'demise':
                if record.eos_stage_status == 'disbursal':
                    if self.env.user != record.eos_temp_closed_accounts_user_id:
                        raise AccessError(_("You don't have access to submit this exit request."))
                    if record.disbursal_amount <= 0:
                        raise UserError(_("Please set disbursal amount"))
                    if not record.disbursal_status:
                        raise UserError(_("Please select disbursal status"))
                    if not (
                            record.company_id.amount_disbursal_journal_id or record.company_id.amount_disbursal_credit_ac_id or record.company_id.amount_disbursal_debit_ac_id):
                        raise UserError(_("Please set amount disbursal accounts and journal"))
                    if record.disbursal_amount > 0:
                        move = self.env['account.move'].create({
                            'ref': record.name,
                            'journal_id': record.company_id.amount_disbursal_journal_id.id,
                            'company_id': record.company_id.id,
                            'employee_exit_management_id': record.id,
                            'move_type': 'entry',
                        })
                        move.line_ids = [(0, 0, {
                            'account_id': record.company_id.amount_disbursal_debit_ac_id.id,
                            'debit': record.disbursal_amount,
                            'name': record.name + " - Disbursal",
                            'partner_id': record.employee_id.work_contact_id.id
                        }), (0, 0, {
                            'account_id': record.company_id.amount_disbursal_credit_ac_id.id,
                            'credit': record.disbursal_amount,
                            'name': record.name + " - Disbursal",
                            'partner_id': record.employee_id.work_contact_id.id
                        })]
                        record.disbursal_move_id = move.id
                    record.write({
                        'eos_stage_status': 'eos_temp_hr_verification',
                        'is_self_assign': False
                    })
                    group = self.env.ref('department_groups.group_hr_mgr')
                    users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                    record.assigned_group_ids = False
                    record.assigned_group_ids = [Command.link(self.env.ref('department_groups.group_hr_mgr').id)]
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_eos_temp_close_attach_death_certificate',
                        raise_if_not_found=False)
                    emails = [user.partner_id.email for user in users]
                    if emails and record.eos_temp_closed_accounts_user_id.partner_id.email:
                        email_values = {'email_to': ','.join(emails),
                                        'email_from': record.eos_temp_closed_accounts_user_id.partner_id.email}
                        email_template.send_mail(record.id, email_values=email_values, force_send=True)
                    record.assigned_user_ids = False
                    record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_close_demise_eos(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'eos' and record.exit_reason_type == 'demise':
                if record.eos_stage_status == 'eos_temp_hr_verification':
                    if self.env.user != record.eos_demise_close_hr_user_id:
                        raise AccessError(_("You don't have access to submit this exit request."))
                    if not record.death_certificate:
                        raise UserError(_("Please attach death certificate"))
                    if not record.exit_reason_id.departure_reason_id:
                        raise UserError(_("Please provide departure reason in exit reason"))
                    record.is_self_assign = False
                    record.status = 'closed'
                    emails = [record.employee_id.work_email, record.create_uid.partner_id.email]
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_exit_closed',
                        raise_if_not_found=False)
                    departure = self.env['hr.departure.wizard'].create({
                        'departure_date': fields.Date.today(),
                        'employee_id': record.employee_id.id,
                        'departure_reason_id': record.exit_reason_id.departure_reason_id.id,
                    })
                    departure.with_context(toggle_active=True).action_register_departure()
                    if emails and self.env.user.partner_id.email:
                        email_values = {'email_to': ','.join(emails),
                                        'email_from': self.env.user.partner_id.email}
                        email_template.send_mail(record.id, email_values=email_values, force_send=True)
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_visa_cancellation_date_submit(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'visa_cancellation' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                if record.visa_cancellation_stage_status == 'new':
                    if self.env.user == record.visa_cancellation_hr_assist_user_id:
                        if not record.visa_cancellation_to_be_carried_out_date:
                            raise UserError(_("Please set a date on which visa cancellation is to be carried out"))
                        record.visa_cancellation_stage_status = 'visa_cancellation'
                        record.is_self_assign = False
                        groups = [self.env.ref('department_groups.group_pro_mgr').id,
                                  self.env.ref('department_groups.group_pro_asst').id]
                        users = self.env['res.users'].search([('groups_id', 'in', groups)])
                        emails = [user.partner_id.email for user in users]
                        email_template = self.env.ref(
                            'employee_exit_management.email_template_visa_cancellation_document_updation_pro',
                            raise_if_not_found=False)
                        if emails and record.visa_cancellation_hr_assist_user_id.partner_id.email:
                            email_values = {'email_to': ','.join(emails),
                                            'email_from': record.visa_cancellation_hr_assist_user_id.partner_id.email}
                            email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        record.assigned_group_ids = False
                        record.assigned_group_ids = [Command.link(group) for group in groups]
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                    else:
                        raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_submit_visa_cancellation_document(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'visa_cancellation' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                if record.visa_cancellation_stage_status == 'visa_cancellation':
                    if self.env.user == record.visa_cancellation_pro_user_id:
                        if not record.cancellation_document:
                            raise UserError(_("Please upload visa cancellation document."))
                        record.is_self_assign = False
                        record.is_cancellation_document_updated_by_pro = True
                        group = self.env.ref('department_groups.group_hr_asst')
                        users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                        record.assigned_group_ids = False
                        record.assigned_group_ids = [Command.link(self.env.ref('department_groups.group_hr_asst').id)]
                        email_template = self.env.ref(
                            'employee_exit_management.email_template_update_visa_cancellation_document_hr_asst',
                            raise_if_not_found=False)
                        emails = [user.partner_id.email for user in users]
                        if emails and record.visa_cancellation_pro_user_id.partner_id.email:
                            email_values = {'email_to': ','.join(emails),
                                            'email_from': record.visa_cancellation_pro_user_id.partner_id.email}
                            email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                    else:
                        raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_submit_signed_visa_cancellation_document(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'visa_cancellation' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                if record.visa_cancellation_stage_status == 'visa_cancellation':
                    if self.env.user == record.visa_cancellation_hr_assist_second_user_id:
                        if not record.cancellation_document_signed_by_employee:
                            raise UserError(_("Please upload cancellation document signed by employee."))
                        record.is_self_assign = False
                        record.visa_cancellation_stage_status = 'lc_immigration_cancellation'
                        groups = [self.env.ref('department_groups.group_pro_mgr').id,
                                  self.env.ref('department_groups.group_pro_asst').id]
                        users = self.env['res.users'].search([('groups_id', 'in', groups)])
                        emails = [user.partner_id.email for user in users]
                        email_template = self.env.ref(
                            'employee_exit_management.email_template_labour_immigration_pro',
                            raise_if_not_found=False)
                        if emails and record.visa_cancellation_hr_assist_second_user_id.partner_id.email:
                            email_values = {'email_to': ','.join(emails),
                                            'email_from': record.visa_cancellation_hr_assist_second_user_id.partner_id.email}
                            email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        record.assigned_group_ids = False
                        record.assigned_group_ids = [Command.link(group) for group in groups]
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                    else:
                        raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_submit_immigration_cancellation(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'visa_cancellation' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                    'retirement'):
                if record.visa_cancellation_stage_status == 'lc_immigration_cancellation':
                    if self.env.user == record.immigration_cancellation_pro_user_id:
                        grace_period = int(self.env['ir.config_parameter'].sudo().get_param(
                            'employee_exit_management.insurance_deletion_grace_period'))
                        if not grace_period:
                            raise UserError(_("Please set insurance deletion grace period."))
                        if not record.immigration_cancellation_document:
                            raise UserError(_("Please upload immigration cancellation document."))
                        if not (
                                record.grace_period_expiry_date or record.visa_cancellation_date or record.visa_cancellation_status):
                            raise UserError(_("Please update visa cancellation details."))
                        expense_category = self.env['product.product'].search(
                            [('is_visa_cancellation', '=', True), ('can_be_expensed', '=', True)], limit=1)
                        if record.visa_cancellation_amount <= 0:
                            record.visa_cancellation_amount = expense_category.standard_price
                        if record.visa_cancellation_amount > 0:
                            employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)],
                                                                             limit=1)
                            if not employee:
                                raise UserError(_("Please create %s as an employee", self.env.user.name))
                            expense = self.env['hr.expense'].create({
                                'employee_id': employee.id,
                                'date': fields.Date.today(),
                                'name': record.name + " - Visa Cancellation",
                                'payment_mode': 'own_account',
                                'department_id': employee.department_id.id,
                                'total_amount': record.visa_cancellation_amount,
                                'product_id': expense_category.id,
                                'on_behalf_of_employee_id': record.employee_id.id,
                                'employee_exit_management_id': record.id,
                                'quantity': 1
                            })
                            if record.visa_cancellation_amount > expense_category.maximum_allowed_amount:
                                expense.action_submit_expenses()
                                expense.sheet_id.sudo().action_submit_sheet()
                            else:

                                expense.action_submit_expenses()
                                expense.sheet_id.sudo().action_submit_sheet()
                                expense.sheet_id.sudo().action_approve_expense_sheets()
                                expense.sheet_id.sudo().action_approve_finance_manager()
                            record.write(
                                {
                                    'visa_expense_sheet_id': expense.sheet_id.id,
                                    'visa_expense_id': expense.id,

                                }
                            )
                        email_to_users = self.env['res.users'].search([
                            ('groups_id', 'in', [self.env.ref('department_groups.group_hr_asst').id,
                                                 self.env.ref('department_groups.group_hr_mgr').id])])
                        emails = [user.partner_id.email for user in email_to_users]
                        email_template = self.env.ref(
                            'employee_exit_management.email_template_visa_cancellation_completed',
                            raise_if_not_found=False)
                        if emails and record.immigration_cancellation_pro_user_id.partner_id.email:
                            email_values = {'email_to': ','.join(emails),
                                            'email_from': record.immigration_cancellation_pro_user_id.partner_id.email}
                            email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        record.insurance_deletion_date = fields.Date.today() + datetime.timedelta(days=grace_period)
                        record.status = 'insurance_deletion'
                        record.is_self_assign = False
                        group = self.env.ref('department_groups.group_hr_asst')
                        users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                        record.assigned_group_ids = False
                        record.assigned_group_ids = [Command.link(self.env.ref('department_groups.group_hr_asst').id)]
                        email_template = self.env.ref(
                            'employee_exit_management.email_template_insurance_deletion_notification',
                            raise_if_not_found=False)
                        emails = [user.partner_id.email for user in users]
                        if emails and record.immigration_cancellation_pro_user_id.partner_id.email:
                            email_values = {'email_to': ','.join(emails),
                                            'email_from': record.immigration_cancellation_pro_user_id.partner_id.email}
                            email_template.send_mail(record.id, email_values=email_values, force_send=True)
                        if record.exit_reason_type == 'demise':
                            record.camp_exit_status = 'live_out'
                            record.employee_id.accommodation_status = 'live_out'
                            groups = [self.env.ref('department_groups.group_hr_mgr').id,
                                      self.env.ref('department_groups.group_hr_asst').id]
                            res_users = self.env['res.users'].search([('groups_id', 'in', groups)])
                            template_id = self.env.ref(
                                'employee_exit_management.email_template_camp_exit_status_updated',
                                raise_if_not_found=False)
                            emails = [user.partner_id.email for user in res_users]
                            if emails and self.env.user.partner_id.email:
                                email_values = {'email_to': ','.join(emails),
                                                'email_from': self.env.user.partner_id.email}
                                template_id.send_mail(record.id, email_values=email_values, force_send=True)
                            record.camp_supervisor_user_id = self.env.user.id
                        record.assigned_user_ids = False
                        record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]

                    else:
                        raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_submit_air_ticket(self):
        for record in self:
            current_user = self.env.user
            if record.status == 'air_ticket' and record.exit_reason_type in (
                    'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'retirement'):
                if not (record.flight_number or record.ticket_copy or record.ticket_date) or record.ticket_amount <= 0:
                    raise UserError(_("Please update air ticket details"))
                if self.env.user == record.air_ticket_hr_assist_user_id:
                    record.is_self_assign = False
                    record.status = 'visa_cancellation'
                    record.visa_cancellation_stage_status = 'new'
                    if not record.employee_id.camp_manager_name_id or not record.employee_id.camp_manager_name_id.user_id:
                        raise UserError(_("Please set camp manager and related user to camp manager in employee"))
                    res_users = self.env['res.users'].search(
                        [('id', '=', record.employee_id.camp_manager_name_id.user_id.id)])
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_visa_cancellation_hr_asst_approval',
                        raise_if_not_found=False)
                    emails = [user.partner_id.email for user in res_users]
                    if emails and record.air_ticket_hr_assist_user_id.partner_id.email:
                        email_values = {'email_to': ','.join(emails),
                                        'email_from': record.air_ticket_hr_assist_user_id.partner_id.email}
                        email_template.send_mail(record.id, email_values=email_values, force_send=True)
                    camp_email_template = self.env.ref(
                        'employee_exit_management.email_template_camp_supervisor_update',
                        raise_if_not_found=False)
                    attachment_ids = []
                    if record.ticket_copy:
                        attach_data = {
                            'name': record.ticket_copy_name,
                            'datas': record.ticket_copy,
                            'type': 'binary',
                            'mimetype': 'application/x-pdf',
                            'public': True,
                        }
                        attach_id = self.env['ir.attachment'].create(attach_data)
                        attachment_ids.append(attach_id.id)
                    if attachment_ids:
                        camp_email_template.write({'attachment_ids': [(6, 0, attachment_ids)]})

                    group = self.env.ref('department_groups.group_hr_asst')
                    users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                    to_emails = [user.partner_id.email for user in users]
                    record.assigned_group_ids = False
                    record.assigned_group_ids = [Command.link(self.env.ref('department_groups.group_hr_asst').id)]
                    if to_emails and record.air_ticket_hr_assist_user_id.partner_id.email:
                        email_values = {'email_to': ','.join(emails),
                                        'email_from': record.air_ticket_hr_assist_user_id.partner_id.email}
                        camp_email_template.send_mail(record.id, email_values=email_values, force_send=True)
                    camp_email_template.attachment_ids = False
                    record.assigned_user_ids = False
                    record.sudo().assigned_user_ids = [Command.link(user.id) for user in users]
                else:
                    raise ValidationError(_("You don't have access to submit this exit request"))
                if current_user not in record.sudo().assigned_user_ids:
                    tree_view = {
                        'name': _('Employee Exit Management'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'view_id': False,
                        'res_model': 'employee.exit.management',
                        'type': 'ir.actions.act_window',
                        'target': 'main',
                    }
                    return tree_view

    def action_update_camp_exit_status(self):
        self.ensure_one()
        return {
            'name': 'Update Camp Exit Status',
            'type': 'ir.actions.act_window',
            'res_model': 'update.camp.exit.status',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def _initiate_end_of_service(self):
        for record in self.filtered(
                lambda l: l.actual_last_working_date <= fields.Date.today() and l.status == 'approved'
                          and l.exit_reason_type in (
                                  'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit',
                                  'retirement')):
            record.status = 'eos'
            record.eos_stage_status = 'asset_collection'
            asset_issued = self.env['asset.issue'].search([('employee_id', '=', record.employee_id.id),
                                                           ('is_asset_recollected', '=', False)])
            asset_collection = []
            for asset in asset_issued:
                asset_collection.append((0, 0, {
                    'product_id': asset.product_id.id,
                    'quantity': asset.quantity,
                    'issued_date': asset.date,
                    'from_location_id': asset.from_location_id.id,
                    'asset_issue_id': asset.id,
                }))
            if asset_collection:
                record.asset_collection_ids = asset_collection
            if record.type == 'indirect':
                asst_mgr_group = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                finance_act_group = self.env.ref('department_groups.group_finance_accountant')
                payroll_act_group = self.env.ref('department_groups.group_finance_mgr')
                account_users = self.env['res.users'].search(
                    [('groups_id', 'in', [asst_mgr_group.id, finance_act_group.id, payroll_act_group.id])])
                email_template = self.env.ref('employee_exit_management.email_template_asset_collection',
                                              raise_if_not_found=False)
                email_to = [user.partner_id.email for user in account_users]
                email_values = {'email_to': ','.join(email_to)}
                if email_template and email_to:
                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.assigned_user_ids = False
                record.assigned_group_ids = False
                record.assigned_user_ids = [Command.link(user.id) for user in account_users]
                record.assigned_group_ids = [Command.link(asst_mgr_group.id), Command.link(finance_act_group.id),
                                             Command.link(payroll_act_group.id)]
            else:
                if (record.exit_reason_type in ('resignation', 'contract_expiry', 'termination',
                                                'refuse_to_work', 'medically_unfit', 'retirement')
                        and record.type == 'direct' and record.ticket_eligibility == 'yes'):
                    ticket_type = self.env['airline.ticket.type'].search([('is_exit', '=', True)], limit=1)
                    airline_ticket = self.env['airline.ticket'].create({
                        'employee_exit_management_id': record.id,
                    })
                    airline_ticket.ticket_details_ids = [(0, 0, {
                        'employee_id': record.employee_id.id,
                        'employee_name': record.employee_name,
                        'department_id': record.employee_id.department_id.id,
                        'trip_type': 'one_way',
                        'ticket_type_id': ticket_type.id,
                        'airline_ticket_id': airline_ticket.id,
                    })]
                    record.airline_ticket_id = airline_ticket.id
                    hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                    hr_manager = self.env['res.users'].search(
                        [('groups_id', 'in', [hr_manager_group.id])])
                    email_template = self.env.ref('employee_exit_management.email_template_air_ticket_request_created',
                                                  raise_if_not_found=False)
                    email_to = [user.partner_id.email for user in hr_manager]
                    email_values = {'email_to': ','.join(email_to)}
                    if email_template and email_to:
                        email_template.send_mail(record.id, email_values=email_values, force_send=True)

                store_keeper_group = self.env.ref('department_groups.group_finance_store_keeper')
                users = self.env['res.users'].search(
                    [('groups_id', 'in', [store_keeper_group.id])])
                email_template = self.env.ref('employee_exit_management.email_template_asset_collection',
                                              raise_if_not_found=False)
                email_to = [user.partner_id.email for user in users]
                email_values = {'email_to': ','.join(email_to)}
                if email_template and email_to:
                    email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.assigned_user_ids = False
                record.assigned_group_ids = False
                record.assigned_user_ids = [Command.link(user.id) for user in users]
                record.assigned_group_ids = [Command.link(store_keeper_group.id)]

    def _initiate_end_of_service_cron(self):
        exit_records = self.search([('actual_last_working_date', '<=', fields.Date.today()),
                                    ('status', '=', 'approved'), ('exit_reason_id.type', 'in', (
                'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'retirement'))],
                                   limit=100)
        try:  # try initiate EOS in batch
            with self.env.cr.savepoint():
                exit_records._initiate_end_of_service()
        except UserError:
            for record in exit_records:
                try:
                    with self.env.cr.savepoint():
                        record._initiate_end_of_service()
                except UserError as e:
                    msg = _('The exit request could not be moved to EOS for the following reason: %(error_message)s',
                            error_message=e)
                    record.message_post(body=msg, message_type='comment')

        if len(exit_records) == 100:  # assumes there are more whenever search hits limit
            self.env.ref('employee_exit_management.ir_cron_employee_exit_end_of_service')._trigger()

    def _send_reminder_to_update_camp_exit_status_not_eligible(self):
        days = int(self.env['ir.config_parameter'].sudo().get_param(
            'employee_exit_management.ticket_not_eligible_camp_status_update_reminder_days'))
        date = (datetime.datetime.now() - datetime.timedelta(days=days)).date()
        today = fields.Date.today()
        exit_records = self.search([
            ('employee_id.type', '=', 'direct'), ('exit_reason_id.type', 'in', (
                'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'retirement')),
            ('ticket_eligibility', '=', 'no'), ('final_settlement_date', '!=', False),
            ('camp_supervisor_user_id', '=', False), ('final_settlement_date', '<=', date),
            '|', ('supervisor_reminder_send_date', '=', False), ('supervisor_reminder_send_date', '!=', today)],
            limit=100)
        groups = [self.env.ref('department_groups.group_construction_camp_mgr').id,
                  self.env.ref('department_groups.group_staff_camp_mgr').id,
                  self.env.ref('department_groups.group_soft_ser_camp_mgr').id,
                  self.env.ref('department_groups.group_hr_mgr').id,
                  self.env.ref('department_groups.group_hr_welfare_officer').id,
                  self.env.ref('department_groups.group_finance_asst_mgr_accountant').id,
                  self.env.ref('department_groups.group_construction_asst_mgr_acc_transport').id,
                  self.env.ref('department_groups.group_staff_asst_mgr_acc_transport').id,
                  self.env.ref('department_groups.group_soft_ser_asst_mgr_acc_transport').id]

        users = self.env['res.users'].search(
            [('groups_id', 'in', groups)])
        email_template = self.env.ref('employee_exit_management.email_template_reminder_to_update_camp_exit_status',
                                      raise_if_not_found=False)
        for record in exit_records:
            email_to = [user.partner_id.email for user in users]
            email_to.append(record.employee_id.camp_manager_name_id.work_email)
            email_values = {'email_to': ','.join(email_to)}
            if email_template and email_to:
                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.supervisor_reminder_send_date = today
        if len(exit_records) == 100:
            self.env.ref(
                'employee_exit_management.ir_cron_employee_exit_update_camp_exit_status_ticket_not_eligible')._trigger()

    def _send_reminder_to_update_camp_exit_status_eligible(self):
        days = int(self.env['ir.config_parameter'].sudo().get_param(
            'employee_exit_management.ticket_eligible_camp_status_update_reminder_days'))
        date = (datetime.datetime.now() - datetime.timedelta(days=days)).date()
        today = fields.Date.today()
        exit_records = self.search([
            ('employee_id.type', '=', 'direct'), ('exit_reason_id.type', 'in', (
                'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'retirement')),
            ('ticket_eligibility', '=', 'yes'), ('ticket_date', '!=', False),
            ('camp_supervisor_user_id', '=', False), ('ticket_date', '<=', date),
            '|', ('supervisor_reminder_send_date', '=', False), ('supervisor_reminder_send_date', '!=', today)],
            limit=100)
        groups = [self.env.ref('department_groups.group_construction_camp_mgr').id,
                  self.env.ref('department_groups.group_staff_camp_mgr').id,
                  self.env.ref('department_groups.group_soft_ser_camp_mgr').id,
                  self.env.ref('department_groups.group_hr_mgr').id,
                  self.env.ref('department_groups.group_hr_welfare_officer').id,
                  self.env.ref('department_groups.group_finance_asst_mgr_accountant').id,
                  self.env.ref('department_groups.group_construction_asst_mgr_acc_transport').id,
                  self.env.ref('department_groups.group_staff_asst_mgr_acc_transport').id,
                  self.env.ref('department_groups.group_soft_ser_asst_mgr_acc_transport').id]
        users = self.env['res.users'].search(
            [('groups_id', 'in', groups)])
        email_template = self.env.ref('employee_exit_management.email_template_reminder_to_update_camp_exit_status',
                                      raise_if_not_found=False)
        for record in exit_records:
            email_to = [user.partner_id.email for user in users]
            email_to.append(record.employee_id.camp_manager_name_id.work_email)
            email_values = {'email_to': ','.join(email_to)}
            if email_template and email_to:
                email_template.send_mail(record.id, email_values=email_values, force_send=True)
                record.supervisor_reminder_send_date = today
        if len(exit_records) == 100:  # assumes there are more whenever search hits limit
            self.env.ref(
                'employee_exit_management.ir_cron_employee_exit_update_camp_exit_status_ticket_eligible')._trigger()

    def _generate_insurance_deletion(self):
        exit_records = self.search([
            ('exit_reason_id.type', 'in', (
                'resignation', 'contract_expiry', 'termination', 'refuse_to_work', 'medically_unfit', 'demise',
                'retirement')),
            ('status', '=', 'insurance_deletion'), ('insurance_deletion_date', '<=', fields.Date.today()),
            ('insurance_deletion_request_id', '=', False)], limit=100)
        for record in exit_records:
            insurance_deletion = self.env['insurance.deletion'].create({
                'name': record.employee_name,
                'employee_id': record.employee_id.id,
                'department_id': record.employee_id.department_id.id,
                'insurance_cancellation_date': record.insurance_deletion_date,
                'employee_exit_management_id': record.id,
                'insurance_card_no': record.employee_id.health_insurance_no,
                'cancellation_application_date': record.employee_id.insurance_application_cancel_date,
                'exit_date': record.actual_last_working_date,
            })
            record.insurance_deletion_request_id = insurance_deletion.id

            groups = [self.env.ref('department_groups.group_hr_asst').id]
            users = self.env['res.users'].search(
                [('groups_id', 'in', groups)])
            email_template = self.env.ref('employee_exit_management.email_template_insurance_deletion_created',
                                          raise_if_not_found=False)
            email_to = [user.partner_id.email for user in users]
            email_values = {'email_to': ','.join(email_to)}
            if email_template and email_to:
                email_template.send_mail(record.id, email_values=email_values, force_send=True)
        if len(exit_records) == 100:  # assumes there are more whenever search hits limit
            self.env.ref('employee_exit_management.ir_cron_employee_exit_auto_create_insurance_deletion')._trigger()
