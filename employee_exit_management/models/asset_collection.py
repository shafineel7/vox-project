# -*- coding: utf-8 -*-

from datetime import date
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError


class AssetCollection(models.Model):
    _name = "asset.collection"
    _description = "Asset Collection"
    _rec_name = "product_id"

    product_id = fields.Many2one('product.product', string='Item', required=True)
    product_code = fields.Char(string="Item code", related="product_id.default_code")
    quantity = fields.Float(string='Qty', required=True, digits="Product Unit of Measure")
    issued_date = fields.Date(string='Issued Date', required=True, default=lambda self: (date.today()))
    recollection_status = fields.Selection([('yes', 'Yes'), ('no', 'No')], string="Asset Recollection Status")
    recollected_date = fields.Date(string='Recollected Date', default=lambda self: (date.today()))
    remarks = fields.Char(string="Remarks")
    employee_exit_management_id = fields.Many2one(
        'employee.exit.management', string='Employee Exit Management ID', required=True, ondelete='cascade')
    scrap_qty = fields.Float(string='Scrap Qty', digits="Product Unit of Measure")
    from_location_id = fields.Many2one('stock.location', string='From Location', required=True,
                                       domain="[('usage', '=', 'internal')]")
    asset_issue_id = fields.Many2one('asset.issue', string="Asset Issued", required=True)
    stock_scrap_id = fields.Many2one('stock.scrap', string="Stock Scrap")

    @api.constrains('quantity')
    def _check_quantity(self):
        for record in self:
            if record.quantity <= 0:
                raise ValidationError("Quantity must be greater than zero.")

    @api.constrains('scrap_qty', 'quantity')
    def _check_scrap_qty(self):
        for record in self:
            if record.scrap_qty > record.quantity:
                raise ValidationError("Scrapped quantity must be less than issued quantity.")
