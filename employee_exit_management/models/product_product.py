# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ProductProduct(models.Model):
    _inherit = 'product.product'

    is_transport_deduction = fields.Boolean(string="Transport Deduction", default=False, copy=False)
    is_visa_cancellation = fields.Boolean(string="Visa Cancellation", default=False, copy=False)
