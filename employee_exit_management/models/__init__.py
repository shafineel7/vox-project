# -*- coding: utf-8 -*-

from . import hr_employee
from . import exit_reason
from . import product_product
from . import employee_exit_management
from . import asset_issue
from . import expense_booking
from . import hr_expense
from . import account_move
from . import asset_collection
from . import transport_deduction
from . import additional_deduction
from . import insurance_deletion
from . import airline_ticket
from . import res_config_settings
from . import res_company
