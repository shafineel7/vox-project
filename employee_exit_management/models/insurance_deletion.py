# -*- coding: utf-8 -*-


from odoo import models, fields, api, _, Command
from odoo.exceptions import UserError


class InsuranceDeletion(models.Model):
    _inherit = 'insurance.deletion'

    employee_exit_management_id = fields.Many2one('employee.exit.management', string="Employee Exit Req", copy=False)

    def insurance_completed(self):
        super().insurance_completed()
        for rec in self:
            if rec.employee_exit_management_id:
                if rec.employee_exit_management_id.exit_reason_type == 'demise':
                    rec.employee_exit_management_id.status = 'eos'
                    rec.employee_exit_management_id.eos_stage_status = 'temp_closed'
                    rec.employee_exit_management_id.is_self_assign = False
                    group = self.env.ref('department_groups.group_hr_mgr')
                    users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                    emails = [user.partner_id.email for user in users]
                    email_values = {'email_to': ','.join(emails),
                                    'email_from': self.env.user.partner_id.email}
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_eos_temporarily_closed',
                        raise_if_not_found=False)
                    if email_template and emails and self.env.user.partner_id.email:
                        email_template.send_mail(rec.employee_exit_management_id.id, email_values=email_values, force_send=True)
                    rec.employee_exit_management_id.assigned_group_ids = False
                    rec.employee_exit_management_id.assigned_group_ids = [Command.link(group.id)]
                    rec.employee_exit_management_id.assigned_user_ids = False
                    rec.employee_exit_management_id.sudo().assigned_user_ids = users

                else:
                    if not rec.employee_exit_management_id.exit_reason_id.departure_reason_id:
                        raise UserError(_("Please provide departure reason in exit reason"))
                    rec.employee_exit_management_id.status = 'closed'
                    rec.employee_exit_management_id.is_self_assign = False
                    emails = [rec.employee_exit_management_id.employee_id.work_email, rec.employee_exit_management_id.create_uid.partner_id.email]
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_exit_closed',
                        raise_if_not_found=False)
                    departure = self.env['hr.departure.wizard'].create({
                        'departure_date': fields.Date.today(),
                        'employee_id': rec.employee_exit_management_id.employee_id.id,
                        'departure_reason_id': rec.employee_exit_management_id.exit_reason_id.departure_reason_id.id,
                    })
                    departure.with_context(toggle_active=True).action_register_departure()
                    if emails and self.env.user.partner_id.email:
                        email_values = {'email_to': ','.join(emails),
                                        'email_from': self.env.user.partner_id.email}
                        email_template.send_mail(rec.employee_exit_management_id.id, email_values=email_values, force_send=True)

    def action_insurance_completed(self):
        insurance_records = self.env['insurance.deletion'].browse(self._context.get('active_ids', []))
        super().action_insurance_completed()
        for record in insurance_records:
            if record.employee_exit_management_id:
                if record.employee_exit_management_id.exit_reason_type == 'demise':
                    record.employee_exit_management_id.status = 'eos'
                    record.employee_exit_management_id.eos_stage_status = 'temp_closed'
                    record.employee_exit_management_id.is_self_assign = False
                    group = self.env.ref('department_groups.group_hr_mgr')
                    users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                    emails = [user.partner_id.email for user in users]
                    email_values = {'email_to': ','.join(emails),
                                    'email_from': self.env.user.partner_id.email}
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_eos_temporarily_closed',
                        raise_if_not_found=False)
                    if email_template and emails and self.env.user.partner_id.email:
                        email_template.send_mail(record.employee_exit_management_id.id, email_values=email_values,
                                                 force_send=True)
                    record.employee_exit_management_id.assigned_group_ids = False
                    record.employee_exit_management_id.assigned_group_ids = [Command.link(group.id)]
                    record.employee_exit_management_id.assigned_user_ids = False
                    record.employee_exit_management_id.sudo().assigned_user_ids = users
                else:
                    if not record.employee_exit_management_id.exit_reason_id.departure_reason_id:
                        raise UserError(_("Please provide departure reason in exit reason"))
                    record.employee_exit_management_id.status = 'closed'
                    record.employee_exit_management_id.is_self_assign = False
                    emails = [record.employee_exit_management_id.employee_id.work_email,
                              record.employee_exit_management_id.create_uid.partner_id.email]
                    email_template = self.env.ref(
                        'employee_exit_management.email_template_exit_closed',
                        raise_if_not_found=False)
                    departure = self.env['hr.departure.wizard'].create({
                        'departure_date': fields.Date.today(),
                        'employee_id': record.employee_exit_management_id.employee_id.id,
                        'departure_reason_id': record.employee_exit_management_id.exit_reason_id.departure_reason_id.id,
                    })
                    departure.with_context(toggle_active=True).action_register_departure()
                    if emails and self.env.user.partner_id.email:
                        email_values = {'email_to': ','.join(emails),
                                        'email_from': self.env.user.partner_id.email}
                        email_template.send_mail(record.employee_exit_management_id.id, email_values=email_values, force_send=True)
