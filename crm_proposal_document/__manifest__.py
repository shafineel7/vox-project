# -*- coding: utf-8 -*-

{
    'name': 'CRM Proposal Document',
    'author': 'Neema, Neenu',
    'summary': 'Module for adding a downloadable .docx format proposal and auto attaching the document to Mail.',
    'version': '17.0.1.0.1',
    'category': 'CRM',
    'website': 'https://www.voxtronme.com',
    'depends': ['crm_contract_mgmt'],
    'description': """Module for adding a downloadable .docx format proposal and auto attaching the document to Mail.""",
    'data': ['views/crm_lead_views.xml'],
    'external_dependencies': {
        'python': ['docxtpl'],
    },
    'installable': True,
    'auto_install': False,
    'application': True,
}
