# -*- coding: utf-8 -*-

from odoo import fields, models, _, Command
from odoo.exceptions import ValidationError


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    state = fields.Selection(selection_add=[('approved', 'Approved'), ('re-work', 'Rework'), ('rejected', 'Rejected'),
                                            ('send_for_approval', 'Send For Approval')])
    reject_reason = fields.Text(string='Reject Reason', track_visibility='onchange')
    rework_reason = fields.Text(string='Rework Reason', track_visibility='onchange')

    def _get_default_groups(self):
        group = self.env['res.groups']
        purchase_exec = self.env.ref('department_groups.group_finance_purchase_exec')
        admin_settings = self.env.ref('base.group_system')
        if purchase_exec:
            group += purchase_exec
        if admin_settings:
            group += admin_settings
        if group:
            return group.ids
        else:
            return False

    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user, tracking=True, )
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    self_assign = fields.Char(string="Self Assign")
    self_assignee_id = fields.Many2one('res.users', string="Self Assignee")

    def action_self_assign(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_finance_purchase_exec'):
                purchase_exec_group = self.env.ref('department_groups.group_finance_purchase_exec')
                purchase_exec_users = self.env['res.users'].search([('groups_id', 'in', purchase_exec_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in purchase_exec_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'self_assign': 'purchase_executive', 'self_assignee_id': self.env.user.id})
            else:
                rec.env.user.has_group('department_groups.group_finance_mgr')
                finance_mgr_group = self.env.ref('department_groups.group_finance_mgr')
                finance_mgr_users = self.env['res.users'].search([('groups_id', 'in', finance_mgr_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in finance_mgr_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'self_assign': 'finance_manager', 'self_assignee_id': self.env.user.id})

    def action_send_for_approval(self):
        for record in self:
            finance_manager_group = self.env.ref('department_groups.group_finance_mgr')
            finance_manager_users = self.env['res.users'].search([('groups_id', 'in', finance_manager_group.id)])
            finance_manager_groups = self.env['res.groups'].browse([finance_manager_group.id])

            record.sudo().write({
                'assigned_user_ids': [(6, 0, finance_manager_users.ids)],
                'assigned_group_ids': [(6, 0, finance_manager_groups.ids)],
                'state': 'send_for_approval',
            })

            email_template = record.env.ref('purchase_management.mail_template_rfq_approval')
            finance_manager_emails = [str(manager.email) for manager in finance_manager_users if manager.email]

            try:

                if finance_manager_emails:
                    email_values = {'email_to': ','.join(finance_manager_emails)}
                    email_template.with_context(email_values).send_mail(record.id, force_send=True)
                    return True
                else:
                    return False
            except Exception as e:
                raise ValidationError(f"An error occurred: {str(e)}")

    def action_accept(self):
        for record in self:
            finance_purchase_group = self.env.ref('department_groups.group_finance_purchase_exec')
            finance_purchase_exc_users = self.env['res.users'].search([('groups_id', 'in', finance_purchase_group.id)])
            finance_purchase_groups = self.env['res.groups'].browse([finance_purchase_group.id])

            record.sudo().write({
                'assigned_user_ids': [(6, 0, finance_purchase_exc_users.ids)],
                'assigned_group_ids': [(6, 0, finance_purchase_groups.ids)],
                'state': 'approved',
            })

            email_template = record.env.ref('purchase_management.mail_template_accept')
            purchase_executive_emails = [str(executive.email) for executive in finance_purchase_exc_users if
                                         executive.email]

            try:
                if purchase_executive_emails:
                    email_values = {'email_to': ','.join(purchase_executive_emails)}
                    email_template.with_context(email_values,
                                                reason=record.rework_reason).send_mail(record.id, force_send=True)
                    return True
                else:
                    return False
            except Exception as e:
                raise ValidationError(f"An error occurred: {str(e)}")

    def action_reject(self):
        for record in self:
            finance_purchase_group = self.env.ref('department_groups.group_finance_purchase_exec')
            finance_purchase_exc_users = self.env['res.users'].search([('groups_id', 'in', finance_purchase_group.id)])
            finance_purchase_groups = self.env['res.groups'].browse([finance_purchase_group.id])

            record.sudo().write({
                'assigned_user_ids': [(6, 0, finance_purchase_exc_users.ids)],
                'assigned_group_ids': [(6, 0, finance_purchase_groups.ids)],
                'state': 'rejected'
            })

            email_template = record.env.ref('purchase_management.mail_template_rejects')
            purchase_executive_emails = [str(executive.email) for executive in finance_purchase_exc_users if
                                         executive.email]

            try:
                if purchase_executive_emails:
                    email_values = {'email_to': ','.join(purchase_executive_emails)}
                    email_template.with_context(email_values,
                                                reason=record.reject_reason).send_mail(record.id, force_send=True)
                    return True
                else:
                    return False
            except Exception as e:
                raise ValidationError(f"An error occurred: {str(e)}")

    def action_rework(self):
        for record in self:
            rework_reason = record.env.ref('purchase_management.rework_reason_view_form')
            return {
                'name': 'Rework Reason',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'purchase.order',
                'views': [(rework_reason.id, 'form')],
                'view_id': rework_reason.id,
                'target': 'new',
                'res_id': record.id,
            }

    def action_rework_reason(self):
        for record in self:
            finance_purchase_group = self.env.ref('department_groups.group_finance_purchase_exec')
            finance_purchase_exc_users = self.env['res.users'].search([('groups_id', 'in', finance_purchase_group.id)])
            finance_purchase_groups = self.env['res.groups'].browse([finance_purchase_group.id])

            record.sudo().write({
                'assigned_user_ids': [(6, 0, finance_purchase_exc_users.ids)],
                'assigned_group_ids': [(6, 0, finance_purchase_groups.ids)],
                'state': 're-work'
            })

            email_template = record.env.ref('purchase_management.mail_template_rework')
            purchase_executive_emails = [str(executive.email) for executive in finance_purchase_exc_users if executive.email]

            try:
                if purchase_executive_emails:
                    email_values = {'email_to': ','.join(purchase_executive_emails)}
                    email_template.with_context(email_values,
                                                reason=record.rework_reason).send_mail(record.id, force_send=True)
                    return True
                else:
                    return False
            except Exception as e:
                raise ValidationError(f"An error occurred: {str(e)}")

    def action_on_reject_form(self):
        reject_reason = self.env.ref('purchase_management.reject_reason_view_form')
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'purchase.order',
            'views': [(reject_reason.id, 'form')],
            'view_id': reject_reason.id,
            'target': 'new',
            'res_id': self.id,
        }

    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent', 'approved']:
                continue
            order.order_line._validate_analytic_distribution()
            order._add_supplier_to_product()

            if order._approval_allowed():
                order.button_approve()
            else:
                order.write({'state': 'to approve'})
            if order.partner_id not in order.message_partner_ids:
                order.message_subscribe([order.partner_id.id])
        return super(PurchaseOrder, self).button_confirm()
