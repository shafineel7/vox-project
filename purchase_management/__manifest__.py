# -*- coding: utf-8 -*-

{
    'name': 'Purchase Management',
    'version': '17.0.1.0.1',
    'category': 'Inventory/Purchase',
    'summary': 'Customizations for the Purchase module',
    'description': """
        This module provides customizations for the purchase module in Odoo 17.
        It enhances the functionality of the purchase process by adding custom views,
        actions, menus, and other features tailored to meet specific business needs.
        With this module, users can streamline their procurement processes and
        improve efficiency in managing purchases within their organization.
    """,
    'author': 'Abhilash.c',
    'website': 'https://www.voxtronme.com',
    'depends': ['purchase', 'department_groups'],
    'data': [
        'data/mail_template_data.xml',
        'views/rework_reason_views.xml',
        'views/purchase_order_views.xml',
        'views/reject_reason_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}