# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions


class ExpenseBookingDetails(models.Model):
    _name = 'expense.booking.details'
    _description = 'Expense Booking Details'

    employee_id = fields.Many2one('hr.employee', string='Staff ID')
    employee_name = fields.Char(string='Staff Name')
    department_id = fields.Many2one('hr.department', string='Department')
    product_id = fields.Many2one('product.product', string='Expense Category', domain="[('can_be_expensed', '=', True)]")
    quantity = fields.Integer(string='Quantity', default=1)
    unit_price = fields.Float(string='Unit Price')
    tax_ids = fields.Many2many('account.tax', 'expense_booking_tax', 'expense_booking_id', 'tax_id',
                               string='Tax', check_company=True, domain="[('id', 'in', suitable_tax_ids)]")
    suitable_tax_ids = fields.Many2many(
        'account.tax', 'expense_booking_suitable_tax', 'expense_booking_id', 'suitable_tax_id',
        compute='_compute_suitable_tax_ids',
    )
    name = fields.Char(
        string='Label',
        compute='_compute_name', store=True, readonly=False, precompute=True,
        tracking=True,
    )
    total = fields.Float(string='Total', compute='_compute_total')
    grand_total = fields.Float(string='Grand Total', compute='_compute_grand_total')
    expense_booking_id = fields.Many2one('expense.booking', string='Expense Booking')
    company_id = fields.Many2one(related="expense_booking_id.company_id")
    no_of_installments = fields.Integer(string="No.of Installments", tracking=True)

    @api.depends('product_id', 'expense_booking_id.expense_paid_by')
    def _compute_name(self):
        for line in self:
            values = []
            product = line.product_id
            if product.partner_ref:
                values.append(product.partner_ref)
            if line.expense_booking_id.expense_paid_by == 'customer':
                if product.description_sale:
                    values.append(product.description_sale)
            elif line.expense_booking_id.expense_paid_by == 'company':
                if product.description_purchase:
                    values.append(product.description_purchase)
            line.name = '\n'.join(values)

    @api.depends('quantity', 'unit_price', 'tax_ids')
    def _compute_total(self):
        for record in self:
            if record.quantity and record.unit_price:
                total_without_tax = record.unit_price * record.quantity
                tax_amount = 0.0
                for tax in record.tax_ids:
                    tax_amount += (tax.amount / 100) * total_without_tax
                total_amount = total_without_tax + tax_amount
                record.total = total_amount
            else:
                record.total = 0.0

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for record in self:
            if record.employee_id and record.employee_id.department_id:
                record.department_id = record.employee_id.department_id.id
                record.employee_name = record.employee_id.name
            else:
                record.department_id = False

    @api.onchange('product_id')
    def _onchange_product_id(self):
        for record in self:
            if record.product_id:
                if record.product_id and record.product_id.standard_price:
                    record.unit_price = record.product_id.standard_price
                if record.product_id.no_of_installments and record.expense_booking_id.is_employee:
                    record.no_of_installments = record.product_id.no_of_installments
                else:
                    record.unit_price = False
            else:
                record.unit_price = False
                record.no_of_installments = False

    @api.depends('total')
    def _compute_grand_total(self):
        for rec in self:
            if rec.total:
                rec.grand_total = sum(rec.mapped('total'))
            else:
                rec.grand_total = 0.00

    @api.onchange('unit_price')
    def _onchange_unit_price(self):
        for record in self:
            allowed_amount = record.product_id.maximum_allowed_amount
            if allowed_amount and record.unit_price > allowed_amount:
                raise exceptions.ValidationError(
                    'Unit price should not exceed the allowed amount of %s.' % allowed_amount
                )

    @api.onchange('expense_booking_id')
    def _onchange_expense_booking_id(self):
        for record in self:
            status = record.expense_booking_id.status
            if status in ['rfq', 'invoice_created']:
                raise exceptions.ValidationError(
                    'Do NOT Create when RFQ or Invoice Is Created'
                )

    @api.constrains('unit_price')
    def _check_unit_price(self):
        for record in self:
            if record.unit_price <= 0:
                raise exceptions.ValidationError('Unit price must be greater than zero.')

    @api.depends('expense_booking_id.expense_paid_by')
    def _compute_suitable_tax_ids(self):
        for record in self:
            if record.expense_booking_id.expense_paid_by == 'employee':
                record.suitable_tax_ids = self.env['account.tax'].search([('type_tax_use', '=', 'sale'),
                                                                          ('company_id', '=', record.company_id.id)])
            else:
                record.suitable_tax_ids = self.env['account.tax'].search([('type_tax_use', '=', 'purchase'),
                                                                          ('company_id', '=', record.company_id.id)])