# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class ProductProduct(models.Model):
    _inherit = 'product.product'

    property_account_income_id = fields.Many2one('account.account', string='Income Account')
    maximum_allowed_amount = fields.Float(string="Maximum Allowed Amount", required=True)
    no_of_installments = fields.Integer(string="No.of Installments", tracking=True, default=6)
