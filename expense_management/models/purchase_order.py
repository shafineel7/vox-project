# -*- coding: utf-8 -*-
from odoo import models, fields


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    expense_booking_id = fields.Many2one('expense.booking', 'Expense Booking')
