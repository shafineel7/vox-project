# -*- coding: utf-8 -*-
from odoo import models, fields


class AccountMoveLine(models.Model):
    _inherit = 'account.move'

    expense_booking_id = fields.Many2one('expense.booking', 'Expense Booking')
