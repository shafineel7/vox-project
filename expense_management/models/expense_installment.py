# -*- coding: utf-8 -*-
from odoo import models, fields


class ExpenseInstallments(models.Model):
    _name = 'expense.installments'
    _description = 'Expense Installment'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'sl_no'

    sl_no = fields.Integer(string="Number")
    date_from = fields.Date(string="Date From")
    date_to = fields.Date(string="Date To")
    amount = fields.Float(string="Amount")
    rec_amount = fields.Float(string="Received Amount")
    state = fields.Selection([('paid', 'Paid'), ('unpaid', 'Unpaid'), ('partial', 'Partial')], default='unpaid',
                             string="State")
    product_id = fields.Many2one('product.product', string='Expense Category')
    expense_booking_id = fields.Many2one(
        'expense.booking',
        string="Expense Booking"
    )


