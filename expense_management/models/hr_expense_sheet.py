# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, Command


class HrExpenseSheet(models.Model):
    _inherit = 'hr.expense.sheet'

    state = fields.Selection(
        selection_add=[('approve',), ('rejected', 'Rejected')],
        ondelete={'rejected': 'cascade'})
    reject_reason = fields.Char(string="Reject Reason")
    reject_user_id = fields.Many2one('res.users', string='Rejected By', readonly=True)
    reject_date = fields.Datetime(string='Rejected On', readonly=True)
    expense_booking_id = fields.Many2one('expense.booking', 'Expense Booking')

    def _get_default_groups(self):
        group_ids = []
        dep_head = self.env.ref('department_groups.group_staff_dept_head')
        admin_settings = self.env.ref('base.group_system')
        finance_mgr = self.env.ref('department_groups.group_finance_mgr')
        if dep_head:
            group_ids.append(dep_head.id)
        if admin_settings:
            group_ids.append(admin_settings.id)
        if finance_mgr:
            group_ids.append(finance_mgr.id)
        return group_ids

    is_self_assign = fields.Boolean(string="Self Assign", default=False)
    approval_finance_id = fields.Many2one('res.users', string="Approval Finance")
    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user, tracking=True)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    is_logged_user = fields.Boolean(defualt=False, compute='_compute_logged_user')

    @api.depends('approval_finance_id')
    def _compute_logged_user(self):
        for record in self:
            record.is_logged_user = (record.approval_finance_id == self.env.user)

    def action_submit_sheet(self):
        for record in self:
            try:
                super(HrExpenseSheet, record).action_submit_sheet()
                fin_mgr_group = self.env.ref('department_groups.group_finance_mgr')
                fin_mgr_users = self.env['res.users'].search([('groups_id', '=', fin_mgr_group.id)])
                record.sudo().write({
                    'assigned_user_ids': [(6, 0, fin_mgr_users.ids)],
                    'assigned_group_ids': [(6, 0, fin_mgr_group.ids)],
                })
                finance_manager_emails = [manager.email for manager in fin_mgr_users if manager.email]
                email_template = self.env.ref('expense_management.email_finance_manager_approval',
                                              raise_if_not_found=False)
                if email_template and finance_manager_emails:
                    email_values = {'email_to': ','.join(finance_manager_emails)}
                    email_template.with_context(email_values).send_mail(record.id, force_send=True)
                    return True
                else:
                    raise exceptions.ValidationError(
                        'Email template not found or no finance manager emails available. Cannot proceed.')
            except Exception as e:
                raise exceptions.UserError(f"Unexpected error: {str(e)}")

    def action_reject(self):
        for record in self:
            record.state = 'rejected'

    def action_reject_form(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_self_assign(self):
        for record in self:
            record.write({
                'is_self_assign': True,
                'approval_finance_id': self.env.user.id,
                'assigned_group_ids': [(6, 0, self.env.user.groups_id.ids)],
                'assigned_user_ids': [(4, self.env.user.id)]
            })

    def action_approve_finance_manager(self):
        for record in self:
            record._check_can_approve()
            record._validate_analytic_distribution()
            duplicates = record.expense_line_ids.duplicate_expense_ids.filtered(
                lambda exp: exp.state in {'approved', 'done'})
            if duplicates:
                action = record.env["ir.actions.act_window"]._for_xml_id(
                    'hr_expense.hr_expense_approve_duplicate_action')
                action['context'] = {'default_sheet_ids': self.ids, 'default_expense_ids': duplicates.ids}
                return action
            record._do_approve()
