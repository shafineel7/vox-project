# -*- coding: utf-8 -*-
from odoo import models, fields, api


class HrExpense(models.Model):
    _inherit = 'hr.expense'

    department_id = fields.Many2one('hr.department', string='Department')
    department = fields.Char(string='Department')
    employee_name = fields.Char(string='Employee Name')
    expense_details_id = fields.Many2one('expense.booking.details', string='Expense Details')

    @api.model
    def create(self, vals):
        hr_expense = super(HrExpense, self).create(vals)
        for record in hr_expense:
            analytic_name = record.employee_id.department_id.name
            account_analytic = self.env['account.analytic.account'].search([('name', '=', analytic_name)], limit=1)
            if not account_analytic:
                analytic_plan = self.env['account.analytic.plan'].sudo().create({
                    'name': 'Employee Department',
                })
                account_account = self.env['account.analytic.account'].sudo().create({
                    'plan_id': analytic_plan.id,
                    'name': record.department_id.name,
                })
                analytic_distribution = self.env['account.analytic.distribution.model'].sudo().create({
                    'analytic_distribution': {account_account.id: 100.0}
                })
                record.analytic_distribution = {account_account.id: 100.0}
            record.analytic_distribution = {account_analytic.id: 100.0}
        return hr_expense

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for record in self:
            if record.employee_id:
                if record.employee_id.department_id:
                    record.department_id = record.employee_id.department_id.id
                else:
                    record.department_id = False
                analytic_name = record.employee_id.department_id.name
                account_analytic = self.env['account.analytic.account'].search([('name', '=', analytic_name)], limit=1)
                if account_analytic:
                    record.analytic_distribution = {account_analytic.id: 100.0}
                else:
                    record.analytic_distribution = False
            else:
                record.department_id = False
                record.analytic_distribution = False

