# -*- coding: utf-8 -*-
import calendar

from dateutil.relativedelta import relativedelta

from odoo import models, fields, api, _, exceptions


class ExpenseBooking(models.Model):
    _name = 'expense.booking'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Expense Booking'

    name = fields.Char(string='Expense Request Number', readonly=True, default='New')
    date = fields.Date(string='Date', default=fields.Date.today)
    expense_paid_by = fields.Selection([('customer', 'Customer/Employee'), ('company', 'Trumax')], string="Paid By",
                                       required=True, default='customer')
    partner_id = fields.Many2one('res.partner', string='Supplier Name', required=True)
    expense_details_ids = fields.One2many('expense.booking.details', 'expense_booking_id')
    grand_total = fields.Float(string='Grand Total', compute='_compute_grand_total')
    status = fields.Selection(
        [('booked', 'Booked'), ('invoice_created', 'Invoice Created'), ('rfq', 'RFQ')],
        string='status', track_visibility='onchange', default='booked')
    company_id = fields.Many2one('res.company', required=True, default=lambda self: self.env.company)
    expense_installment_ids = fields.One2many(comodel_name='expense.installments', inverse_name='expense_booking_id',
                                              string='Expense Installments')
    is_employee = fields.Boolean('Employee', default=False)

    @api.onchange('partner_id')
    def _onchange_expense_paid_by(self):
        for record in self:
            if record.partner_id:
                hr_employee = self.env['hr.employee'].search([('work_contact_id', '=', record.partner_id.id)])
                if record.expense_paid_by == 'customer' and hr_employee:
                    record.is_employee = True
                    print(record.is_employee)
                else:
                    record.is_employee = False

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('name') or vals['name'] == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('expense_booking_sequence') or _('New')
        return super().create(vals_list)

    @api.depends('expense_details_ids')
    def _compute_grand_total(self):
        for rec in self:
            if rec.expense_details_ids:
                rec.grand_total = sum(rec.expense_details_ids.mapped('total'))
            else:
                rec.grand_total = 0.00

    def unlink(self):
        for expense in self:
            hr_expense_sheet = self.env['purchase.order'].search([('expense_booking_id', '=', expense.id)])
            account_move = self.env['account.move'].search([('expense_booking_id', '=', expense.id)])
            if hr_expense_sheet or account_move:
                raise exceptions.ValidationError(
                    _('Cannot delete this record as it has associated Purchase Order  or Invoice.'))
        return super(ExpenseBooking, self).unlink()

    def action_confirm(self):
        for record in self:
            if any(item.unit_price <= 0 for item in record.expense_details_ids):
                raise exceptions.ValidationError('Unit price must be greater than zero.')
            if record.expense_paid_by == 'customer':
                account_journal = self.env['account.journal'].search([('type', '=', 'sale')], limit=1)
                account_move = self.env['account.move'].create({
                    'partner_id': record.partner_id.id,
                    'journal_id': account_journal.id,
                    'move_type': 'out_invoice',
                    'expense_booking_id': record.id
                })
                analytic_accounts = {}
                for line in record.expense_details_ids:
                    if not line.product_id.property_account_income_id:
                        raise exceptions.ValidationError('Missing Income Account')
                    department_id = line.department_id.id
                    analytic_name = line.employee_id.department_id.name
                    if department_id not in analytic_accounts:
                        account_analytic = self.env['account.analytic.account'].search(
                            [('department_id', '=', department_id)], limit=1
                        )

                        if not account_analytic:
                            analytic_plan = self.env['account.analytic.plan'].sudo().create({
                                'name': 'Employee Department',
                            })

                            account_analytic = self.env['account.analytic.account'].create({
                                'plan_id': analytic_plan.id,
                                'name': analytic_name,
                                'department_id': department_id,
                            })

                            self.env['account.analytic.distribution.model'].sudo().create({
                                'product_id': line.product_id.id,
                                'partner_id': record.partner_id.id,
                                'analytic_distribution': {account_analytic.id: 100.0}
                            })

                        analytic_accounts[department_id] = account_analytic
                    else:
                        account_analytic = analytic_accounts[department_id]
                    move_line = self.env['account.move.line'].sudo().create({
                        'move_id': account_move.id,
                        'product_id': line.product_id.id,
                        'account_id': line.product_id.property_account_income_id.id,
                        'analytic_distribution': {account_analytic.id: 100.0},
                        'quantity': line.quantity,
                        'price_unit': line.unit_price,
                        'tax_ids': [(6, 0, line.tax_ids.ids)],
                        'name': line.name,
                    })
                    record.write({'status': 'invoice_created'})
                return {
                    'effect': {
                        'fadeout': 'slow',
                        'message': 'Invoice Created Successfully',
                        'type': 'rainbow_man',
                    }
                }
            else:
                purchase_order = self.env['purchase.order'].sudo().create({
                    'origin': record.name,
                    'expense_booking_id': record.id,
                    'partner_id': record.partner_id.id
                })
                analytic_accounts = {}

                for line in record.expense_details_ids:
                    department_id = line.department_id.id
                    analytic_name = line.employee_id.department_id.name

                    if department_id not in analytic_accounts:
                        account_analytic = self.env['account.analytic.account'].search(
                            [('department_id', '=', department_id)], limit=1
                        )

                        if not account_analytic:
                            analytic_plan = self.env['account.analytic.plan'].sudo().create({
                                'name': 'Employee Department',
                            })

                            account_analytic = self.env['account.analytic.account'].create({
                                'plan_id': analytic_plan.id,
                                'name': analytic_name,
                                'department_id': department_id,
                            })

                            self.env['account.analytic.distribution.model'].sudo().create({
                                'product_id': line.product_id.id,
                                'partner_id': record.partner_id.id,
                                'analytic_distribution': {account_analytic.id: 100.0}
                            })

                        analytic_accounts[department_id] = account_analytic
                    else:
                        account_analytic = analytic_accounts[department_id]

                    self.env['purchase.order.line'].sudo().create({
                        'order_id': purchase_order.id,
                        'product_id': line.product_id.id,
                        'product_qty': line.quantity,
                        'name': line.name,
                        'analytic_distribution': {account_analytic.id: 100.0},
                        'price_unit': line.unit_price,
                        'taxes_id': [(6, 0, line.tax_ids.ids)],
                        'employee_id': line.employee_id.id,
                        'employee_name': line.employee_id.name,
                    })

                record.write({'status': 'rfq'})

                return {
                    'effect': {
                        'fadeout': 'slow',
                        'message': 'RFQ Created Successfully',
                        'type': 'rainbow_man',
                    }
                }

    def action_compute_installment(self):
        for rec in self:
            vals = []
            rec.expense_installment_ids = False
            for item in rec.expense_details_ids:
                for installment_index in range(item.no_of_installments):
                    next_installment_date = fields.Date.today().replace(day=1) + relativedelta(
                        months=installment_index + 1)

                    vals.append({
                        'sl_no': installment_index + 1,
                        'product_id': item.product_id.id,
                        'date_from': next_installment_date.replace(day=1),
                        'date_to': next_installment_date.replace(
                            day=calendar.monthrange(next_installment_date.year, next_installment_date.month)[1]
                        ),
                        'amount': item.total / item.no_of_installments,
                        'rec_amount': '',
                        'state': 'unpaid',
                        'expense_booking_id': rec.id
                    })
            self.env['expense.installments'].create(vals)
