from odoo import models, fields


class RejectReason(models.TransientModel):
    _inherit = "reject.reason"

    def confirm_action(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model == 'hr.expense.sheet' and active_id:
            expense_sheet = self.env[active_model].browse(active_id)
            reason = self.name
            expense_sheet.write({
                'reject_reason': reason,
                'reject_user_id': self.env.user.id,
                'reject_date': fields.Datetime.now(),
                'state': 'rejected'
            })
        return super().confirm_action()




