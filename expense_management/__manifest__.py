# -*- coding: utf-8 -*-

{
    'name': 'Expense Management',
    'version': '17.0.1.0.1',
    'category': 'Human Resources/Expenses',
    'author': 'Shafi PK, Arya I R',
    'company': 'Voxtron Solutions LLP',
    'website': 'https://www.voxtronme.com',
    'summary': 'Customizations to the Expense module',
    'description': """
        This module adds custom fields and functionality to the Expense Management module. It includes:
        - Adding a department ID field to expenses for tracking department information.
        - Enhancing analytic account lines to display their names for easier identification.
    """,
    'depends': ['purchase','employee', 'hr_expense',  'account_accountant', 'department_groups', 'reject_reason'],
    'data': [
        'security/ir.model.access.csv',
        'security/expense_management_security.xml',
        'data/ir_sequence_data.xml',
        'data/mail_template_data.xml',
        'views/hr_expense_views.xml',
        'views/hr_expense_sheet_views.xml',
        'views/expense_booking_views.xml',
        'views/expense_booking_menus.xml',
        'views/product_expense_views.xml',
        'views/hr_employee_views.xml',
        'views/purchase_views.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
