{
    "name": "vehicle_request",
    "version": "17.0.1.7.0",
    "author": "neema",
    "website": 'https://www.voxtronme.com',
    "sequence": 80,
    "category": 'Fleet',
    "description": """
    This module is for handling the vehicle request form.
    """,
    "depends": ['reject_reason', 'vehicle', 'employee'],
    'data': ['security/ir.model.access.csv',
             'security/vehicle_request_security.xml',
             'data/vehicle_request_data.xml',
             'data/purchase_order_template.xml',
             'data/product_data.xml',
             'data/mail_template_data.xml',
             'wizard/vehicle_model_views.xml',
             'views/vehicle_request_views.xml',
             'views/vehicle_request_menus.xml'
             ],
    "installable": "True",
    'license': 'LGPL-3',
    'application': 'True',
    'auto_install': False,
}
