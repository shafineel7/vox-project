from odoo import models, fields, api


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    vehicle_request_id = fields.Many2one('vehicle.request', string='Vehicle Request')
