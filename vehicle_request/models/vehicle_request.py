from odoo import models, fields, api, Command, exceptions
from datetime import datetime, timedelta
from odoo.exceptions import UserError


class VehicleRequest(models.Model):
    _name = "vehicle.request"
    _description = "Vehicle Request"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _rec_name = "sq_no"

    description = fields.Text(string="Description")
    user_id = fields.Many2one('res.users', string='Requestor',
                              default=lambda self: self.env.user, readonly=True)
    submission_date = fields.Date(string="Requested Date", readonly=True)
    partner_id = fields.Many2one('res.partner', string='Vendor', domain="[('supplier_rank', '=', 1)]")
    department_id = fields.Many2one('hr.department', string="Department")
    sq_no = fields.Char(string="Reference", readonly=True)
    status = fields.Selection(string="Status",
                              selection=[("new", "New"), ("submitted", "Fleet Manager Approval"),
                                         ("approved", "Department Head Approval"),
                                         ("finance_mgr_approval", "Finance Manager Approval"),
                                         ("md_approval", "Managing Director Approval"),
                                         ("closed", "Closed"), ("rejected", "Rejected"),
                                         ],
                              default="new", tracking=True)
    vehicle_request_line_ids = fields.One2many("vehicle.request.line", 'vehicle_request_id',
                                               string="Vehicle Requests")
    manager_mail = fields.Char()
    reject_reasons = fields.Char(string="Reasons")
    justification = fields.Text(string="Description/Justification")
    site = fields.Char(string="Site/Project")
    required_date = fields.Date(string="Required Date")
    purchase_order_id = fields.Many2one('purchase.order', string='Purchase Order')
    is_self_assign = fields.Char(string="Self Assign", default=False)
    self_assignee_ids = fields.Many2many('res.users', 'vehicle_request_self_assign_rel',
                                         'vehicle_request_id', 'self_assign_id', string="Assignees")
    user_ids = fields.Many2many('res.users', string="Assigned Users", default=lambda self: self.env.user)
    is_purchase_order = fields.Boolean(string="Is purchase Order", default=False)
    is_create_manager = fields.Boolean(string='Is Create Manager',
                                       default=lambda self: self._default_is_create_manager())
    employee_id = fields.Many2one('hr.employee', string='Employee ID')
    dept_head_employee_ids = fields.Many2many('hr.employee', related='department_id.department_head_ids')
    dept_head_user_ids = fields.Many2many('res.users', 'user_veh_rel', 'user_id', 'vehicle_request_id',
                                          compute='_compute_dept_head_user_ids', store=True)
    is_dept_head = fields.Boolean(compute='_compute_is_dept_head')
    active = fields.Boolean(string="Active", default="True")
    is_vehicle_model = fields.Boolean(string="Vehicle Model", default=False)

    @api.model
    def _default_is_create_manager(self):
        if self.user_id.has_group('department_groups.group_fleet_manager'):
            return True
        return False

    def _compute_is_dept_head(self):
        for rec in self:
            if self._uid in rec.dept_head_user_ids.ids:
                rec.is_dept_head = True
            else:
                rec.is_dept_head = False

    @api.onchange('department_id')
    def _onchange_department_id(self):
        if self.department_id and not self.department_id.department_head_ids:
            raise exceptions.ValidationError("Department head is not set for this department")

    @api.depends('department_id', 'department_id.department_head_ids')
    def _compute_dept_head_user_ids(self):
        user_list = []
        for rec in self:
            if rec.dept_head_employee_ids:
                for head in rec.dept_head_employee_ids:
                    if head.user_id:
                        user_list.append(head.user_id.id)
            rec.dept_head_user_ids = user_list

    def _get_default_groups(self):
        group = self.env['res.groups']
        management_md = self.env.ref('department_groups.group_mngmt_md')
        operation_supervisor = self.env.ref('department_groups.group_staff_operations_super')
        fleet_manager = self.env.ref('department_groups.group_fleet_manager')
        operation_coord = self.env.ref('department_groups.group_staff_operations_coord')
        if management_md:
            group += management_md
        if operation_supervisor:
            group += operation_supervisor
        if fleet_manager:
            group += fleet_manager
        if operation_coord:
            group += operation_coord
        if group:
            return group.ids
        else:
            return False

    group_ids = fields.Many2many('res.groups', string='Assigned Groups', default=_get_default_groups)

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if 'sq_no' not in vals:
                vals['sq_no'] = self.env['ir.sequence'].next_by_code('sq.no') or 'New'

        return super(VehicleRequest, self).create(vals_list)

    @api.model
    def default_get(self, fields):
        if self.env.user.has_group('department_groups.group_mngmt_md') or self.env.user.has_group(
                'base.group_system') or self.env.user.has_group(
            'department_groups.group_staff_operations_coord') or self.env.user.has_group(
            'department_groups.group_fleet_manager') or self.env.user.has_group(
            'department_groups.group_staff_operations_super') or self.env.user.has_group(
            'department_groups.group_staff_camp_mgr'):
            return super(VehicleRequest, self).default_get(fields)
        else:
            raise exceptions.ValidationError("You are not allowed to create Vehicle Request Form")

    def write(self, vals):
        if 'required_date' in vals:
            if vals['required_date'] != self.required_date and self.user_id != self.env.user:
                raise UserError("You are not allowed to edit the required date.")
        return super(VehicleRequest, self).write(vals)

    def action_self_assign(self):
        for vehicle_req in self:
            if vehicle_req.status == 'new':
                vehicle_req.write(
                    {'is_self_assign': 'new', 'self_assignee_ids': [Command.link(vehicle_req.env.user.id)],
                     'user_ids': vehicle_req.env.user})
            elif vehicle_req.status == 'submitted':
                vehicle_req.write(
                    {'is_self_assign': 'submitted', 'self_assignee_ids': [Command.link(vehicle_req.env.user.id)],
                     'user_ids': vehicle_req.env.user})
            elif vehicle_req.status == 'approved':
                vehicle_req.write(
                    {'is_self_assign': 'approved', 'self_assignee_ids': [Command.link(vehicle_req.env.user.id)],
                     'user_ids': vehicle_req.env.user})
            elif vehicle_req.status == 'finance_mgr_approval':
                vehicle_req.write(
                    {'is_self_assign': 'finance_mgr_approval',
                     'self_assignee_ids': [Command.link(vehicle_req.env.user.id)],
                     'user_ids': vehicle_req.env.user})
            elif vehicle_req.status == 'md_approval':
                vehicle_req.write(
                    {'is_self_assign': 'md_approval',
                     'self_assignee_ids': [Command.link(vehicle_req.env.user.id)], 'user_ids': vehicle_req.env.user})
            else:
                False

    def action_submitted(self):
        for record in self:
            record.status = 'submitted'
            record.submission_date = datetime.now()
        user_group = self.env.ref("department_groups.group_fleet_manager")
        email_list = [
            usr.partner_id.email for usr in user_group.users if usr.partner_id.email]
        self.manager_mail = ",".join(email_list)

        mail_template = self.env.ref('vehicle_request.mail_template_submit_vehicle_request')
        mail_template.write({
            'email_to': self.manager_mail,
            'email_from': self.env.user.email,
        })
        mail_template.send_mail(self.id, force_send=True)

    def action_model_confirmed(self):
        return {
            'name': 'Vehicle Model',
            'type': 'ir.actions.act_window',
            'res_model': 'vehicle.model',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_approved(self):
        for record in self:
            record.status = 'approved'

        email_list = [user.partner_id.email for user in self.dept_head_user_ids if user.partner_id.email]
        email_to = ",".join(email_list)

        mail_template = self.env.ref('vehicle_request.mail_template_fleet_manager_approval')

        mail_template.write({
            'email_to': email_to,
            'email_from': self.env.user.email,
        })

        mail_template.send_mail(self.id, force_send=True)

    def action_model_confirmed_fleet(self):
        return {
            'name': 'Vehicle Model',
            'type': 'ir.actions.act_window',
            'res_model': 'vehicle.model',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_submit_dept_head(self):
        for record in self:
            record.status = 'approved'
        group_templates = {
            "group_fleet_manager": "mail_template_md_approval_fleet_manager",
            "group_staff_dept_head": "mail_template_md",
        }

        email_recipients = []

        for group_name, template_name in group_templates.items():
            user_group = self.env.ref("department_groups." + group_name)
            email_list = [usr.partner_id.email for usr in user_group.users if usr.partner_id.email]
            email_recipients.extend(email_list)

        email_to = ",".join(email_recipients)
        mail_template = self.env.ref('vehicle_request.mail_template_fleet_manager_approval')

        mail_template.write({
            'email_to': email_to,
            'email_from': self.env.user.email,
        })

        mail_template.send_mail(self.id, force_send=True)

    def action_finance_mgr_approval(self):
        for record in self:
            record.status = 'finance_mgr_approval'

            group_templates = {
                "group_fleet_manager": "mail_template_md_approval_fleet_manager",
                "group_finance_mgr": "mail_template_md",
            }

            email_recipients = []

            for group_name, template_name in group_templates.items():
                user_group = self.env.ref("department_groups." + group_name)
                email_list = [usr.partner_id.email for usr in user_group.users if usr.partner_id.email]
                email_recipients.extend(email_list)

            email_to = ",".join(email_recipients)
            mail_template = self.env.ref('vehicle_request.mail_template_dept_head_approval')

            mail_template.write({
                'email_to': email_to,
                'email_from': self.env.user.email,
            })

            mail_template.send_mail(self.id, force_send=True)

    def action_md_approval(self):
        for record in self:
            record.status = 'md_approval'

        group_templates = {
            "group_fleet_manager": "mail_template_md_approval_fleet_manager",
            "group_mngmt_md": "mail_template_md",
        }

        email_recipients = []

        for group_name, template_name in group_templates.items():
            user_group = self.env.ref("department_groups." + group_name)
            email_list = [usr.partner_id.email for usr in user_group.users if usr.partner_id.email]
            email_recipients.extend(email_list)

        email_to = ",".join(email_recipients)
        mail_template = self.env.ref('vehicle_request.mail_template_finance_manager_approval')

        mail_template.write({
            'email_to': email_to,
            'email_from': self.env.user.email,
        })

        mail_template.send_mail(self.id, force_send=True)

    def action_closed(self):
        for record in self:
            record.status = 'closed'

        mail_template_user = self.env.ref('vehicle_request.mail_template_managing_director_approval')
        mail_template_user.write({
            'email_to': self.user_id.partner_id.email,
            'email_from': self.env.user.email,

        })
        mail_template_user.send_mail(self.id, force_send=True)

        group_templates = {
            "group_fleet_manager": "mail_template_md_approval_fleet_manager",
            "group_finance_mgr":
                "mail_template_fin_mgr",
            "group_finance_purchase_exec":
                "mail_template_purchase",
            "group_finance_accountant":
                "mail_template_fin_acc",
            "group_finance_asst_mgr_accountant":
                "mail_template_fin_asst_mgr",

        }

        email_recipients = []

        for group_name, template_name in group_templates.items():
            user_group = self.env.ref("department_groups." + group_name)
            email_list = [usr.partner_id.email for usr in user_group.users if usr.partner_id.email]
            email_recipients.extend(email_list)

        email_to = ",".join(email_recipients)
        mail_template = self.env.ref('vehicle_request.mail_template_md_approval')

        mail_template.write({
            'email_to': email_to,
            'email_from': self.env.user.email,
        })

        mail_template.send_mail(self.id, force_send=True)

    def action_view_purchase_order(self):
        self.ensure_one()
        if self.purchase_order_id:
            action = self.env.ref('purchase.purchase_form_action').sudo().read()[0]
            action['domain'] = [('vehicle_request_id', '=', self.id)]
            return action
        else:
            raise UserError('No purchase order has been created for this vehicle request yet.')

    def action_create_purchase_order(self):
        for record in self:
            record.is_purchase_order = True
        product = self.env['product.template'].browse(
            self.env.ref('vehicle_request.product_vehicle_1').id)
        vehicle_product = self.env['product.product'].search([('product_tmpl_id', '=', product.id)], limit=1)
        quotation_lines = []
        for vehicle in self.vehicle_request_line_ids:
            quotation_line = (0, 0, {
                'product_id': vehicle_product.id,
                'name': vehicle.model,
                'product_qty': vehicle.fleet_quantity,
                'price_unit': 0
            })
            quotation_lines.append(quotation_line)
        quotation_values = {
            'partner_id': self.partner_id.id,
            'order_line': quotation_lines,
        }
        purchase_order = self.env['purchase.order'].create(quotation_values)
        self.purchase_order_id = purchase_order.id
        self.purchase_order_id.vehicle_request_id = self.id
        for record in self:
            if record.purchase_order_id:
                purchase_order = record.purchase_order_id
                record.message_post_with_source(
                    'vehicle_request.purchase_order_summary',
                    render_values={'self': record, 'origin': purchase_order},
                )

    def action_rejected(self):

        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }
