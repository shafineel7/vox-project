from odoo import models, fields, api


class VehicleRequestLine(models.Model):
    _name = "vehicle.request.line"
    _description = "This is a model for storing vehicle request form details"
    _rec_name = "model"

    model = fields.Char(string='Model')
    fleet_type = fields.Selection(selection=[('internal', 'Internal'), ('external', 'External')], string='Fleet Type',
                                  )
    fleet_category = fields.Selection(string='Fleet Category',
                                      selection=[("permanent", "Permanent"), ("temporary", "Temporary")])
    fleet_quantity = fields.Text(string='Fleet Quantity', )
    remarks = fields.Text(string='Remarks')
    vehicle_request_id = fields.Many2one('vehicle.request', string="Vehicle Request")
    seating_capacity = fields.Integer(string="Seating Capacity")
    addition = fields.Selection(string="Addition",
                                selection=[("yes", "Yes"), ("no", "No")])
    replacement = fields.Selection(string="Replacement",
                                   selection=[("yes", "Yes"), ("no", "No")], default=False)
    replace_veh_no = fields.Char(string="Replacement Fleet No")
    driver_required = fields.Selection(string="Driver Required",
                                       selection=[("yes", "Yes"), ("no", "No")])

