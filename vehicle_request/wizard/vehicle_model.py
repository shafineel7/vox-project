from odoo import models, fields


class VehicleModel(models.Model):
    _name = "vehicle.model"
    _description = "Vehicle Model Confirmation"

    def confirm_action(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')
        if active_model == 'vehicle.request' and active_id:
            vehicle_request = self.env[active_model].browse(active_id)
            vehicle_request.is_vehicle_model = True
