from odoo import models, fields


class RejectReason(models.TransientModel):
    _inherit = "reject.reason"

    def confirm_action(self):

        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model == 'vehicle.request' and active_id:
            vehicle_request = self.env[active_model].browse(active_id)
            vehicle_request.reject_reasons = self.name
            vehicle_request.message_post(body=f"Reason for Rejection is {vehicle_request.reject_reasons}")
            user_email = vehicle_request.user_id.email
            vehicle_request.status = 'rejected'
            mail_template = self.env.ref('vehicle_request.mail_template_reject_vehicle_request')
            mail_template.write({
                'email_to': user_email,
                'email_from': self.env.user.email,
            })
            mail_template.send_mail(vehicle_request.id, force_send=True)

        return super().confirm_action()
