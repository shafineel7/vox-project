from odoo import models, fields, api


class HrAppraisal(models.Model):
    _inherit = 'hr.appraisal'

    company_id = fields.Many2one('res.company', string='Company', readonly=True)
    branches_ids = fields.Many2many('res.company', string='Branch', readonly=True)
    employee_name = fields.Char(string='Employee Name', readonly=True)

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for record in self:
            if record.employee_id:
                record.write({
                    'company_id': record.employee_id.company_id.id,
                    'branches_ids': [(6, 0, record.employee_id.branches_ids.ids)],
                    'employee_name': record.employee_id.name
                })
