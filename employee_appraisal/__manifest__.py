{
    'name': 'Employee Appraisal',
    'author': 'Sourav',
    'summary': 'Module for showing employee details appraisal views.',
    'version': '17.0.1.0.0',
    'category': 'Human Resources/Appraisals',
    'website': 'https://www.voxtronme.com',
    'depends': ['hr_appraisal', 'employee'],
    'description': """Extra information about employee, such as company, branch, department will be available
    in employee appraisal views.""",
    'data': [
        'views/hr_appraisal_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
