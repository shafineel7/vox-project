# -*- coding: utf-8 -*-

from odoo import models, fields, Command
from docxtpl import DocxTemplate
import base64
import os


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    def generate_docx_file(self):
        print(self.stage_id.stage_code)
        template_path = "/home/voxtron/PycharmProjects/TT_MAX/trumax/crm_contracts_docx/contract_proposal_template/ProposalNew.docx"
        document = DocxTemplate(template_path)

        context = {
            'estimations': enumerate(self.lead_estimation_ids),
            'date': self.submit_date,
            'name': self.display_name
        }

        document.render(context)

        temp_file_path = f'/home/voxtron/PycharmProjects/TT_MAX/trumax/crm_contracts_docx/contract_proposal_docx/{self.display_name}.docx'

        document.save(temp_file_path)

        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()

        docx_base64 = base64.b64encode(docx_content)

        attachment = self.env['ir.attachment'].sudo().create({
            'name': f"{self.display_name}.docx",
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'crm.lead',
            'res_id': self.id,
            'store_fname': f"{self.display_name}.docx"
        })


        os.remove(temp_file_path)

        url = '/download_cp/docx/%d' % attachment.id

        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self'
        }

    def generate_docx_attach(self):
        template_path = "/home/voxtron/PycharmProjects/TT_MAX/trumax/crm_contracts_docx/contract_proposal_template/ProposalNew.docx"
        document = DocxTemplate(template_path)
        context = {
            'estimations': enumerate(self.lead_estimation_ids),
            'date': self.submit_date,
            'name': self.display_name
        }
        document.render(context)

        temp_file_path = f'/home/voxtron/PycharmProjects/TT_MAX/trumax/crm_contracts_docx/contract_proposal_docx/{self.display_name}.docx'
        document.save(temp_file_path)
        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()
        docx_base64 = base64.b64encode(docx_content)
        attachment = self.env['ir.attachment'].sudo().create({
            'name': f"{self.display_name}.docx",
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'crm.lead',
            'res_id': self.id,
            'store_fname': f"{self.display_name}.docx"
        })

        os.remove(temp_file_path)
        return attachment

    def submit_quote(self):
        res = super(CrmLead, self).submit_quote()
        if 'context' in res:
            ctx = res['context']
            attachment = self.generate_docx_attach()
            if attachment:
                ctx['default_attachment_ids'] = [Command.link(attachment.id)]
                res['context'] = ctx
        return res



