{
    'name': 'Purchase Order Document',
    'author': 'Sourav',
    'summary': 'Module for Downloading Purchase Order PDF report',
    'version': '17.0.1.0.0',
    'category': 'Inventory/Purchase',
    'website': 'https://www.voxtronme.com',
    'depends': ['purchase_management', 'web'],
    'description': """This module is for creating Purchase Order PDF report""",
    'data': [
        'report/purchase_tax_group_totals_template.xml',
        'report/purchase_tax_totals_template.xml',
        'report/purchase_order_doc.xml',
        'report/ir_actions_report.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
