# -*- coding: utf-8 -*-
{
    'name': 'Department Groups',
    'version': '17.0.1.1.0',
    'summary': 'The module is used to manage department groups',
    'description': """The module handles different department group,
    such as construction,staffing,soft services,finance,HR,PRO,management 
    and agent
    """,
    'category': 'Human Resources',
    'author': 'Muhammed Aslam',
    'company': 'Voxtron Solutions LLP',
    'website': 'https://www.voxtronme.com',
    'depends': ['base'],
    'data': [
        'security/department_groups.xml',
        'data/ir_module_category_data.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
