# -*- coding: utf-8 -*-
{
    'name': "HR Employee Warning Management",

    'summary': "HR employee warning management",

    'description': """
        This app allows HR Assistant, Camp manager and operation coordinators to create warning notices to employees for breaching company policy and rules. 
        Allowed users can send the confirmed notices through mail and also can post fine if there is any.
    """,
    'author': "Arya I R",
    'category': 'Human Resources/HR Warning',
    'version': '17.0.1.0.2',

    # any module necessary for this one to work correctly
    'depends': ['department_groups', 'employee', 'expense_management'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/hr_employee_warning_security.xml',
        'data/ir_sequence_data.xml',
        'data/warning_letter_report_paperformat.xml',
        'data/mail_template_data.xml',
        'data/product_data.xml',
        'views/product_views.xml',
        'views/hr_employee_warning_views.xml',
        'views/hr_employee_warning_menus.xml',
        'views/hr_employee_views.xml',
        'views/expense_booking_views.xml',
        'reports/header_footer_template.xml',
        'reports/warning_letter_report.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}

