# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, AccessError


class HrEmployeeWarning(models.Model):
    _name = 'hr.employee.warning'
    _description = 'Hr Employee Warning'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _get_default_hr_manager_id(self):
        user = self.env['res.users'].search([('groups_id', '=', self.env.ref("department_groups.group_hr_mgr").id),
                                             ('company_ids', '=', self.env.company.id)], limit=1)
        return user.id

    name = fields.Char(string="Notice Reference", required=True, copy=False, readonly=False, index='trigram',
                       default=lambda self: _('New'))
    employee_id = fields.Many2one('hr.employee', string='Employee ID', required=True,
                                  domain="[('company_id', '=', company_id)]")
    employee_name = fields.Char(string='Employee Name', related="employee_id.name")
    designation_id = fields.Many2one('designation', string='Designation', related="employee_id.designation_id")
    remarks = fields.Char(string="Remarks")
    reason = fields.Html(string="Reason", required=True)
    type = fields.Selection([
        ('direct', 'Direct'),
        ('indirect', 'In Direct'),
    ], string='Type', related="employee_id.type", required=True)
    company_id = fields.Many2one('res.company', required=True, default=lambda self: self.env.company)
    currency_id = fields.Many2one(
        string='Company Currency', related='company_id.currency_id', readonly=True, )
    date = fields.Date(default=lambda self: fields.Date.today(), required=True, copy=False)
    created_by_user = fields.Selection([('hr_assistant', 'HR Assistant'), ('camp_manager', 'Camp Manager'),
                                        ('operations_coord', 'Operations Coordinator')], string="Created By")
    if_any_fine = fields.Boolean(string="Any Fine?", default=False, copy=False)
    employee_disciplinary_fine_ids = fields.One2many('employee.disciplinary.fine', 'hr_employee_warning_id',
                                                     string="Disciplinary Fines")
    total_disciplinary_fine = fields.Monetary(string='Total Fine', compute='_compute_total_disciplinary_fine', store=True, copy=False)
    state = fields.Selection([
        ('draft', 'New'),
        ('sent', 'Sent'),
        ('cancel', 'Cancelled')
    ], string='Status', copy=False, tracking=True, help='Status of the employee warning', default='draft')
    is_self_assigned = fields.Boolean(default=False, string="Is self Assigned?", copy=False)
    self_assigned_user_id = fields.Many2one('res.users', string="Assigned User", copy=False)
    move_ids = fields.Many2many('account.move', string="Fine Entries", copy=False)
    hr_signed_warning_letter = fields.Binary(string="HR Signed Warning Letter", copy=False)
    hr_signed_warning_letter_name = fields.Char(copy=False)
    employee_signed_warning_letter = fields.Binary(string="Employee Signed Warning Letter", copy=False)
    employee_signed_warning_letter_name = fields.Char(copy=False)
    hr_manager_id = fields.Many2one(
        'res.users', string="HR Manager",
        domain=lambda self: "[('groups_id', '=', {}), ('company_ids', '=', company_id)]".format(
            self.env.ref("department_groups.group_hr_mgr").id), default=_get_default_hr_manager_id, required=True)
    op_coordinator_user_ids = fields.Many2many('res.users', 'op_coordinator_user_warning_rel', 'warning_id', 'user_id',
                                               compute='_compute_op_coordinator_user_ids', store=True)
    is_self_assigned_user = fields.Boolean(compute="_compute_is_self_assigned_user")
    expense_booking_ids = fields.Many2many('expense.booking', string='Expense Bookings')
    expense_booking_count = fields.Integer(string="Expense Booking Count", compute='_compute_expense_booking_count')

    @api.depends('expense_booking_ids')
    def _compute_expense_booking_count(self):
        for record in self:
            if record.expense_booking_ids:
                record.expense_booking_count = len(record.expense_booking_ids)
            else:
                record.expense_booking_count = 0

    def _compute_is_self_assigned_user(self):
        for record in self:
            if record.self_assigned_user_id and self.env.user == record.self_assigned_user_id:
                record.is_self_assigned_user = True
            else:
                record.is_self_assigned_user = False

    @api.constrains('employee_id')
    def _check_employee_type(self):
        for record in self:
            if not record.employee_id.type:
                raise UserError(_("Please provide type of employee in employee master"))

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        if self.employee_id.type == 'indirect':
            self.created_by_user = 'hr_assistant'
        else:
            if self.employee_id.camp_manager_name_id.user_id:
                self.created_by_user = 'camp_manager'
            else:
                self.created_by_user = 'operations_coord'

    @api.depends('employee_disciplinary_fine_ids.total')
    def _compute_total_disciplinary_fine(self):
        for record in self:
            if record.employee_disciplinary_fine_ids:
                record.total_disciplinary_fine = sum(record.employee_disciplinary_fine_ids.mapped('total'))
            else:
                record.total_disciplinary_fine = 0

    @api.depends('employee_id', 'employee_id.department_id.operations_coordinator_ids')
    def _compute_op_coordinator_user_ids(self):
        user_list = []
        for record in self:
            if record.employee_id.department_id.operations_coordinator_ids:
                for emp in record.employee_id.department_id.operations_coordinator_ids:
                    if emp.user_id:
                        user_list.append(emp.user_id.id)
            record.op_coordinator_user_ids = user_list

    @api.model
    def default_get(self, fields):
        if self.user_has_groups('department_groups.group_hr_asst, '
                                'department_groups.group_construction_operations_coord,'
                                'department_groups.group_staff_operations_coord,'
                                'department_groups.group_soft_ser_operations_coord,'
                                'department_groups.group_construction_camp_mgr,'
                                'department_groups.group_staff_camp_mgr,'
                                'department_groups.group_soft_ser_camp_mgr,'
                                'base.group_user'):
            return super(HrEmployeeWarning, self).default_get(fields)
        else:
            raise AccessError(_("You don't have the access rights to create warning"))

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if 'employee_id' in vals:
                employee = self.env['hr.employee'].browse(vals['employee_id'])
                if employee.type == 'indirect':
                    if not self.env.user.has_group('department_groups.group_hr_asst'):
                        raise AccessError(
                            _("You don't have the access rights to create a employee warning to indirect employees"))
                else:
                    allowed_user = []
                    for coord in employee.department_id.operations_coordinator_ids:
                        if coord.user_id:
                            allowed_user.append(coord.user_id.id)
                    if self.env.user.id not in allowed_user and self.env.user != employee.camp_manager_name_id.user_id:
                        raise AccessError(
                            _("You don't have the access rights to create a employee warning to direct employees"))
            if 'company_id' in vals:
                self = self.with_company(vals['company_id'])
            if vals.get('name', _("New")) == _("New"):
                vals['name'] = self.env['ir.sequence'].next_by_code('employee.warning') or _("New")
        return super().create(vals_list)

    def unlink(self):
        for record in self:
            if record.state == 'sent':
                raise UserError(_("You can't delete this warning letter"))
        return super().unlink()

    def action_self_assign(self):
        for record in self:
            if record.type == 'indirect':
                if not self.env.user.has_group('department_groups.group_hr_asst'):
                    raise UserError(_("You are not allowed to self assign this record"))
            else:
                if self.env.user not in record.op_coordinator_user_ids and self.env.user != record.employee_id.camp_manager_name_id.user_id:
                    raise UserError(_("You are not allowed to self assign this record"))
            record.is_self_assigned = True
            record.self_assigned_user_id = record.env.user.id

    def button_cancel(self):
        for record in self:
            record.state = 'cancel'

    def action_sent_warning_to_employee(self):
        for record in self:
            if record.self_assigned_user_id != record.env.user:
                raise AccessError(_("You don't have the access rights to send warning letter."))
            company_expense_booking = []
            partner_expense_booking = []
            for line in record.employee_disciplinary_fine_ids.filtered(lambda l: l.paid_by == 'company'):
                company_expense_booking.append(
                    (0, 0, {
                        'employee_id': record.employee_id.id,
                        'employee_name': record.employee_id.name,
                        'department_id': record.employee_id.department_id.id,
                        'quantity': 1,
                        'product_id': line.product_id.id,
                        'unit_price': line.total,
                        'tax_ids': False,
                        'name': line.name,
                    })
                )
            if len(company_expense_booking) > 0:
                expense_booking = self.env['expense.booking'].create({
                    'date': fields.Date.today(),
                    'expense_paid_by': 'company',
                    'expense_details_ids': company_expense_booking,
                    'partner_id': record.company_id.partner_id.id,
                    'company_id': record.company_id.id,
                    'hr_employee_warning_id': record.id,
                })
                res = expense_booking.action_confirm()
                record.expense_booking_ids = [(4, expense_booking.id)]
            for line in record.employee_disciplinary_fine_ids.filtered(
                    lambda l: l.paid_by == 'employee'):
                partner_expense_booking.append(
                    (0, 0, {
                        'employee_id': record.employee_id.id,
                        'employee_name': record.employee_id.name,
                        'department_id': record.employee_id.department_id.id,
                        'quantity': 1,
                        'product_id': line.product_id.id,
                        'unit_price': line.total,
                        'tax_ids': False,
                        'name': line.name,
                    })
                )
            if len(partner_expense_booking) > 0:
                expense_booking = self.env['expense.booking'].create({
                    'date': fields.Date.today(),
                    'expense_paid_by': 'customer',
                    'expense_details_ids': partner_expense_booking,
                    'partner_id': record.employee_id.work_contact_id.id,
                    'company_id': record.company_id.id,
                    'hr_employee_warning_id': record.id,
                })
                res = expense_booking.action_confirm()
                record.expense_booking_ids = [(4, expense_booking.id)]

            record.date = fields.Date.today()
            email_template = self.env.ref('hr_employee_warning.email_template_employee_warning_letter',
                                          raise_if_not_found=False)
            attachment_ids = []
            if record.hr_signed_warning_letter:
                attach_data = {
                    'name': record.hr_signed_warning_letter_name,
                    'datas': record.hr_signed_warning_letter,
                    'type': 'binary',
                    'mimetype': 'application/x-pdf',
                    'public': True,
                }
                attach_id = self.env['ir.attachment'].create(attach_data)
                attachment_ids.append(attach_id.id)
            if attachment_ids:
                email_template.write({'attachment_ids': [(6, 0, attachment_ids)]})
            if record.employee_id.work_email and record.self_assigned_user_id.partner_id.email:
                email_values = {'email_to': record.employee_id.work_email,
                                'email_from': record.self_assigned_user_id.partner_id.email}
                email_template.send_mail(record.id, email_values=email_values, force_send=True)
            email_template.attachment_ids = False
            record.state = 'sent'

    def action_view_expense_booking(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("expense_management.expense_booking_action")
        action['domain'] = [('id', 'in', self.expense_booking_ids.ids)]
        action['context'] = dict(self._context, create=False)
        return action
