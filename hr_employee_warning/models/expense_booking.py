# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ExpenseBooking(models.Model):
    _inherit = 'expense.booking'

    hr_employee_warning_id = fields.Many2one('hr.employee.warning', 'Employee Warning Ref', copy=False)
