# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class EmployeeDisciplinaryFine(models.Model):
    _name = "employee.disciplinary.fine"
    _description = "Employee Disciplinary Fine"

    def _get_default_product_id(self):
        expense = self.env['product.product'].search([('is_additional_deduction', '=', True), ('can_be_expensed', '=', True)], limit=1)
        return expense.id

    name = fields.Char("Description", required=True)
    product_id = fields.Many2one(
        'product.product', string='Expense Category', required=True, default=_get_default_product_id,
        domain=[('is_additional_deduction', '=', True), ('can_be_expensed', '=', True)])
    total = fields.Monetary(string='Amount', required=True, digits="Product Price")
    hr_employee_warning_id = fields.Many2one(
        'hr.employee.warning', string='HR Employee Warning Ref', required=True, ondelete='cascade')
    company_id = fields.Many2one(related="hr_employee_warning_id.company_id")
    currency_id = fields.Many2one(
        string='Company Currency',
        related='company_id.currency_id', readonly=True,
    )
    paid_by = fields.Selection([('employee', 'Employee'), ('company', 'Company')], string="Paid By", required=True, default='employee')

    @api.constrains('total')
    def _check_total(self):
        for record in self:
            if record.total <= 0:
                raise ValidationError("Amount must be greater than zero.")
