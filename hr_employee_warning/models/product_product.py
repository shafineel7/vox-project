# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ProductProduct(models.Model):
    _inherit = 'product.product'

    is_additional_deduction = fields.Boolean(string="Additional Deduction", default=False, copy=False)
