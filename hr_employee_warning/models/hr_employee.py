# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class HrEmployeePrivate(models.Model):
    _inherit = "hr.employee"

    hr_employee_warning_ids = fields.One2many('hr.employee.warning', 'employee_id', string='Employee Warnings')
    warning_counts = fields.Integer(compute='_compute_warning_counts', string='Warning Count')
    warning_current_contract = fields.Integer(compute='_compute_warning_current_contract', string='Warning Count', store=True)

    @api.depends('contract_id', 'hr_employee_warning_ids.state')
    def _compute_warning_current_contract(self):
        for record in self:
            if record.hr_employee_warning_ids and record.contract_id:
                if record.contract_id.date_start and not record.contract_id.date_end:
                    warnings = record.hr_employee_warning_ids.filtered(lambda l: l.state == 'sent' and l.date > record.contract_id.date_start)
                else:
                    warnings = record.hr_employee_warning_ids.filtered(
                        lambda l: l.state == 'sent' and record.contract_id.date_end > l.date > record.contract_id.date_start)

                record.warning_current_contract = len(warnings.ids) if warnings else 0
            else:
                record.warning_current_contract = 0

    def _compute_warning_counts(self):
        for record in self:
            if record.hr_employee_warning_ids:
                warnings = record.hr_employee_warning_ids.filtered(lambda l: l.state == 'sent')
                record.warning_counts = len(warnings.ids) if warnings else 0
            else:
                record.warning_counts = 0

    def action_open_hr_employee_warning(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('hr_employee_warning.hr_employee_warning_action')
        action['views'] = [(False, 'form')]
        if not self.hr_employee_warning_ids:
            if self.env.user.has_group('department_groups.group_hr_asst') and self.type == 'indirect':
                action['context'] = {
                    'default_employee_id': self.id,
                }
                action['target'] = 'new'
            elif (self.user_has_groups(
                    'department_groups.group_construction_operations_coord,'
                    'department_groups.group_staff_operations_coord,'
                    'department_groups.group_soft_ser_operations_coord,'
                    'department_groups.group_construction_camp_mgr,'
                    'department_groups.group_staff_camp_mgr,'
                    'department_groups.group_soft_ser_camp_mgr')
            ) and self.type == 'direct':
                action['context'] = {
                    'default_employee_id': self.id,
                }
                action['target'] = 'new'
            else:
                action = {'type': 'ir.actions.act_window_close'}
        else:
            warnings = self.hr_employee_warning_ids.filtered(lambda l: l.state == 'sent')
            if len(warnings.ids) > 1:
                action['domain'] = [('id', 'in', warnings.ids)]
                action['views'] = [(False, 'tree'), (False, 'form')]
            else:
                form_view = [(self.env.ref('hr_employee_warning.hr_employee_warning_view_form').id, 'form')]
                if 'views' in action:
                    action['views'] = form_view + [(state, view) for state, view in action['views'] if view != 'form']
                else:
                    action['views'] = form_view
                action['res_id'] = warnings.id
        return action
