# -*- coding: utf-8 -*-
from odoo import models, fields


class Partner(models.Model):
    _inherit = 'res.partner'

    type = fields.Selection(
        selection_add=[('head_office', 'Head Office Address')],
    )
