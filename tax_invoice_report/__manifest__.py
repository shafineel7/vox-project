# -*- coding: utf-8 -*-
{
    'name': 'Tax Invoice Document',
    'version': '17.0.1.0.0',
    'category': 'Accounting/Accounting',
    'summary': 'Module for Downloading Tax Invoice with Head Office Address PDF Report',
    'description': """This module is for creating Tax Invoice with Head Office Address PDF report.""",
    'author': 'Anoopa',
    'website': 'https://www.voxtronme.com',
    'depends': ['account_accountant', 'purchase'],
    'data': [
        'report/header_footer_template.xml',
        'report/tax_invoice_report.xml',
        'report/tax_invoice_report_template.xml',
        'views/res_partner_views.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'AGPL-3',
}
