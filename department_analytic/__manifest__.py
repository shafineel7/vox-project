# -*- coding: utf-8 -*-
{
    'name': 'Department Analytic Account Mapping',
    'version': '17.0.1.0.2',
    'category': 'accounting',
    'summary': 'Module automatically creates analytic account for departments created.',
    'description': """Module automatically creates analytic account for departments created.""",
    'author': 'Neenu R',
    'website': 'http://www.voxtronme.com',
    'depends': ['hr', 'account_accountant', 'department_groups'],
    'data': [
        'data/analytic_plan_data.xml',
        'data/mail_template_data.xml',
        'data/ir_cron.xml',
        'views/hr_department_views.xml'
    ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'AGPL-3',
    'images': [],
}
