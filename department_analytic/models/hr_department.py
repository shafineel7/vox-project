# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HrDepartment(models.Model):
    _inherit = 'hr.department'

    analytic_account_id = fields.Many2one('account.analytic.account', string="Analytic Account")

    def add_analytic_account(self):
        if not self.analytic_account_id:
            plan = self.env.ref('department_analytic.analytic_plan_department').id
            if self.name:
                analytic_account = self.env['account.analytic.account'].sudo().create({
                    'name': self.name,
                    'plan_id': plan,
                })
                if analytic_account:
                    self.analytic_account_id = analytic_account.id

    @api.model_create_multi
    def create(self, vals_list):
        res = super(HrDepartment, self).create(vals_list)
        for vals in res:
            if vals.name:
                vals.add_analytic_account()
        return res




