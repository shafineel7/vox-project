# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'

    def _cron_send_analytic_balance_notify(self):
        analytic_accounts = self.env['account.analytic.account'].sudo().search([])
        analytic_account = self.env['account.analytic.account'].sudo()
        for accounts in analytic_accounts:
            lines = self.env['account.analytic.line'].search([('auto_account_id', '=', accounts.id)])
            if lines:
                amt = sum(lines.mapped('amount'))
                if amt < 0:
                    analytic_account += accounts
        cost_centers = []
        if analytic_account:
            fm_group = self.env.ref('department_groups.group_finance_mgr')
            if fm_group:
                users = fm_group.users
                if users:
                    emails = []
                    email_to = ''
                    for user in users:
                        if user.email:
                            emails.append(user.email)
                    email_to = ','.join(emails)
                    count = 1
                    accounts = self.env['account.analytic.account']
                    for accounts in analytic_account:
                        cost_centers.append({'sr': count, 'name': accounts.name})
                        count += 1
                    self.env.ref('department_analytic.email_negative_cost_center').with_context(
                        email_to=email_to, cost_centers=cost_centers
                    ).send_mail(accounts.id)




