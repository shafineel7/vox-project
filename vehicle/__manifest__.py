# -*- coding: utf-8 -*-

{
    'name': 'Vehicle',
    'author': 'Sangeeth, Neema',
    'summary': 'Module for managing the information about the vehicles.',
    'version': '17.0.1.2.0',
    'category': 'Fleet',
    'depends': ['fleet', 'mail', 'department_groups', 'purchase'],
    'description': "It includes Information about the fleets",
    'data': [
        'security/ir.model.access.csv',
        'security/vehicle_security.xml',
        'data/vehicle_data.xml',
        'data/ir_cron_data.xml',
        'data/mail_template_data.xml',
        'views/fleet_vehicle_views.xml',
        'views/fleet_vehicle_cost_views.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
