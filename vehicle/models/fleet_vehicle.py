# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions
from datetime import datetime, timedelta


class FleetVehicle(models.Model):
    _inherit = 'fleet.vehicle'

    rf_no = fields.Char(string='Serial NO', required=True)
    vendor_supplier_id = fields.Many2one('res.partner', string='Vendor', domain="[('supplier_rank', '=', 1)]")
    vehicle_supplier = fields.Selection([
        ('benz', 'Benz'), ('ferrari', 'Ferrari'), ('lamborghini', 'Lamborghini')
    ], string='Vehicle Supplier')
    agreement_number = fields.Char(string='Agreement number')
    lease = fields.Selection([
        ('lease', 'Lease'), ('monthly', 'Monthly')
    ], string='Duration')
    driver_preference = fields.Selection([('with_driver', 'With Driver'),
                                          ('without_driver', 'Without Driver')], string='Driver Preference')
    km_allowed_yearly = fields.Float(string='KM Allowed yearly')
    excess_km_charges = fields.Float(string='Excess KM charges')
    make = fields.Char('Make')
    fleet_model = fields.Char('Model')
    vehicle_no = fields.Char('Vehicle No')
    fleet_type = fields.Selection([
        ('internal', 'Internal'), ('external', 'External')
    ], string='Fleet type')
    fleet_purpose = fields.Selection([
        ('delivery', 'Delivery'), ('camp', 'Camp')
    ], string='Fleet purpose')
    fleet_status = fields.Selection([
        ('own', 'Own'), ('hired', 'Hired')
    ], string='Fleet status')
    begin_date = fields.Date(string='Begin date')
    expiry_date = fields.Date(string='Expiry date')
    department_id = fields.Many2one('hr.department', string='Department')
    mulkiya_expiry_date = fields.Date(string='Mulkiya expiry date')
    insurance_expiry_date = fields.Date(string='Insurance expiry date')
    site_or_project = fields.Char(string='Site/ Project')
    monthly_rent = fields.Float(string='Monthly rent')
    penalty_charges = fields.Float(string='Penalty charges/ Termination charges')
    lpo_number = fields.Char(string='LPO number')
    replacement = fields.Selection([('yes', 'Yes'), ('no', 'No')], string='Replacement', default=False)
    replacement_fleet_no = fields.Char(string='Replacement fleet no')
    disallowed_expense_rate = fields.Float(string='Disallowed expense rate')
    fleet_category = fields.Selection(string='Fleet Category',
                                      selection=[("permanent", "Permanent"), ("temporary", "Temporary")])
    lpo_attachment = fields.Binary(string='LPO Attachment', attachment=True)
    lpo_attachment_filename = fields.Char(string='Lpo Attachment Name')
    contract_start_date = fields.Date(string="Contract Start Date", compute='_compute_contract_start_date')
    contract_end_date = fields.Date(string="Contract End Date", compute='_compute_contract_end_date')
    lpo_end_date = fields.Date(string="LPO End Date")

    def _get_manager_field(self):
        users_search = self.env['res.users'].search([])
        users = []
        for user in users_search:
            if user.has_group('department_groups.group_fleet_manager'):
                users.append(user.id)
        return [('id', 'in', users)]

    manager_id = fields.Many2one(
        'res.users', 'Fleet Manager',
        domain=_get_manager_field, )

    drivers_id = fields.Many2one('hr.employee', string='Driver')

    @api.constrains('fleet_category', 'lpo_attachment')
    def _check_lpo_attachment_required(self):
        for record in self:
            if record.fleet_category == 'temporary' and not record.lpo_attachment:
                raise exceptions.ValidationError("LPO Attachment is required when Fleet Category is set to Temporary.")

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if 'rf_no' not in vals:
                vals['rf_no'] = self.env['ir.sequence'].next_by_code('rf.no') or 'New'
        return super(FleetVehicle, self).create(vals_list)

    @api.depends('log_contracts', 'log_contracts.start_date')
    def _compute_contract_start_date(self):
        for rec in self:
            contracts = rec.log_contracts.filtered(lambda c: c.state not in ['expired', 'closed'])
            if contracts:
                rec.contract_start_date = contracts[0].start_date
            else:
                rec.contract_start_date = False

    @api.depends('log_contracts', 'log_contracts.expiration_date')
    def _compute_contract_end_date(self):
        for rec in self:
            contracts = rec.log_contracts.filtered(lambda c: c.state not in ['expired', 'closed'])
            if contracts:
                rec.contract_end_date = contracts[0].expiration_date
            else:
                rec.contract_end_date = False

    def send_contract_pre_reminder(self):
        group_templates = {
            "group_fleet_manager": "mail_template_md_approval_fleet_manager",
            "group_finance_mgr": "mail_template_finance_mgr",
            "group_finance_asst_mgr_accountant": "mail_template_asst_mgr_acc",
            "group_finance_accountant": "mail_template_finance_acc",
            "group_finance_purchase_exec": "mail_template_purchase_exec"
        }

        email_recipients = []

        for group_name, template_name in group_templates.items():
            user_group = self.env.ref("department_groups." + group_name)
            email_list = [usr.partner_id.email for usr in user_group.users if usr.partner_id.email]
            email_recipients.extend(email_list)

        email_to = ",".join(email_recipients)

        mail_template_1 = self.env.ref('vehicle.mail_template_contract_lpo_expiry_reminder')
        mail_template_2 = self.env.ref('vehicle.mail_template_contract_mulkiya_expiry_reminder')
        mail_template_3 = self.env.ref('vehicle.mail_template_contract_insurance_expiry_reminder')
        mail_template_4 = self.env.ref('vehicle.mail_template_contract_pre_reminder')

        today = datetime.now().date()
        seven_days_ago = today + timedelta(days=7)

        vehicles = self.env['fleet.vehicle'].search([
            '|', '|',
            ('lpo_end_date', '=', seven_days_ago),
            ('mulkiya_expiry_date', '=', seven_days_ago),
            ('insurance_expiry_date', '=', seven_days_ago)
        ])

        contracts_exp = self.env['fleet.vehicle.log.contract'].search([
            ('expiration_date', '=', seven_days_ago)
        ])

        for vehicle in vehicles:
            if vehicle.lpo_end_date == seven_days_ago:
                mail_template = mail_template_1
                mail_template.sudo().write({
                    'email_to': email_to,
                    'email_from': self.env.user.email,
                })
                mail_template.sudo().send_mail(vehicle.id, force_send=True)

            if vehicle.mulkiya_expiry_date == seven_days_ago:
                mail_template = mail_template_2
                mail_template.sudo().write({
                    'email_to': email_to,
                    'email_from': self.env.user.email,
                })
                mail_template.sudo().send_mail(vehicle.id, force_send=True)

            if vehicle.insurance_expiry_date == seven_days_ago:
                mail_template = mail_template_3
                mail_template.sudo().write({
                    'email_to': email_to,
                    'email_from': self.env.user.email,
                })
                mail_template.sudo().send_mail(vehicle.id, force_send=True)

        for contract in contracts_exp:
            mail_template = mail_template_4

            mail_template.write({
                'email_to': email_to,
                'email_from': self.env.user.email,
            })
            mail_template.send_mail(contract.id, force_send=True)

    def send_contract_reminder(self):

        group_templates = {
            "group_fleet_manager": "mail_template_md_approval_fleet_manager",
            "group_finance_mgr": "mail_template_finance_mgr",
            "group_finance_asst_mgr_accountant":
                "mail_template_asst_mgr_acc",
            "group_finance_accountant":
                "mail_template_finance_acc",
            "group_finance_purchase_exec": "mail_template_purchase_exec"

        }
        email_recipients = []

        for group_name, template_name in group_templates.items():
            user_group = self.env.ref("department_groups." + group_name)
            email_list = [usr.partner_id.email for usr in user_group.users if usr.partner_id.email]
            email_recipients.extend(email_list)

        email_to = ",".join(email_recipients)
        mail_template = self.env.ref('vehicle.mail_template_contract_post_reminder')

        vehicles = self.env['fleet.vehicle'].search([])

        today = datetime.now().date()
        thirty_days_ago = today - timedelta(days=30)

        vehicles_without_contracts = vehicles.search(
            [('log_contracts', '=', False), ('fleet_category', '=', 'permanent'),
             ('acquisition_date', '=', thirty_days_ago)])

        for vehicle in vehicles_without_contracts:
            mail_template.write({
                'email_to': email_to,
                'email_from': self.env.user.email,
            })
            mail_template.send_mail(vehicle.id, force_send=True)

    @api.model
    def default_get(self, fields):
        if self.env.user.has_group('department_groups.group_mngmt_md') or self.env.user.has_group(
                'base.group_system') or self.env.user.has_group(
            'department_groups.group_finance_mgr') or self.env.user.has_group(
            'department_groups.group_fleet_manager') or self.env.user.has_group(
            'department_groups.group_finance_asst_mgr_accountant') or self.env.user.has_group(
              'department_groups.group_finance_accountant') or self.env.user.has_group(
              'department_groups.group_finance_purchase_exec'):
            return super(FleetVehicle, self).default_get(fields)
        else:
            raise exceptions.ValidationError("You are not allowed to create vehicles")
