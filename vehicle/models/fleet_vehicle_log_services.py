from odoo import models, fields, api, exceptions


class FleetVehicleLogServices(models.Model):
    _inherit = 'fleet.vehicle.log.services'

    fleet_driver_id = fields.Many2one('hr.employee', string='Driver', related='vehicle_id.drivers_id')

    @api.model
    def default_get(self, fields):
        if self.env.user.has_group('department_groups.group_mngmt_md') or self.env.user.has_group(
                'base.group_system') or self.env.user.has_group(
                 'department_groups.group_fleet_manager'):
            return super(FleetVehicleLogServices, self).default_get(fields)
        else:
            raise exceptions.ValidationError("You are not allowed to create vehicle Services")

