from odoo import models, fields, api


class FleetVehicleOdometer(models.Model):
    _inherit = 'fleet.vehicle.odometer'

    fleet_driver_id = fields.Many2one('hr.employee', string='Driver', related='vehicle_id.drivers_id')
