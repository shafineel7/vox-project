from odoo import models, fields, api, exceptions
from datetime import datetime, timedelta


class FleetVehicleLogContract(models.Model):
    _inherit = 'fleet.vehicle.log.contract'

    fleet_driver_id = fields.Many2one('hr.employee', string='Driver', related='vehicle_id.drivers_id')

    @api.model
    def create(self, vals):
        vehicle_id = vals.get('vehicle_id')
        existing_contract = self.search([
            ('vehicle_id', '=', vehicle_id),
            ('state', 'not in', ['expired', 'closed'])
        ], limit=1)

        if existing_contract:
            raise exceptions.ValidationError('A contract for this vehicle already exists')

        return super(FleetVehicleLogContract, self).create(vals)

    @api.model
    def default_get(self, fields):
        if self.env.user.has_group('department_groups.group_mngmt_md') or self.env.user.has_group(
                'base.group_system') or self.env.user.has_group(
                 'department_groups.group_fleet_manager'):
            return super(FleetVehicleLogContract, self).default_get(fields)
        else:
            raise exceptions.ValidationError("You are not allowed to create vehicle contracts")
