# -*- coding: utf-8 -*-
{
    'name': 'Employee Recruitment Onboarding',
    'version': '17.0.1.0.0',
    'category': 'Human Resources',
    'summary': 'Employee Recruitment Onboarding',
    'description': """
        Employee onboarding creation via Recruitment
    """,
    'author': 'HILSHA P H',
    'website': 'https://www.voxtronme.com',
    'depends': ['product','account','hr_expense','manpower_overseas','employee_onboarding',],
    'data': [
        'data/mail_template_data.xml',
        'data/employee_onoarding_cron.xml',
        'views/product_expense_views.xml',
        'views/hr_expense_sheet_views.xml',
        'views/hr_expense_views.xml',
        'views/employee_onboarding_local_views.xml',
        'views/employee_onboarding_overseas_views.xml',
    ],

    'license': 'LGPL-3',
    'installable': True,
    'application': True,
    'auto_install': False,

}
