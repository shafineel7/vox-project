# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions


class HrExpenseSheet(models.Model):
    _inherit = 'hr.expense.sheet'

    employee_onboarding_id = fields.Many2one('employee.onboarding', readonly=True)
    onboarding_employee_id = fields.Many2one('hr.employee',string="Onboarding Employee")


