
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, Command, models, _
from odoo.exceptions import UserError, ValidationError


class HrExpense(models.Model):
    _inherit = "hr.expense"

    employee_onboarding_id = fields.Many2one('employee.onboarding', readonly=True)
    onboarding_employee_id = fields.Many2one('hr.employee',string="Onboarding Employee")


    def _prepare_payments_vals(self):
        self.ensure_one()

        journal = self.sheet_id.journal_id
        payment_method_line = self.sheet_id.payment_method_line_id
        if not payment_method_line:
            raise UserError(_("You need to add a manual payment method on the journal (%s)", journal.name))
        move_lines = []
        tax_data = self.env['account.tax']._compute_taxes([
            self._convert_to_tax_base_line_dict(price_unit=self.total_amount_currency, currency=self.currency_id)
        ])
        rate = abs(self.total_amount_currency / self.total_amount) if self.total_amount else 1.0
        base_line_data, to_update = tax_data['base_lines_to_update'][0]  # Add base line
        amount_currency = to_update['price_subtotal']
        expense_name = self.name.split("\n")[0][:64]
        if self.employee_onboarding_id and self.onboarding_employee_id:
            base_move_line = {
                'name': f'{self.onboarding_employee_id.name}: {expense_name}',
                'account_id': base_line_data['account'].id,
                'product_id': base_line_data['product'].id,
                'analytic_distribution': base_line_data['analytic_distribution'],
                'expense_id': self.id,
                'tax_ids': [Command.set(self.tax_ids.ids)],
                'tax_tag_ids': to_update['tax_tag_ids'],
                'amount_currency': amount_currency,
                'currency_id': self.currency_id.id,
                'partner_id': self.employee_id.sudo().work_contact_id.id,
            }
            move_lines.append(base_move_line)
            total_tax_line_balance = 0.0
            for tax_line_data in tax_data['tax_lines_to_add']:  # Add tax lines
                tax_line_balance = self.company_currency_id.round(tax_line_data['tax_amount'] / rate)
                total_tax_line_balance += tax_line_balance
                tax_line = {
                    'name': self.env['account.tax'].browse(tax_line_data['tax_id']).name,
                    'account_id': tax_line_data['account_id'],
                    'analytic_distribution': tax_line_data['analytic_distribution'],
                    'expense_id': self.id,
                    'tax_tag_ids': tax_line_data['tax_tag_ids'],
                    'balance': tax_line_balance,
                    'amount_currency': tax_line_data['tax_amount'],
                    'tax_base_amount': self.company_currency_id.round(tax_line_data['base_amount'] / rate),
                    'currency_id': self.currency_id.id,
                    'tax_repartition_line_id': tax_line_data['tax_repartition_line_id'],
                }
                move_lines.append(tax_line)
            base_move_line['balance'] = self.total_amount - total_tax_line_balance
            expense_name = self.name.split("\n")[0][:64]
            move_lines.append({  # Add outstanding payment line
                'name': f'{self.onboarding_employee_id.name}: {expense_name}',
                'account_id': self.sheet_id._get_expense_account_destination(),
                'balance': -self.total_amount,
                'amount_currency': self.currency_id.round(-self.total_amount_currency),
                'currency_id': self.currency_id.id,
                'partner_id': self.employee_id.sudo().work_contact_id.id,
            })
            return {
                **self.sheet_id._prepare_move_vals(),
                'ref': self.name,
                'journal_id': journal.id,
                'move_type': 'entry',
                'amount': self.total_amount_currency,
                'payment_type': 'outbound',
                'partner_type': 'supplier',
                'payment_method_line_id': payment_method_line.id,
                'partner_id': self.employee_id.sudo().work_contact_id.id,
                'currency_id': self.currency_id.id,
                'line_ids': [Command.create(line) for line in move_lines],
                'attachment_ids': [
                    Command.create(
                        attachment.copy_data({'res_model': 'account.move', 'res_id': False, 'raw': attachment.raw})[0])
                    for attachment in self.message_main_attachment_id]
            }

        else:
            base_move_line = {
                'name': f'{self.employee_id.name}: {expense_name}',
                'account_id': base_line_data['account'].id,
                'product_id': base_line_data['product'].id,
                'analytic_distribution': base_line_data['analytic_distribution'],
                'expense_id': self.id,
                'tax_ids': [Command.set(self.tax_ids.ids)],
                'tax_tag_ids': to_update['tax_tag_ids'],
                'amount_currency': amount_currency,
                'currency_id': self.currency_id.id,
            }
            move_lines.append(base_move_line)
            total_tax_line_balance = 0.0
            for tax_line_data in tax_data['tax_lines_to_add']:  # Add tax lines
                tax_line_balance = self.company_currency_id.round(tax_line_data['tax_amount'] / rate)
                total_tax_line_balance += tax_line_balance
                tax_line = {
                    'name': self.env['account.tax'].browse(tax_line_data['tax_id']).name,
                    'account_id': tax_line_data['account_id'],
                    'analytic_distribution': tax_line_data['analytic_distribution'],
                    'expense_id': self.id,
                    'tax_tag_ids': tax_line_data['tax_tag_ids'],
                    'balance': tax_line_balance,
                    'amount_currency': tax_line_data['tax_amount'],
                    'tax_base_amount': self.company_currency_id.round(tax_line_data['base_amount'] / rate),
                    'currency_id': self.currency_id.id,
                    'tax_repartition_line_id': tax_line_data['tax_repartition_line_id'],
                }
                move_lines.append(tax_line)
            base_move_line['balance'] = self.total_amount - total_tax_line_balance
            expense_name = self.name.split("\n")[0][:64]
            move_lines.append({  # Add outstanding payment line
                'name': f'{self.employee_id.name}: {expense_name}',
                'account_id': self.sheet_id._get_expense_account_destination(),
                'balance': -self.total_amount,
                'amount_currency': self.currency_id.round(-self.total_amount_currency),
                'currency_id': self.currency_id.id,
            })
            return {
                **self.sheet_id._prepare_move_vals(),
                'ref': self.name,
                'journal_id': journal.id,
                'move_type': 'entry',
                'amount': self.total_amount_currency,
                'payment_type': 'outbound',
                'partner_type': 'supplier',
                'payment_method_line_id': payment_method_line.id,
                'currency_id': self.currency_id.id,
                'line_ids': [Command.create(line) for line in move_lines],
                'attachment_ids': [
                    Command.create(attachment.copy_data({'res_model': 'account.move', 'res_id': False, 'raw': attachment.raw})[0])
                    for attachment in self.message_main_attachment_id]
            }


    def _prepare_move_lines_vals(self):
        self.ensure_one()
        account = self.account_id
        if not account:
            # We need to do this as the installation process may delete the original account, and it doesn't recompute properly after.
            # This forces the default values if none is found
            if self.product_id:
                account = self.product_id.product_tmpl_id._get_product_accounts()['expense']
            else:
                account = self.env['ir.property']._get('property_account_expense_categ_id', 'product.category')
        expense_name = self.name.split('\n')[0][:64]
        if self.employee_onboarding_id and self.onboarding_employee_id:
            res={
            'name': f'{self.onboarding_employee_id.name}: {expense_name}',
            'account_id': account.id,
            'quantity': self.quantity or 1,
            'price_unit': self.price_unit,
            'product_id': self.product_id.id,
            'product_uom_id': self.product_uom_id.id,
            'analytic_distribution': self.analytic_distribution,
            'expense_id': self.id,
            'partner_id': self.employee_id.sudo().work_contact_id.id,
            'tax_ids': [Command.set(self.tax_ids.ids)],
        }

        else:
            res = {
            'name': f'{self.employee_id.name}: {expense_name}',
            'account_id': account.id,
            'quantity': self.quantity or 1,
            'price_unit': self.price_unit,
            'product_id': self.product_id.id,
            'product_uom_id': self.product_uom_id.id,
            'analytic_distribution': self.analytic_distribution,
            'expense_id': self.id,
            'partner_id': False if self.payment_mode == 'company_account' else self.employee_id.sudo().work_contact_id.id,
            'tax_ids': [Command.set(self.tax_ids.ids)],
        }

        return res
