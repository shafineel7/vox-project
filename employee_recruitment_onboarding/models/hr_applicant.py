# -*- coding: utf-8 -*-

from odoo import api, models, fields
from odoo.exceptions import ValidationError


class HrApplicant(models.Model):
    _inherit = 'hr.applicant'

    # local onboarding
    def action_process_onboarding(self):
        res = super(HrApplicant, self).action_process_onboarding()
        employee_onboarding = self.env['employee.onboarding']

        onboard = self.env.ref('department_groups.group_hr_asst_onboard')

        acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                        [onboard.id])])
        acc_team_groups = self.env['res.groups'].browse((onboard.id))

        for applicant in self:
            employee_onboarding.create({
                'partner_name': applicant.partner_name,
                'application': applicant.name,
                'email': applicant.email_from,
                'phone': applicant.partner_phone,
                'mobile': applicant.partner_mobile,
                'department_id': applicant.department_id.id,
                'onboarding_type': applicant.recruitment_type,
                'role': applicant.job_id.name,
                'skills_ids': applicant.job_id.skills_ids.ids,
                'type': applicant.type,
                'qualification_ids': applicant.job_id.qualification_ids.ids,
                'gender': applicant.gender,
                'dob': applicant.dob,
                'marital_status': applicant.marital_status,
                'document': applicant.document,
                'agent_id': applicant.agent_id.agent_id.id,
                'user_id': applicant.user_id.id,
                'nationality_id': applicant.nationality_id.id,
                'is_driver': applicant.is_driver,
                'user_ids': [(6, 0, acc_team_users.ids)],
                'group_ids': [(6, 0, acc_team_groups.ids)],
                'signed_offer_letter': applicant.signed_offer_letter,
                'signed_offer_letter_file_name': applicant.signed_offer_letter_file_name,
                'cv': applicant.cv,
                'cv_file_name': applicant.cv_file_name,
                'educational_certificates': applicant.educational_certificates,
                'educational_certificates_file_name': applicant.educational_certificates_file_name,
                'medical_fitness_certificate': applicant.fitness_certificate,
                'medical_fitness_certificate_file_name': applicant.certificate_name,
                'applicant_id':applicant.id

            })

        return res

    # overseas onboarding
    def action_onboarding(self):
        res = super(HrApplicant, self).action_onboarding()
        employee_onboarding = self.env['employee.onboarding']
        onboard = self.env.ref('department_groups.group_hr_asst_onboard')

        acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                        [onboard.id])])
        acc_team_groups = self.env['res.groups'].browse((onboard.id))
        for applicant in self:
            employee_onboarding.create({
                'partner_name': applicant.partner_name,
                'application': applicant.name,
                'email': applicant.email_from,
                'phone': applicant.partner_phone,
                'mobile': applicant.partner_mobile,
                'department_id': applicant.department_id.id,
                'onboarding_type': applicant.recruitment_type,
                'role': applicant.job_id.name,
                'skills_ids': applicant.job_id.skills_ids.ids,
                'type': applicant.type,
                'qualification_ids': applicant.job_id.qualification_ids.ids,
                'gender': applicant.gender,
                'dob': applicant.dob,
                'marital_status': applicant.marital_status,
                'document': applicant.document,
                'agent_id': applicant.agent_id.agent_id.id,
                'user_id': applicant.user_id.id,
                'nationality_id': applicant.nationality_id.id,
                'is_driver': applicant.is_driver,
                'user_ids': [(6, 0, acc_team_users.ids)],
                'group_ids': [(6, 0, acc_team_groups.ids)],
                'signed_offer_letter': applicant.signed_offer_letter,
                'signed_offer_letter_file_name': applicant.signed_offer_letter_file_name,
                'cv': applicant.cv,
                'cv_file_name': applicant.cv_file_name,
                'educational_certificates': applicant.educational_certificates,
                'educational_certificates_file_name': applicant.educational_certificates_file_name,
                'medical_fitness_certificate':applicant.fitness_certificate,
                'medical_fitness_certificate_file_name':applicant.certificate_name,
                'applicant_id': applicant.id

            })

        return res
