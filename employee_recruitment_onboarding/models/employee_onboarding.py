# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from datetime import datetime, timedelta,date

from odoo.exceptions import UserError, ValidationError



class EmployeeOnboarding(models.Model):
    _inherit = 'employee.onboarding'


    applicant_id = fields.Many2one('hr.applicant',string="Application")
    mol_expense_amount = fields.Float(string="MOL expense Amount",copy=False, store=True,default=lambda self: self.env['product.product'].search([('is_mol_expense', '=', True)], limit=1).standard_price)
    @api.onchange('mol_stage')
    def _onchange_mol_stage(self):
        for rec in self:
            if rec.mol_stage=='applied':
                rec.mol_expense_amount = self.env['product.product'].search([('is_mol_expense', '=', True)], limit=1).standard_price

    expense_sheet_id = fields.Many2one('hr.expense.sheet', store=True,copy=False)
    expense_id = fields.Many2one('hr.expense', store=True,copy=False)


    visa_processing_amount = fields.Float(string="Visa Processing Amount",copy=False, store=True, default=lambda self: self.env['product.product'].search([('is_visa_processing', '=', True)], limit=1).standard_price)
    @api.onchange('visa_processing_stage')
    def _onchange_visa_processing_stage(self):
        for rec in self:
            if rec.visa_processing_stage == 'applied':
                rec.visa_processing_amount = self.env['product.product'].search([('is_visa_processing','=',True)],limit=1).standard_price

    visa_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False,store=True)
    visa_expense_id = fields.Many2one('hr.expense', store=True,copy=False)

    change_status_amount = fields.Float(string="Change status Amount", copy=False,store=True, default=lambda self: self.env['product.product'].search([('is_change_status', '=', True)], limit=1).standard_price)
    @api.onchange('change_status')
    def _onchange_change_status(self):
        for rec in self:
            if rec.change_status == 'change_status_applied':
                rec.change_status_amount = self.env['product.product'].search([('is_change_status', '=', True)], limit=1).standard_price

    change_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', store=True,copy=False,)
    change_status_expense_id = fields.Many2one('hr.expense', store=True,copy=False)


    medical_status_amount = fields.Float(string="Medical status Amount", copy=False,store=True,default=lambda self: self.env['product.product'].search([('is_medical_status', '=', True)], limit=1).standard_price)

    @api.onchange('medical_status')
    def _onchange_medical_status(self):
        for rec in self:
            if rec.medical_status == 'appointment_scheduled' or rec.medical_status == 'appointment_rescheduled':
                rec.medical_status_amount = self.env['product.product'].search([('is_medical_status', '=', True)],limit=1).standard_price
    medical_status_expense_sheet_id = fields.Many2one('hr.expense.sheet',copy=False, store=True)
    medical_status_expense_id = fields.Many2one('hr.expense', store=True,copy=False)

    schedule_status_amount = fields.Float(string="Schedule status Amount", copy=False, store=True,
                                         default=lambda self: self.env['product.product'].search(
                                             [('is_schedule_status', '=', True)], limit=1).standard_price)
    @api.onchange('schedule_status')
    def _onchange_schedule_status(self):
        for rec in self:
            if rec.schedule_status == 'reached':
                rec.schedule_status_amount = self.env['product.product'].search([('is_schedule_status', '=', True)],limit=1).standard_price
    schedule_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    schedule_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)


    tawjeeh_status_amount = fields.Float(string="Tawjeeh status Amount", copy=False,store=True, default=lambda self: self.env['product.product'].search([('is_tawjeeh_status', '=', True)], limit=1).standard_price)

    @api.onchange('tawjeeh_status')
    def _onchange_tawjeeh_status(self):
        for rec in self:
            if rec.tawjeeh_status == 'appointment_scheduled' or rec.tawjeeh_status == 'appointment_rescheduled':
                rec.tawjeeh_status_amount = self.env['product.product'].search([('is_tawjeeh_status', '=', True)],
                                                                            limit=1).standard_price

    tawjeeh_status_expense_sheet_id = fields.Many2one('hr.expense.sheet',copy=False, store=True)
    tawjeeh_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    eid_status_amount = fields.Float(string="Eid status Amount", copy=False, store=True,
                                         default=lambda self: self.env['product.product'].search([('is_eid_status', '=', True)], limit=1).standard_price)
    @api.onchange('eid_status')
    def _onchange_eid_status(self):
        for rec in self:
            if rec.eid_status == 'eid_applied':
                rec.eid_status_amount = self.env['product.product'].search([('is_eid_status', '=', True)],limit=1).standard_price
    eid_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    eid_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    insurance_status_amount = fields.Float(string="Insurance status Amount", copy=False, store=True,
                                     default=lambda self: self.env['product.product'].search(
                                         [('is_insurance_status', '=', True)], limit=1).standard_price)

    @api.onchange('insurance_status')
    def _onchange_insurance_status(self):
        for rec in self:
            if rec.insurance_status == 'insurance_applied':
                rec.insurance_status_amount = self.env['product.product'].search([('is_insurance_status', '=', True)],
                                                                           limit=1).standard_price

    insurance_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    insurance_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)


    def _finance_manager_approval_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'ministry_of_labor_application' and rec.expense_sheet_id != False:
                if rec.expense_sheet_id.state not in ('approve','post'):
                    if rec.onboarding_approval_date:
                        exp_date = (date_now - rec.onboarding_approval_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 3:
                            template_id = self.env.ref('employee_recruitment_onboarding.email_template_finance_manager_approval')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)



    def action_mol_post(self):
        expense_ids = []
        mol_expense = self.env['product.product'].search([('is_mol_expense', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.mol_expense_amount
            if rec.mol_stage == 'applied':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = rec.employee_id.department_id.id
                analytic_name = rec.employee_id.department_id.name

                # if department_id not in analytic_accounts:
                #     account_analytic = self.env['account.analytic.account'].search(
                #         [('department_id', '=', department_id)], limit=1)
                #
                #     if not account_analytic:
                #         analytic_plan = self.env['account.analytic.plan'].sudo().create({
                #             'name': 'Employee Department',
                #         })
                #         account_analytic = self.env['account.analytic.account'].create({
                #             'plan_id': analytic_plan.id,
                #             'name': analytic_name,
                #             'department_id': department_id,
                #         })
                #
                #         self.env['account.analytic.distribution.model'].sudo().create({
                #             'product_id': mol_expense.id,
                #             "partner_id": rec.employee_id.sudo().work_contact_id.id,
                #             'analytic_distribution': {account_analytic.id: 100.0}
                #         })
                #
                #     analytic_accounts[department_id] = account_analytic
                # else:
                #     account_analytic = analytic_accounts[department_id]
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    # 'analytic_distribution': {account_analytic.id: 100.0},
                    # 'total_amount': mol_expense.standard_price,
                    'total_amount': expense_value,
                    'product_id': mol_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.id,
                    'quantity':1
                })
                expense_ids.append(expense.id)

                if expense_value>mol_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'expense_sheet_id':expense.sheet_id.id,
                        'expense_id': expense.id,

                    }
                )
            return expense_ids

    def action_visa_post(self):
        expense_ids = []
        visa_processing_expense = self.env['product.product'].search([('is_visa_processing', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.visa_processing_amount
            if rec.visa_processing_stage == 'applied':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': visa_processing_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > visa_processing_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'visa_expense_sheet_id': expense.sheet_id.id,
                        'visa_expense_id': expense.id,

                    }
                )
            return expense_ids


    def action_change_status_post(self):
        expense_ids = []
        change_status_expense = self.env['product.product'].search([('is_change_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.change_status_amount
            if rec.change_status == 'change_status_applied':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': change_status_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > change_status_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'change_status_expense_sheet_id': expense.sheet_id.id,
                        'change_status_expense_id': expense.id,

                    }
                )
            return expense_ids


    def action_medical_status_post(self):
        expense_ids = []
        medical_status_expense = self.env['product.product'].search([('is_medical_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.medical_status_amount
            if rec.medical_status == 'appointment_scheduled':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': medical_status_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > medical_status_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'medical_status_expense_sheet_id': expense.sheet_id.id,
                        'medical_status_expense_id': expense.id,

                    }
                )
            return expense_ids


    def action_tawjeeh_post(self):
        expense_ids = []
        tawjeeh_status_expense = self.env['product.product'].search([('is_tawjeeh_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.tawjeeh_status_amount
            if rec.tawjeeh_status == 'appointment_scheduled':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': tawjeeh_status_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > tawjeeh_status_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'tawjeeh_status_expense_sheet_id': expense.sheet_id.id,
                        'tawjeeh_status_expense_id': expense.id,

                    }
                )
            return expense_ids


    def action_eid_post(self):
        expense_ids = []
        eid_status_expense = self.env['product.product'].search([('is_eid_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.eid_status_amount
            if rec.eid_status == 'eid_applied':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': eid_status_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > eid_status_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'eid_status_expense_sheet_id': expense.sheet_id.id,
                        'eid_status_expense_id': expense.id,

                    }
                )
            return expense_ids

    def action_insurance_post(self):
        expense_ids = []
        insurance_status_expense = self.env['product.product'].search([('is_insurance_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.insurance_status_amount
            if rec.insurance_status == 'insurance_applied':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': insurance_status_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > insurance_status_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'insurance_status_expense_sheet_id': expense.sheet_id.id,
                        'insurance_status_expense_id': expense.id,

                    }
                )
            return expense_ids

    def action_schedule_status_post(self):
        expense_ids = []
        insurance_status_expense = self.env['product.product'].search([('is_schedule_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.schedule_status_amount
            analytic_accounts = {}
            employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
            department_id = employee.department_id.id
            analytic_name = employee.department_id.name
            expense = self.env['hr.expense'].create({
                'employee_id': employee.id,
                'date': fields.Date.today(),
                'name': rec.name,
                'payment_mode': 'own_account',
                'department_id': employee.department_id.id,
                'total_amount': expense_value,
                'product_id': insurance_status_expense.id,
                'onboarding_employee_id': rec.employee_id.id,
                'employee_onboarding_id': rec.id,
                'quantity': 1
            })
            expense_ids.append(expense.id)

            if expense_value > insurance_status_expense.maximum_allowed_amount:
                expense.action_submit_expenses()
                expense.sheet_id.sudo().action_submit_sheet()
            else:

                expense.action_submit_expenses()
                expense.sheet_id.sudo().action_submit_sheet()
                expense.sheet_id.sudo().action_approve_expense_sheets()
                expense.sheet_id.sudo().action_approve_finance_manager()
            rec.sudo().write(
                {
                    'schedule_status_expense_sheet_id': expense.sheet_id.id,
                    'schedule_status_expense_id': expense.id,

                }
            )
            return expense_ids




    def write(self, vals):
        onboarding_users = self.env.user.has_group('department_groups.group_hr_asst_onboard')
        pro_manager = self.env.user.has_group('department_groups.group_pro_mgr')
        pro_assistant = self.env.user.has_group('department_groups.group_pro_asst')
        agent_users = self.env.user.has_group('department_groups.group_agent_agent')
        hr_aast = self.env.user.has_group('department_groups.group_hr_asst')
        hr_mgr = self.env.user.has_group('department_groups.group_hr_mgr')
        welfare_users = self.env.user.has_group('department_groups.group_hr_welfare_officer')
        camp_manager = self.env.user.has_group('department_groups.group_construction_camp_mgr')
        staff_camp_manager = self.env.user.has_group('department_groups.group_staff_camp_mgr')
        soft_camp_manager = self.env.user.has_group('department_groups.group_soft_ser_camp_mgr')
        res = super(EmployeeOnboarding, self).write(vals)
        if self.stage == 'new':
            if not onboarding_users:
                raise UserError("You cannot edit The data in new stage")
        if self.stage == 'ministry_of_labor_application':
            if not (onboarding_users or pro_manager or pro_assistant or agent_users):
                raise UserError("You cannot edit The data in new stage")
            if pro_assistant or pro_manager:
                if 'signed_mol' in vals:
                    raise UserError("You cannot edit The data")
            if agent_users:
                if 'mol_stage' in vals:
                    raise UserError("You cannot edit The data")
                if 'st_and_mb' in vals:
                    raise UserError("You cannot edit The data")
            if onboarding_users:
                if 'mol_expense_amount' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'visa_processing':
            if not (onboarding_users or pro_manager or pro_assistant):
                raise UserError("You cannot edit The data in new stage")
            if onboarding_users:
                if 'visa_processing_amount' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'change_status':
            if not (onboarding_users or pro_manager or pro_assistant):
                raise UserError("You cannot edit The data in new stage")
            if onboarding_users:
                if 'change_status_amount' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'scheduled_arrivals':
            if not (onboarding_users or agent_users or welfare_users or camp_manager or staff_camp_manager or soft_camp_manager):
                raise UserError("You cannot edit The data in new stage")
            if not (camp_manager or staff_camp_manager or soft_camp_manager):
                if 'room_allocation' in vals:
                    raise UserError("You cannot edit The data")
                if 'accommodation_status' in vals:
                    raise UserError("You cannot edit The data")
            if not welfare_users:
                if 'schedule_status' in vals:
                    raise UserError("You cannot edit The data")
                if 'schedule_status_amount' in vals:
                    raise UserError("You cannot edit The data")
            if not onboarding_users:
                if 'camp_manager_id' in vals:
                    raise UserError("You cannot edit The data")
            if not agent_users:
                if 'date_of_journey' in vals:
                    raise UserError("You cannot edit The data")
                if 'date_time_of_landing' in vals:
                    raise UserError("You cannot edit The data")
                if 'airport_destination_id' in vals:
                    raise UserError("You cannot edit The data")
                if 'terminal_id' in vals:
                    raise UserError("You cannot edit The data")
                if 'flight_no' in vals:
                    raise UserError("You cannot edit The data")
                if 'ticket' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'visa_stamping':
            if not (onboarding_users or pro_manager or pro_assistant or hr_aast):
                raise UserError("You cannot edit The data in new stage")
            if pro_manager or pro_assistant:
                if 'collection_of_passport' in vals:
                    raise UserError("You cannot edit The data")
                if 'collection_date' in vals:
                    raise UserError("You cannot edit The data")
                if 'passport_custody_status' in vals:
                    raise UserError("You cannot edit The data")
            if hr_aast:
                if 'visa_stamping_stage' in vals:
                    raise UserError("You cannot edit The data")
                if 'visa_application_number' in vals:
                    raise UserError("You cannot edit The data")
                if 'visa_expiry_date' in vals:
                    raise UserError("You cannot edit The data")
                if 'visa_copy' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'eid_application_insurance' and self.env.context.get('insurance_value')==False:
            if not (onboarding_users or pro_manager or pro_assistant or hr_aast or welfare_users):
                raise UserError("You cannot edit The data in new stage")
            if onboarding_users:
                if 'insurance_status_amount' in vals:
                    raise UserError("You cannot edit The data")
                if 'eid_status_amount' in vals:
                    raise UserError("You cannot edit The data")
            if not (hr_aast or onboarding_users):
                if 'insurance_status' in vals:
                    raise UserError("You cannot edit The data")
                if 'group_code' in vals:
                    raise UserError("You cannot edit The data")
                if 'insurance_number' in vals:
                    raise UserError("You cannot edit The data")
                if 'authority_number' in vals:
                    raise UserError("You cannot edit The data")
                if 'insurance_card' in vals:
                    raise UserError("You cannot edit The data")
                if 'insurance_document' in vals:
                    raise UserError("You cannot edit The data")
            if not (welfare_users):
                if 'biometric_attendance_status' in vals:
                    raise UserError("You cannot edit The data")
            if not(pro_manager or pro_assistant):
                if 'eid_status_amount' in vals:
                    raise UserError("You cannot edit The data")

            if not(pro_manager or pro_assistant or onboarding_users):
                if 'eid_status' in vals:
                    raise UserError("You cannot edit The data")
                if 'eid_application_number' in vals:
                    raise UserError("You cannot edit The data")
                if 'biometric' in vals:
                    raise UserError("You cannot edit The data")
                if 'eid_application_receipt' in vals:
                    raise UserError("You cannot edit The data")
                if 'biometric_date' in vals:
                    raise UserError("You cannot edit The data")
                if 'biometric_location' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'medical_fitness':
            if not (onboarding_users or welfare_users):
                raise UserError("You cannot edit The data in new stage")
            if not welfare_users:
                if 'medical_attendance_status' in vals:
                    raise UserError("You cannot edit The data")
                if 'tawjeeh_attendance_status' in vals:
                    raise UserError("You cannot edit The data")

            if not onboarding_users:
                if 'medical_status_amount' in vals:
                    raise UserError("You cannot edit The data")
                if 'tawjeeh_status_amount' in vals:
                    raise UserError("You cannot edit The data")
                if 'medical_appointment_date' in vals:
                    raise UserError("You cannot edit The data")
                if 'medical_receipt_copy' in vals:
                    raise UserError("You cannot edit The data")
                if 'medical_center_id' in vals:
                    raise UserError("You cannot edit The data")



        return res
