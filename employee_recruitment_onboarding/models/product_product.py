# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class ProductProduct(models.Model):
    _inherit = 'product.product'

    is_mol_expense = fields.Boolean(string="MOL Expense")
    is_visa_processing = fields.Boolean(string="visa processing")
    is_change_status = fields.Boolean(string="Change Status")
    is_medical_status = fields.Boolean(string="Medical Status")
    is_tawjeeh_status = fields.Boolean(string="Tawheeh Status")
    is_schedule_status = fields.Boolean(string="schedule Status")
    is_eid_status = fields.Boolean(string="Eid Status")
    is_insurance_status = fields.Boolean(string="Insurance Status")