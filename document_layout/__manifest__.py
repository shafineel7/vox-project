# -*- coding: utf-8 -*-

{
    'name': 'Document Layout',
    'version': '17.0.1.0.0',
    'summary': """
      Report Layout for Qweb Pdf reports""",
    'description': """
       This module is used to adjust the layout of the pdf report document
    """,
    'author': 'Shafi PK',
    'category': 'Uncategorized',
    'depends': ['base', 'web',],
    'data': [
        'report/header_footer_templates.xml',
        'views/res_company_views.xml',
        'wizard/base_document_layout_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,

}
