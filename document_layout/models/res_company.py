# -*- coding: utf-8 -*-

from odoo import models, fields


class ResCompany(models.Model):
    _inherit = "res.company"

    header_image = fields.Binary(string="Header Image")
    report_footer = fields.Html(string='Footer Text')
