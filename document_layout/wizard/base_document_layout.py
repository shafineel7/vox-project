# -*- coding: utf-8 -*-
from odoo import models, fields


class BaseDocumentLayout(models.TransientModel):
    _inherit = 'base.document.layout'

    header_image = fields.Binary(string="Header Image", related='company_id.header_image', readonly=False)

    def document_layout_save(self):
        res = super(BaseDocumentLayout, self).document_layout_save()
        for wizard in self:
            wizard.company_id.header_image
        return res
