{
    'name': 'Recruitment Service Agreement',
    'author': 'Shafi PK',
    'summary': 'Module for Recruitment Service Agreement docx download',
    'version': '17.0.1.0.0',
    'category': 'Technical/Technical',
    'website': 'https://www.voxtronme.com',
    'depends': ['employee'],
    'description': """This module is for downloading word document of recruitment service agreement.""",
    'data': [
        'views/agent_agent_views.xml'
    ],
    'external_dependencies': {
        'python': [
            'docxtpl',
        ],
    },
    'installable': True,
    'auto_install': False,
    'application': True,
}
