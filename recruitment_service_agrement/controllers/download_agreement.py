# -*- coding: utf-8 -*-
import base64

from odoo import http
from odoo.http import request


class DownloadServiceAgreement(http.Controller):
    @http.route('/download_rsa/docx/<int:attachment_id>', type='http', auth='public')
    def download_docx_attachment(self, attachment_id):
        attachment = request.env['ir.attachment'].sudo().browse(attachment_id)

        if attachment and attachment.type == 'binary':
            file_content = base64.b64decode(attachment.datas)

            headers = [
                ('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
                ('Content-Disposition', f'attachment; filename="{attachment.name}"'),
            ]

            attachment.unlink()

            return request.make_response(file_content, headers)
        else:
            request.not_found()
