# -*- coding: utf-8 -*-
import base64
import os

from docxtpl import DocxTemplate, RichText

from odoo import models


class AgentAgent(models.Model):
    _inherit = 'agent.agent'

    def action_generate_agreement(self):
        module_path = os.path.dirname(os.path.abspath(__file__))
        template_path = os.path.join(module_path, '..', 'recruitment_service_agreement_template',
                                     'recruitment_service_agreement.docx')

        document = DocxTemplate(template_path)
        company = self.env.company
        company_data = {
            'name': RichText(company.name, bold=True, font='Arial'),
            'street': company.street,
            'state': company.state_id.name,
            'country': company.country_id.name,
            'zip': company.zip,
        }

        context = {
            'company_data': company_data,
            'name': RichText(self.name, bold=True, font='Arial'),
            'license': self.license_no,
        }

        document.render(context, autoescape=True)

        temp_dir = os.path.join(module_path, '..', 'temp')
        os.makedirs(temp_dir, exist_ok=True)
        temp_file_path = os.path.join(temp_dir, f'{self.name}.docx')

        document.save(temp_file_path)

        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()

        docx_base64 = base64.b64encode(docx_content)

        attachment = self.env['ir.attachment'].sudo().create({
            'name': f"Recruitment Service Agreement - {self.name}.docx",
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'agent.agent',
            'res_id': self.id,
            'store_fname': f"{self.name}.docx"
        })

        os.remove(temp_file_path)

        url = '/download_rsa/docx/%d' % attachment.id

        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self'
        }
