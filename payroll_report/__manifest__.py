# -*- coding: utf-8 -*-

{
    'name': 'Payroll',
    'version': '17.0.1.0.0',
    'category': 'Human Resources/Payroll',
    'summary': 'payslip of the payroll',
    'description': """This module is for downloading payslip report 
    """,
    'author': 'Abhilash.c',
    'website': 'https://www.voxtronme.com',
    'depends': ['hr_payroll'],
    'data': [
        'reports/payroll_payslip_report.xml',
        'reports/payslip_report.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'AGPL-3',
}
