# -*- coding: utf-8 -*-

from odoo import models


class Payslip(models.Model):
    _inherit = "hr.payslip"

    def action_print_payslip(self):
        return self.env.ref('payroll_report.action_payslip_report').report_action(self.ids)
