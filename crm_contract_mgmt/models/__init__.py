# -*- coding: utf-8 -*-

from . import crm_lead
from . import res_partner
from . import crm_stage
from . import hr_job
from . import lead_estimation
from . import cost_components
from . import designation
from . import lead_revision
from . import department




