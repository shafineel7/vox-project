# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
# import datetime
from datetime import timedelta, datetime


class HrDepartment(models.Model):
    _inherit = 'hr.department'

    apply_crm = fields.Boolean(string="Apply in CRM")
    seq_code = fields.Char(string="CRM Code")
