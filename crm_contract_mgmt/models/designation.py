# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class Designation(models.Model):
    _inherit = 'designation'

    des_estimation_ids = fields.One2many('job.estimation', 'designation_id', string="Estimation")
