# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError


class LeadEstimation(models.Model):
    _name = 'crm.lead.estimation'
    _description = 'Lead Estimation Details'

    product_id = fields.Many2one('product.product', string='Service')
    description = fields.Char(string="Description")
    qty = fields.Float(string="Quantity")
    unit_price = fields.Float(string="Unit Price")
    sub_total = fields.Float(string="Sub Total", compute='_compute_amount',
                             store=True, precompute=True)
    discount = fields.Float(string="Discount")
    total = fields.Float(string="Total", compute='_compute_amount',
                         store=True, precompute=True)
    cost = fields.Float(string="Cost", compute='_compute_total_exp_cost')
    lead_id = fields.Many2one('crm.lead', string="Lead")
    job_id = fields.Many2one('hr.job', string="Job Position")
    tax_id = fields.Many2many(comodel_name='account.tax')
    salary_ids = fields.One2many('estimation.salary.lines', 'est_id', string="Gross Salary")
    direct_exp_ids = fields.One2many('estimation.direct.lines', 'est_id', string="Direct Expenses")
    overhead_ids = fields.One2many('estimation.overhead.lines', 'est_id', string="Overheads")
    tax_totals = fields.Binary(compute='_compute_tax_totals', exportable=False)
    company_id = fields.Many2one(
        comodel_name='res.company',
        required=True, index=True,
        default=lambda self: self.env.company)
    currency_id = fields.Many2one(
        'res.currency', 'Currency', compute='_compute_currency_id')
    est_add = fields.Boolean(string="Est Add")
    # gross_margin = fields.Float(string="Gross Margin", compute='_compute_gross_margin')
    gross_percent = fields.Float(string="Gross Margin(%)", compute='_compute_gross_margin')
    net_profit = fields.Float(string="Net Profit", compute='_compute_gross_margin')
    total_net_profit = fields.Float(string="Net Profit", compute='_compute_gross_margin')
    total_gross_percent = fields.Float(string="Total Gross Margin(%)", compute='_compute_gross_margin')

    np_percent = fields.Float(string="NP(%)")
    contract_type = fields.Selection(
        [('hourly', 'Hourly'), ('daily', 'Daily'), ('monthly', 'Monthly'), ('fixed', 'Fixed Cost')],
        string="Contract Type")
    designation_id = fields.Many2one('designation', string="Designation")
    total_cost = fields.Float(string="Total Cost", compute="_compute_total_cost")
    approval_status = fields.Selection(
        [('draft', 'Draft'), ('dept_approval', 'Department Head Approval'), ('finance', 'Finance Approval'),
         ('md', 'MD Approval'), ('rework', 'Rework'), ('approved', 'Approved')], default='draft',
        string="Approval Status", related='lead_id.approval_status')

    @api.onchange('product_id')
    def onchange_product_details(self):
        if self.product_id:
            self.description = self.product_id.name

    @api.depends('cost', 'qty')
    def _compute_total_cost(self):
        for vals in self:
            vals.total_cost = 0
            if vals.qty > 0 and vals.cost > 0:
                vals.total_cost = vals.cost * vals.qty

    @api.depends('cost', 'unit_price', 'qty')
    def _compute_gross_margin(self):
        for vals in self:
            vals.gross_percent = 0
            vals.net_profit = 0
            vals.total_net_profit = 0
            vals.total_gross_percent = 0
            if vals.unit_price > 0:
                vals.gross_percent = ((vals.unit_price - vals.cost) / vals.unit_price) * 100
                vals.total_gross_percent = vals.gross_percent * vals.qty
                vals.net_profit = vals.unit_price - vals.cost
                vals.total_net_profit = vals.net_profit * vals.qty

    @api.depends('company_id')
    def _compute_currency_id(self):
        for order in self:
            order.currency_id = order.company_id.currency_id

    @api.depends_context('lang')
    @api.depends('tax_id', 'unit_price', 'total')
    def _compute_tax_totals(self):
        for order in self:
            order.tax_totals = self.env['account.tax']._prepare_tax_totals(
                [order._convert_to_tax_base_line_dict()],
                order.currency_id or order.company_id.currency_id,
            )

    @api.depends('salary_ids', 'direct_exp_ids', 'overhead_ids')
    def _compute_total_exp_cost(self):
        for vals in self:
            vals.cost = 0
            cost = 0
            if vals.salary_ids:
                cost += sum(vals.salary_ids.mapped('amount'))
            if vals.direct_exp_ids:
                cost += sum(vals.direct_exp_ids.mapped('amount'))
            if vals.overhead_ids:
                cost += sum(vals.overhead_ids.mapped('amount'))
            vals.cost = cost

    @api.depends('qty', 'discount', 'unit_price', 'tax_id')
    def _compute_amount(self):
        for line in self:
            tax_results = self.env['account.tax']._compute_taxes([
                line._convert_to_tax_base_line_dict()
            ])
            totals = list(tax_results['totals'].values())[0]
            amount_untaxed = totals['amount_untaxed']
            amount_tax = totals['amount_tax']

            line.update({
                'sub_total': amount_untaxed,
                # 'price_tax': amount_tax,
                'total': amount_untaxed + amount_tax,
            })

    def _convert_to_tax_base_line_dict(self, **kwargs):
        self.ensure_one()
        return self.env['account.tax']._convert_to_tax_base_line_dict(
            self,
            partner=False,
            currency=self.currency_id,
            product=self.product_id,
            taxes=self.tax_id,
            price_unit=self.unit_price,
            quantity=self.qty,
            discount=self.discount,
            price_subtotal=self.total,
            **kwargs,
        )

    def add_estimation(self):
        if not self.est_add:
            # if not self.
            if not self.designation_id:
                raise UserError("Please add a Designation.")
            if not self.designation_id.des_estimation_ids:
                raise UserError("No estimation matrix added for the Job Position.")
            salaries = self.designation_id.des_estimation_ids.filtered(lambda x: x.est_id.exp_head == 'gross_salary')
            if salaries:
                for salary in salaries:
                    est_sal = {
                        'comp_id': salary.est_id.id,
                        'est_id': self.id
                    }
                    self.env['estimation.salary.lines'].create(est_sal)
            directs = self.designation_id.des_estimation_ids.filtered(lambda x: x.est_id.exp_head == 'direct')
            if directs:
                for direct in directs:
                    est_sal = {
                        'comp_id': direct.est_id.id,
                        'est_id': self.id
                    }
                    self.env['estimation.direct.lines'].create(est_sal)
            overheads = self.designation_id.des_estimation_ids.filtered(lambda x: x.est_id.exp_head == 'overhead')
            if directs:
                for overhead in overheads:
                    est_sal = {
                        'comp_id': overhead.est_id.id,
                        'est_id': self.id
                    }
                    self.env['estimation.overhead.lines'].create(est_sal)
            self.est_add = True
        view_id = self.env.ref('crm_contract_mgmt.crm_lead_estimation_splits_inherit')
        ctx = self.env.context.copy()
        return {
            'name': _('Add Estimation'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'crm.lead.estimation',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx
        }

    def update_from_job(self):
        if self.approval_status not in ['draft', 'rework']:
            raise UserError("Opportunity is under Approval. Please sent for Rework to update.")
        msg = """Updating Estimation Head from Job Application will remove the existing calculations. Click Update to continue."""
        value = self.env['display.conf.message'].sudo().create({'message': msg, 'job_est_id': self.id})
        return {
            'type': 'ir.actions.act_window',
            'message': 'Message',
            'res_model': 'display.conf.message',
            'view_mode': 'form',
            'target': 'new',
            'res_id': value.id
        }

    def update_est_heads(self):
        self.salary_ids = [(5, 0, 0)]
        self.direct_exp_ids = [(5, 0, 0)]
        self.overhead_ids = [(5, 0, 0)]
        self.est_add = False
        return self.add_estimation()


class SalaryEstimationLines(models.Model):
    _name = 'estimation.salary.lines'
    _description = 'Salary Estimation'

    name = fields.Char(string="Description")
    comp_id = fields.Many2one('estimate.cost.component', string="Description")
    amount = fields.Float(string="Amount")
    est_id = fields.Many2one('crm.lead.estimation', string="Estimation")
    lead_id = fields.Many2one('crm.lead', related='est_id.lead_id')


class DirectExpenseEstimationLines(models.Model):
    _name = 'estimation.direct.lines'
    _description = 'Salary Estimation'

    name = fields.Char(string="Description")
    amount = fields.Float(string="Amount")
    comp_id = fields.Many2one('estimate.cost.component', string="Description")

    est_id = fields.Many2one('crm.lead.estimation', string="Estimation")
    lead_id = fields.Many2one('crm.lead', related='est_id.lead_id')


class OverheadExpenseEstimationLines(models.Model):
    _name = 'estimation.overhead.lines'
    _description = 'Salary Estimation'

    name = fields.Char(string="Description")
    amount = fields.Float(string="Amount")
    comp_id = fields.Many2one('estimate.cost.component', string="Description")

    est_id = fields.Many2one('crm.lead.estimation', string="Estimation")
    lead_id = fields.Many2one('crm.lead', related='est_id.lead_id')
