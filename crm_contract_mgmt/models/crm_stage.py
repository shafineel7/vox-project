# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class CrmLead(models.Model):
    _inherit = 'crm.stage'

    fields_ids = fields.Many2many('ir.model.fields', string='Mandatory Fields')
    is_site = fields.Boolean(string="Site Visit Stage")
    is_estimate = fields.Boolean(string="Estimate Stage")
    stage_code = fields.Selection(
        [('new', 'New'), ('req', 'Requirement Analysis'), ('est', 'Estimation'), ('site', 'Site Visit'),
         ('quote', 'Quotation'), ('won', 'Won'), ('lost', 'Lost'), ('hold', 'Onhold'), ('negotiation', 'Negotiation')],
        string="Code")
