# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError


class CostComponents(models.Model):
    _name = 'estimate.cost.component'
    _description = 'Cost Components Description'

    name = fields.Char(string="Name")
    exp_head = fields.Selection(
        [('gross_salary', 'Gross Salary'), ('direct', 'Direct Expense'), ('overhead', 'Overhead')],
        string="Expense Head")
