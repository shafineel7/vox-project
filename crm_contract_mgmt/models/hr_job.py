# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError


class HrJob(models.Model):
    _inherit = 'hr.job'

    job_estimation_ids = fields.One2many('job.estimation', 'job_id', string="Estimation")


class JobEstimation(models.Model):
    _name = 'job.estimation'
    _description = 'Job Estimation Line structure'

    name = fields.Char(string="Description")
    job_id = fields.Many2one('hr.job', string="Job Position")
    sequence = fields.Integer(string="Sequence", default=10)
    display_type = fields.Selection(
        selection=[
            ('line_section', "Section"),
            ('line_note', "Note"),
        ],
        default=False)
    est_id = fields.Many2one('estimate.cost.component', string="Cost Component")
    designation_id = fields.Many2one('designation', string="Designation")
