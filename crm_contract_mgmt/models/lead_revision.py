# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class LeadQuoteRevision(models.Model):
    _name = 'lead.quote.revisions'
    _description = 'Adding Lead Revisions'
    _order = 'id desc'

    name = fields.Char(string="Name")
    lead_id = fields.Many2one('crm.lead', string="Lead")
    date = fields.Date(string="Date")
    quote_line_ids = fields.One2many('lead.quote.revisions.lines', 'quote_id', string="Revision Lines")

    def view_details(self):
        view_id = self.env.ref('crm_contract_mgmt.lead_quote_revision_view_form')
        ctx = self.env.context.copy()
        return {
            'name': _('Add Estimation'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'lead.quote.revisions',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx
        }


class LeadQuoteRevisionLines(models.Model):
    _name = 'lead.quote.revisions.lines'
    _description = 'Adding Lead Revision Lines'
    _order = 'id desc'

    quote_id = fields.Many2one('lead.quote.revisions', string="Revision")
    product_id = fields.Many2one('product.product', string='Service')
    description = fields.Char(string="Description")
    qty = fields.Float(string="Quantity")
    unit_price = fields.Float(string="Unit Price")
    sub_total = fields.Float(string="Sub Total")
    discount = fields.Float(string="Discount")
    total = fields.Float(string="Total")
    cost = fields.Float(string="Cost")
    gross_percent = fields.Float(string="Gross Margin(%)")
    net_profit = fields.Float(string="Net Profit")
    total_net_profit = fields.Float(string="Net Profit")
    np_percent = fields.Float(string="NP(%)")
    contract_type = fields.Selection(
        [('hourly', 'Hourly'), ('daily', 'Daily'), ('monthly', 'Monthly'), ('fixed', 'Fixed Cost')],
        string="Contract Type")
    designation_id = fields.Many2one('designation', string="Designation")
    total_cost = fields.Float(string="Total Cost")
    salary_ids = fields.One2many('estimation.salary.revision.lines', 'est_id', string="Gross Salary")
    direct_exp_ids = fields.One2many('estimation.direct.revision.lines', 'est_id', string="Direct Expenses")
    overhead_ids = fields.One2many('estimation.overhead.revision.lines', 'est_id', string="Overheads")

    def view_details(self):
        view_id = self.env.ref('crm_contract_mgmt.lead_quote_revisions_lines_view_form')
        ctx = self.env.context.copy()
        return {
            'name': _('Estimation Heads'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'lead.quote.revisions.lines',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx
        }



class SalaryEstimationRevisionLines(models.Model):
    _name = 'estimation.salary.revision.lines'
    _description = 'Salary Estimation'

    name = fields.Char(string="Description")
    comp_id = fields.Many2one('estimate.cost.component', string="Description")
    amount = fields.Float(string="Amount")
    est_id = fields.Many2one('lead.quote.revisions.lines', string="Estimation")


class DirectExpenseEstimationRevisionLines(models.Model):
    _name = 'estimation.direct.revision.lines'
    _description = 'Salary Estimation'

    name = fields.Char(string="Description")
    amount = fields.Float(string="Amount")
    comp_id = fields.Many2one('estimate.cost.component', string="Description")
    est_id = fields.Many2one('lead.quote.revisions.lines', string="Estimation")


class OverheadExpenseEstimationRevisionLines(models.Model):
    _name = 'estimation.overhead.revision.lines'
    _description = 'Salary Estimation'

    name = fields.Char(string="Description")
    amount = fields.Float(string="Amount")
    comp_id = fields.Many2one('estimate.cost.component', string="Description")
    est_id = fields.Many2one('lead.quote.revisions.lines', string="Estimation")



