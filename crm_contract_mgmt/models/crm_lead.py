# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, Command
from odoo.exceptions import ValidationError, UserError
# import datetime
from datetime import timedelta, datetime


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    def get_default_groups(self):
        gr = self.env['res.groups']
        bid = self.env.ref('department_groups.group_contarct_bids_exec')
        bde = self.env.ref('department_groups.group_contarct_bde')
        erp = self.env.ref('base.group_erp_manager')
        if bid:
            gr += bid
        if bde:
            gr += bde
        if erp:
            gr += erp
        if gr:
            return gr.ids
        else:
            return False

    lead_type = fields.Selection([('new', 'New'), ('addendum', 'Contract Addendum'), ('renewal', 'Renewal')],
                                 default='new', string='Type')
    contract = fields.Char(string="Contract ID")
    lead_source = fields.Selection([('bids', 'Bids'), ('other', ('Others'))], default='bids',
                                   string="Opportunity Category")
    clarify_date = fields.Date(string="Clarification Date")
    submit_date = fields.Date(string="Submission Date")
    target_date = fields.Date(string="Target Date")
    supervisor_id = fields.Many2one('res.users', string="Supervisor")
    hse_exec_id = fields.Many2one('res.users', string="HSE Executive")
    visit_date = fields.Date(string="Site Visit Date")
    site_report = fields.Binary(string="Site Visit Report")
    is_site = fields.Boolean(string="Is site visit stage", compute='compute_site_stage')
    is_site_visit = fields.Boolean(string="Is site visit stage", compute='compute_site_stage')
    is_estimation = fields.Boolean(string="Is Estimation stage", compute='compute_site_stage')
    is_req = fields.Boolean(string="Is Analysis", compute='compute_site_stage')
    is_quote = fields.Boolean(string="Is Quote", compute='compute_site_stage')
    allow_site_display = fields.Boolean(string="Allow Site Visit")
    allow_estimate_display = fields.Boolean(string="Allow Estimate Visit")
    lead_estimation_ids = fields.One2many('crm.lead.estimation', 'lead_id', string="Estimation Details")
    currency_id = fields.Many2one(
        'res.currency', 'Currency', compute='_compute_currency_id')
    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user,
        check_company=True, tracking=True)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=get_default_groups)
    is_assign = fields.Boolean(string="Self Assign")
    logged_user = fields.Boolean(string="Logged USer", compute='compute_logged_user')
    logged_group = fields.Boolean(string="Logged Group", compute='compute_logged_user')
    approval_status = fields.Selection(
        [('draft', 'Draft'), ('dept_approval', 'Department Head Approval'), ('finance', 'Finance Approval'),
         ('md', 'MD Approval'), ('rework', 'Rework'), ('approved', 'Approved')], default='draft',
        string="Approval Status")
    dept_approver_id = fields.Many2one('res.users', string="Department Head")
    fin_approver_id = fields.Many2one('res.users', string="Finance Dept Approved By")
    md_id = fields.Many2one('res.users', string="Management Level Approved By")
    dept_approved_on = fields.Date(string="Department Head Approved on")
    fin_approved_on = fields.Date(string="Finance Department Approved on")
    md_approved_on = fields.Date(string="MD Approved on")
    rework_date = fields.Date(string="Rework Date")
    rework_reason = fields.Char(string="Rework Reason")
    rework_sample = fields.Char(string="Rework Reason")
    request_user_id = fields.Many2one('res.users', string="Requested By")
    requested_group_id = fields.Many2one('res.groups', string="Requested Group")
    bd_user_id = fields.Many2one('res.users', string="BD User")
    bd_group_id = fields.Many2one('res.groups', string="BD Groups")
    is_rework = fields.Boolean(string="Rework")
    lead_revision_ids = fields.One2many('lead.quote.revisions', 'lead_id', string="Revisions")
    service_type = fields.Selection(
        [('ss', 'Soft Services'), ('cao', 'Construction & Delivery'), ('mpo', 'Staffing Services')],
        string="Service Type", tracking=True)
    department_id = fields.Many2one('hr.department', string="Service Type")
    req_assign = fields.Boolean(string="Self Assign", default=False)
    hse_group_id = fields.Many2one('res.groups', string="HSE Groups")
    hse_note = fields.Text(string="HSE Notes")
    supervisor_note = fields.Text(string="Supervisor Notes")
    is_supervisor = fields.Boolean(string="Is Supervisor", compute='compute_logged_user')
    is_hse = fields.Boolean(string="Is HSE", compute='compute_logged_user')

    @api.onchange('department_id')
    def onchange_department(self):
        if self.department_id:
            if not self.department_id.seq_code:
                raise UserError("Sequence Code not configured in Service Type.")
            if self.department_id.seq_code:
                if self.department_id.seq_code == 'CAO':
                    self.hse_group_id = self.env.ref("department_groups.group_construction_hse_officer").id
                    print("HSEEEEEEEEEEEEEEEEee")
                elif self.department_id.seq_code == 'SS':
                    self.hse_group_id = self.env.ref("department_groups.group_soft_ser_hse_officer").id
                elif self.department_id.seq_code == 'MPO':
                    self.hse_group_id = self.env.ref("department_groups.group_staff_hse_officer").id

    @api.depends('assigned_user_ids', 'assigned_group_ids', 'supervisor_id', 'hse_exec_id')
    def compute_logged_user(self):
        for vals in self:
            vals.logged_user = False
            vals.logged_group = False
            vals.is_supervisor = False
            vals.is_hse = False
            if vals.env.user in vals.assigned_user_ids:
                vals.logged_user = True
            if self.env.user.has_group('base.group_erp_manager'):
                vals.logged_user = True
            if vals.env.user in vals.assigned_group_ids.users:
                vals.logged_group = True
            if vals.hse_exec_id:
                if vals.env.user == vals.hse_exec_id:
                    vals.is_hse = True
            if vals.supervisor_id:
                if vals.env.user == vals.supervisor_id:
                    vals.is_supervisor = True





    @api.onchange('lead_type')
    def onchange_lead_type(self):
        if self.lead_type:
            if self.lead_type == 'new':
                self.contract = False

    @api.depends('company_id')
    def _compute_currency_id(self):
        for order in self:
            order.currency_id = order.company_id.currency_id

    @api.depends('stage_id')
    def compute_site_stage(self):
        for vals in self:
            vals.is_site = False
            vals.is_req = False
            vals.is_site_visit = False
            vals.is_estimation = False
            vals.is_quote = False
            if vals.type == 'opportunity':

                if vals.stage_id.stage_code == 'req':
                    vals.is_site = True
                if vals.stage_id.stage_code == 'new':
                    vals.is_req = True
                if vals.stage_id.stage_code == 'site':
                    vals.is_site_visit = True
                if vals.stage_id.stage_code in ['est', 'negotiation']:
                    vals.is_estimation = True
                if vals.stage_id.stage_code in ['quote', 'negotiation']:
                    vals.is_quote = True

    @api.model
    def default_get(self, fields):
        if self.env.user.has_group('department_groups.group_contarct_bids_exec') or self.env.user.has_group(
                'department_groups.group_contarct_bde') or self.env.user.has_group('base.group_erp_manager'):
            return super(CrmLead, self).default_get(fields)
        else:
            raise ValidationError("You cannot create Leads / Opportunities")

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            parent_cust = self.env['res.partner']
            if 'partner_id' not in vals or ('partner_id' in vals and not vals['partner_id']):
                if 'partner_name' in vals and vals['partner_name']:
                    parent_cust = self.env['res.partner'].sudo().create({
                        'company_type': 'company',
                        'name': vals['partner_name'],
                        'street': vals['street'] if 'street' in vals else False,
                        'street2': vals['street2'] if 'street2' in vals else False,
                        'city': vals['city'] if 'city' in vals else False,
                        'state_id': vals['state_id'] if 'state_id' in vals else False,
                        'zip': vals['zip'] if 'zip' in vals else False,
                        'country_id': vals['country_id'] if 'country_id' in vals else False
                    })
                if 'contact_name' in vals and vals['contact_name']:
                    ch_cust = self.env['res.partner'].sudo().create({
                        'name': vals['contact_name'] if 'contact_name' in vals else False,
                        'company_type': 'person',
                        'email': vals['email_from'] if 'email_from' in vals else False,
                        'mobile': vals['mobile'] if 'mobile' in vals else False,
                        'function': vals['function'] if 'function' in vals else False,
                        'phone': vals['phone'] if 'phone' in vals else False,
                        'parent_id': parent_cust.id if parent_cust else False
                    })
                    if ch_cust:
                        vals['partner_id'] = ch_cust.id
        res = super(CrmLead, self).create(vals_list)
        for data in res:
            if data.clarify_date:
                self.env['calendar.event'].create({
                    'name': data.name + " - Clarification Date",
                    'partner_ids': [Command.link(user.partner_id.id) for user in
                                    data.assigned_user_ids] if data.assigned_user_ids else False,
                    'description': data.name + " - Clarification Date",
                    'opportunity_id': data.id,
                    'start_date': data.clarify_date
                })
            if data.submit_date:
                self.env['calendar.event'].create({
                    'name': data.name + " - Submission Date",
                    'partner_ids': [Command.link(user.partner_id.id) for user in
                                    data.assigned_user_ids] if data.assigned_user_ids else False,
                    'description': data.name + " - Submission Date",
                    'opportunity_id': data.id,
                    'start_date': data.clarify_date
                })
            if data.target_date:
                self.env['calendar.event'].create({
                    'name': data.name + " - Target Date",
                    'partner_ids': [Command.link(user.partner_id.id) for user in
                                    data.assigned_user_ids] if data.assigned_user_ids else False,
                    'description': data.name + " - Target Date",
                    'opportunity_id': data.id,
                    'start_date': data.target_date
                })

        return res

    def write(self, vals):
        if 'stage_id' in vals:
            ctx = self.env.context
            if 'allow_stage' not in ctx:
                raise UserError("You cannot change the stage directly")
        res = super(CrmLead, self).write(vals)
        if ("clarify_date" in vals and vals["clarify_date"]) or ("submit_date" in vals and vals["submit_date"]):
            if self.clarify_date > self.submit_date:
                raise UserError("Submission Date should be greater than Clarification Date.")
        if 'clarify_date' in vals and vals['clarify_date']:
            self.env['calendar.event'].create({
                'name': self.name + " - Clarification Date",
                # 'partner_ids': self.env.user.partner_id.ids if self.assigned_user_ids else False,
                'partner_ids': [Command.link(user.partner_id.id) for user in
                                self.assigned_user_ids] if self.assigned_user_ids else False,
                'description': self.name + " - Clarification Date",
                'opportunity_id': self.id,
                'start_date': self.clarify_date
            })
        if 'submit_date' in vals and vals['submit_date']:
            self.env['calendar.event'].create({
                'name': self.name + " - Submission Date",
                'partner_ids': [Command.link(user.partner_id.id) for user in
                                self.assigned_user_ids] if self.assigned_user_ids else False,
                'description': self.name + " - Submission Date",
                'opportunity_id': self.id,
                'start_date': self.submit_date
            })
        if 'target_date' in vals and vals['target_date']:
            self.env['calendar.event'].create({
                'name': self.name + " - Target Date",
                'partner_ids': [Command.link(user.partner_id.id) for user in
                                self.assigned_user_ids] if self.assigned_user_ids else False,
                'description': self.name + " - Target Date",
                'opportunity_id': self.id,
                'start_date': self.target_date
            })

        return res

    def assign_site_visitors(self):
        action = self.env.ref("crm.crm_lead_action_pipeline").sudo().read()[0]
        if not self.is_assign:
            grp = self.env['res.groups']
            oper = self.env.ref('department_groups.group_construction_operations_super')
            if oper:
                grp += oper

            hse = self.env.ref('department_groups.group_construction_hse_officer')
            if hse:
                grp += hse
            if grp:
                self.sudo().assigned_group_ids = grp.ids
            user = self.env['res.users']
            if self.hse_exec_id:
                user += self.hse_exec_id
            if self.supervisor_id:
                user += self.supervisor_id
            self.sudo().assigned_user_ids = user.ids
        estimate_stage = self.env['crm.stage'].sudo().search([('stage_code', '=', 'site')], limit=1)
        if estimate_stage:
            ctx = self.env.context.copy()
            ctx['allow_stage'] = True
            self.env.context = ctx
            self.sudo().stage_id = estimate_stage
            self.sudo().allow_site_display = True
            return action

        # else:

    def submit_sitevisit(self):
        if not self.visit_date or not self.site_report:
            raise UserError("Please add Site Visit Date and Report")
        estimate_stage = self.env['crm.stage'].sudo().search([('stage_code', '=', 'est')], limit=1)
        if estimate_stage:
            ctx = self.env.context.copy()
            ctx['allow_stage'] = True
            self.env.context = ctx

            self.stage_id = estimate_stage

    def update_sitevisit(self):
        view_id = self.env.ref('crm_contract_mgmt.crm_lead_site_visit_form')
        ctx = self.env.context.copy()
        return {
            'name': _('Site Visit'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'crm.lead',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx
        }

    def change_req_analysis(self):
        if not self.target_date:
            view_id = self.env.ref('crm_contract_mgmt.crm_lead_set_target_date_form')
            ctx = self.env.context.copy()
            return {
                'name': _('Add Target Date'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'crm.lead',
                'views': [(view_id.id, 'form')],
                'view_id': view_id.id,
                'target': 'new',
                'res_id': self.id,
                'context': ctx
            }

        stage = self.env['crm.stage'].sudo().search([('stage_code', '=', 'req')], limit=1)
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        self.env.context = ctx
        self.stage_id = stage

    @api.onchange('is_assign')
    def onchange_is_assign(self):
        if self.is_assign:
            self.hse_exec_id = False
            self.supervisor_id = False

    def submit_site_visit(self):
        action = self.env.ref("crm.crm_lead_action_pipeline").sudo().read()[0]
        gr = self.env['res.groups']
        bid = self.env.ref('department_groups.group_contarct_bids_exec')
        bde = self.env.ref('department_groups.group_contarct_bde')
        if bid:
            gr += bid
        if bde:
            gr += bde
        if gr:
            self.sudo().assigned_group_ids = gr.ids
        stage = self.env['crm.stage'].sudo().search([('stage_code', '=', 'est')], limit=1)
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        self.env.context = ctx
        self.sudo().stage_id = stage
        self.sudo().allow_estimate_display = True
        self.sudo().assigned_user_ids = self.create_uid

        return action

    def create_quote_new(self):
        quotation_context = {
            'opportunity_id': self.id,
            'partner_id': self.partner_id.id,
            'campaign_id': self.campaign_id.id,
            'medium_id': self.medium_id.id,
            'origin': self.name,
            'source_id': self.source_id.id,
            'company_id': self.company_id.id or self.env.company.id,
            'tag_ids': [(6, 0, self.tag_ids.ids)],
            'team_id': self.team_id.id,
            'user_id': self.user_id.id,
            'state': 'draft',

        }
        order = self.env['sale.order'].sudo().create(quotation_context)
        if self.lead_estimation_ids:
            for est in self.lead_estimation_ids:
                lines = self.env['sale.order.line'].sudo().create({
                    'product_template_id': est.product_id.product_tmpl_id.id,
                    'product_id': est.product_id.id,
                    'name': est.description,
                    'product_uom_qty': est.qty,
                    'price_unit': est.unit_price,
                    'discount': est.discount,
                    'company_id': self.company_id.id or self.env.company.id,
                    'order_id': order.id

                })
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("sale.action_orders")
        # if len(orders) == 1:
        action['views'] = [(self.env.ref('sale.view_order_form').id, 'form')]
        action['res_id'] = order.id
        return action

    def submit_to_dep_head(self):
        users = self.env['res.users']
        action = self.env.ref("crm.crm_lead_action_pipeline").sudo().read()[0]
        self.req_assign = True
        if not self.lead_estimation_ids:
            raise UserError("Please Add Estimation Details")
        self.bd_user_id = self.assigned_user_ids[0].id
        self.bd_group_id = self.assigned_group_ids[0].id
        self.assigned_group_ids = False
        if self.department_id.department_head_ids:
            users = self.department_id.department_head_ids.mapped('user_id')
        self.sudo().approval_status = 'dept_approval'
        self.sudo().is_rework = False
        if not users:
            self.sudo().assigned_user_ids = False
        else:
            self.sudo().assigned_user_ids = users.ids

        return action

    def approve(self):
        action = self.env.ref("crm.crm_lead_action_pipeline").sudo().read()[0]

        gr = self.env['res.groups']

        if self.approval_status == 'dept_approval':
            fin_gr = self.env.ref('department_groups.group_finance_mgr')
            if fin_gr:
                gr += fin_gr
                self.assigned_group_ids = gr.ids
                if fin_gr.users:
                    self.sudo().assigned_user_ids = fin_gr.users.ids
                else:
                    self.sudo().assigned_user_ids = False
            self.sudo().approval_status = 'finance'
            self.sudo().dept_approver_id = self.env.user.id
            self.sudo().dept_approved_on = datetime.now().date()
        elif self.approval_status == 'finance':
            md = self.env.ref('department_groups.group_mngmt_md')
            if md:
                self.assigned_group_ids = md.ids
                if md.users:
                    self.sudo().assigned_user_ids = md.users.ids
                else:
                    self.sudo().assigned_user_ids = False
            self.sudo().approval_status = 'md'
            self.sudo().fin_approver_id = self.env.user.id
            self.sudo().fin_approved_on = datetime.now().date()
        elif self.approval_status == 'md':
            self.approval_status = 'approved'
            self.assigned_group_ids = self.bd_group_id.ids
            self.sudo().assigned_user_ids = self.bd_user_id.ids
            self.sudo().md_id = self.env.user.id
            self.sudo().md_approved_on = datetime.now().date()
            if not self.stage_id.stage_code == 'negotiation':
                stage = self.env['crm.stage'].sudo().search([('stage_code', '=', 'quote')], limit=1)
                ctx = self.env.context.copy()
                ctx['allow_stage'] = True
                self.env.context = ctx
                self.sudo().stage_id = stage
        self.sudo().req_assign = True

        return action

    def submit_rework(self):
        action = self.env.ref("crm.crm_lead_action_pipeline").sudo().read()[0]

        if self.rework_sample:
            if self.rework_reason:
                self.rework_reason = str(self.rework_reason) + "\n" + str(
                    (datetime.utcnow() + timedelta(hours=4)).replace(microsecond=0)) + ' #' + str(
                    self.env.user.name) + ' - ' + str(self.rework_sample)
            else:
                self.rework_reason = str(
                    (datetime.utcnow() + timedelta(hours=4)).replace(microsecond=0)) + ' #' + str(
                    self.env.user.name) + ' - ' + str(self.rework_sample)
            self.rework_sample = False
        self.sudo().assigned_user_ids = self.bd_user_id.ids
        self.sudo().assigned_group_ids = self.bd_group_id.ids
        self.sudo().approval_status = 'rework'
        self.sudo().is_rework = True
        return action

    def add_rework(self):
        view_id = self.env.ref('crm_contract_mgmt.crm_lead_rework_form_inherit_construction')
        ctx = self.env.context.copy()
        return {
            'name': _('Add Rework'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'crm.lead',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx
        }

    def submit_quote(self):
        template_id = self.env.ref('crm_contract_mgmt.email_template_crm_quote_sent', raise_if_not_found=False)
        if template_id:
            lang = self.partner_id.lang
            template = self.env['mail.template'].browse(template_id.id)
            template.send_mail(self.id, force_send=True)
            ctx = {
                'default_model': 'crm.lead',
                'default_res_ids': self.ids,
                'default_template_id': template.id if template else None,
                'default_composition_mode': 'comment',
                'mark_so_as_sent': True,
                'default_email_layout_xmlid': 'mail.mail_notification_layout_with_responsible_signature',
                'force_email': True,
                'model_description': self.with_context(lang=lang).name,
            }
            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'mail.compose.message',
                'views': [(False, 'form')],
                'view_id': False,
                'target': 'new',
                'context': ctx,
            }

    def create_revision(self):
        stage = self.env['crm.stage'].sudo().search([('stage_code', '=', 'negotiation')], limit=1)
        self.approval_status = 'draft'
        ver = """"""
        if not self.lead_revision_ids:
            ver = """1.0"""
        else:
            ver = str(float(len(self.lead_revision_ids) + 1))
        rev = self.env['lead.quote.revisions'].sudo().create({
            'name': self.name + " version-" + ver,
            'lead_id': self.id,
            'date': datetime.now().date()
        })
        if self.lead_estimation_ids:
            for est in self.lead_estimation_ids:
                vals = {
                    'quote_id': rev.id,
                    'product_id': est.product_id.id,
                    'description': est.description,
                    'qty': est.qty,
                    'unit_price': est.unit_price,
                    'sub_total': est.sub_total,
                    'discount': est.discount,
                    'total': est.total,
                    'cost': est.cost,
                    'gross_percent': est.gross_percent,
                    'net_profit': est.net_profit,
                    'total_net_profit': est.total_net_profit,
                    'np_percent': est.np_percent,
                    'contract_type': est.contract_type,
                    'designation_id': est.designation_id.id,
                    'total_cost': est.total_cost
                }
                line = self.env['lead.quote.revisions.lines'].sudo().create(vals)
                if est.salary_ids:
                    for salary in est.salary_ids:
                        line_sal = {
                            'est_id': line.id,
                            'comp_id': salary.comp_id.id,
                            'amount': salary.amount,
                            'name': salary.name
                        }
                        sal_line = self.env['estimation.salary.revision.lines'].sudo().create(line_sal)
                if est.direct_exp_ids:
                    for direct in est.direct_exp_ids:
                        line_sal = {
                            'est_id': line.id,
                            'comp_id': direct.comp_id.id,
                            'amount': direct.amount,
                            'name': direct.name
                        }
                        sal_line = self.env['estimation.direct.revision.lines'].sudo().create(line_sal)
                if est.overhead_ids:
                    for overhead in est.overhead_ids:
                        line_sal = {
                            'est_id': line.id,
                            'comp_id': overhead.comp_id.id,
                            'amount': overhead.amount,
                            'name': overhead.name
                        }
                        sal_line = self.env['estimation.overhead.revision.lines'].sudo().create(line_sal)
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        self.env.context = ctx
        self.sudo().stage_id = stage

    def assign_self(self):
        self.req_assign = False
        self.assigned_user_ids = self.env.user.ids

    def redirect_lead_opportunity_view(self):
        res = super(CrmLead, self).redirect_lead_opportunity_view()
        res['view_id'] = self.env.ref('crm.crm_lead_view_form').id
        return res
