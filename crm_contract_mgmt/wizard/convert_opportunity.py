# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Lead2OpportunityPartner(models.TransientModel):
    _inherit = 'crm.lead2opportunity.partner'

    lead_type = fields.Selection([('new', 'New'), ('addendum', 'Contract Addendum'), ('renewal', 'Renewal')],
                                 default='new', string='Type')
    contract = fields.Char(string="Contract")

    @api.onchange('lead_type')
    def onchange_lead_type(self):
        if self.lead_type:
            if self.lead_type == 'new':
                self.contract = False

    @api.depends('duplicated_lead_ids')
    def _compute_name(self):
        super(Lead2OpportunityPartner, self)._compute_name()
        for convert in self:
            convert.name = 'convert'

    @api.depends('lead_id')
    def _compute_action(self):
        super(Lead2OpportunityPartner, self)._compute_action()
        for convert in self:
            convert.action = 'exist'

    def _convert_and_allocate(self, leads, user_ids, team_id=False):
        for lead in leads:
            lead.lead_type = self.lead_type
            lead.contract = self.contract
        return super(Lead2OpportunityPartner, self)._convert_and_allocate(leads, user_ids, team_id)

    def _action_convert(self):
        """ """
        result_opportunities = self.env['crm.lead'].browse(self._context.get('active_ids', []))
        self._convert_and_allocate(result_opportunities, [self.user_id.id], team_id=self.team_id.id)
        return result_opportunities[0]

