# -*- coding: utf-8 -*-

from odoo import models, fields


class DisplayConfirmationMessage(models.TransientModel):
    _name = 'display.conf.message'

    message = fields.Text(string="Message")
    job_est_id = fields.Many2one('crm.lead.estimation', string="Lead")

    def continue_action(self):
        return self.job_est_id.update_est_heads()
