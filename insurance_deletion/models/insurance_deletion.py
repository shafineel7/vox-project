# -*- coding: utf-8 -*-


from odoo import models, fields, api


class InsuranceDeletion(models.Model):
    _name = 'insurance.deletion'
    _inherit = ['mail.thread']

    name = fields.Char(string="Name", required=True)
    employee_id = fields.Many2one('hr.employee', string="Employee Id", required=True)
    department_id = fields.Many2one('hr.department', string="Department")
    insurance_card_no = fields.Char(string="Insurance No")
    exit_date = fields.Date(string="Exit Date")
    status = fields.Selection(
        [('new', 'New'), ('insurance_deletion', 'Insurance Deletion'), ('completed', 'Completed')],
        copy=False, default='new')
    cancellation_application_date = fields.Date(string="Cancellation Application Date")
    insurance_cancellation_date = fields.Date(string="Insurance Cancellation Date")

    def insurance_deletion(self):
        for rec in self:
            rec.status = 'insurance_deletion'
            rec.message_post(body="Insurance Deletion is Requested")

    def action_insurance_deletion(self):
        insurance_records = self.env['insurance.deletion'].browse(self._context.get('active_ids', []))
        for record in insurance_records:
            record.status = 'insurance_deletion'
            record.message_post(body="Insurance Deletion is Requested")

    def insurance_completed(self):
        for rec in self:
            rec.status = 'completed'
            rec.message_post(body="Insurance Deletion is completed")

    def action_insurance_completed(self):
        insurance_records = self.env['insurance.deletion'].browse(self._context.get('active_ids', []))
        for record in insurance_records:
            record.status = 'completed'
            record.message_post(body="Insurance Deletion is completed")

    def action_print_insurance_deletion_xls(self):
        records_data = []
        for record in self:
            record_data = {
                'form': record.read()[0]
            }
            records_data.append(record_data)
        return self.env.ref('insurance_deletion.action_insurance_deletion_generate_report_xls').sudo().report_action(self,
                                                                                                              data={
                                                                                                                  'records': records_data})

    @api.onchange('employee_id')
    def _onchange_employee_insurance_details(self):
        for record in self:
            if record.employee_id:
                values = {
                    'name': record.employee_id.name,
                }
                record.write(values)

