# -*- coding: utf-8 -*-
{
    'name': 'Insurance Deletion',
    'version': '17.0.1.0.0',
    'category': 'Insurance Management',
    'summary': 'Insurance Deletion of an employee',
    'description': """
        This module allows you to Maintain employee insurance deletion.
    """,
    'author': 'Devika P',
    'website': 'https://www.voxtronme.com',
    'depends': ['employee', 'mail', 'report_xlsx', 'department_groups'],

    'data': [
        'security/ir.model.access.csv',
        'views/insurance_deletion_views.xml',
        'views/insurance_deletion_menus.xml',

    ],
    'license': 'LGPL-3',
    'installable': True,
    'application': True,
    'auto_install': False,


}