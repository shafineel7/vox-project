# -*- coding: utf-8 -*-

import base64
import io
from odoo import models, api
from datetime import datetime, timedelta
from operator import itemgetter
import re
import xlsxwriter
import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class GeneratePmoReportXlsx(models.AbstractModel):
    _name = 'report.insurance_deletion.report_insurance_deletion_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, complaints):
        user = self.env.user
        user_tz = self.env.user.tz or pytz.utc
        services = []
        local = pytz.timezone(user_tz)

        insurance_deletion = self.env['insurance.deletion'].search(
            [('status', '=', 'new'), ('id', 'in', self._context.get('active_ids', []))])
        sheet = workbook.add_worksheet('Insurance Deletion')
        merge_format = workbook.add_format({
            'bold': 0,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter', })
        merge_format.set_font_size(16)
        bold = workbook.add_format({'bold': False})
        row = 0
        column = 0

        sheet.write(row, column, 'INSURANCE CARD NO', bold)
        sheet.set_column(column, column, 15)
        sheet.write(row, column + 1, 'EXIT DATE', bold)
        sheet.set_column(column + 2, column + 2, 15)


        for insurance in insurance_deletion:
            row += 1
            sheet.write(row, column, '')
            sheet.write(row + 1, column, '', bold)
