# -*- coding: utf-8 -*-
{
    'name': 'vendor payment Approval',
    'version': '17.0.1.0.0',
    'category': 'Human Resources',
    'author': 'Hilsha P H',
    'summary': 'Vendor payment Approval',
    'description': """
       Vendor payment approval levels
    """,
    'depends': ['account', 'employee','department_groups','reject_reason'],
    'data': [

        'views/account_payment_views.xml',
        'views/account_move_views.xml',
        'views/account_payment_register_views.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
