from odoo import models, fields
from odoo.exceptions import ValidationError


class RejectReason(models.TransientModel):
    _inherit = "reject.reason"

    def confirm_action(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model == 'account.payment' and active_id:
            payment = self.env[active_model].browse(active_id)
            payment.write({
                'reject_reason': self.name,
                'state': 'rejected'
            })
        return super().confirm_action()
