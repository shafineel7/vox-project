# -*- coding: utf-8 -*-
from odoo import fields, models, _, Command
from odoo.exceptions import ValidationError, UserError


class AccountPayment(models.Model):
    _inherit = "account.payment"
    _inherits = {'account.move': 'move_id'}

    def _compute_is_approve_person(self):
        fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
        fin_manager_users = self.env['res.users'].search([('groups_id', 'in', fin_manager_group.id)])
        assigned_user_ids = [user.id for user in fin_manager_users]
        for rec in self:
            if rec.payment_type == 'outbound':
                rec.is_approve_person = True if (self.env.user.id in assigned_user_ids) else False
            else:
                rec.is_approve_person = False

    is_approve_person = fields.Boolean(string='Approving Person',
                                       compute=_compute_is_approve_person,
                                       readonly=True,
                                       help="Enable/disable if approving"
                                            " person.")

    reject_reason = fields.Char(string="Reject Reason")
    is_vendor_approval = fields.Boolean(string="Vendor Approval", copy=False)
    account_invoice_line = fields.Many2one('account.move.line', string='Account Invoice line', ondelete='cascade')
    approval_finance_id = fields.Many2one('res.users', string="Approval Finance")
    is_self_assign = fields.Boolean(string="Self Assign")
    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user, tracking=True)

    def action_self_assign(self):
        for rec in self:
            if rec.state in ('waiting_approval', 'approved'):
                fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                fin_manager_users = self.env['res.users'].search([('groups_id', 'in', fin_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in fin_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_finance_id': rec.env.user.id})
            elif rec.state in ('approved','draft'):
                acc_manager_group = self.env.ref('account.group_account_manager')
                acc_user_group = self.env.ref('account.group_account_user')
                acc_billing_group = self.env.ref('account.group_account_invoice')
                account_users = self.env['res.users'].search(
                    [('groups_id', 'in', [acc_manager_group.id, acc_user_group.id, acc_billing_group.id])])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in account_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_finance_id': rec.env.user.id})

    def action_post(self):
        for rec in self:
            if rec.state == "draft" and rec.payment_type == 'outbound':
                validation = self._check_payment_approval()
                if validation:
                    if rec.state == (
                            'posted', 'cancel', 'waiting_approval', 'rejected'):
                        raise UserError(
                            _("Only a draft or approved payment can be posted."))
                    if any(inv.state != 'posted' for inv in
                           rec.reconciled_invoice_ids):
                        raise ValidationError(_("The payment cannot be processed "
                                                "because the invoice is not open!"))
                    rec.move_id._post(soft=False)
            elif self.is_vendor_approval == True and rec.state != "draft" and rec.payment_type == 'outbound':
                super().action_post()
                domain = [
                    ('parent_state', '=', 'posted'),
                    ('account_type', 'in', self.env['account.payment']._get_valid_payment_account_types()),
                    ('reconciled', '=', False),
                ]
                payment_lines = self.line_ids.filtered_domain(domain)
                # lines = self.line_ids.filtered(lambda e: e.account_type == 'liability_payable')
                lines = self.account_invoice_line

                for account in payment_lines.account_id:
                    (payment_lines + lines) \
                        .filtered_domain([('account_id', '=', account.id), ('reconciled', '=', False)]) \
                        .reconcile()
            else:
                return super().action_post()

    def _check_payment_approval(self):
        for rec in self:
            if rec.state == "draft" and rec.payment_type == 'outbound':
                rec.move_id.write({
                    'state': 'waiting_approval',
                    'is_register_payment': True
                })
                rec.write({
                    'state': 'waiting_approval',
                    'is_self_assign': False
                })
                return False
        return True

    def approve_transfer(self):
        for rec in self:
            if rec.is_approve_person:
                rec.move_id.write({
                    'state': 'approved'
                })
                rec.write({
                    'state': 'approved',
                    'is_self_assign': False
                })

    def reject_transfer(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }
