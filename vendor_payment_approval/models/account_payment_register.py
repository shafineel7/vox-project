# -*- coding: utf-8 -*-
from collections import defaultdict

from odoo import models, fields, api, _


class AccountPaymentRegister(models.TransientModel):
    _inherit = 'account.payment.register'

    is_vendor_approval = fields.Boolean(string="Vendor Approval", copy=False)
    account_invoice_line = fields.Many2one('account.move.line', string='Account Invoice line', ondelete='cascade')



    def _create_payment_vals_from_wizard(self,batch_result):
        # OVERRIDE
        payment_vals = super()._create_payment_vals_from_wizard(batch_result)
        payment_vals['is_vendor_approval'] = True if self.payment_type=='outbound' else False
        payment_vals['account_invoice_line'] = self.account_invoice_line if self.payment_type=='outbound' else False
        return payment_vals
    def _reconcile_payments(self, to_process, edit_mode=False):
        res = super()._reconcile_payments(to_process, edit_mode=edit_mode)
        for vals in to_process:
            lines = vals['to_reconcile']
            payments = vals['payment']
            for payment in payments:
                if payment.payment_type=='outbound':
                    payment.account_invoice_line = lines
        return res

    def action_create_payments(self):
        move = self.env['account.move'].browse(self.env.context['active_id'])
        if move.move_type in ('in_invoice', 'in_refund'):
            move.is_register_payment = True
        return super().action_create_payments()

    #
    #     domain = [
    #         ('parent_state', '=', 'posted'),
    #         ('account_type', 'in', self.env['account.payment']._get_valid_payment_account_types()),
    #         ('reconciled', '=', False),
    #     ]
    #     for vals in to_process:
    #         payment_lines = vals['payment'].line_ids.filtered_domain(domain)
    #         lines = vals['to_reconcile']
    #
    #         for account in payment_lines.account_id:
    #             (payment_lines + lines) \
    #                 .filtered_domain([('account_id', '=', account.id), ('reconciled', '=', False)]) \
    #                 .reconcile()