# -*- coding: utf-8 -*-

{

    'name': 'Material Requisition',
    'version': '17.0.1.0.1',
    'summary': 'This module is used to handle material requisition in inventory.',
    'description': 'Material requisition module handles the required materials form and '
                   'if that form is approved it will be converted into RFQ',
    'author': 'Abhilash.c',
    'website': 'https://www.voxtronme.com',
    'category': 'Inventory/Inventory',
    'depends': ['stock', 'purchase_management', 'employee'],
    'data': [
        'security/ir.model.access.csv',
        'security/material_requisition_security.xml',
        'data/material_sequence_data.xml',
        'data/mail_template_data.xml',
        'wizard/add_remarks_views.xml',
        'views/material_material_views.xml',
        'views/asset_issue_views.xml',
        'views/employee_views.xml',
        'views/material_requisition_menus.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,

}