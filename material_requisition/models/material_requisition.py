# -*- coding: utf-8 -*-

from odoo import api, fields, models


class MaterialRequisition(models.Model):
    _name = "material.requisition"
    _description = "Material requisition"

    product_id = fields.Many2one('product.product', string='Materials Required', required=True)
    quantity = fields.Integer(string='Quantity', required=True)
    delivery_date = fields.Date(string='Delivery Date', required=True)
    materials_id = fields.Many2one('material.material', string='Material')
    status = fields.Selection([
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
    ], string='Status', tracking=True, readonly=True)
    remarks = fields.Char(string='Remarks', readonly=True)
    partner_ids = fields.Many2many("res.partner", string='Vendor')
    is_visible = fields.Boolean(string='Visible')
    is_approve_visible = fields.Boolean(string='Approve visible')

    def action_approve(self):
        for rec in self:
            if rec.product_id:
                rec.status = 'approved'

    def action_reject(self):
        for rec in self:
            return {
                'name': 'Add remarks',
                'type': 'ir.actions.act_window',
                'res_model': 'add.remarks',
                'view_mode': 'form',
                'target': 'new',
                'context': {'active_id': rec.id},
            }

    @api.onchange('product_id')
    def onchange_product_id(self):
        for rec in self:
            if rec.product_id:
                rec.partner_ids = False

                vendors = rec.product_id.seller_ids.mapped('partner_id')

                rec.partner_ids = [(6, 0, vendors.ids)]
