# -*- coding: utf-8 -*-

from datetime import date
from odoo import api, fields, models, _, Command
from odoo.exceptions import ValidationError, UserError


class MaterialMaterial(models.Model):
    _name = "material.material"
    _description = "Material form"
    _inherit = ['mail.thread']

    name = fields.Char(string='ID', readonly=True, default='New', copy=False)
    material_ids = fields.One2many('material.requisition', 'materials_id', string='Material')
    create_date = fields.Date(string='Created Date', default=lambda self: (date.today()))
    department_id = fields.Many2one('hr.department', string='Department')
    branches_ids = fields.Many2many('res.company', string='Branches')
    state = fields.Selection([
        ('new', 'New'),
        ('submitted', 'Submitted'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('rfq', 'RFQ'),
    ], default='new', string='State', tracking=True)

    def _get_default_groups(self):
        group = self.env['res.groups']
        store_keeper = self.env.ref('department_groups.group_finance_store_keeper')
        admin_settings = self.env.ref('base.group_system')
        construction_operations_coord = self.env.ref('department_groups.group_construction_operations_coord')
        construction_operations_super = self.env.ref('department_groups.group_construction_operations_super')
        staff_operations_super = self.env.ref('department_groups.group_staff_operations_super')
        staff_operations_coord = self.env.ref('department_groups.group_staff_operations_coord')
        soft_ser_operations_super = self.env.ref('department_groups.group_soft_ser_operations_super')
        soft_ser_operations_coord = self.env.ref('department_groups.group_soft_ser_operations_coord')
        if store_keeper:
            group += store_keeper
        if admin_settings:
            group += admin_settings
        if construction_operations_coord:
            group += construction_operations_coord
        if construction_operations_super:
            group += construction_operations_super
        if staff_operations_super:
            group += staff_operations_super
        if staff_operations_coord:
            group += staff_operations_coord
        if soft_ser_operations_super:
            group += soft_ser_operations_super
        if soft_ser_operations_coord:
            group += soft_ser_operations_coord
        if group:
            return group.ids
        else:
            return False

    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user, tracking=True, )
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    self_assign = fields.Char(string="Self Assign")
    self_assignee_id = fields.Many2one('res.users', string="Self Assignee")
    is_invisible = fields.Boolean(string='Invisible', default=False)
    is_self_assign = fields.Boolean(string="Is Self Assign", default=False)

    def action_self_assign(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_finance_store_keeper'):
                store_keeper_group = self.env.ref('department_groups.group_finance_store_keeper')
                store_keeper_users = self.env['res.users'].search([('groups_id', 'in', store_keeper_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in store_keeper_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'self_assignee_id': self.env.user.id})
            elif rec.env.user.has_group('department_groups.group_finance_purchase_exec'):
                purchase_exec_group = self.env.ref('department_groups.group_finance_purchase_exec')
                purchase_exec_users = self.env['res.users'].search([('groups_id', 'in', purchase_exec_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in purchase_exec_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'self_assignee_id': self.env.user.id})
            elif rec.env.user.has_group('department_groups.group_finance_mgr'):
                finance_mgr_group = self.env.ref('department_groups.group_finance_mgr')
                finance_mgr_users = self.env['res.users'].search([('groups_id', 'in', finance_mgr_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in finance_mgr_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'self_assignee_id': self.env.user.id})
            elif rec.env.user.has_group('department_groups.group_construction_operations_coord'):
                operations_coord_group = self.env.ref('department_groups.group_construction_operations_coord')
                operations_coord_users = self.env['res.users'].search([('groups_id', 'in', operations_coord_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in operations_coord_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'self_assignee_id': self.env.user.id})
            elif rec.env.user.has_group('department_groups.group_construction_operations_super'):
                operations_super_group = self.env.ref('department_groups.group_construction_operations_super')
                operations_super_users = self.env['res.users'].search([('groups_id', 'in', operations_super_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in operations_super_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'self_assignee_id': self.env.user.id})
            elif rec.env.user.has_group('department_groups.group_staff_operations_super'):
                staff_operations_group = self.env.ref('department_groups.group_staff_operations_super')
                staff_operations_users = self.env['res.users'].search([('groups_id', 'in', staff_operations_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in staff_operations_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'self_assignee_id': self.env.user.id})
            elif rec.env.user.has_group('department_groups.group_staff_operations_coord,'):
                staff_operations_coord_group = self.env.ref('department_groups.group_staff_operations_coord')
                staff_operations_coord_users = self.env['res.users'].search(
                    [('groups_id', 'in', staff_operations_coord_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in staff_operations_coord_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'self_assignee_id': self.env.user.id})
            elif rec.env.user.has_group('department_groups.group_soft_ser_operations_super'):
                soft_ser_operations_super_group = self.env.ref('department_groups.group_soft_ser_operations_super')
                soft_ser_operations_super_users = self.env['res.users'].search(
                    [('groups_id', 'in', soft_ser_operations_super_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in soft_ser_operations_super_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'self_assignee_id': self.env.user.id})
            elif rec.env.user.has_group('department_groups.group_soft_ser_operations_coord'):
                soft_ser_operations_coord_group = self.env.ref('department_groups.group_soft_ser_operations_coord')
                soft_ser_operations_coord_users = self.env['res.users'].search(
                    [('groups_id', 'in', soft_ser_operations_coord_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in soft_ser_operations_coord_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'self_assignee_id': self.env.user.id})

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('name') or vals['name'] == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('material.requisition') or _('New')
        return super().create(vals_list)

    def submit(self):
        for record in self:
            if not record.material_ids:
                raise ValidationError("Please add materials before submitting.")

            finance_manager_group = self.env.ref('department_groups.group_finance_mgr')
            finance_manager_users = self.env['res.users'].search([('groups_id', 'in', [finance_manager_group.id])])
            finance_team_groups = self.env['res.groups'].browse([finance_manager_group.id])

            record.material_ids.write({'is_visible': True})
            record.sudo().write({
                'assigned_user_ids': [(6, 0, finance_manager_users.ids)],
                'assigned_group_ids': [(6, 0, finance_team_groups.ids)],
                'state': 'submitted',
                'is_self_assign': False
            })

            email_template = self.env.ref('material_requisition.mail_template_finance_manager_approval')
            finance_manager_emails = [manager.email for manager in finance_manager_users]

            try:
                if finance_manager_emails:
                    email_values = {'email_to': ','.join(finance_manager_emails)}
                    email_template.with_context(email_values).send_mail(record.id, force_send=True)
                    return True
                else:
                    return False
            except Exception as e:
                raise ValidationError(f"An error occurred: {str(e)}")

    def submit_procurement_team(self):
        PurchaseOrder = self.env['purchase.order']
        purchase_orders = {}

        for record in self.material_ids:
            if record.remarks == False:
                for vendor in record.partner_ids:
                    if vendor.id not in purchase_orders:
                        purchase_orders[vendor.id] = {
                            'partner_id': vendor.id,
                            'order_lines': []
                        }

                    purchase_order_line = {
                        'product_id': record.product_id.id,
                        'product_qty': record.quantity,
                    }

                    purchase_orders[vendor.id]['order_lines'].append((0, 0, purchase_order_line))

        for vendor_id, purchase_order_data in purchase_orders.items():
            PurchaseOrder.create({
                'origin': self.name,
                'materials_id': self.id,
                'partner_id': purchase_order_data['partner_id'],
                'order_line': purchase_order_data['order_lines'],
            })

            self.write({'state': 'rfq'})

        return {
            'effect': {
                'fadeout': 'slow',
                'message': 'RFQ Created Successfully',
                'type': 'rainbow_man',
            }
        }

    def action_open_purchase_order(self):
        return {
            'name': 'Purchase Order',
            'type': 'ir.actions.act_window',
            'res_model': 'purchase.order',
            'view_mode': 'tree,form',
            'domain': [('materials_id', '=', self.id)],
            'context': "{'create': False}"
        }

    @api.model
    def default_get(self, fields):
        if self.user_has_groups(
                'department_groups.group_finance_store_keeper,'
                'base.group_system,'
                'department_groups.group_construction_operations_coord,'
                'department_groups.group_construction_operations_super,'
                'department_groups.group_staff_operations_super,'
                'department_groups.group_staff_operations_coord,'
                'department_groups.group_soft_ser_operations_super,'
                'department_groups.group_soft_ser_operations_coord'):
            return super(MaterialMaterial, self).default_get(fields)
        else:
            raise UserError(_('You cannot create records. Please contact Administrator!'))

    def action_approve(self):
        for record in self:
            has_approved_material = False
            for material in record.material_ids:
                if not material.status:
                    raise ValidationError("Please ensure all products have a status before approving.")
                if material.status == "approved":
                    has_approved_material = True

            if not has_approved_material:
                raise ValidationError("Please approve at least one product before approving.")

            finance_purchase_group = self.env.ref('department_groups.group_finance_purchase_exec')
            finance_purchase_exc_users = self.env['res.users'].search([('groups_id', 'in', finance_purchase_group.id)])
            finance_purchase_groups = self.env['res.groups'].browse([finance_purchase_group.id])

            record.sudo().write({
                'assigned_user_ids': [(6, 0, finance_purchase_exc_users.ids)],
                'assigned_group_ids': [(6, 0, finance_purchase_groups.ids)],
                'state': 'approved',
                'is_self_assign': False
            })

            email_template = self.env.ref('material_requisition.mail_template_finance_purchase_executive')
            finance_purchase_exc_emails = [manager.email for manager in finance_purchase_exc_users]

            try:
                if finance_purchase_exc_emails:
                    email_values = {'email_to': ','.join(finance_purchase_exc_emails)}
                    email_template.with_context(email_values).send_mail(record.id,
                                                                        force_send=True)
                    return True
                else:
                    return False
            except Exception as e:
                raise ValidationError(f"An error occurred: {str(e)}")

    def action_reject(self):
        for record in self:
            for material in record.material_ids:
                if material.status == 'approved':
                    raise ValidationError("Rejection is not allowed if any product is approved.")
                if material.status != "rejected":
                    raise ValidationError("Please validate products before rejecting.")
            record.state = 'rejected'
            record.material_ids.write({'is_approve_visible': True})

    @api.constrains('material_ids')
    def _check_quantity(self):
        for record in self:
            for material in record.material_ids:
                if material.quantity <= 0:
                    raise ValidationError("Quantity must be greater than zero.")

