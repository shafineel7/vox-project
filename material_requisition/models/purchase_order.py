# -*- coding: utf-8 -*-
from odoo import fields, models


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    materials_id = fields.Many2one('material.material', string='Material Requisition')

