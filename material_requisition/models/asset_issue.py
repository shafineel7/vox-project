# -*- coding: utf-8 -*-

from datetime import date
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError


class AssetIssue(models.Model):
    _name = "asset.issue"
    _description = "Asset Issue"

    employee_id = fields.Many2one('hr.employee', string='Employee ID', required=True)
    date = fields.Date(string='Issued Date', default=lambda self: (date.today()), readonly=True)
    expiry_date = fields.Date(string='Expiry Date', required=True)
    product_id = fields.Many2one('product.product', string='Product', required=True)
    from_location_id = fields.Many2one('stock.location', string='From Location', required=True,
                                       domain="[('usage', '=', 'internal')]")
    to_location = fields.Char(string='To location', readonly=True)
    quantity = fields.Integer(string='Quantity', required=True)
    unit_id = fields.Many2one('uom.uom', string='Unit of Measure', readonly=True)
    status = fields.Selection([('in_progress', 'In Progress'), ('issued', 'Issued')], string='Status',
                              default='in_progress', readonly=True)
    is_asset_before_exp = fields.Boolean(string='Asset Issue Before Expiry')

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for rec in self:
            if rec.employee_id:
                rec.to_location = rec.employee_id.name

    @api.constrains('quantity', 'product_id', 'from_location_id')
    def _check_quantity(self):
        for record in self:
            if record.quantity <= 0:
                raise ValidationError("Quantity must be greater than zero.")
            if record.product_id and record.from_location_id:
                stock_quant = self.env['stock.quant'].search([
                    ('product_id', '=', record.product_id.id),
                    ('location_id', '=', record.from_location_id.id)
                ], limit=1)
                if not stock_quant or stock_quant.quantity < record.quantity:
                    raise ValidationError("Insufficient quantity available in stock.")

    def _update_qty_available(self):
        for record in self:
            if record.product_id and record.quantity and record.from_location_id:
                stock_quant = self.env['stock.quant'].search([
                    ('product_id', '=', record.product_id.id),
                    ('location_id', '=', record.from_location_id.id)
                ], limit=1)
                if stock_quant:
                    new_qty_available = stock_quant.quantity - record.quantity
                    stock_quant.write({'quantity': new_qty_available})

    @api.model
    def create(self, vals):
        record = super(AssetIssue, self).create(vals)
        record.status = 'issued'
        record._update_qty_available()
        return record

    @api.onchange('product_id')
    def _onchange_product_id(self):
        for rec in self:
            if rec.product_id:
                rec.unit_id = rec.product_id.uom_id.id

    def write(self, vals):
        user = self.env.user
        is_admin = user.has_group('base.group_system')

        if not is_admin:
            restricted_fields = [
                'employee_id', 'expiry_date', 'product_id',
                'from_location_id', 'quantity', 'unit_id'
            ]

            if any(field in vals for field in restricted_fields):
                raise UserError("You are not allowed to edit these fields")

            if 'is_asset_before_exp' in vals:
                for record in self:
                    if record.is_asset_before_exp:
                        raise UserError("You are not allowed to edit again")

        result = super(AssetIssue, self).write(vals)
        return result

    @api.model
    def default_get(self, fields):
        if (self.env.user.has_group('department_groups.group_finance_store_keeper') or self.env.user.has_group
            ('base.group_system')):
            return super(AssetIssue, self).default_get(fields)
        else:
            raise UserError(_('You cannot create records. Please contact Administrator!'))
