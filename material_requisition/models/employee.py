# -*- coding: utf-8 -*-

from odoo import fields, models


class Employee(models.Model):
    _inherit = "hr.employee"

    def action_open_asset_issues(self):
        return {
            'name': 'Asset Issues',
            'type': 'ir.actions.act_window',
            'res_model': 'asset.issue',
            'view_mode': 'tree,form',
            'domain': [('employee_id', '=', self.id)],
            'context': "{'create': False}"
        }

