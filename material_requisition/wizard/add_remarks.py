# -*- coding: utf-8 -*-

from odoo import fields, models


class AddRemarks(models.Model):
    _name = "add.remarks"
    _description = "Add Remarks"

    remarks = fields.Text(string='Remarks', required=True)

    def done(self):
        active_id = self.env.context.get('active_id')
        material = self.env['material.requisition'].browse(active_id)
        for rec in self:
            if rec.remarks:
                material.remarks = rec.remarks
                material.status = 'rejected'

        return {'type': 'ir.actions.act_window_close'}
