# -*- coding: utf-8 -*-
from odoo import fields, models, api


class RejectedReason(models.TransientModel):
    _name = 'rejected.reason'
    _description = 'Rejected Reason'

    reject_reason = fields.Text(string="Reason")

    def action_rejected(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')
        if active_model == 'employee.onboarding' and active_id:
            employee_onboarding = self.env[active_model].browse(active_id)
            employee_onboarding.reject_reasons = self.reject_reason
            employee_onboarding.message_post(body=f"Reason for Rejection is {employee_onboarding.reject_reasons}")


