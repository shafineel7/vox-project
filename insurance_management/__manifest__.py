# -*- coding: utf-8 -*-
{
    'name': 'Insurance Management',
    'version': '17.0.1.1.0',
    'category': 'Human Resources',
    'summary': 'Insurance Maintance of an employee',
    'description': """
        This module allows you to Maintain employee insurance.
    """,
    'author': 'Devika P',
    'website': 'https://www.voxtronme.com',
    'depends': ['mail', 'employee_onboarding', 'report_xlsx'],

    'data': [
        'security/ir.model.access.csv',
        'views/employee_onboarding_views.xml',
        'data/insurance_management_report_data.xml',
        'report/insurance_addition_header_footer_views.xml',
        'report/insurance_addition_report_views.xml',
        'views/hr_assistant_approval_mail_template_views.xml',
        'views/member_type_views.xml',
        'views/employee_location_views.xml',
        'views/employee_relation_views.xml',
        'views/salary_brand_views.xml',
        'views/entry_type_views.xml',
        'views/entity_type_views.xml',
        'wizard/reject_reason_views.xml',
        'views/residential_location_views.xml',
        'views/work_location_views.xml',
        'views/insurance_management_menus.xml',
    ],
    'license': 'LGPL-3',
    'installable': True,
    'application': True,
    'auto_install': False,

}
