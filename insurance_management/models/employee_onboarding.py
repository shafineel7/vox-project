# -*- coding: utf-8 -*-
import json
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class EmployeeOnboarding(models.Model):
    _inherit = 'employee.onboarding'

    employee_name = fields.Char("Name")
    occupation = fields.Char(string="Occupation")
    marital_status = fields.Selection(
        selection_add=[('unmarried', 'Unmarried'), ('widow', 'Widow')])
    address1 = fields.Char(string='Address1',related='employee_id.private_street',readonly=False,store=True)
    address2 = fields.Char(string='Address2',related='employee_id.private_street2',readonly=False,store=True)
    passport_no = fields.Char(string="Passport No")
    location = fields.Char(string="Location")
    work_location = fields.Char(string="Work Location",related='employee_id.work_location_id.name',readonly=False,store=True)
    emirates_no = fields.Char(string="Emirates Id",related='employee_id.eid_number',readonly=False,store=True)
    contact_no = fields.Char(string="Contact No",related='employee_id.private_phone',readonly=False,store=True)
    age = fields.Char(string="Age",related='employee_id.age',readonly=False,store=True)
    status = fields.Selection([('new', 'New'), ('insurance_applied', 'Insurance Applied'),
                               ('approved', 'Approved'), ('rejected', 'Rejected'),
                               ('onhold', 'Onhold')], 'State', copy=False, default='new')
    current_user_id = fields.Many2one("res.users", default=lambda self: self.env.user, string="Customer User")
    member_name = fields.Char(string="Member Name",related='employee_id.name',readonly=False,store=True)
    member_sequence = fields.Char('Member ID',related='employee_id.employee_sequence',readonly=False,store=True)
    emirates = fields.Selection(string="Emirates",
                                selection=[('abu_dubai', 'Abu Dhabi'), ('ajman', 'Ajman'), ('fujairah', 'Fujairah'),
                                           ('sharjah', 'Sharjah'), ('ras_al_khaimah', 'Ras al Khaimah'),
                                           ('umm_al_quwain', 'Umm al quwain')])
    reject_reasons = fields.Text(string="Reject Reason")

    def insurance_applied(self):
        for rec in self:
            rec.status = "insurance_applied"
            rec.message_post(body="Status updated in to insurance applied")

    def action_print_insurance_xls(self):
        records_data = []
        for record in self:
            record_data = {
                'form': record.read()[0]
            }
            records_data.append(record_data)
        return self.env.ref('insurance_management.action_insurance_generate_report_xls').sudo().report_action(self,
                                                                                                              data={
                                                                                                                  'records': records_data})

    def generate_report(self):
        report = self.env.ref('insurance_management.insurance_addition_report')
        return report.report_action(self)

    def action_insurance_applied(self):
        onboarding_records = self.env['employee.onboarding'].browse(self._context.get('active_ids', []))
        for record in onboarding_records:
            record.status = 'insurance_applied'
            record.message_post(body="Status updated in to insurance applied")

    def action_reject(self):
        for rec in self:
            rec.status = 'rejected'
            rec.message_post(body="Status updated in to Rejected")

            return {
                'type': 'ir.actions.act_window',
                'name': 'Rejected Reason',
                'res_model': 'rejected.reason',
                'view_mode': 'form',
                'target': 'new',
                'context': {'active_id': self.id},
            }

    def action_approved(self):
        for rec in self:
            rec.status = 'approved'
            rec.insurance_status = 'insurance_approved'
            rec.message_post(body="Status updated in to insurance approved")

    def action_onhold(self):
        for rec in self:
            rec.status = 'onhold'
            rec.message_post(body="Status updated in to onhold")

    def action_insurance_approved(self):
        onboarding_records = self.env['employee.onboarding'].browse(self._context.get('active_ids', []))
        for record in onboarding_records:
            record.status = 'approved'
            record.message_post(body="Status updated in to insurance approved")

    def medical_tawjeeh_completed(self):
        res = super(EmployeeOnboarding, self).medical_tawjeeh_completed()
        hr_assistant_group = self.env.ref('department_groups.group_hr_asst')

        for record in self:
            record.write({
                'employee_id': record.employee_id.id,
                'employee_name': record.employee_id.name,
                'member_name': record.employee_id.name,
                'member_sequence': record.employee_sequence,
                'gender': record.employee_id.gender,
                'dob': record.employee_id.birthday,
                'marital_status': record.employee_id.marital,
                'address1': record.employee_id.private_street,
                'address2': record.employee_id.private_street2,
                'work_location': record.employee_id.work_location_id.name,
                'nationality_id': record.employee_id.country_id.id,
                'age': record.employee_id.age,
                'phone': record.employee_id.work_phone,
                'mobile': record.employee_id.mobile_phone,
                'emirates_no': record.employee_id.eid_number,
                'name': record.employee_id.name,
                'passport_no': record.applicant_id.passport_no,
                'contact_no': record.employee_id.private_phone,


            })


        hr_assistant = self.env['res.users'].search([('groups_id', 'in', hr_assistant_group.id)])
        for assistant in hr_assistant:
            email_template = self.env.ref('insurance_management.hr_assistant_approval_template')
            email_template.send_mail(self.id, force_send=True,
                                     email_values={'recipient_ids': [(4, assistant.partner_id.id)]})
        return res

    @api.onchange('employee_id')
    def _onchange_employee_details(self):
        for record in self:
            if record.employee_id:
                values = {
                    'employee_id': record.employee_id.id,
                    'employee_name': record.employee_id.name,
                    'member_name': record.employee_id.name,
                    'member_sequence': record.employee_sequence,
                    'gender': record.employee_id.gender,
                    'dob': record.employee_id.birthday,
                    'marital_status': record.employee_id.marital,
                    'address1': record.employee_id.private_street,
                    'address2': record.employee_id.private_street2,
                    'work_location': record.employee_id.work_location_id.name,
                    'nationality_id': record.employee_id.country_id.id,
                    'age': record.employee_id.age,
                    'phone': record.employee_id.work_phone,
                    'mobile': record.employee_id.mobile_phone,
                    'emirates_no': record.employee_id.eid_number,
                    'name': record.employee_id.name,
                    'passport_no': record.applicant_id.passport_no,
                    'contact_no': record.employee_id.private_phone,
                }
                record.write(values)

    @api.model
    def _get_view(self, view_id=None, view_type='form', **options):
        arch, view = super()._get_view(view_id, view_type, **options)
        for field in arch.xpath("//field"):
            if field.get('name'):
                if (self.env.user.has_group('department_groups.group_mngmt_md') and self.env.context.get('insurance_value')==True) or (self.env.user.has_group(
                    'department_groups.group_finance_mgr') and self.env.context.get('insurance_value')==True):
                    field.set('readonly', '1')

        return arch, view

    @api.model
    def default_get(self, fields):
        rec = self.env.context
        if 'default_stage' in rec:
            if rec['default_stage'] == 'eid_application_insurance' and self.env.context.get('insurance_value')==True:
                if not self.env.user.has_group('department_groups.group_hr_asst'):
                    raise ValidationError("You cannot create insurance addition")
        return super(EmployeeOnboarding, self).default_get(fields)

    def write(self, values):
        users = self.env.ref('department_groups.group_hr_asst').users + self.env.ref(
            'department_groups.group_hr_mgr').users
        if self.stage == 'eid_application_insurance' and self.env.context.get('insurance_value')==True:
            if self.env.user not in users:
                raise ValidationError("You cannot edit insurance addition")
        return super(EmployeeOnboarding, self).write(values)

    def unlink(self):
        for record in self:
            if record.stage == 'eid_application_insurance' and self.env.context.get('insurance_value')==True:
                if self.env.user.has_group('department_groups.group_hr_asst'):
                    return super(EmployeeOnboarding, self).unlink()
                else:
                    raise ValidationError("You cannot Delete insurance addition")
