# -*- coding: utf-8 -*-


from odoo import models, fields, api


class SalaryBrand(models.Model):
    _name = 'salary.brand'

    name = fields.Char(string="Salary Brand", required=True)
