# -*- coding: utf-8 -*-


from odoo import models, fields, api


class ResidentialLocation(models.Model):
    _name = 'residential.location'

    name = fields.Char(string="Residential Location", required=True)
