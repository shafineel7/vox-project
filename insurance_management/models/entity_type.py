# -*- coding: utf-8 -*-


from odoo import models, fields, api


class EntityType(models.Model):
    _name = 'entity.type'

    name = fields.Char(string="Entity Type", required=True)
