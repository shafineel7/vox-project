# -*- coding: utf-8 -*-
from . import employee_onboarding
from . import member_type
from . import employee_relation
from . import employee_location
from . import salary_brand
from . import entity_type
from . import entry_type
from . import residential_location
from . import work_location
