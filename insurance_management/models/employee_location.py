# -*- coding: utf-8 -*-


from odoo import models, fields, api


class EmployeeLocation(models.Model):
    _name = 'employee.location'

    name = fields.Char(string="Location", required=True)
