# -*- coding: utf-8 -*-


from odoo import models, fields, api


class MemberType(models.Model):
    _name = 'member.type'

    name = fields.Char(string="Member Type", required=True)
