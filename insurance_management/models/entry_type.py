# -*- coding: utf-8 -*-


from odoo import models, fields, api


class EntryType(models.Model):
    _name = 'entry.type'

    name = fields.Char(string="Entity Type", required=True)
