# -*- coding: utf-8 -*-


from odoo import models, fields, api


class WorkLocation(models.Model):
    _name = 'work.location'

    name = fields.Char(string="Work Location", required=True)
