# -*- coding: utf-8 -*-


from odoo import models, fields, api


class EmployeeRelation(models.Model):
    _name = 'employee.relation'

    name = fields.Char(string="Relation", required=True)
