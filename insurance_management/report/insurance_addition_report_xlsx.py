# -*- coding: utf-8 -*-

import base64
import io
from odoo import models, api
from datetime import datetime, timedelta
from operator import itemgetter
import re
import xlsxwriter
import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class GeneratePmoReportXlsx(models.AbstractModel):
    _name = 'report.insurance_management.report_employee_insurance_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, complaints):
        user = self.env.user
        user_tz = self.env.user.tz or pytz.utc
        services = []
        local = pytz.timezone(user_tz)

        employee_insurance = self.env['employee.onboarding'].search(
            [('status', '=', 'new'), ('id', 'in', self._context.get('active_ids', []))])
        sheet = workbook.add_worksheet('Insurance Addition')
        merge_format = workbook.add_format({
            'bold': 0,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter', })
        merge_format.set_font_size(16)
        bold = workbook.add_format({'bold': False})
        row = 0
        column = 0

        sheet.write(row, column, 'SNO', bold)
        sheet.set_column(column, column, 15)
        sheet.write(row, column + 1, 'POLICY_ID', bold)
        sheet.set_column(column + 2, column + 2, 15)
        sheet.write(row, column + 2, 'MEMBER_REF_NO', bold)
        sheet.set_column(column + 3, column + 3, 15)
        sheet.write(row, column + 3, 'PARENT_ID', bold)
        sheet.set_column(column + 4, column + 4, 15)
        sheet.write(row, column + 4, 'SUB_GROUP', bold)
        sheet.set_column(column + 5, column + 5, 15)
        sheet.write(row, column + 5, 'CATEGORY', bold)
        sheet.set_column(column + 6, column + 6, 15)
        sheet.write(row, column + 6, 'INCEPTION_DATE', bold)
        sheet.set_column(column + 7, column + 7, 15)
        sheet.write(row, column + 7, 'DATE_OF_ENTRY', bold)
        sheet.set_column(column + 8, column + 8, 15)
        sheet.write(row, column + 8, 'MEMBER_NAME', bold)
        sheet.set_column(column + 9, column + 9, 15)
        sheet.write(row, column + 9, 'SECONDNAME', bold)
        sheet.set_column(column + 10, column + 10, 15)
        sheet.write(row, column + 10, 'FAMILYNAME', bold)
        sheet.set_column(column + 11, column + 11, 15)
        sheet.write(row, column + 11, 'MEMBER_TYPE', bold)
        sheet.set_column(column + 12, column + 12, 15)
        sheet.write(row, column + 12, 'RELATION', bold)
        sheet.set_column(column + 13, column + 13, 15)
        sheet.write(row, column + 13, 'GENDER', bold)
        sheet.set_column(column + 14, column + 14, 15)
        sheet.write(row, column + 14, 'DATE_OF_BIRTH', bold)
        sheet.set_column(column + 15, column + 15, 15)
        sheet.write(row, column + 15, 'MARITAL_STATUS', bold)
        sheet.set_column(column + 16, column + 16, 15)
        sheet.write(row, column + 16, 'OCCUPATION', bold)
        sheet.set_column(column + 17, column + 12, 15)
        sheet.write(row, column + 17, 'NATIONALITY', bold)
        sheet.set_column(column + 18, column + 18, 15)
        sheet.write(row, column + 18, 'ADDRESS1', bold)
        sheet.set_column(column + 19, column + 19, 15)
        sheet.write(row, column + 19, 'ADDRESS2', bold)
        sheet.set_column(column + 20, column + 20, 15)
        sheet.write(row, column + 20, 'PHONE_NO', bold)
        sheet.set_column(column + 21, column + 21, 15)
        sheet.write(row, column + 21, 'MOBILE_NO', bold)
        sheet.set_column(column + 22, column + 22, 15)
        sheet.write(row, column + 22, 'EMAIL_ID', bold)
        sheet.set_column(column + 23, column + 23, 15)
        sheet.write(row, column + 23, 'PASSPORT_NO', bold)
        sheet.set_column(column + 24, column + 24, 15)
        sheet.write(row, column + 24, 'LOCATION', bold)
        sheet.set_column(column + 25, column + 25, 15)
        sheet.write(row, column + 25, 'RESIDENTIALLOCATION', bold)
        sheet.set_column(column + 26, column + 26, 15)
        sheet.write(row, column + 26, 'WORKLOCATION', bold)
        sheet.set_column(column + 27, column + 27, 15)
        sheet.write(row, column + 27, 'SALARYBAND', bold)
        sheet.set_column(column + 28, column + 28, 15)
        sheet.write(row, column + 28, 'EMIRATES_ID', bold)
        sheet.set_column(column + 29, column + 29, 15)
        sheet.write(row, column + 29, 'UIDNUMBER', bold)
        sheet.set_column(column + 30, column + 30, 15)
        sheet.write(row, column + 30, 'STAFF_ID', bold)
        sheet.set_column(column + 31, column + 31, 15)
        sheet.write(row, column + 31, 'ENTRY_TYPE', bold)
        sheet.set_column(column + 32, column + 32, 15)
        sheet.write(row, column + 32, 'ISCOMMISSION', bold)
        sheet.set_column(column + 33, column + 33, 15)
        sheet.write(row, column + 33, 'ENTITYTYPE', bold)
        sheet.set_column(column + 34, column + 34, 15)
        sheet.write(row, column + 34, 'ENTITYID', bold)
        sheet.set_column(column + 35, column + 35, 15)
        sheet.write(row, column + 35, 'CONTACTNUMBER', bold)
        sheet.set_column(column + 36, column + 36, 15)
        sheet.write(row, column + 36, 'COMPANYEMAIL', bold)
        sheet.set_column(column + 37, column + 37, 15)
        sheet.write(row, column + 37, 'GDRFA_FILENUMBER', bold)
        sheet.set_column(column + 38, column + 38, 15)
        sheet.write(row, column + 38, 'BIRTHCERTIFICATEID', bold)
        sheet.set_column(column + 39, column + 39, 15)
        sheet.write(row, column + 39, 'VIP_YN', bold)
        sheet.set_column(column + 40, column + 40, 15)
        sheet.write(row, column + 40, 'EXISTING_POLICY_EXP_DATE', bold)
        sheet.set_column(column + 41, column + 41, 15)
        sheet.write(row, column + 41, 'HEIGHT', bold)
        sheet.set_column(column + 42, column + 42, 15)
        sheet.write(row, column + 42, 'WEIGHT', bold)
        sheet.set_column(column + 43, column + 43, 15)
        sheet.write(row, column + 43, 'EMIRATES', bold)
        sheet.set_column(column + 44, column + 44, 15)
        sheet.write(row, column + 44, 'HEALTH_DECLARATION_FORM_YN', bold)
        sheet.set_column(column + 45, column + 45, 15)
        sheet.write(row, column + 45, 'GROUP_DECLARATION_FORM_YN', bold)
        sheet.set_column(column + 46, column + 46, 15)
        sheet.write(row, column + 46, 'HEALTH_DECLARATION_WAIVE_YN', bold)
        sheet.set_column(column + 47, column + 47, 15)

        for insurance in employee_insurance:
            row += 1
            sheet.write(row, column, '')
            sheet.write(row + 1, column, '', bold)
            sheet.write(row, column + 2, '')
            sheet.write(row, column + 3, '')
            sheet.write(row, column + 4, '')
            sheet.write(row, column + 5, '')
            sheet.write(row, column + 6, '')
            sheet.write(row, column + 7, '')
            sheet.write(row, column + 8, '')
            sheet.write(row, column + 9, insurance.member_name if insurance.member_name else '')
            sheet.write(row, column + 10, '')
            sheet.write(row, column + 11, '')
            sheet.write(row, column + 12, '')
            sheet.write(row, column + 13, insurance.gender if insurance.gender else '')
            sheet.write(row, column + 14, insurance.dob.strftime('%d/%m/%Y %H:%M:%S') if insurance.dob else '')
            sheet.write(row, column + 15, insurance.marital_status if insurance.marital_status else '')
            sheet.write(row, column + 16, insurance.occupation if insurance.occupation else '')
            sheet.write(row, column + 17, insurance.nationality_id.name if insurance.nationality_id.name else '')
            # sheet.write(row, column + 17, '')
            sheet.write(row, column + 18, insurance.address1 if insurance.address1 else '')
            sheet.write(row, column + 19, insurance.address2 if insurance.address2 else '')
            sheet.write(row, column + 20, insurance.phone if insurance.phone else '')
            sheet.write(row, column + 21, insurance.mobile if insurance.mobile else '')
            sheet.write(row, column + 22, insurance.email if insurance.email else '')
            sheet.write(row, column + 23, insurance.passport_no if insurance.passport_no else '')
            sheet.write(row, column + 24, insurance.location if insurance.location else '')
            sheet.write(row, column + 25, '')
            sheet.write(row, column + 26, insurance.work_location if insurance.work_location else '')
            sheet.write(row, column + 27, '')
            sheet.write(row, column + 28, insurance.emirates_no if insurance.emirates_no else '')
            sheet.write(row, column + 29, '')
            sheet.write(row, column + 30, insurance.employee_id.employee_sequence if insurance.employee_id.employee_sequence else '')
            sheet.write(row, column + 31, '')
            sheet.write(row, column + 32, '')
            sheet.write(row, column + 33, '')
            sheet.write(row, column + 34, '')
            sheet.write(row, column + 35, insurance.contact_no if insurance.contact_no else '')
            sheet.write(row, column + 36, '')
            sheet.write(row, column + 37, '')
            sheet.write(row, column + 38, '')
            sheet.write(row, column + 39, '')
            sheet.write(row, column + 40, '')
            sheet.write(row, column + 41, '')
            sheet.write(row, column + 42, '')
            sheet.write(row, column + 43, insurance.emirates if insurance.emirates else '')
            sheet.write(row, column + 44, '')
            sheet.write(row, column + 45, '')
            sheet.write(row, column + 46, '')
            sheet.write(row, column + 47, '')

        member_type = self.env['member.type'].search([]).mapped("name")
        sheet.data_validation(
            1,
            11,
            1000,
            11,
            {
                "validate": "list",
                "source": member_type,
                'dropdown': True
            }
        )

        employee_relation = self.env['employee.relation'].search([]).mapped("name")
        sheet.data_validation(
            1,
            12,
            1000,
            12,
            {
                "validate": "list",
                "source": employee_relation,
                'dropdown': True
            }
        )

        sheet.data_validation(
            1,
            13,
            1000,
            13,
            {
                "validate": "list",
                "source": ['male', 'female', 'others'],
                'dropdown': True
            }
        )

        sheet.data_validation(
            1,
            15,
            1000,
            15,
            {
                "validate": "list",
                "source": ['single', 'married', 'widow', 'Divorsed'],
                'dropdown': True
            }
        )

        salary_brand = self.env['salary.brand'].search([]).mapped("name")
        sheet.data_validation(
            1,
            27,
            1000,
            27,
            {
                "validate": "list",
                "source": salary_brand,
                'dropdown': True
            }
        )

        residential_location = self.env['residential.location'].search([]).mapped("name")
        sheet.data_validation(
            1,
            25,
            1000,
            25,
            {
                "validate": "list",
                "source": residential_location,
                'dropdown': True
            }
        )

        nationality = self.env['res.country'].search([],limit=100).mapped("name")
        sheet.data_validation(
            1,
            17,
            100,
            17,
            {
                "validate": "list",
                "source": nationality,
                'dropdown': True
            }
        )

        work_location = self.env['work.location'].search([]).mapped("name")
        sheet.data_validation(
            1,
            26,
            1000,
            26,
            {
                "validate": "list",
                "source": work_location,
                'dropdown': True
            }
        )

        sheet.data_validation(
            1,
            32,
            1000,
            32,
            {
                "validate": "list",
                "source": ['yes', 'no'],
                'dropdown': True
            }
        )

        entity_type = self.env['entity.type'].search([]).mapped("name")
        sheet.data_validation(
            1,
            33,
            1000,
            33,
            {
                "validate": "list",
                "source": entity_type,
                'dropdown': True
            }
        )

        employee_location = self.env['employee.location'].search([]).mapped("name")
        sheet.data_validation(
            1,
            24,
            1000,
            24,
            {
                "validate": "list",
                "source": employee_location,
                'dropdown': True
            }
        )

        entry_type = self.env['entry.type'].search([]).mapped("name")
        sheet.data_validation(
            1,
            31,
            1000,
            31,
            {
                "validate": "list",
                "source": entry_type,
                'dropdown': True
            }
        )

        sheet.data_validation(
            1,
            39,
            1000,
            39,
            {
                "validate": "list",
                "source": ['yes', 'no'],
                'dropdown': True
            }
        )

        sheet.data_validation(
            1,
            43,
            1000,
            43,
            {
                "validate": "list",
                "source": ['Abu Dhabi', 'Ajman', 'Fujairah', 'Sharjah', 'Ras al Khaimah', 'Umm al quwain'],
                'dropdown': True
            }
        )

        sheet.data_validation(
            1,
            44,
            1000,
            44,
            {
                "validate": "list",
                "source": ['yes', 'no'],
                'dropdown': True
            }
        )

        sheet.data_validation(
            1,
            45,
            1000,
            45,
            {
                "validate": "list",
                "source": ['yes', 'no'],
                'dropdown': True
            }
        )

        sheet.data_validation(
            1,
            46,
            1000,
            46,
            {
                "validate": "list",
                "source": ['yes', 'no'],
                'dropdown': True
            }
        )
