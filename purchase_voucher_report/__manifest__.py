# -*- coding: utf-8 -*-
{
    'name': 'Purchase Voucher Document',
    'version': '17.0.1.0.0',
    'category': 'Accounting/Accounting',
    'author': 'Anoopa',
    'summary': 'Module for Downloading Purchase Voucher PDF Report',
    'description': """This module is for creating Purchase Voucher PDF report.""",
    'website': 'https://www.voxtronme.com',
    'depends': ['account_accountant', 'purchase'],
    'data': [
        'report/purchase_voucher_header.xml',
        'report/purchase_voucher_report.xml',
        'report/purchase_voucher_report_template.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'AGPL-3',
}
