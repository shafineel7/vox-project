# -*- coding: utf-8 -*-

from odoo import api, fields, models, exceptions, Command, _
from odoo.exceptions import ValidationError
import base64
import re
from datetime import datetime, timedelta,date


class HrApplicant(models.Model):
    _inherit = "hr.applicant"

    application_interview_level_ids = fields.One2many('job.application.interview.level', 'application_id',
                                                      string='Position')
    is_selected = fields.Boolean(string="Is Stage Selected", compute='_compute_is_selected')
    is_stage_signed = fields.Boolean(string="Is Stage Signed", compute='_compute_is_stage_signed')
    is_stage_onboard = fields.Boolean(string="Is Stage Onboard", compute='_compute_is_stage_onboard')
    type = fields.Selection([('direct', 'Direct'), ('indirect', 'Indirect')], string='Type', required=True)
    recruitment_type = fields.Selection([('local', 'Local'), ('overseas', 'Overseas')], string='Recruitment Type',
                                        required=True)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female'), ('others', 'Others')], string='Gender')
    dob = fields.Date(string='Date Of Birth')
    send_offer_letter_date = fields.Date(string='Send Offer letter')
    marital_status = fields.Selection([('single', 'Single'), ('married', 'Married')], string='Marital Status')
    email_to = fields.Char(string='Email To')
    nationality_id = fields.Many2one('res.country', string='Nationality')
    is_driver = fields.Boolean(string="Is Driver",copy=False)
    is_send_offer_letter = fields.Boolean(string="Is send Offer Letter",copy=False)
    is_offer_letter_issue = fields.Boolean(string="Is Offer Letter Issued",copy=False)
    designation_id = fields.Many2one('designation', string='Designation')
    passport_no = fields.Char(string='Passport Number')

    offer_letter_issue_date = fields.Date(string='Offer letter')
    is_rejected = fields.Boolean(string='Reject')
    interview_user_ids = fields.Many2many('res.users', 'interview_user_applicant_rel', 'user_id', 'interview_applicant_rel_req_id',string="Interview Users",default=lambda self: self.env.user.ids)




    def _get_default_groups(self):
        head = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
        # mgr = self.env.ref('department_groups.group_hr_mgr')
        return [Command.link(head.id)]

    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user, tracking=True)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    is_assign = fields.Boolean(string="Self Assign")
    is_logged_user = fields.Boolean(string="Logged USer", compute='_compute_logged_user')
    is_logged_group = fields.Boolean(string="Logged Group", compute='_compute_logged_user')
    is_self_assign = fields.Boolean(string="Self Assign")
    self_assignee_ids = fields.Many2many('res.users', 'hr_applicant_self_assign_rel',
                                         'hr_applicant_id', 'self_assign_id', string="Assignees")

    is_ta_stage = fields.Boolean(string="Is TA Stage",compute='_compute_is_ta_stage')


    @api.constrains('email_from', 'email_cc')
    def _check_email(self):
        for rec in self:
            if rec.email_from:
                if not re.match('(\w+[.|\w])*@(\w+[.])*\w+', rec.email_from):
                    raise exceptions.ValidationError("Wrong mail")
            if rec.email_cc:
                if not re.match('(\w+[.|\w])*@(\w+[.])*\w+', rec.email_cc):
                    raise exceptions.ValidationError("Wrong mail")


    def action_self_assign(self):
        for rec in self:
            rec.self_assignee_ids = False
            rec.assigned_user_ids = False
            rec.assigned_user_ids = [Command.link(rec.env.user.id)]
            rec.write({'is_self_assign': True, 'self_assignee_ids': [Command.link(rec.env.user.id)], })



    @api.depends('stage_id.sequence')
    def _compute_is_ta_stage(self):
        for rec in self:
            if rec.recruitment_type == 'local':
                rec.is_ta_stage = (rec.stage_id.sequence == 10)
            else:
                rec.is_ta_stage = (rec.stage_id.sequence == 28)



    @api.depends('assigned_user_ids', 'assigned_group_ids')
    def _compute_logged_user(self):
        for vals in self:
            vals.is_logged_user = False
            vals.is_logged_group = False
            if vals.env.user in vals.assigned_user_ids:
                vals.is_logged_user = True
            if vals.env.user in vals.assigned_group_ids.users:
                vals.is_logged_group = True

    @api.model
    def default_get(self, fields):
        purchase_lines = []
        active_ids = self.env.context.get('active_id')
        active_ids = active_ids or False
        interview=[]
        if self.env.context.get('active_id'):
            hr_job = self.env['hr.job'].browse(active_ids)
            if self.env.user.has_group('department_groups.group_hr_talent_acquisition_exec') or (
                    hr_job.recruitment_type == 'overseas' and self.env.user.has_group(
                'department_groups.group_agent_agent')):
                result = super(HrApplicant, self).default_get(fields)
                i = 1
                if self.env.context.get('active_id'):
                    hr_job = self.env['hr.job'].browse(active_ids)
                    result['name'] = hr_job.name

                    for line in hr_job.position_interview_level_ids:
                        if line.stage_id:
                            vals = {
                                'stage_id': line.stage_id.id if line.stage_id else False,
                                'user_ids': [(6, 0, line.user_ids.ids)],

                            }
                            i += 1
                            purchase_lines.append((0, 0, vals))

                result['application_interview_level_ids'] = purchase_lines

                return result
            else:
                raise exceptions.ValidationError("You cannot Create Applications")

        return super(HrApplicant, self).default_get(fields)

    @api.model_create_multi
    def create(self, vals_list):
        res = super().create(vals_list)
        template_id = self.env.ref('recruitment.email_template_application_creation')

        mail_to = []
        if res.email_from:
            mail_to.append(res.email_from)
            reorder_qty_mail_to = ",".join(mail_to)
            if reorder_qty_mail_to:
                if template_id and (res.id or res._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(res.id or res._origin.id,
                                                                                      force_send=True,
                                                                                  raise_exception=False)

        return res


    @api.depends('stage_id.sequence')
    def _compute_is_selected(self):
        for applicant in self:
            if applicant.recruitment_type == 'overseas':
                applicant.is_selected = (applicant.stage_id.sequence == 21)
            else:
                applicant.is_selected = (applicant.stage_id.sequence == 10)


    def action_send_offer_letter(self):
        if self.recruitment_type == 'local':
            self.email_to = self.email_from
            mail_partner = self.user_id.partner_id.ids
        else:
            self.email_to = self.agent_id.login
            mail_partner = self.agent_id.ids

        self.is_self_assign = False
        self.is_send_offer_letter = True
        date_now = date.today()
        self.send_offer_letter_date = date_now


        # template = self.env.ref('recruitment.mail_template_offer_letter_email', raise_if_not_found=False)
        # pdf = self.env.ref('recruitment.offer_letter')._render_qweb_pdf('recruitment.report_offer_letter_template', self.ids)
        # data_record = base64.b64encode(pdf[0])
        # ir_values = {
        #     'name': "Offer Letter",
        #     'type': 'binary',
        #     'datas': data_record,
        #     'store_fname': "Offer Letter",
        #     'res_model': 'hr.applicant',
        #     'res_id': self.id,
        #     'mimetype': 'application/x-pdf',
        # }
        # data_id = self.env['ir.attachment'].create(ir_values)
        # template.attachment_ids = [(6, 0, [data_id.id])]
        # mail_to = []
        # if self.email_to:
        #     mail_to.append(self.email_to)
        # reorder_qty_mail_to = ",".join(mail_to)
        # email_values = {'email_to':reorder_qty_mail_to,}
        #                 # 'email_from': self.env.user.email}
        # template.send_mail(self.id, email_values=email_values, force_send=True)
        # template.attachment_ids = [(3, data_id.id)]
        # return True


        template = self.env.ref('recruitment.mail_template_offer_letter_email', raise_if_not_found=False)

        pdf = self.env.ref('recruitment.offer_letter')._render_qweb_pdf(
            'recruitment.report_offer_letter_template', self.ids)
        b64_pdf = base64.b64encode(pdf[0])
        name = "Offer Letter"
        attachment = self.env['ir.attachment'].sudo().create({
            'name': name,
            'type': 'binary',
            'datas': b64_pdf,
            'store_fname': name,
            'res_model': 'hr.applicant',
            'res_id': self.id,
            'mimetype': 'application/x-pdf'
        })

        # self.message_post(attachment_ids=attachment.ids)
        # template.attachment_ids = [(6, 0, [attachment.id])]

        mail_to = []
        if self.email_to:
                mail_to.append(self.email_to)
        reorder_qty_mail_to = ",".join(mail_to)
        if template and self.id and reorder_qty_mail_to:
            template.write({'email_to': reorder_qty_mail_to,
                            'attachment_ids': [(6, 0, [attachment.id])]

                            })
            template.with_context({'email_to': reorder_qty_mail_to}). \
                send_mail(self.id, force_send=True, raise_exception=False)


    # def action_send_offer_letter(self):
    #     if self.recruitment_type == 'local':
    #         self.email_to = self.email_from
    #         mail_partner = self.user_id.partner_id.ids
    #     else:
    #         self.email_to = self.agent_id.login
    #         mail_partner = self.agent_id.ids
    #
    #     self.is_self_assign = False
    #     self.is_send_offer_letter = True
    #     date_now = date.today()
    #     self.send_offer_letter_date = date_now
    #
    #
    #     template = self.env.ref('recruitment.mail_template_offer_letter_email', raise_if_not_found=False)
    #
    #     pdf = self.env.ref('recruitment.offer_letter')._render_qweb_pdf(
    #         'recruitment.report_offer_letter_template', self.ids)
    #     b64_pdf = base64.b64encode(pdf[0])
    #     name = "Offer Letter"
    #     attachment = self.env['ir.attachment'].sudo().create({
    #         'name': name,
    #         'type': 'binary',
    #         'datas': b64_pdf,
    #         'store_fname': name,
    #         'res_model': 'hr.applicant',
    #         'res_id': self.id,
    #         'mimetype': 'application/x-pdf'
    #     })
    #
    #     # self.message_post(attachment_ids=attachment.ids)
    #
    #     mail_to = []
    #     if self.email_to:
    #             mail_to.append(self.email_to)
    #     reorder_qty_mail_to = ",".join(mail_to)
    #     if template and self.id and reorder_qty_mail_to:
    #         template.write({'email_to': reorder_qty_mail_to,
    #                         'attachment_ids': [(4, attachment.id)]
    #
    #                         })
    #         template.with_context({'email_to': reorder_qty_mail_to}). \
    #             send_mail(self.id, force_send=True, raise_exception=False)




    @api.depends('stage_id.sequence')
    def _compute_is_stage_signed(self):
        for applicant in self:
            applicant.is_stage_signed = (applicant.stage_id.sequence == 11)


    @api.depends('stage_id.sequence')
    def _compute_is_stage_onboard(self):
        for onboard in self:
            onboard.is_stage_onboard = (onboard.stage_id.sequence == 12)


    def action_offer_letter_signed(self):
        group = self.env['res.groups']
        head = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
        mgr = self.env.ref('department_groups.group_hr_mgr')
        hr_users = self.env['res.users'].search([('groups_id', 'in', [head.id, mgr.id])])
        acc_team_users = self.env['res.users'].search([('groups_id', 'in', [head.id, mgr.id])])
        acc_team_groups = self.env['res.groups'].browse((head.id, mgr.id))
        for rec in self:
            rec.sudo().write({
                'assigned_user_ids': [(6, 0, acc_team_users.ids)],
                'assigned_group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False

            })



        offer_letter_signed = self.env['hr.recruitment.stage'].search([('sequence', '=', 12)], limit=1)
        if offer_letter_signed:
            self.write({'stage_id': offer_letter_signed.id})
        else:
            raise ValidationError("Offer Letter Issued stage not found!")


    def action_offer_letter_issued(self):
        offer_letter_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 11)], limit=1)
        date_now = date.today()
        if offer_letter_stage:
            self.write({'stage_id': offer_letter_stage.id,
                        'offer_letter_issue_date':date_now,
                        'is_offer_letter_issue':True

                        })
        else:
            raise ValidationError("Offer Letter Issued stage not found!")


    @api.onchange('job_id')
    def _onchange_job_id(self):
        for rec in self:
            if rec.job_id:
                rec.type = rec.job_id.type
                rec.recruitment_type = rec.job_id.recruitment_type


    @api.constrains('recruitment_type', 'agent_id')
    def _check_agent_id_required(self):
        for applicant in self:
            if applicant.recruitment_type == 'overseas' and not applicant.agent_id:
                raise ValidationError("Agent is required for overseas recruitment.")


    def action_process_onboarding(self):
        process_onboarding = self.env['hr.recruitment.stage'].search([('sequence', '=', 13)], limit=1)
        if process_onboarding:
            self.write({'stage_id': process_onboarding.id,
                        'is_self_assign': False})
        else:
            raise ValidationError("Process onboarding stage not found!")
