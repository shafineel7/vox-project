# -*- coding: utf-8 -*-

from odoo import fields, models


class HrJobPosition(models.Model):
    _name = "job.position.interview.level"
    _description = "Job Position Interview Level"

    user_ids = fields.Many2many('hr.employee', string='Interviewer', required=True)
    stage_id = fields.Many2one('hr.recruitment.stage', string='Level', required=True,
                               domain="[('is_interview_stage', '=', True)]")
    job_id = fields.Many2one('hr.job', string='Job')
    date = fields.Date('Schedule Date')
