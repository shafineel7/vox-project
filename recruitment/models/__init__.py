# -*- coding: utf-8 -*-

from . import hr_recruitment_stage
from . import job_position_interview
from . import hr_job
from . import job_application_interview
from . import hr_applicant
