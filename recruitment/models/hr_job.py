# -*- coding: utf-8 -*-

from odoo import fields, models, api, _,Command,exceptions
from odoo.exceptions import ValidationError


class HrJob(models.Model):
    _inherit = "hr.job"

    position_interview_level_ids = fields.One2many('job.position.interview.level', 'job_id', string='Position')
    interview_levels = fields.Integer(string="Interview Levels")
    recruitment_type = fields.Selection([('overseas', 'Overseas'), ('local', 'Local')], required=True,
                                        string='Recruitment From',
                                        default='local')

    is_self_assign = fields.Boolean(string="Self Assign")
    self_assignee_ids = fields.Many2many('res.users', 'hr_job_self_assign_rel',
                                         'hr_job_id', 'self_assign_id', string="Assignees")
    interview_user_ids = fields.Many2many('res.users', 'interview_user_hr_rel', 'user_id', 'interview_job_rel_req_id',string="Agent Users",default=lambda self: self.env.user.ids)



    @api.model
    def default_get(self, fields):

        if self.env.user.user_has_groups('department_groups.group_hr_talent_acquisition_exec'):
            return super(HrJob, self).default_get(fields)
        else:
            raise exceptions.ValidationError("You cannot Create Job position")


    def action_self_assign(self):
        for onboarding in self:
            onboarding.write({'is_self_assign': True, 'self_assignee_ids': [Command.link(onboarding.env.user.id)], })

    @api.constrains('interview_levels')
    def _check_number(self):
        number = self.interview_levels
        if number and number > 5:
            raise ValidationError(_('maximum Interval level is 5'))

    @api.onchange('interview_levels')
    def _onchange_interview_levels(self):
        interview_list = []
        for jobs in self:
            if jobs.interview_levels == 1:
                interview_list.append(2)
            if jobs.interview_levels == 2:
                interview_list = [2, 3]
            if jobs.interview_levels == 3:
                interview_list = [2, 3, 6]
            if jobs.interview_levels == 4:
                interview_list = [2, 3, 6, 7]
            if jobs.interview_levels == 5:
                interview_list = [2, 3, 6, 7, 8]

        interview_stages = self.env['hr.recruitment.stage'].search(
            [('is_interview_stage', '=', True), ('sequence', 'in', interview_list)])

        stage_list = []
        for rec in interview_stages:
            stage_list.append([0, 0, {
                'stage_id': rec.id,
            }])
        self.position_interview_level_ids = [(6, 0, [])]
        self.write({
            'position_interview_level_ids': stage_list
        })
        return

    def update_interview_levels(self):
        for job in self:
            for application in job.application_ids:
                records_to_delete = application.application_interview_level_ids.filtered(
                    lambda r: r.stage_id not in job.position_interview_level_ids.mapped('stage_id')
                )
                if records_to_delete:
                    records_to_delete.unlink()

                for position in job.position_interview_level_ids:
                    existing_record = application.application_interview_level_ids.filtered(
                        lambda r: r.stage_id.id == position.stage_id.id
                    )
                    if existing_record:
                        for record in existing_record:
                            if not record.interview_stage:
                                record.write({
                                    'user_ids': [(6, 0, position.user_ids.ids)],
                                    'stage_id': position.stage_id.id,
                                    'scheduled_date': position.date,
                                })
                    else:
                        application.application_interview_level_ids = [(0, 0, {
                            'user_ids': [(6, 0, position.user_ids.ids)],
                            'stage_id': position.stage_id.id,
                            'scheduled_date': position.date,
                        })]
