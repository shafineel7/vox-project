# -*- coding: utf-8 -*-

from odoo import fields, models


class RecruitmentStage(models.Model):
    _inherit = "hr.recruitment.stage"

    is_interview_stage = fields.Boolean(string='Is Interview Stage')
    is_rejected_stage = fields.Boolean(string='Is Rejected Stage')
