# -*- coding: utf-8 -*-

from odoo import api, fields, models,_
from odoo.exceptions import ValidationError



class JobApplicationInterviewLevel(models.Model):
    _name = "job.application.interview.level"
    _description = "Job Application Interview Level"

    user_ids = fields.Many2many('hr.employee', string='Interviewer')
    stage_id = fields.Many2one('hr.recruitment.stage', string='Level', domain="[('is_interview_stage', '=', True)]")
    remark = fields.Text(string='Remark', readonly=True)
    interview_stage = fields.Selection([
        ('pass', 'Pass'),
        ('reject', 'Reject'),
        ('on-hold', 'On-hold'),
    ], string='Status')
    scheduled_date = fields.Date(string='Interview Date')
    actual_date = fields.Date(string='Actual Date')
    application_id = fields.Many2one('hr.applicant', string='Job')
    mail_id = fields.Many2one('mail.template', string='mail')
    # is_rejected = fields.Boolean(string='Reject')

    @api.onchange('remark')
    def _onchange_remark(self):
        for rec in self:
            if self.env.user.id not in rec.user_ids.user_id.ids:
                raise ValidationError(_("You are not allowed to edit the data."))


    @api.onchange('user_ids')
    def _onchange_user_ids(self):
        interview_list = []
        for rec in self:
            for record in rec.user_ids:
                interview_list.append(record.id)
            interview_list = list(set(interview_list))
            rec.application_id.interview_user_ids = [(6, 0, interview_list)]
            template_id = self.env.ref('recruitment.email_template_assigned_interview_notification')
            acc_team_users = self.env['res.users'].search([('id', 'in', rec.user_ids.ids)])
            mail_to = []
            for user in acc_team_users:
                if user.partner_id.email:
                    mail_to.append(user.partner_id.email)
                    list(set(mail_to))
            reorder_qty_mail_to = ",".join(mail_to)
            if template_id and (rec.id or rec._origin.id) and reorder_qty_mail_to:
                template_id.write({'email_to': reorder_qty_mail_to})
                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                    send_mail(rec.id or rec._origin.id, force_send=True, raise_exception=False)

    def add_remark(self):
        if not self.env.user.user_has_groups('department_groups.group_hr_talent_acquisition_exec'):
            for rec in self:
                if self.env.user.id not in rec.user_ids.ids:
                    raise ValidationError(_("You are not allowed to edit the data."))


        add_remark = self.env['add.remark'].create({
            'applicant_id': self.application_id.id
        })
        return {
            'name': 'Add remark',
            'type': 'ir.actions.act_window',
            'res_model': 'add.remark',
            'view_mode': 'form',
            'res_id': add_remark.id,
            'target': 'new',
            'context': {'active_id': self.id},
        }


    @api.onchange('interview_stage')
    def _onchange_interview_stage(self):
        for rec in self:
            if self.env.user.id not in rec.user_ids.user_id.ids and not self.env.user.user_has_groups('department_groups.group_hr_talent_acquisition_exec'):
                raise ValidationError(_("You are not allowed to edit the data."))
            if not rec.scheduled_date:
                raise ValidationError(_("Please Add the Interview Date"))




        stage_list = self.mapped('application_id').application_interview_level_ids.stage_id.mapped('sequence')
        gr = self.env['res.groups']
        head = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
        mgr = self.env.ref('department_groups.group_hr_mgr')
        if head:
            gr += head
        if mgr:
            gr+= mgr
        if stage_list:
            last_stage = stage_list[-1]
            selected_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 10)], limit=1)
            for interview_level in self:
                if (interview_level.stage_id.sequence == 2 and
                        interview_level.interview_stage == 'pass' and interview_level.stage_id.sequence != last_stage):
                    application = interview_level.application_id
                    second_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 3)], limit=1)
                    if second_interview_stage:
                        application.write({'stage_id': second_interview_stage.id})
                elif (interview_level.stage_id.sequence == 2 and
                      interview_level.interview_stage == 'pass' and interview_level.stage_id.sequence == last_stage):
                    application = interview_level.application_id
                    if selected_stage:
                        application.write({'stage_id': selected_stage.id})

                elif (interview_level.stage_id.sequence == 3 and
                      interview_level.interview_stage == 'pass' and interview_level.stage_id.sequence != last_stage):
                    application = interview_level.application_id
                    third_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 6)], limit=1)
                    if third_interview_stage:
                        application.write({'stage_id': third_interview_stage.id})
                elif (interview_level.stage_id.sequence == 3 and
                      interview_level.interview_stage == 'pass' and interview_level.stage_id.sequence == last_stage):
                    application = interview_level.application_id
                    if selected_stage:
                        application.write({'stage_id': selected_stage.id})

                elif (interview_level.stage_id.sequence == 6 and
                      interview_level.interview_stage == 'pass' and interview_level.stage_id.sequence != last_stage):
                    application = interview_level.application_id
                    fourth_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 7)], limit=1)
                    if fourth_interview_stage:
                        application.write({'stage_id': fourth_interview_stage.id})
                elif (interview_level.stage_id.sequence == 6 and
                      interview_level.interview_stage == 'pass' and interview_level.stage_id.sequence == last_stage):
                    application = interview_level.application_id
                    if selected_stage:
                        application.write({'stage_id': selected_stage.id})

                elif (interview_level.stage_id.sequence == 7 and
                      interview_level.interview_stage == 'pass' and interview_level.stage_id.sequence != last_stage):
                    application = interview_level.application_id
                    fifth_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 8)], limit=1)
                    if fifth_interview_stage:
                        application.write({'stage_id': fifth_interview_stage.id})
                elif (interview_level.stage_id.sequence == 7 and
                      interview_level.interview_stage == 'pass' and interview_level.stage_id.sequence == last_stage):
                    application = interview_level.application_id
                    if selected_stage:
                        application.write({'stage_id': selected_stage.id})

                elif  interview_level.stage_id.sequence == 8:
                    application = interview_level.application_id
                    if interview_level.interview_stage == 'pass':
                        sixth_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 10)],
                                                                                        limit=1)
                        if sixth_interview_stage:
                            application.write({'stage_id': sixth_interview_stage.id})

                    elif interview_level.interview_stage == 'reject':
                        seventh_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 9)],
                                                                                          limit=1)
                        if seventh_interview_stage:
                            application.write({'stage_id': seventh_interview_stage.id,
                                               'is_rejected': True

                                               })

                elif  interview_level.interview_stage == 'reject':
                    application = interview_level.application_id
                    seventh_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 9)], limit=1)
                    if seventh_interview_stage:
                        application.write({'stage_id': seventh_interview_stage.id,
                                           'is_rejected':True})

                elif  interview_level.interview_stage == 'on-hold':
                    application = interview_level.application_id
                    seventh_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 23)], limit=1)
                    if seventh_interview_stage:
                        application.write({'stage_id': seventh_interview_stage.id})


                for rec in interview_level.application_id:
                    if gr:
                        rec.assigned_group_ids = gr.ids
                        if gr.users:
                            rec.assigned_user_ids = gr.users.ids
                        else:
                            rec.assigned_user_ids = False
                    else:
                        rec.assigned_user_ids = False

        return


    def write(self, vals):
        result = super(JobApplicationInterviewLevel, self).write(vals)
        stage_list = self.mapped('application_id').application_interview_level_ids.stage_id.mapped('sequence')
        gr = self.env['res.groups']
        head = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
        mgr = self.env.ref('department_groups.group_hr_mgr')
        if head:
            gr += head
        if mgr:
            gr+= mgr
        if stage_list:
            last_stage = stage_list[-1]
            selected_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 10)], limit=1)
            for interview_level in self:
                if ('interview_stage' in vals and interview_level.stage_id.sequence == 2 and
                        vals['interview_stage'] == 'pass' and interview_level.stage_id.sequence != last_stage):
                    application = interview_level.application_id
                    second_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 3)], limit=1)
                    if second_interview_stage:
                        application.write({'stage_id': second_interview_stage.id})
                elif ('interview_stage' in vals and interview_level.stage_id.sequence == 2 and
                      vals['interview_stage'] == 'pass' and interview_level.stage_id.sequence == last_stage):
                    application = interview_level.application_id
                    if selected_stage:
                        application.write({'stage_id': selected_stage.id})

                elif ('interview_stage' in vals and interview_level.stage_id.sequence == 3 and
                      vals['interview_stage'] == 'pass' and interview_level.stage_id.sequence != last_stage):
                    application = interview_level.application_id
                    third_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 6)], limit=1)
                    if third_interview_stage:
                        application.write({'stage_id': third_interview_stage.id})
                elif ('interview_stage' in vals and interview_level.stage_id.sequence == 3 and
                      vals['interview_stage'] == 'pass' and interview_level.stage_id.sequence == last_stage):
                    application = interview_level.application_id
                    if selected_stage:
                        application.write({'stage_id': selected_stage.id})

                elif ('interview_stage' in vals and interview_level.stage_id.sequence == 6 and
                      vals['interview_stage'] == 'pass' and interview_level.stage_id.sequence != last_stage):
                    application = interview_level.application_id
                    fourth_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 7)], limit=1)
                    if fourth_interview_stage:
                        application.write({'stage_id': fourth_interview_stage.id})
                elif ('interview_stage' in vals and interview_level.stage_id.sequence == 6 and
                      vals['interview_stage'] == 'pass' and interview_level.stage_id.sequence == last_stage):
                    application = interview_level.application_id
                    if selected_stage:
                        application.write({'stage_id': selected_stage.id})

                elif ('interview_stage' in vals and interview_level.stage_id.sequence == 7 and
                      vals['interview_stage'] == 'pass' and interview_level.stage_id.sequence != last_stage):
                    application = interview_level.application_id
                    fifth_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 8)], limit=1)
                    if fifth_interview_stage:
                        application.write({'stage_id': fifth_interview_stage.id})
                elif ('interview_stage' in vals and interview_level.stage_id.sequence == 7 and
                      vals['interview_stage'] == 'pass' and interview_level.stage_id.sequence == last_stage):
                    application = interview_level.application_id
                    if selected_stage:
                        application.write({'stage_id': selected_stage.id})

                elif 'interview_stage' in vals and interview_level.stage_id.sequence == 8:
                    application = interview_level.application_id
                    if vals['interview_stage'] == 'pass':
                        sixth_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 10)],
                                                                                        limit=1)
                        if sixth_interview_stage:
                            application.write({'stage_id': sixth_interview_stage.id})

                    elif vals['interview_stage'] == 'reject':
                        seventh_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 9)],
                                                                                          limit=1)
                        if seventh_interview_stage:
                            application.write({'stage_id': seventh_interview_stage.id,
                                               'is_rejected': True

                                               })

                elif 'interview_stage' in vals and vals['interview_stage'] == 'reject':
                    application = interview_level.application_id
                    seventh_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 9)], limit=1)
                    if seventh_interview_stage:
                        application.write({'stage_id': seventh_interview_stage.id,
                                           'is_rejected':True})

                elif 'interview_stage' in vals and vals['interview_stage'] == 'on-hold':
                    application = interview_level.application_id
                    seventh_interview_stage = self.env['hr.recruitment.stage'].search([('sequence', '=', 23)], limit=1)
                    if seventh_interview_stage:
                        application.write({'stage_id': seventh_interview_stage.id})


                for rec in interview_level.application_id:
                    if gr:
                        rec.assigned_group_ids = gr.ids
                        if gr.users:
                            rec.assigned_user_ids = gr.users.ids
                        else:
                            rec.assigned_user_ids = False
                    else:
                        rec.assigned_user_ids = False

        return result
