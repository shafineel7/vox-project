import io
from odoo import models, fields, api
from datetime import datetime
import pytz


class JobPositionReportXlsx(models.AbstractModel):
    _name = 'report.recruitment.report_job_position_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, complaints):
        user = self.env.user
        user_tz = self.env.user.tz or pytz.utc
        services = []
        local = pytz.timezone(user_tz)

        job_position_selected = data['form'][0]['job_position_id']
        recruitment_type = data['form'][0]['recruitment_type']
        types = data['form'][0]['type']
        agent_selected = data['form'][0]['agent_ids']
        agent_id = []
        for agent in agent_selected:
            agent_id.append(agent)
        from_date = data['form'][0]['from_date']
        to_date = data['form'][0]['to_date']
        search_condition = [()]

        if user.has_group('department_groups.group_agent_agent'):
            agent_id = [user.id]

        if job_position_selected:
            search_condition = ['|', '|', '|', ('stage_id.sequence', '=', 21), ('stage_id.sequence', '=', 22),
                                ('stage_id.sequence', '=', 9), ('stage_id.sequence', '=', 10)]
            job_position_id = job_position_selected[0]
            search_condition.append(('job_id', '=', job_position_id))

            if types == 'direct':
                search_condition.append(('type', '=', 'direct'))
            elif types == 'indirect':
                search_condition.append(('type', '=', 'indirect'))

            if recruitment_type == 'local':
                search_condition.append(('job_id.recruitment_type', '=', 'local'))
            elif recruitment_type == 'overseas':
                search_condition.append(('job_id.recruitment_type', '=', 'overseas'))
            if agent_selected:
                search_condition.append(('agent_id', 'in', agent_id))
        elif not job_position_selected:
            search_condition = ['|', '|', '|', ('stage_id.sequence', '=', 21), ('stage_id.sequence', '=', 22),
                                ('stage_id.sequence', '=', 9), ('stage_id.sequence', '=', 10)]
            if types == 'direct':
                search_condition.append(('type', '=', 'direct'))
            elif types == 'indirect':
                search_condition.append(('type', '=', 'indirect'))

            if recruitment_type == 'local':
                search_condition.append(('job_id.recruitment_type', '=', 'local'))
            elif recruitment_type == 'overseas':
                search_condition.append(('job_id.recruitment_type', '=', 'overseas'))
            if agent_selected:
                search_condition.append(('agent_id', 'in', agent_id))


        if from_date:
            search_condition += [('create_date', '>=', from_date)]

        if to_date:
            search_condition += [('create_date', '<=', to_date)]
        applicants = self.env['hr.applicant'].search(search_condition)

        total_applicants_condition = []
        if job_position_selected:
            total_applicants_condition = [('job_id', '=', job_position_selected[0])]
            if types == 'direct':
                total_applicants_condition.append(('type', '=', 'direct'))
            elif types == 'indirect':
                total_applicants_condition.append(('type', '=', 'indirect'))

            if recruitment_type == 'local':
                total_applicants_condition.append(('job_id.recruitment_type', '=', 'local'))
            elif recruitment_type == 'overseas':
                total_applicants_condition.append(('job_id.recruitment_type', '=', 'overseas'))
            if agent_selected:
                total_applicants_condition.append(('agent_id', 'in', agent_id))
        elif not job_position_selected:
            if types == 'direct':
                total_applicants_condition.append(('type', '=', 'direct'))
            elif types == 'indirect':
                total_applicants_condition.append(('type', '=', 'indirect'))

            if recruitment_type == 'local':
                total_applicants_condition.append(('job_id.recruitment_type', '=', 'local'))
            elif recruitment_type == 'overseas':
                total_applicants_condition.append(('job_id.recruitment_type', '=', 'overseas'))
            if agent_selected:
                total_applicants_condition.append(('agent_id', 'in', agent_id))


        total_applicants_count = self.env['hr.applicant'].search_count(total_applicants_condition)

        selected_condition_local = [('stage_id.sequence', '=', 10), ('job_id.recruitment_type', '=', 'local')]
        selected_condition_overseas = [('stage_id.sequence', '=', 21), ('job_id.recruitment_type', '=', 'overseas')]
        rejected_condition_local = [('stage_id.sequence', '=', 9), ('job_id.recruitment_type', '=', 'local')]
        rejected_condition_overseas = [('stage_id.sequence', '=', 22), ('job_id.recruitment_type', '=', 'overseas')]
        selected_condition_total = ['|', ('stage_id.sequence', '=', 10), ('stage_id.sequence', '=', 21)]
        rejected_condition_total = ['|', ('stage_id.sequence', '=', 9), ('stage_id.sequence', '=', 22)]

        if job_position_selected:
            selected_condition_local.append(('job_id', '=', job_position_selected[0]))
            selected_condition_overseas.append(('job_id', '=', job_position_selected[0]))
            rejected_condition_local.append(('job_id', '=', job_position_selected[0]))
            rejected_condition_overseas.append(('job_id', '=', job_position_selected[0]))

        if agent_selected:
            selected_condition_local.append(('agent_id', 'in', agent_id))
            selected_condition_overseas.append(('agent_id', 'in', agent_id))
            rejected_condition_local.append(('agent_id', 'in', agent_id))
            rejected_condition_overseas.append(('agent_id', 'in', agent_id))
            selected_condition_total.append(('agent_id', 'in', agent_id))
            rejected_condition_total.append(('agent_id', 'in', agent_id))

        if from_date:
            selected_condition_local += [('create_date', '>=', from_date), ]
            selected_condition_overseas += [('create_date', '>=', from_date)]
            rejected_condition_local += [('create_date', '>=', from_date)]
            rejected_condition_overseas += [('create_date', '>=', from_date)]
            selected_condition_total += [('create_date', '>=', from_date)]
            rejected_condition_total += [('create_date', '>=', from_date)]

        if to_date:
            selected_condition_local += [('create_date', '<=', to_date)]
            selected_condition_overseas += [('create_date', '<=', to_date)]
            rejected_condition_local += [('create_date', '<=', to_date)]
            rejected_condition_overseas += [('create_date', '<=', to_date)]
            selected_condition_total += [('create_date', '>=', from_date)]
            rejected_condition_total += [('create_date', '>=', from_date)]

        if types:
            selected_condition_local += [('type', '=', types)]
            selected_condition_overseas += [('type', '=', types)]
            rejected_condition_local += [('type', '=', types)]
            rejected_condition_overseas += [('type', '=', types)]
            selected_condition_total += [('type', '=', types)]
            rejected_condition_total += [('type', '=', types)]


        if recruitment_type == 'local':
            selected_count = self.env['hr.applicant'].search_count(selected_condition_local)
            rejected_count = self.env['hr.applicant'].search_count(rejected_condition_local)
        elif recruitment_type == 'overseas':
            selected_count = self.env['hr.applicant'].search_count(selected_condition_overseas)
            rejected_count = self.env['hr.applicant'].search_count(rejected_condition_overseas)
        else:
            selected_count = self.env['hr.applicant'].search_count(selected_condition_total)
            rejected_count = self.env['hr.applicant'].search_count(rejected_condition_total)

        sheet = workbook.add_worksheet('Job Position')
        merge_format = workbook.add_format({
            'bold': 0,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter', })
        merge_format.set_font_size(16)
        bold = workbook.add_format({'bold': True})
        bold_center = workbook.add_format({'bold': True, 'align': 'center', 'valign': 'vcenter'})
        bold_center.set_font_size(14)
        regular_center = workbook.add_format({'align': 'center', 'valign': 'vcenter'})
        regular_center.set_font_size(12)

        box_format = workbook.add_format({'bold': True, 'border': 1, 'align': 'center', 'valign': 'vcenter'})
        box_format.set_font_size(12)
        row = 0
        column = 0

        headers = ['JOB POSITION', 'TYPE', 'RECRUITMENT FROM', 'NAME OF APPLICANT', 'STAGE', 'AGENT', '', '', ]

        column_widths = [20, 20, 20, 20, 20, 20, 10, 10, 21, 15, 15]
        for col_num, width in enumerate(column_widths):
            sheet.set_column(col_num, col_num, width)

        for col_num, header in enumerate(headers):
            sheet.write(0, col_num, header, bold)

        row = 1
        for applicant in applicants:
            sheet.write(row, 0, applicant.job_id.name if applicant.job_id else ''),
            sheet.write(row, 1, applicant.job_id.type if applicant.job_id.type else ''),
            sheet.write(row, 2, applicant.job_id.recruitment_type if applicant.job_id.recruitment_type else ''),
            sheet.write(row, 3, applicant.partner_name if applicant.partner_name else ''),
            sheet.write(row, 4, applicant.stage_id.name if applicant.stage_id else ''),
            sheet.write(row, 5, applicant.agent_id.name if applicant.agent_id else '')

            row += 1

        start_row = 1
        start_col = 7

        sheet.merge_range(start_row, start_col, start_row, start_col + 1, '', box_format)
        sheet.merge_range(start_row + 1, start_col, start_row + 1, start_col + 1, 'TOTAL NO OF APPLICANTS', box_format)
        sheet.merge_range(start_row + 2, start_col, start_row + 2, start_col + 1, total_applicants_count, box_format)

        current_row = start_row + 3

        sheet.merge_range(current_row, start_col, current_row, start_col + 1, 'SELECTED APPLICANTS', box_format)
        sheet.merge_range(current_row + 1, start_col, current_row + 1, start_col + 1, selected_count, box_format)
        current_row += 2

        sheet.merge_range(current_row, start_col, current_row, start_col + 1, 'REJECTED APPLICANTS', box_format)
        sheet.merge_range(current_row + 1, start_col, current_row + 1, start_col + 1, rejected_count, box_format)
