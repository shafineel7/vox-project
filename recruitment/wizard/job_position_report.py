from odoo import fields, models, api


class JobPositionReport(models.TransientModel):
    _name = "job.position.report"
    _description = "Hiring Statistics Report"

    job_position_id = fields.Many2one('hr.job', string="Job Positions", domain="[('id', 'in', job_ids)]",
                                      default=False)
    type = fields.Selection([('direct', 'Direct'), ('indirect', 'Indirect')], string='Type')
    recruitment_type = fields.Selection([('overseas', 'Overseas'), ('local', 'Local')], string='Recruitment From',
                                        )
    agent_ids = fields.Many2many('res.users', string='Agent', domain="[('agent_id', '!=', False)]")
    from_date = fields.Date(string="From Date")
    to_date = fields.Date(string="To Date")
    job_ids = fields.Many2many('hr.job', compute='_compute_job_ids')

    @api.depends('job_position_id')
    def _compute_job_ids(self):
        if self.env.user.has_group('department_groups.group_agent_agent'):
            applicants = self.env['hr.applicant'].search([('agent_id', '=', self.env.user.id)])
            self.job_ids = applicants.mapped('job_id').ids
        else:
            all_jobs = self.env['hr.job'].search([])
            self.job_ids = all_jobs.ids

    @api.model
    def default_get(self, fields):
        res = super(JobPositionReport, self).default_get(fields)
        if self.env.user.has_group('department_groups.group_agent_agent'):
            res['recruitment_type'] = 'overseas'
            res['agent_ids'] = self.env.user
        return res

    def print_report_xlsx(self):
        self.ensure_one()
        data = self.read()
        data = {
            'form': data,
        }
        return self.env.ref('recruitment.action_job_position_generate_report_xls').sudo().report_action(self, data=data)

    @api.onchange("job_position_id")
    def _onchange_model_type_id(self):
        for record in self:
            if record.job_position_id:
                record.write({
                    'type': record.job_position_id.type,
                    'recruitment_type': record.job_position_id.recruitment_type,
                })
