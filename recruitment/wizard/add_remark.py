# -*- coding: utf-8 -*-

from odoo import fields, models
from odoo.exceptions import ValidationError


class AddRemark(models.TransientModel):
    _name = "add.remark"
    _description = "Add Remark"

    remarks = fields.Text(string='Remarks')
    applicant_id = fields.Many2one('hr.applicant', string='Job Application')

    def send_rejection_email(self):
        try:
            template_id = self.env.ref('recruitment.mail_template_rejection_email')
            template_id.send_mail(template_id.id, force_send=True)
        except ValidationError:
            pass

    def update_interview_level(self, interview_stage):
        active_id = self.env.context.get('active_id')
        interview_level = self.env['job.application.interview.level'].browse(active_id)
        if self.remarks:
            interview_level.remark = self.remarks
            interview_level.interview_stage = interview_stage

            if interview_level.stage_id.sequence == 9 and interview_stage == 'reject':
                self.send_rejection_email()

        return {'type': 'ir.actions.act_window_close'}

    def passed(self):
        return self.update_interview_level('pass')

    def reject(self):
        return self.update_interview_level('reject')

    def on_hold(self):
        return self.update_interview_level('on-hold')
