# -*- coding: utf-8 -*-

{

    'name': 'Recruitment',
    'author': 'Abhilash.c',
    'version': '17.0.1.0.1',
    'summary': 'Hr Recruitment Module.',
    'category': 'Recruitment',
    'depends': ['manpower_requisition', 'mail', 'contacts', 'employee_timesheet', 'report_xlsx'],
    'description': 'Added different level of interview stages from job position to job application',
    'data': [
        'security/ir.model.access.csv',
        'security/recruitment_security.xml',
        'data/hr_recruitment_stage_demo.xml',
        'data/mail_template_data.xml',
        'report/offer_letter_report_views.xml',
        'wizard/add_remark_views.xml',
        'wizard/job_position_report_views.xml',
        'views/hr_recruitment_stage_views.xml',
        'views/hr_job_views.xml',
        'views/job_position_interview_views.xml',
        'views/hr_applicant_views.xml',
        'views/job_application_interview_views.xml',
        'views/recruitment_menus.xml'

    ],
    'installable': True,
    'auto_install': False,
    'application': True,

}
