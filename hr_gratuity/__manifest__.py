# -*- coding: utf-8 -*-
{
    'name': "HR Gratuity",

    'summary': "For gratuity calculation",

    'description': """
        This module help users to calculate gratuity for employees during employee exit. HR manager can create gratuity configuration and rules. The Gratuity Amount is calculated based on this rules.
    """,

    'author': "Arya I R",
    'website': "https://www.voxtronme.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Human Resources',
    'version': '17.0.1.0.0',

    # any module necessary for this one to work correctly
    'depends': ['employee'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/gratuity_configuration_security.xml',
        'views/hr_salary_rule_views.xml',
        'views/gratuity_configuration_views.xml',
        'views/gratuity_configuration_menus.xml',
        'views/gratuity_rule_views.xml',
        'views/designation_views.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}

