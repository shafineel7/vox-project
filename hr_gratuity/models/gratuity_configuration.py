# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import math
from collections import defaultdict
from odoo.tools import float_round
from odoo.tools.float_utils import float_compare
from odoo.tools.safe_eval import safe_eval
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError


class GratuityConfiguration(models.Model):
    """ Model for gratuity duration configuration details """
    _name = 'gratuity.configuration'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Gratuity Configuration"
    _rec_name = "name"

    name = fields.Char(string='Name', help='Name of Gratuity', required=True)
    active = fields.Boolean(default=True)
    gratuity_rule_ids = fields.One2many('gratuity.rule', 'gratuity_configuration_id', string="Gratuity Rules")
    salary_rule_ids = fields.Many2many('hr.salary.rule', string="Salary Rules", required=True,
                                       domain="[('is_rule_for_gratuity', '=', True)]")
    company_id = fields.Many2one('res.company', 'Company',
                                 required=True, help="Company",
                                 index=True,
                                 default=lambda self: self.env.company)

    _sql_constraints = [('name_uniq', 'unique(name)',
                         'Gratuity configuration name should be unique!')]

    def _get_base_local_dict(self):
        return {
            'float_round': float_round,
            'float_compare': float_compare,
            "relativedelta": relativedelta,
            "ceil": math.ceil,
            "floor": math.floor,
            'UserError': UserError,
            'date': date,
            'datetime': datetime,
        }

    def get_localdict(self, employee):
        self.ensure_one()
        localdict = {
            **self._get_base_local_dict(),
            **{
                'categories': defaultdict(lambda: 0),
                'rules': defaultdict(lambda: dict(total=0, amount=0, quantity=0)),
                'payslip': False,
                'worked_days': {},
                'inputs': {},
                'employee': employee,
                'contract': employee.contract_id,
                'result_rules': defaultdict(lambda: dict(total=0, amount=0, quantity=0, rate=0)),
                'same_type_input_lines': {},
            }
        }
        return localdict

    def get_basic_salary(self, employee):
        self.ensure_one()
        if not employee.contract_id:
            raise UserError(
                _("There's no contract set on employee %s. Check that there is at least a contract set on the employee form.",
                  employee.name))
        localdict = self.get_localdict(employee)
        rules_dict = localdict['rules']
        result_rules_dict = localdict['result_rules']
        result = {}
        basic_salary = 0
        for rule in sorted(self.salary_rule_ids, key=lambda x: x.sequence):
            localdict.update({
                'result': None,
                'result_qty': 1.0,
                'result_rate': 100,
                'result_name': False
            })
            if rule._satisfy_condition(localdict):
                if rule.condition_select == 'none':
                    if rule.amount_select == 'percentage':
                        try:
                            safe_eval(rule.amount_percentage_base, localdict)
                        except Exception as e:
                            continue
                            rule._raise_error(localdict, _("Wrong percentage base or quantity defined for:"), e)
                    if rule.amount_select == 'code':
                        try:
                            safe_eval(rule.amount_python_compute or 0.0, localdict, mode='exec', nocopy=True)
                        except Exception as e:
                            continue
                            rule._raise_error(localdict, _("Wrong python code defined for:"), e)

                amount, qty, rate = rule._compute_rule(localdict)
                # check if there is already a rule computed with that code
                previous_amount = localdict.get(rule.code, 0.0)
                # set/overwrite the amount computed for this rule in the localdict
                tot_rule = amount * qty * rate / 100.0
                localdict[rule.code] = tot_rule
                result_rules_dict[rule.code] = {'total': tot_rule, 'amount': amount, 'quantity': qty,
                                                'rate': rate}
                rules_dict[rule.code] = rule
                # sum the amount for its salary category
                localdict = rule.category_id._sum_salary_rule_category(localdict, tot_rule - previous_amount)
                basic_salary += float(qty) * amount * rate / 100
        return basic_salary

    def calculate_gratuity(self, employee, employee_gratuity_years, actual_last_working_date):
        self.ensure_one()
        gratuity_rule_id = False
        employee_gratuity_amount = 0
        gratuity_details = {}
        gratuity_splitup = ''' '''
        employee_basic_salary = self.get_basic_salary(employee)
        for rule in self.gratuity_rule_ids:
            if (rule.from_year and rule.to_year and
                    rule.from_year <= employee_gratuity_years <=
                    rule.to_year):
                gratuity_rule_id = rule
                break
            elif (rule.from_year and not rule.to_year and
                  rule.from_year <= employee_gratuity_years):
                gratuity_rule_id = rule
                break
            elif (rule.to_year and not rule.from_year and
                  employee_gratuity_years <= rule.to_year):
                gratuity_rule_id = rule
                break
        months = actual_last_working_date.month - employee.date_of_joining.month
        years = actual_last_working_date.year - employee.date_of_joining.year
        days = actual_last_working_date.day - employee.date_of_joining.day
        if employee_gratuity_years < 1:
            employee_gratuity_amount = 0
        else:
            if gratuity_rule_id:
                if gratuity_rule_id.parent_id:
                    remaining_years = years - gratuity_rule_id.parent_id.to_year

                    daily_wage_parent = (
                                employee_basic_salary / gratuity_rule_id.parent_id.employee_daily_wage_days)
                    working_days_salary_parent = (daily_wage_parent * gratuity_rule_id.parent_id.employee_working_days)
                    employee_gratuity_amount_parent = (working_days_salary_parent * gratuity_rule_id.parent_id.to_year)
                    employee_gratuity_amount += employee_gratuity_amount_parent

                    # Remaining years gratuity
                    daily_wage_main = (
                            employee_basic_salary / gratuity_rule_id.employee_daily_wage_days)
                    working_days_salary_main = (daily_wage_main * gratuity_rule_id.employee_working_days)
                    if remaining_years:
                        employee_gratuity_amount += working_days_salary_main * remaining_years
                    if months:
                        employee_gratuity_amount += working_days_salary_main * (months/12)
                    if days:
                        employee_gratuity_amount += working_days_salary_main * (days/365)
                    gratuity_splitup = ("<b>" + gratuity_rule_id.parent_id.name + "</b><br/>" + str(gratuity_rule_id.parent_id.employee_working_days) + " days salary = "
                    + str(round(working_days_salary_parent, 2)) + "<br/> Up to " + str(gratuity_rule_id.parent_id.to_year)
                    + " year salary = " + str(round(employee_gratuity_amount_parent, 2)) + "<br/><b> " + gratuity_rule_id.name + "</b><br/>"
                    + str(gratuity_rule_id.employee_working_days) + " days salary = " + str(round(working_days_salary_main, 2)) + "<br/> Remining " + str(remaining_years) + " years salary = " + str(round(employee_gratuity_amount -employee_gratuity_amount_parent,2)))

                else:
                    daily_wage = (employee_basic_salary / gratuity_rule_id.employee_daily_wage_days)
                    working_days_salary = (daily_wage * gratuity_rule_id.employee_working_days)
                    if years:
                        employee_gratuity_amount += working_days_salary * years
                    if months:
                        employee_gratuity_amount += working_days_salary * (months/12)
                    if days:
                        employee_gratuity_amount += working_days_salary * (days/365)

                    gratuity_splitup = ("<b>" + gratuity_rule_id.name + "</b><br/>" + str(
                        gratuity_rule_id.employee_working_days) + " days salary = " + str(round(working_days_salary, 2))
                                        + "<br/> Up to " + str(gratuity_rule_id.to_year) + " year salary = " + str(round(employee_gratuity_amount, 2)))

        return gratuity_splitup, employee_basic_salary, employee_gratuity_amount
