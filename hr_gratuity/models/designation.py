# -*- coding: utf-8 -*-

from odoo import models, fields


class Designation(models.Model):
    _inherit = 'designation'

    gratuity_configuration_id = fields.Many2one('gratuity.configuration', 'Gratuity Configuration')

