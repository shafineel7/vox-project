# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class HrSalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    is_rule_for_gratuity = fields.Boolean(default=False, string="Consider for Gratuity")
