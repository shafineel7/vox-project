# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class GratuityRule(models.Model):
    _name = 'gratuity.rule'
    _description = "Gratuity Rule"
    _rec_name = "name"

    name = fields.Char(string="Name", required=True)
    active = fields.Boolean(default=True, help="Active the record")
    from_year = fields.Float(string="From Year")
    to_year = fields.Float(string="To Year")
    is_yr_from_flag = fields.Boolean(compute="_compute_yr_field_required",
                                  store=True, help="Ensure the from date")
    is_yr_to_flag = fields.Boolean(compute="_compute_yr_field_required",
                                store=True, help="Ensure the end date")
    company_id = fields.Many2one(related="gratuity_configuration_id.company_id")
    employee_daily_wage_days = fields.Integer(default=30, help="Total number "
                                                               "of employee "
                                                               "wage days")
    employee_working_days = fields.Integer(string='Working Days', default=21,
                                           help='Number of working days per '
                                                'month')
    percentage = fields.Float(default=1, help="Gratuity payment percentage")
    parent_id = fields.Many2one('gratuity.rule', string="Parent Rule")
    gratuity_configuration_id = fields.Many2one('gratuity.configuration', 'Gratuity Configuration')

    @api.onchange('from_year', 'to_year')
    def _onchange_year(self):
        if self.from_year and self.to_year:
            if not self.from_year < self.to_year:
                raise UserError(_("Invalid year configuration!"))

    @api.depends('from_year', 'to_year')
    def _compute_yr_field_required(self):
        for rec in self:
            rec.is_yr_from_flag = True if not rec.to_year else False
            rec.is_yr_to_flag = True if not rec.from_year else False
