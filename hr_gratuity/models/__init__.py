# -*- coding: utf-8 -*-

from . import hr_salary_rule
from . import gratuity_configuration
from . import gratuity_rule
from . import designation
