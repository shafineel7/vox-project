# -*- coding: utf-8 -*-
{
    'name': 'Employee Onboarding',
    'version': '17.0.1.0.0',
    'category': 'Human Resources',
    'summary': 'Employee Onboarding',
    'description': """
        This module allows you to track employee onboarding process.
    """,

    'author': 'HILSHA P H',
    'website': 'https://www.voxtronme.com',
    'depends': ['employee', 'department_groups', 'recruitment'],
    'data': [
        'security/ir.model.access.csv',
        'security/employee_onboarding_security.xml',
        'data/onboading_cron.xml',
        'data/employee_onboarding_sequence_data.xml',
        'data/mail_template_data.xml',
        'views/employee_onboarding_local_views.xml',
        'views/employee_onboarding_overseas_views.xml',
        'views/employee_onboarding_menus.xml',
        'views/hr_employee_views.xml',
        'views/agent_agent_views.xml',
    ],
    'license': 'LGPL-3',
    'installable': True,
    'application': True,
    'auto_install': False,

}
