# -*- coding: utf-8 -*-

from odoo import api, fields, models,_


class Employee(models.Model):
    _inherit = "hr.employee"

    employee_onboarding_id = fields.Many2one('employee.onboarding', string="Onboarding")
    attachment_ids = fields.Many2many('ir.attachment', string='Attachments')

    @api.model
    def create(self, vals):
        templates = super(Employee, self).create(vals)
        for template in templates:
            if template.attachment_ids:
                template.attachment_ids.write({'res_model': self._name, 'res_id': template.id})
        return templates



    def action_documents(self):
        domain = [('id', 'in', self.sudo().attachment_ids.ids)]

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'ir.attachment',
            'name': _('Documents'),
            'view_mode': 'kanban,tree,form',
            'domain':domain,
        }
