# -*- coding: utf-8 -*-
from odoo import models, fields, api, _


class AgentAgent(models.Model):
    _inherit = 'agent.agent'

    def action_agent_local_onboarding(self):
        # action = self.env.ref('employee_onboarding.employee_onboarding_local_action').sudo().read()[0]
        # action['domain'] = [("agent_id", "=", self.id)]
        # return action
        return {
            "type": "ir.actions.act_window",
            "name": "Employee Local Onboarding",
            "res_model": "employee.onboarding",
            "views": [[self.env.ref("employee_onboarding.employee_onboarding_local_view_tree").id, "tree"],
                      [self.env.ref("employee_onboarding.employee_onboarding_local_view_form").id, "form"]],

            "view_mode": "tree,form",
            "domain": [("agent_id", "=", self.id),('onboarding_type','=', 'local')],
        }

    def action_agent_overseas_onboarding(self):
        return {
            "type": "ir.actions.act_window",
            "name": "Employee Overseas Onboarding",
            "res_model": "employee.onboarding",
            "views": [[self.env.ref("employee_onboarding.employee_onboarding_overseas_view_tree").id, "tree"],
                      [self.env.ref("employee_onboarding.employee_onboarding_overseas_view_form").id, "form"]],
            "view_mode": "tree,form",
            "domain": [("agent_id", "=", self.id),('onboarding_type','=', 'overseas')],
        }
