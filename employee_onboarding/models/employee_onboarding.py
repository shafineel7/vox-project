# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, Command
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta,date
import os


class EmployeeOnboarding(models.Model):
    _name = 'employee.onboarding'
    _description = 'Employee Onboarding Local/Overseas'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char('Name', copy=False)
    partner_name = fields.Char('Applicant Name')
    application = fields.Char('Application')
    email = fields.Char('E-mail')
    department_id = fields.Many2one('hr.department', string='Department')
    employee_sequence = fields.Char('Employee ID')
    onboarding_type = fields.Selection([('local', 'Local'), ('overseas', 'Overseas')], string='Onboarding Type')
    role = fields.Char(string='Role')
    skills_ids = fields.Many2many('hr.skill', string='Skills')
    type = fields.Selection([('direct', 'Direct'), ('indirect', 'Indirect')], string='Type')
    qualification_ids = fields.Many2many('qualification', 'onboarding_qualification_relation', 'onboarding_id',
                                         'qualification_id', string="Qualifications")
    gender = fields.Selection([('male', 'Male'), ('female', 'Female'), ('others', 'Others')], string='Gender')
    dob = fields.Date(string='Date Of Birth')
    marital_status = fields.Selection([('single', 'Single'), ('married', 'Married')], string='Marital Status')
    phone = fields.Char(string='Phone')
    mobile = fields.Char(string='Mobile')
    document = fields.Binary(string="Supporting Documents")
    agent_id = fields.Many2one('agent.agent', string='Agent')
    user_id = fields.Many2one("res.users", default=lambda self: self.env.user, string="User")
    nationality_id = fields.Many2one('res.country', string='Nationality')
    is_driver = fields.Boolean(string="Is Driver")

    @api.model
    def default_get(self, fields):
        if self.env.context.get('local_onboarding') == 1 or self.env.context.get('overseas_onboarding') == 1 :
            if not (self.env.user.has_group('department_groups.group_hr_talent_acquisition_exec') or self.env.user.has_group('department_groups.group_hr_asst_onboard')) :
                raise ValidationError("You cannot create onboarding")
        return super(EmployeeOnboarding, self).default_get(fields)
    def _get_default_groups(self):
        onboarding = self.env.ref('department_groups.group_hr_asst_onboard').id
        return [Command.link(onboarding)]

    user_ids = fields.Many2many('res.users', string="Assigned Users", default=lambda self: self.env.user,
                                track_visibility='always')
    group_ids = fields.Many2many('res.groups', string='Assigned Groups', default=_get_default_groups)

    is_self_assign = fields.Boolean(string="Self Assign")
    self_assignee_ids = fields.Many2many('res.users', 'onbaording_self_assign_rel',
                                         'onboarding_id', 'self_assign_id', string="Assignees")

    stage = fields.Selection([('new', 'New'), ('ministry_of_labor_application', 'Ministry of Labor Application'),
                              ('visa_processing', 'Visa processing'), ('scheduled_arrivals', 'Scheduled Arrivals'),
                              ('change_status', 'Change Status'),
                              ('medical_fitness', 'Medical Fitness'),
                              ('eid_application_insurance', 'EID application & Insurance'),
                              ('visa_stamping', 'Visa stamping'), ('employee_onboarding', 'Employee Onboarding'),
                              ('on_hold', 'On Hold')], group_expand='_group_expand_states', copy=False, default="new",
                             string='Stage',track_visibility='onchange')

    employee_id = fields.Many2one('hr.employee', string='Employee')


    def _group_expand_states(self, states, domain, order):
        if self.env.context.get('default_onboarding_type') == 'local':
            return ['new','ministry_of_labor_application','visa_processing','change_status','medical_fitness','eid_application_insurance','visa_stamping','employee_onboarding','on_hold']
        else:
            return ['new','ministry_of_labor_application','visa_processing','scheduled_arrivals','medical_fitness','eid_application_insurance','visa_stamping','employee_onboarding','on_hold']
        # return [key for key, val in type(self).stage.selection]

    signed_offer_letter = fields.Binary(string='Signed offer letter', attachment=True)
    signed_offer_letter_file_name = fields.Char('Signed offer letter Filename')
    cv = fields.Binary(string='CV', attachment=True)
    cv_file_name = fields.Char('CV Filename')
    passport = fields.Binary(string='Passport', attachment=True)
    passport_file_name = fields.Char('Passport Filename')
    photo = fields.Binary(string='Photo', attachment=True)
    pakistani_ref = fields.Binary(string='Pakistani ID', attachment=True)
    pakistani_ref_file_name = fields.Char('Pakistani ID Filename')
    driving_license = fields.Binary(string='Driving license', attachment=True)
    driving_license_file_name = fields.Char('Driving license ID Filename')
    educational_certificates = fields.Binary(string='Educational certificates', attachment=True)
    educational_certificates_file_name = fields.Char('Educational certificates Filename')
    police_clearance_certificate = fields.Binary(string='Police clearance certificate', attachment=True)
    police_clearance_certificate_file_name = fields.Char('Police clearance certificate Filename')
    residence_visa_cancellation = fields.Binary(string='Residence visa cancellation/visit visa', attachment=True)
    residence_visa_cancellation_file_name = fields.Char('Residence visa Filename')
    medical_fitness_certificate = fields.Binary(string='Medical fitness certificate', attachment=True)
    medical_fitness_certificate_file_name = fields.Char('Medical fitness certificate Filename')

    mol_stage = fields.Selection([('applied', 'MOL Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
                                  ('approved', 'MOL Approved')], string='MOL Status')
    mol_offer_letter_date = fields.Date(string='MOL Offer Letter Date')
    st_and_mb = fields.Binary(string='ST and MB', attachment=True)
    signed_mol = fields.Binary(string='Signed MOL', attachment=True)
    mol_onhold_remarks = fields.Char(string='Remarks')
    mol_reject_remarks = fields.Char(string='Remarks')
    mol_applied_date = fields.Date(string='MOL Applied Date')
    signed_mol_date = fields.Date(string='Signed MOL Offer')

    visa_processing_stage = fields.Selection(
        [('applied', 'E-Visa Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('approved', 'E-Visa Approved')], string='E-Visa Status')
    insurance_payment = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Insurance Payment')
    labour_card_payment = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Labour Card Payment')
    entry_permit_payment = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Entry Permit Payment')
    uid_number = fields.Char(string='UID number')
    file_number = fields.Char(string='File number')
    evisa_date = fields.Date(string='E-Visa date')
    # evisa_expiry_date = fields.Date(string='E-Visa Expiry date')
    temporary_labour_card_number = fields.Char(string='Temporary Labour card number')
    temporary_labour_card_expiry_date = fields.Date(string='Temporary Labour card Expiry date')
    evisa_copy = fields.Binary(string='E Visa copy', attachment=True)
    evisa_reject_remarks = fields.Char(string='E-Visa Remarks')

    change_status = fields.Selection(
        [('change_status_applied', 'Change Status Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('change_status_approved', 'Change Status Approved'), ], string='Change status')
    # change_status_onhold_remarks = fields.Char(string='Remarks')
    change_status_reject_remarks = fields.Char(string='Remarks')

    date_of_journey = fields.Date(string='Date of Journey')
    date_time_of_landing = fields.Datetime(string='Date and time of landing')
    destination_airport_terminal = fields.Char(string='Destination Airport, Terminal')
    flight_no = fields.Char(string='Flight No.')
    ticket = fields.Binary(string='Ticket', attachment=True)
    schedule_status = fields.Selection(
        [('reached', 'reached'), ('cancelled', 'Cancelled'), ('yet_to_shift', 'Yet to shift'), ('shifted', 'Shifted'),
         ('postpone', 'Postpone'), ('returned', 'Returned'),
         ('no_show', 'No Show')], string='Schedule status')

    airport_destination_id = fields.Many2one('airport.destination', string="Destination Airport")
    terminal_id = fields.Many2one('terminal.terminal', string="Terminal")

    room_allocation = fields.Boolean(string='Room Allocation')
    accommodation_status = fields.Selection([('live_in', 'Live In'), ('live_out', 'Live Out')],
                                            string='Accommodation Status')
    camp_manager_id = fields.Many2one('hr.employee', string="Camp Manager")

    medical_attendance_status = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Medical Attendance Status')
    medical_center_id = fields.Many2one('medical.center', string='Medical Center')
    # medical_centre_location = fields.Char(string='Medical Centre Location')
    medical_appointment_date = fields.Date(string='Medical Appointment Date')
    medical_receipt_copy = fields.Binary(string='Mediacl Receipt copy', attachment=True)
    medical_status = fields.Selection(
        [('appointment_scheduled', 'Appointment scheduled'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('completed', 'Completed'),
         ('appointment_rescheduled', 'Appointment rescheduled'), ], string='Medical status')

    medical_detail_status = fields.Selection([
        ('done', 'Done'),
        ('not_done', 'Not-Done'),
    ], string='Medical Detail Status')

    medical_application_no = fields.Char(string='Medical Application Number')
    medical_application_date = fields.Date(string='Medical Application Date')
    medical_completed_date = fields.Date(string='Medical Completed Date')

    tawjeeh_attendance_status = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Tawjeeh Attendance Status')
    tawjeeh_location = fields.Char(string='Tawjeeh Location')
    tawjeeh_center_id = fields.Many2one('tawjeeh.center', string='Tawjeeh Center')
    tawjeeh_appointment_date = fields.Date(string='Tawjeeh Appointment Date')
    tawjeeh_receipt_copy = fields.Binary(string='Tawjeeh Receipt copy', attachment=True)
    tawjeeh_status = fields.Selection(
        [('appointment_scheduled', 'Appointment scheduled'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('completed', 'Completed'),
         ('appointment_rescheduled', 'Appointment rescheduled'), ], string='Tawjeeh status')

    tawjeeh_application_no = fields.Char(string='Tawjeeh Application Number')
    tawjeeh_detail_status = fields.Selection([
        ('done', 'Done'),
        ('not_done', 'Not Done'),
    ], string='Tawjeeh Detail Status')

    eid_status = fields.Selection(
        [('eid_applied', 'EID Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('eid_approved', 'EID Approved'), ], string='EID status')
    eid_application_number = fields.Char(string='EID application number')
    biometric = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Biometric')
    eid_application_receipt = fields.Binary(string='EID Application & receipt', attachment=True)
    biometric_date = fields.Date(string='Biometric Date')
    biometric_location = fields.Char(string='Biometric Location')
    biometric_center_id = fields.Many2one('biometric.center', string='Biometric Center')
    biometric_receipt_copy = fields.Binary(string='Biometric Receipt copy', attachment=True)
    biometric_attendance_status = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ],
                                                   string='Biometric Attendance Status')

    insurance_status = fields.Selection(
        [('insurance_applied', 'Insurance Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('insurance_approved', 'Insurance Approved'), ], string='Insurance status')
    group_code = fields.Char(string='Group code')
    insurance_number = fields.Char(string='Insurance Number')
    authority_number = fields.Char(string='Authority number ID')
    insurance_card = fields.Binary(string='Insurance Card', attachment=True)
    health_insurance_date = fields.Date(string='Health Insurance Application Date')
    premium_amount = fields.Float(string='Premium Amount')
    insurance_document = fields.Binary(string='Insurance Document', attachment=True)
    insurance_application_cancel_date = fields.Date(string='Insurance Application Cancellation Date')
    insurance_cancel_date = fields.Date(string='Insurance Cancellation Date')

    visa_stamping_stage = fields.Selection(
        [('applied', 'Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('completed', 'Completed')], string='Visa Stamping Status')
    visa_application_number = fields.Char(string='Visa Application Number')
    visa_expiry_date = fields.Date(string='Visa Expiry Date')
    visa_copy = fields.Binary(string='visa copy', attachment=True)
    collection_of_passport = fields.Boolean(string='Collection of passport', default=False)
    collection_date = fields.Date(string='Collection Date')
    passport_custody_status = fields.Selection(
        [('with_employee', 'With Employee'), ('in_custody', 'In Custody')], string='Passport Custody Status')

    release_status = fields.Char(string="Release Status", default='new')
    onhold_reason_note = fields.Char(string="On Hold Reason Note")
    onhold_reason = fields.Char(string="On Hold Reason")

    approval_hr_id = fields.Many2one('res.users', string="Approval HR")
    approval_talent_id = fields.Many2one('res.users', string="Approval Talent acquisition")

    # approval_finance_id = fields.Many2one('res.users', string="Approval Finance")
    approval_onboarding_id = fields.Many2one('res.users', string="Approval Onboarding")
    # approval_accounts_id = fields.Many2one('res.users', string="Approval Accounts")



    onboarding_approval_date = fields.Date(string='Onboarding Approval Date')
    mol_expense_submit_date = fields.Date(string='MOL Expense Date')

    visa_processing_date = fields.Date(string='E-visa Process Date')
    evisa_applied_date = fields.Date(string='E-visa Applied Date')
    change_status_applied_date = fields.Date(string='Change Status Applied Date')
    changing_status_date = fields.Date(string='Change Status')
    # changing_status_approved_date = fields.Date(string='Change Status approved date')
    eid_application_date = fields.Date(string='Eid Application date')
    eid_biometric_attendance_status_date = fields.Date(string='Eid Biometric attendance status date')
    visa_stamping_stage_date = fields.Date(string='Visa Stamping Stage date')
    scheduled_arrivals_date = fields.Date(string='Schedule Stage date')
    visa_passport_status_date = fields.Date(string='Schedule Stage date')
    medical_fitness_date = fields.Date(string='Medical Stage date')
    medical_attendance_status_date = fields.Date(string='Medical Attendance date')
    tawjeeh_attendance_status_date = fields.Date(string='Tawjeeh attendance date')






    @api.onchange('visa_stamping_stage')
    def _onchange_visa_stamping_stage(self):
        for rec in self:
            if rec.visa_stamping_stage == 'applied':
                rec.visa_stamping_stage_date = fields.date.today()


    @api.onchange('biometric_attendance_status')
    def _onchange_biometric_attendance_status(self):
        for rec in self:
            if rec.biometric_attendance_status == 'no':
                rec.eid_biometric_attendance_status_date = fields.date.today()
    def action_self_assign(self):
        for onboarding in self:
            onboarding.self_assignee_ids = False
            onboarding.user_ids = False
            onboarding.user_ids = [Command.link(onboarding.env.user.id)]
            onboarding.write({'is_self_assign': True, 'self_assignee_ids': [Command.link(onboarding.env.user.id)], 'approval_onboarding_id': onboarding.env.user.id})


    # @api.model
    # def _get_view(self, view_id=None, view_type='form', **options):
    #     arch, view = super()._get_view(view_id, view_type, **options)
    #
    #     for field in arch.xpath("//field"):
    #         if self.env.user.has_group('department_groups.group_hr_talent_acquisition_exec'):
    #             if field.get('name'):
    #                 field.set('readonly', '1')
    #     return arch, view


    def action_submit(self):
        for rec in self:
            if rec.onhold_reason_note:
                if rec.onhold_reason:
                    rec.onhold_reason = str(rec.onhold_reason) + "\n" + str(
                        (datetime.utcnow() + timedelta(hours=4)).replace(microsecond=0)) + ' #' + str(
                        self.env.user.name) + ' - ' + str(self.onhold_reason_note)
                else:
                    rec.onhold_reason = str(
                        (datetime.utcnow() + timedelta(hours=4)).replace(microsecond=0)) + ' #' + str(
                        self.env.user.name) + ' - ' + str(rec.onhold_reason_note)
                rec.onhold_reason_note = False
            rec.sudo().write(
                {'onhold_reason': rec.onhold_reason,
                 'stage': 'on_hold',
                 'is_self_assign': False
                 })

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('name') or vals['name'] == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('onboarding_name') or _('New')
        return super().create(vals_list)

    @api.constrains('signed_offer_letter', 'cv', 'passport', 'pakistani_ref', 'driving_license',
                    'educational_certificates', 'police_clearance_certificate', 'residence_visa_cancellation',
                    'medical_fitness_certificate')
    def _check_document_size(self):
        max_size_mb = 100
        if self.signed_offer_letter and len(self.signed_offer_letter) > max_size_mb * 1024 * 1024:
            raise ValidationError("Document size exceeds the maximum allowed ({} MB)".format(max_size_mb))
        if self.cv and len(self.cv) > max_size_mb * 1024 * 1024:
            raise ValidationError("Document size exceeds the maximum allowed ({} MB)".format(max_size_mb))
        if self.passport and len(self.passport) > max_size_mb * 1024 * 1024:
            raise ValidationError("Document size exceeds the maximum allowed ({} MB)".format(max_size_mb))

        if self.pakistani_ref and len(self.pakistani_ref) > max_size_mb * 1024 * 1024:
            raise ValidationError("Document size exceeds the maximum allowed ({} MB)".format(max_size_mb))
        if self.driving_license and len(self.driving_license) > max_size_mb * 1024 * 1024:
            raise ValidationError("Document size exceeds the maximum allowed ({} MB)".format(max_size_mb))
        if self.educational_certificates and len(self.educational_certificates) > max_size_mb * 1024 * 1024:
            raise ValidationError("Document size exceeds the maximum allowed ({} MB)".format(max_size_mb))
        if self.police_clearance_certificate and len(self.police_clearance_certificate) > max_size_mb * 1024 * 1024:
            raise ValidationError("Document size exceeds the maximum allowed ({} MB)".format(max_size_mb))
        if self.residence_visa_cancellation and len(self.residence_visa_cancellation) > max_size_mb * 1024 * 1024:
            raise ValidationError("Document size exceeds the maximum allowed ({} MB)".format(max_size_mb))
        if self.medical_fitness_certificate and len(self.medical_fitness_certificate) > max_size_mb * 1024 * 1024:
            raise ValidationError("Document size exceeds the maximum allowed ({} MB)".format(max_size_mb))

    @api.constrains('signed_offer_letter', 'cv', 'passport', 'pakistani_ref', 'driving_license',
                    'educational_certificates', 'police_clearance_certificate', 'residence_visa_cancellation',
                    'medical_fitness_certificate')
    def _check_document_format(self):
        allowed_extensions = ['.pdf', '.jpg', '.jpeg']
        for record in self:
            if record.signed_offer_letter:
                filename, extension = os.path.splitext(record.signed_offer_letter_file_name)
                if extension.lower() not in allowed_extensions:
                    raise ValidationError("Documents must be in JPG or PDF format for Signed offer letter")
            if record.cv:
                filename, extension = os.path.splitext(record.cv_file_name)
                if extension.lower() not in allowed_extensions:
                    raise ValidationError("Documents must be in JPG or PDF format for CV")
            if record.passport:
                filename, extension = os.path.splitext(record.passport_file_name)
                if extension.lower() not in allowed_extensions:
                    raise ValidationError("Documents must be in JPG or PDF format for Passport")
            if record.pakistani_ref:
                filename, extension = os.path.splitext(record.pakistani_ref_file_name)
                if extension.lower() not in allowed_extensions:
                    raise ValidationError("Documents must be in JPG or PDF format for Pakistani ID")
            if record.driving_license:
                filename, extension = os.path.splitext(record.driving_license_file_name)
                if extension.lower() not in allowed_extensions:
                    raise ValidationError("Documents must be in JPG or PDF format for Driving license")
            if record.educational_certificates:
                filename, extension = os.path.splitext(record.educational_certificates_file_name)
                if extension.lower() not in allowed_extensions:
                    raise ValidationError("Documents must be in JPG or PDF format for Educational certificates")
            if record.police_clearance_certificate:
                filename, extension = os.path.splitext(record.police_clearance_certificate_file_name)
                if extension.lower() not in allowed_extensions:
                    raise ValidationError("Documents must be in JPG or PDF format for Police clearance certificate")
            if record.residence_visa_cancellation:
                filename, extension = os.path.splitext(record.residence_visa_cancellation_file_name)
                if extension.lower() not in allowed_extensions:
                    raise ValidationError(
                        "Documents must be in JPG or PDF format for Medical fitness certificate for Residence visa cancellation/visit visa")
            if record.medical_fitness_certificate:
                filename, extension = os.path.splitext(record.medical_fitness_certificate_file_name)
                if extension.lower() not in allowed_extensions:
                    raise ValidationError("Documents must be in JPG or PDF format for Medical fitness certificate")

    @api.constrains('photo')
    def _check_photo_size(self):
        max_photo_size_mb = 40
        if self.photo and len(self.photo) > max_photo_size_mb * 1024 * 1024:
            raise ValidationError("Photo size exceeds the maximum allowed ({} MB)".format(max_photo_size_mb))

    def onboarding_team_approval(self):
        today = date.today()

        for onboarding in self:
            onboarding.onboarding_approval_date = today
            if onboarding.nationality_id.code=='pk':
                if not onboarding.pakistani_ref:
                    raise ValidationError("Please attach the pakistani ID Before Approval")
            if onboarding.onboarding_type=='local':
                if not onboarding.residence_visa_cancellation:
                    raise ValidationError("Please attach the Residence visa cancellation/visit visa Before Approval")
            if onboarding.is_driver== True and not onboarding.driving_license:
                raise ValidationError("Please attach the Driving license Before Approval")
            if onboarding.nationality_id.code=='cf' or onboarding.nationality_id.code=='za':
                if not onboarding.police_clearance_certificate:
                    raise ValidationError("Please attach the Police Clearance certificate Before Approval")
            # if not (
            #         onboarding.signed_offer_letter and onboarding.cv and onboarding.passport and onboarding.photo
            #         and onboarding.educational_certificates and onboarding.police_clearance_certificate
            #      and onboarding.medical_fitness_certificate):
            #     raise ValidationError("Please fill the Document Details Before Approval")
            if not (onboarding.passport and onboarding.educational_certificates):
                raise ValidationError("Please fill the Document Details Before Approval")

            attachment_ids = []

            if onboarding.signed_offer_letter:
                attachment = self.env['ir.attachment'].sudo().create({
                    'name': onboarding.signed_offer_letter_file_name,
                    'type': 'binary',
                    'datas': onboarding.signed_offer_letter,
                    'mimetype': 'application/pdf',
                    'res_model': 'hr.employee',
                })
                attachment_ids.append(attachment.id)
            if onboarding.cv:
                cv_attachment = self.env['ir.attachment'].sudo().create({
                    'name': onboarding.cv_file_name,
                    'type': 'binary',
                    'datas': onboarding.cv,
                    'mimetype': 'application/pdf',
                    'res_model': 'hr.employee',
                })
                attachment_ids.append(cv_attachment.id)
            if onboarding.passport:
                attachment = self.env['ir.attachment'].sudo().create({
                    'name': onboarding.passport_file_name,
                    'type': 'binary',
                    'datas': onboarding.passport,
                    'mimetype': 'application/pdf',
                    'res_model': 'hr.employee',
                })
                attachment_ids.append(attachment.id)
            if onboarding.pakistani_ref:
                attachment = self.env['ir.attachment'].sudo().create({
                    'name': onboarding.pakistani_ref_file_name,
                    'type': 'binary',
                    'datas': onboarding.pakistani_ref,
                    'mimetype': 'application/pdf',
                    'res_model': 'hr.employee',
                })
                attachment_ids.append(attachment.id)
            if onboarding.driving_license:
                attachment = self.env['ir.attachment'].sudo().create({
                    'name': onboarding.driving_license_file_name,
                    'type': 'binary',
                    'datas': onboarding.driving_license,
                    'mimetype': 'application/pdf',
                    'res_model': 'hr.employee',
                })
                attachment_ids.append(attachment.id)
            attachment = self.env['ir.attachment'].sudo().create({
                'name': onboarding.educational_certificates_file_name,
                'type': 'binary',
                'datas': onboarding.educational_certificates,
                'mimetype': 'application/pdf',
                'res_model': 'hr.employee',
            })
            attachment_ids.append(attachment.id)
            if onboarding.police_clearance_certificate:
                attachment = self.env['ir.attachment'].sudo().create({
                    'name': onboarding.police_clearance_certificate_file_name,
                    'type': 'binary',
                    'datas': onboarding.police_clearance_certificate,
                    'mimetype': 'application/pdf',
                    'res_model': 'hr.employee',
                })
                attachment_ids.append(attachment.id)
            if onboarding.residence_visa_cancellation:
                attachment = self.env['ir.attachment'].sudo().create({
                    'name': onboarding.residence_visa_cancellation_file_name,
                    'type': 'binary',
                    'datas': onboarding.residence_visa_cancellation,
                    'mimetype': 'application/pdf',
                    'res_model': 'hr.employee',
                })
                attachment_ids.append(attachment.id)
            if onboarding.medical_fitness_certificate:
                attachment = self.env['ir.attachment'].sudo().create({
                    'name': onboarding.medical_fitness_certificate_file_name,
                    'type': 'binary',
                    'datas': onboarding.medical_fitness_certificate,
                    'mimetype': 'application/pdf',
                    'res_model': 'hr.employee',
                })
                attachment_ids.append(attachment.id)

            employee_creation = self.env['hr.employee'].sudo().create([
                {'name': onboarding.partner_name,
                 'department_id': onboarding.department_id.id,
                 'work_email': onboarding.email,
                 'type': 'direct',
                 'employee_onboarding_id': onboarding.id,
                 'attachment_ids': attachment_ids,
                 'country_id': onboarding.nationality_id.id,
                 'cv':onboarding.cv if onboarding.cv else False,
                 'company_id':self.env.company.id,
                 'date_of_joining': fields.Date.today(),
                 'dob':onboarding.dob,
                 'gender': onboarding.gender,
                 'birthday': onboarding.dob,
                 'marital': onboarding.marital_status,
                 'work_phone': onboarding.phone,
                 'mobile_phone': onboarding.mobile,



                 }
            ])
            if not employee_creation.work_contact_id:
                employee_creation.work_contact_id = self.env['res.partner'].sudo().create({
                    'email': employee_creation.work_email,
                    'name': employee_creation.name,
                })

            template_id = self.env.ref('employee_onboarding.email_template_mol_notification')

            pro_manager = self.env.ref('department_groups.group_pro_mgr')
            pro_assistant = self.env.ref('department_groups.group_pro_asst')
            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                            [pro_manager.id, pro_assistant.id])])
            acc_team_groups = self.env['res.groups'].browse((pro_manager.id, pro_assistant.id))

            pro_group = pro_manager + pro_assistant

            onboarding.write({
                'stage': 'ministry_of_labor_application',
                'employee_sequence': employee_creation.employee_sequence,
                'employee_id': employee_creation.id,
                'release_status': 'ministry_of_labor_application',
                'user_ids': [(6, 0, acc_team_users.ids)],
                'group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False,
                'approval_onboarding_id':onboarding.env.user.id
            })

            mail_to = []
            for user in acc_team_users:
                if user.partner_id.email:
                    mail_to.append(user.partner_id.email)
            reorder_qty_mail_to = ",".join(mail_to)
            if template_id and onboarding.id and reorder_qty_mail_to:
                template_id.write({'email_to': reorder_qty_mail_to})
                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                    send_mail(onboarding.id, force_send=True, raise_exception=False)

    def onboarding_team_hold(self):
        # for onboarding in self:
        #     onboarding.write({
        #         'stage': 'on_hold'
        #
        #     })
        view_id = self.env.ref('employee_onboarding.employee_onboarding_view_form')
        ctx = self.env.context.copy()
        self.approval_onboarding_id = self.env.user.id
        return {
            'name': _('On Hold Reason'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'employee.onboarding',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx,
        }

    def mol_completed(self):
        for onboarding in self:
            onboarding.visa_processing_date = fields.Date.today()
            if not (
                    onboarding.mol_stage and onboarding.st_and_mb and onboarding.signed_mol and onboarding.mol_offer_letter_date):
                raise ValidationError("Please fill the MOL Details Before Approval")
            pro_manager = self.env.ref('department_groups.group_pro_mgr')
            pro_assistant = self.env.ref('department_groups.group_pro_asst')
            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                            [pro_manager.id, pro_assistant.id])])
            acc_team_groups = self.env['res.groups'].browse((pro_manager.id, pro_assistant.id))

            onboarding.write({
                'stage': 'visa_processing',
                'release_status': 'visa_processing',
                'user_ids': [(6, 0, acc_team_users.ids)],
                'group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False

            })
            onboarding.employee_id.write({
                'mol_offer_letter_date': onboarding.mol_offer_letter_date,
                'mol_status': onboarding.mol_stage,
                'mol_offer_letter': onboarding.signed_mol,
                'st_document': onboarding.st_and_mb,

            })

    def _evisa_send_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'visa_processing':
                if rec.visa_processing_date:
                    if (not (rec.insurance_payment or rec.labour_card_payment
                            or rec.entry_permit_payment or rec.uid_number or rec.file_number or rec.evisa_date or rec.temporary_labour_card_number
                    or rec.temporary_labour_card_expiry_date or rec.evisa_copy)):

                        exp_date =(date_now - rec.visa_processing_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=6:
                            template_id = self.env.ref('employee_onboarding.email_template_evisa_fields')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.evisa_applied_date:
                    if rec.visa_processing_stage == 'applied':
                        exp_date = (date_now - rec.evisa_applied_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=3:
                            template_id = self.env.ref('employee_onboarding.email_template_evisa_approval')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

    def _medical_status_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'medical_fitness':
                if rec.medical_fitness_date:
                    if rec.medical_status != 'appointment_scheduled' or rec.tawjeeh_status != 'appointment_scheduled':

                        exp_date =(date_now - rec.medical_fitness_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_hr_asst_onboard')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.medical_attendance_status_date:
                    if rec.medical_attendance_status == 'no':

                        exp_date =(date_now - rec.medical_attendance_status_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_hr_asst_onboard')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.tawjeeh_attendance_status_date:
                    if rec.tawjeeh_attendance_status == 'no':

                        exp_date =(date_now - rec.tawjeeh_attendance_status_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_hr_asst_onboard')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)



    def _mol_send_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'ministry_of_labor_application':
                if rec.mol_stage == 'applied' and not rec.st_and_mb:
                    if rec.mol_applied_date:
                        exp_date = (date_now - rec.mol_applied_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=3:
                            template_id = self.env.ref('employee_onboarding.email_template_mol_attachments')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

                if rec.mol_stage == 'approved' and not rec.signed_mol:
                    if rec.mol_applied_date:
                        exp_date = (date_now - rec.mol_applied_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=3:
                            if rec.onboarding_type == 'local':
                                template_id = self.env.ref('employee_onboarding.email_template_mol_attachments')
                                onboarding_users = self.env.ref('department_groups.group_hr_asst_onboard')
                                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                                [onboarding_users.id])])
                            else:
                                template_id = self.env.ref('employee_onboarding.email_template_mol_attachments')
                                onboarding_users = self.env.ref('department_groups.group_agent_agent')
                                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                                [onboarding_users.id])])

                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

    def _change_status_send_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'change_status':
                if rec.changing_status_date:
                    exp_date = (date_now - rec.changing_status_date).days
                    exp_date = abs(exp_date)
                    if exp_date <= 2:
                        template_id = self.env.ref('employee_onboarding.email_template_change_status')
                        pro_manager = self.env.ref('department_groups.group_pro_mgr')
                        pro_assistant = self.env.ref('department_groups.group_pro_asst')
                        acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                        [pro_manager.id, pro_assistant.id])])
                        mail_to = []
                        for user in acc_team_users:
                            if user.partner_id.email:
                                mail_to.append(user.partner_id.email)
                        reorder_qty_mail_to = ",".join(mail_to)
                        if template_id and rec.id and reorder_qty_mail_to:
                            template_id.write({'email_to': reorder_qty_mail_to})
                            template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.change_status_applied_date:
                    if rec.change_status == 'change_status_applied':
                        exp_date = (date_now - rec.change_status_applied_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 3:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
    def _eid_status_send_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'eid_application_insurance':
                if rec.eid_application_date:
                    if not(rec.eid_status or rec.eid_application_number or rec.biometric or rec.eid_application_receipt) or (rec.biometric=='yes' and not(rec.biometric_date or rec.biometric_location)):
                        exp_date = (date_now - rec.eid_application_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 3:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.eid_biometric_attendance_status_date:
                    if rec.biometric_attendance_status == 'no':
                        exp_date = (date_now - rec.eid_biometric_attendance_status_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
    def _schedules_status_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'scheduled_arrivals':
                if rec.scheduled_arrivals_date:
                    if not(rec.date_of_journey or rec.date_time_of_landing or rec.destination_airport_terminal or rec.flight_no or rec.ticket):
                        exp_date = (date_now - rec.scheduled_arrivals_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 7:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_agent_agent')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

    def _visa_status_send_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'visa_stamping':
                if rec.visa_stamping_stage_date:
                    if rec.visa_stamping_stage=='applied' and not rec.visa_application_number:
                        exp_date = (date_now - rec.visa_stamping_stage_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.visa_passport_status_date:
                    if rec.collection_of_passport == True and not (rec.passport_custody_status == 'in_custody' or rec.collection_date):
                        exp_date = (date_now - rec.visa_passport_status_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 30:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_hr_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.visa_stamping_stage != 'completed':
                    if rec.visa_expiry_date :
                        exp_date = (date_now - rec.visa_expiry_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 15:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            hr_mgr = self.env.ref('department_groups.group_hr_mgr')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id,hr_mgr.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

    @api.onchange('visa_processing_stage', )
    def _onchange_visa_processing_stage(self):
        for line in self:
            if line.visa_processing_stage == 'applied':
                line.evisa_applied_date = fields.Date.today()

            if line.visa_processing_stage == 'rejected':
                template_id = self.env.ref('employee_onboarding.email_template_mol_reject_notification')

                onboarding_users = self.env.ref('department_groups.group_hr_asst_onboard')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [onboarding_users.id])])
                acc_team_groups = self.env['res.groups'].browse((onboarding_users.id))

                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (line.id or line._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                        line.id or line._origin.id, force_send=True, raise_exception=False)

    @api.onchange('change_status')
    def _onchange_change_status(self):
        today = date.today()
        for line in self:
            if line.change_status == 'change_status_applied':
                line.change_status_applied_date = today
            # if line.change_status == 'change_status_approved':
            #     line.changing_status_approved_date = today
            if line.change_status == 'rejected':
                template_id = self.env.ref('employee_onboarding.email_template_mol_reject_notification')

                onboarding_users = self.env.ref('department_groups.group_hr_asst_onboard')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [onboarding_users.id])])
                acc_team_groups = self.env['res.groups'].browse((onboarding_users.id))

                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (line.id or line._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                        line.id or line._origin.id, force_send=True, raise_exception=False)

    @api.onchange('medical_status')
    def _onchange_medical_status(self):
        for line in self:
            if line.medical_status == 'appointment_scheduled' or line.medical_status == 'appointment_rescheduled':
                template_id = self.env.ref('employee_onboarding.email_template_medical_fitness_notification')

                onboarding_users = self.env.ref('department_groups.group_hr_welfare_officer')

                acc_team_users = self.env['res.users'].search([('groups_id', 'in', [onboarding_users.id])])
                acc_team_groups = self.env['res.groups'].browse((onboarding_users.id))

                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (line.id or line._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                        line.id or line._origin.id, force_send=True, raise_exception=False)

    @api.onchange('tawjeeh_status')
    def _onchange_tawjeeh_status(self):
        for line in self:
            if line.tawjeeh_status == 'appointment_scheduled' or line.tawjeeh_status == 'appointment_rescheduled':
                template_id = self.env.ref('employee_onboarding.email_template_tawjee_notification')

                onboarding_users = self.env.ref('department_groups.group_hr_welfare_officer')

                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [onboarding_users.id])])
                acc_team_groups = self.env['res.groups'].browse((onboarding_users.id))

                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (line.id or line._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                        line.id or line._origin.id, force_send=True, raise_exception=False)

    @api.onchange('biometric')
    def _onchange_biometric(self):
        for line in self:
            if line.biometric == 'yes':
                template_id = self.env.ref('employee_onboarding.email_template_biomatric_notification')

                onboarding_users = self.env.ref('department_groups.group_hr_welfare_officer')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [onboarding_users.id])])
                acc_team_groups = self.env['res.groups'].browse((onboarding_users.id))
                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (line.id or line._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                        line.id or line._origin.id, force_send=True, raise_exception=False)

    def evisa_completed(self):
        for onboarding in self:
            if not (onboarding.evisa_date and
                    onboarding.visa_processing_stage and onboarding.insurance_payment and onboarding.labour_card_payment and onboarding.entry_permit_payment and onboarding.uid_number and onboarding.file_number and onboarding.temporary_labour_card_number and onboarding.temporary_labour_card_expiry_date and onboarding.evisa_copy):
                raise ValidationError("Please fill the Visa Processing Details Before Approval")
            onboarding.employee_id.write({
                'e_visa_date': onboarding.evisa_date,
                'e_visa_copy': onboarding.evisa_copy,
                'visa_status': onboarding.visa_processing_stage,

            })

            if onboarding.onboarding_type == 'local':
                pro_manager = self.env.ref('department_groups.group_pro_mgr')
                pro_assistant = self.env.ref('department_groups.group_pro_asst')
                # pro_group = pro_manager + pro_assistant
                # users = self.env['res.users'].browse(pro_group)

                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [pro_manager.id, pro_assistant.id])])
                acc_team_groups = self.env['res.groups'].browse((pro_manager.id, pro_assistant.id))

                onboarding.write({
                    'stage': 'change_status',
                    'release_status': 'change_status',
                    'user_ids': [(6, 0, acc_team_users.ids)],
                    'group_ids': [(6, 0, acc_team_groups.ids)],
                    'is_self_assign': False,
                    'changing_status_date':fields.date.today()

                })
            elif onboarding.onboarding_type == 'overseas':
                agent = self.env.ref('department_groups.group_agent_agent')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [agent.id])])
                acc_team_groups = self.env['res.groups'].browse((agent.id))

                onboarding.write({
                    'stage': 'scheduled_arrivals',
                    'release_status': 'scheduled_arrivals',
                    'user_ids': [(6, 0, acc_team_users.ids)],
                    'group_ids': [(6, 0, acc_team_groups.ids)],
                    'is_self_assign': False,
                    'scheduled_arrivals_date':fields.date.today()

                })

    def change_status_completed(self):
        today = date.today()
        for onboarding in self:
            if not (onboarding.change_status):
                raise ValidationError("Please fill the Change Status Details Before Approval")
            onboarding_team = self.env.ref('department_groups.group_hr_asst_onboard')
            welfare_team = self.env.ref('department_groups.group_hr_welfare_officer')
            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                            [onboarding_team.id, welfare_team.id])])
            acc_team_groups = self.env['res.groups'].browse((onboarding_team.id, welfare_team.id))

        onboarding.write({
            'stage': 'medical_fitness',
            'release_status': 'medical_fitness',
            'user_ids': [(6, 0, acc_team_users.ids)],
            'group_ids': [(6, 0, acc_team_groups.ids)],
            'is_self_assign': False,
            'medical_fitness_date':fields.date.today()

        })

    def scheduled_arrival_completed(self):
        for onboarding in self:
            if not (
                    onboarding.date_of_journey and onboarding.date_time_of_landing and onboarding.airport_destination_id and onboarding.terminal_id and onboarding.flight_no and onboarding.ticket and onboarding.schedule_status and onboarding.room_allocation and onboarding.accommodation_status and onboarding.camp_manager_id):
                raise ValidationError("Please fill the Scheduled Arrival Details Before Approval")

            onboarding_team = self.env.ref('department_groups.group_hr_asst_onboard')
            welfare_team = self.env.ref('department_groups.group_hr_welfare_officer')
            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                            [onboarding_team.id, welfare_team.id])])
            acc_team_groups = self.env['res.groups'].browse((onboarding_team.id, welfare_team.id))
            onboarding.write({
                'stage': 'medical_fitness',
                'release_status': 'medical_fitness',
                'user_ids': [(6, 0, acc_team_users.ids)],
                'group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False,
                'medical_fitness_date': fields.date.today()

            })

            onboarding.employee_id.write({
                'date_of_journey': onboarding.date_of_journey,
                'date_time_of_landing': onboarding.date_time_of_landing,
                'flight_no': onboarding.flight_no,
                'destination_airport_id': onboarding.airport_destination_id.id,
                'terminal_id': onboarding.terminal_id.id,
                'ticket_copy': onboarding.ticket,

            })



    @api.onchange('medical_attendance_status')
    def _onchange_medical_attendance_status(self):
        for rec in self:
            if rec.medical_attendance_status == 'no':
                rec.medical_attendance_status_date = fields.date.today()


    @api.onchange('tawjeeh_attendance_status')
    def _onchange_tawjeeh_attendance_status(self):
        for rec in self:
            if rec.tawjeeh_attendance_status == 'no':
                rec.tawjeeh_attendance_status_date = fields.date.today()

    def medical_tawjeeh_completed(self):
        for onboarding in self:
            if not (
                    onboarding.medical_application_no and onboarding.medical_application_date and onboarding.medical_completed_date and onboarding.medical_detail_status and
                    onboarding.medical_center_id and onboarding.medical_appointment_date and onboarding.medical_attendance_status and onboarding.medical_receipt_copy and onboarding.medical_status
                    and onboarding.tawjeeh_center_id and onboarding.tawjeeh_appointment_date and onboarding.tawjeeh_attendance_status and onboarding.tawjeeh_receipt_copy and onboarding.tawjeeh_status
                    and onboarding.tawjeeh_application_no and onboarding.tawjeeh_detail_status):
                raise ValidationError("Please fill the Medical and Tawjeeh Details Before Approval")
            onboarding.employee_id.write({
                'medical_application_no': onboarding.medical_application_no,
                'medical_center_id': onboarding.medical_center_id.id,
                'medical_application_date': onboarding.medical_application_date,
                'receipt_attachment': onboarding.medical_receipt_copy,
                'medical_completed_date': onboarding.medical_completed_date,
                'medical_status': onboarding.medical_detail_status,

                'tawjeeh_application_no': onboarding.tawjeeh_application_no,
                'tawjeeh_status': onboarding.tawjeeh_detail_status,
                'tawjeeh_appointment_date': onboarding.tawjeeh_appointment_date,
                'tawjeeh_receipt_attachment': onboarding.tawjeeh_receipt_copy,
                'tawjeeh_center_id': onboarding.tawjeeh_center_id.id,

            })

            template_id = self.env.ref('employee_onboarding.email_template_eid_insurance_notification')

            onboarding_users = self.env.ref('department_groups.group_hr_welfare_officer')

            hr_assistant = self.env.ref('department_groups.group_hr_asst')
            pro_assistant = self.env.ref('department_groups.group_pro_asst')
            pro_manager = self.env.ref('department_groups.group_pro_mgr')

            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                            [onboarding_users.id, hr_assistant.id, pro_manager.id,
                                                             pro_assistant.id])])
            acc_team_groups = self.env['res.groups'].browse(
                (onboarding_users.id, hr_assistant.id, pro_manager.id, pro_assistant.id))

            mail_to = []
            for user in acc_team_users:
                if user.partner_id.email:
                    mail_to.append(user.partner_id.email)
            reorder_qty_mail_to = ",".join(mail_to)

            if template_id and (onboarding.id or onboarding._origin.id) and reorder_qty_mail_to:
                template_id.write({'email_to': reorder_qty_mail_to})
                template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                    onboarding.id or onboarding._origin.id, force_send=True, raise_exception=False)
            # date_now = date.today()
            onboarding.write({
                'stage': 'eid_application_insurance',
                'release_status': 'eid_application_insurance',
                'user_ids': [(6, 0, acc_team_users.ids)],
                'group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False,
                'eid_application_date':  fields.Date.today()

            })

    def eid_application_insurance_completed(self):
        for onboarding in self:
            if not onboarding.biometric:
                raise ValidationError("Please fill the EID and Insurance Details Before Approval")

            if onboarding.biometric == 'yes':
                if not (
                        onboarding.biometric and onboarding.biometric_date and onboarding.biometric_center_id and onboarding.biometric_receipt_copy and onboarding.eid_application_number and onboarding.eid_application_receipt and onboarding.biometric_attendance_status and onboarding.eid_status and onboarding.group_code and onboarding.insurance_number and onboarding.authority_number and onboarding.insurance_card and onboarding.insurance_status and onboarding.health_insurance_date and onboarding.premium_amount and onboarding.insurance_document):
                    raise ValidationError("Please fill the EID and Insurance Details Before Approval")
            if onboarding.biometric == 'no':
                if not (
                        onboarding.biometric and onboarding.eid_application_number and onboarding.eid_application_receipt and onboarding.biometric_attendance_status and onboarding.eid_status and onboarding.group_code and onboarding.insurance_number and onboarding.authority_number and onboarding.insurance_card and onboarding.insurance_status and onboarding.health_insurance_date and onboarding.premium_amount and onboarding.insurance_document):
                    raise ValidationError("Please fill the EID and Insurance Details Before Approval")

            onboarding.employee_id.write({
                'eid_application_no': onboarding.eid_application_number,
                'eid_application_copy': onboarding.eid_application_receipt,
                'eid_status': onboarding.eid_status,
                'biometric_date': onboarding.biometric_date,
                'biometric_center_id': onboarding.biometric_center_id.id,
                'biometric_status': onboarding.biometric_attendance_status,
                'health_insurance_date': onboarding.health_insurance_date,
                'health_insurance_no': onboarding.insurance_number,
                'group_code': onboarding.group_code,
                'insurance_authority': onboarding.authority_number,
                'premium_amount': onboarding.premium_amount,
                'insurance_card': onboarding.insurance_card,
                'insurance_document': onboarding.insurance_document,
                'insurance_application_cancel_date': onboarding.insurance_application_cancel_date,
                'insurance_cancel_date': onboarding.insurance_cancel_date,
                'health_insurance_status': onboarding.insurance_status,

            })

            onboarding.write({
                'stage': 'visa_stamping',
                'release_status': 'visa_stamping',
                'is_self_assign': False,
                'visa_passport_status_date':fields.date.today()

            })

    def visa_stamping_completed(self):
        for onboarding in self:
            if not (
                    onboarding.visa_stamping_stage and onboarding.visa_application_number and onboarding.visa_expiry_date and onboarding.visa_copy and onboarding.collection_of_passport and onboarding.collection_date and onboarding.passport_custody_status):
                raise ValidationError("Please fill the Visa Stamping Details Before Approval")

            onboarding.write({
                'stage': 'employee_onboarding',
                'release_status': 'employee_onboarding',
                'is_self_assign': False

            })

            onboarding.employee_id.write({
                'visa_expire': onboarding.visa_expiry_date

            })

    def release_status_stage(self):
        for onboarding in self:
            onboarding.write({
                'stage': onboarding.release_status

            })
