# -*- coding: utf-8 -*-
{
    'name': 'Contract Management',
    'version': '17.0.1.0.2',
    'category': 'crm',
    'summary': 'Master to handle all contracts generated across clients.',
    'description': """Master to handle all contracts generated across clients. 
                    Direct contract creation and contracts, renewals and addendum 
                    can be handled inside the module. Contracts based general, payroll, 
                    invoice details are provided in this master.""",
    'author': 'Neenu R',
    'website': 'http://www.voxtronme.com',
    'depends': ['base', 'department_groups', 'stock', 'employee'],
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rules.xml',
        'data/ir_sequence_data.xml',
        'data/overtime_type_data.xml',
        'data/allowance_type_data.xml',
        'data/contract_stage_data.xml',
        'data/mail_template_data.xml',
        'data/ir_cron.xml',
        'wizard/docs_view_views.xml',
        'wizard/change_requests_view.xml',
        'views/contracts_contracts_attachment_view.xml',
        'views/contract_contracts_view.xml',
        'views/contract_lines_views.xml',
        'views/overtime_types_view.xml',
        'views/allowance_type_view.xml',
        'views/contract_stage_view.xml'

    ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'AGPL-3',
    'images': [],
}
