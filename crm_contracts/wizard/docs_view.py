# -*- coding: utf-8 -*-

from odoo import models, fields


class DocsView(models.TransientModel):
    _name = "docs.view"
    _description = "Pop up to view the Document without downloading it."

    doc = fields.Binary(string="Document")
