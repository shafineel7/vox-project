# -*- coding: utf-8 -*-

from odoo import models, fields, Command
from datetime import datetime


class ChangeRequests(models.TransientModel):
    _name = "change.requests"
    _description = "Creating Change Requests"

    name = fields.Text(string="Reason")
    contract_id = fields.Many2one('contract.contracts', string="Contract")
    mark_inactive = fields.Boolean(string="Mark Contract as Inactive")

    def submit_for_approval(self):
        if self.sudo().contract_id.stage_code == '03':
            ctx = self.env.context.copy()
            ctx['allow_stage'] = True
            ctx['change'] = True
            self.env.context = ctx

            self.sudo().contract_id.rework_reason_sample = self.name
            return self.sudo().contract_id.rework()
        else:
            ctx = self.env.context.copy()
            ctx["change"] = True
            self.env.context = ctx
            self.sudo().contract_id.sudo().assigned_users_ids = self.env.user.ids
            change = self.env['contract.change.request'].sudo().create({
                'name': self.name,
                'contract_id': self.contract_id.id,
                'date': datetime.now().date()
            })
            self.sudo().contract_id.change_req_id = change.id
            self.sudo().contract_id.is_self_assign = False
            groups = self.env.ref('department_groups.group_finance_mgr').id
            self.sudo().contract_id.assigned_users_ids = False
            self.sudo().contract_id.assigned_groups_ids = False
            self.sudo().contract_id.assigned_groups_ids = [Command.link(groups), Command.link(self.env.ref("base.group_erp_manager").id)]
            self.sudo().contract_id.change_requested = True
            groups = self.env.ref('department_groups.group_finance_mgr')
            self.sudo().contract_id.assigned_users_ids = [Command.link(user.id) for user in groups.users]
            self.sudo().contract_id.mark_inactive = self.mark_inactive
            template_id = self.env.ref('crm_contracts.email_contract_fm_change_approval', raise_if_not_found=False)
            if template_id:
                lang = self.sudo().contract_id.partner_id.lang
                template = self.env['mail.template'].browse(template_id.id)
                template.send_mail(self.sudo().contract_id.id, force_send=True)

