# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AllowanceType(models.Model):
    _name = 'allowance.type'
    _description = 'Allowance Types'
    _order = "id desc"
    _rec_name = 'name'

    name = fields.Char(string="Name")
    code = fields.Char(string="Code")

    _sql_constraints = [('code_unique', 'unique(code)', "Code already exists!")]