# -*- coding: utf-8 -*-

from odoo import models, fields, api
import random


class ContractLocationDetails(models.Model):
    _name = 'contract.location.details'

    loc_code = fields.Char(string="Location ID")
    loc_name = fields.Char(string="Location Name")
    invoice_loc = fields.Char(string="Invoice Address")
    status = fields.Selection([('yes', 'Yes'), ('no', 'No')], string="Status")
    contract_line_id = fields.Many2one('contract.lines', string="Contract Lines")
    contract_id = fields.Many2one('contract.contracts', string="Contract Lines", related='contract_line_id.contract_id')
