# -*- coding: utf-8 -*-
import datetime

from odoo import models, fields, api, Command, _
from odoo.exceptions import UserError, ValidationError
from dateutil import relativedelta


class ContractContracts(models.Model):
    _name = 'contract.contracts'
    _description = 'Customer Contracts'
    _order = "id desc"
    _rec_name = 'code'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _track_duration_field = 'stage_id'

    @api.model
    def default_get(self, fields):
        if self.env.user.has_group('department_groups.group_contarct_bids_exec') or self.env.user.has_group(
                'department_groups.group_contarct_bde') or self.env.user.has_group("base.group_erp_manager"):
            return super(ContractContracts, self).default_get(fields)
        else:
            raise ValidationError("You cannot create Contracts.")

    def get_dafault_groups(self):
        bid = self.env.ref('department_groups.group_contarct_bids_exec')
        bde = self.env.ref('department_groups.group_contarct_bde')
        return [Command.link(bid.id), Command.link(bde.id)]

    name = fields.Char(string="Name")
    code = fields.Char(string="Contract ID")
    partner_id = fields.Many2one('res.partner', string="Customer", tracking=1)
    phone = fields.Char(string="Phone")
    email = fields.Char(string="Email")
    type = fields.Selection([('new', 'New'), ('addendum', 'Contract Addendum'), ('renewal', 'Contract Renewal')],
                            string="Contract Type", default='new', tracking=1)
    trade_license = fields.Binary(string="Trade License")
    trade_license_name = fields.Char(string="Trade License")
    vat = fields.Binary(string="Vat Certificate")
    vat_name = fields.Char(string="Vat Certificate")
    passport_copy = fields.Binary(string="Passport Copy")
    passport_name = fields.Char(string="Passport Copy")
    eid_copy = fields.Binary(string="Signatory EID")
    eid_name = fields.Char(string="Signatory EID")
    visa_copy = fields.Binary(string="Visa Copy")
    visa_name = fields.Char(string="Visa Copy")
    status = fields.Selection([('active', 'Active'), ('inactive', 'Inactive')], string="Active", default='active',
                              tracking=1)
    stage = fields.Selection([('new', 'New'), ('doc_collect', 'Document Collection'), ('submit', 'Contract Submitted'),
                              ('negotiation', 'Negotiation'), ('signed', 'Signed & Confirmed'), ('reject', 'Reject'),
                              ('hold', 'On-hold')], string="Stage")
    user_id = fields.Many2one('res.users', string="Contract Owner", default=lambda self: self.env.user)
    assigned_users_ids = fields.Many2many('res.users', string="Assigned Users", default=lambda self: self.env.user)
    assigned_groups_ids = fields.Many2many('res.groups', string="Assigned Groups", default=get_dafault_groups)
    last_notify_date = fields.Datetime(string="Last Jafza Notification Send Date")
    contract_lines_ids = fields.One2many('contract.lines', 'contract_id', string="Contract Lines")
    jafza_req = fields.Selection([("yes", "Yes"), ("no", "No")], default="no", string="JAFZA Requirement", tracking=1)
    jafza_status = fields.Selection([('done', 'Done'), ('ndone', 'Not Done')], default='ndone', string="Jafza Status",
                                    tracking=1)
    last_notif_date = fields.Datetime(string="Last Notified On")
    company_id = fields.Many2one(
        comodel_name='res.company',
        required=True, index=True,
        default=lambda self: self.env.company)
    currency_id = fields.Many2one(
        'res.currency', 'Currency', compute='_compute_currency_id')
    service_type = fields.Selection(
        [('ss', 'Soft Services'), ('cao', 'Construction & Delivery'), ('mpo', 'Staffing Services')],
        string="Service Type", tracking=True)
    stage_id = fields.Many2one('contract.stage', string="Stages",
                               default=lambda self: self.env['contract.stage'].search([('code', '=', '01')], limit=1),
                               tracking=1)
    stage_code = fields.Char(string="Stage Code", related="stage_id.code", store=True)
    is_user = fields.Boolean(string="Allowed User", compute='compute_allowed_user_group')
    is_group = fields.Boolean(string="Allowed Group", compute='compute_allowed_user_group')
    is_admin = fields.Boolean(string="Admin", compute='compute_allowed_user_group')
    ack_user_id = fields.Many2one('res.users', string="Acknowledged By")
    is_ack = fields.Boolean(string="Is Acknowledged User", compute='compute_allowed_user_group')
    is_self_assign = fields.Boolean(string="Self Assign", default=False)
    approval_status = fields.Selection([('approve', 'Approved'), ('reject', 'Reject'), ('rework', 'Rework')],
                                       string="FM Approval Status", tracking=1)
    approve_user_id = fields.Many2one('res.users', string="Approved By", tracking=1)
    approved_on = fields.Datetime(string="Approved On", tracking=1)
    rework_reason = fields.Text(string="Rework Reason")
    rework_reason_sample = fields.Text(string="Rework Reason")
    contract_id = fields.Many2one('contract.contracts', string="Contract")
    jafza_req_date = fields.Date(string="Jafza Notify Date")
    contract_start_date = fields.Date(string="Contract Start Date")
    contract_end_date = fields.Date(string="Contract End Date")
    mode = fields.Selection([('open', 'Open'), ('fixed', 'Fixed')], string="Mode")
    lpo_number = fields.Char(string="LPO Number")
    lpo_validity = fields.Integer(string="LPO Validity")
    lpo_attachment = fields.Binary(string="LPO Attachment")
    contract_value = fields.Float(string="Contract Value")
    department_id = fields.Many2one('hr.department', string="Service Type")
    change_ids = fields.One2many('contract.change.request', 'contract_id', string="Change Requests")
    change_req_id = fields.Many2one('contract.change.request', string="Request")
    change_requested = fields.Boolean(string="Change Requested")
    change_approved = fields.Boolean(string="Approved Change Request", default=False)
    reject_reason = fields.Text(string="Reject Reason")
    req_submitted = fields.Boolean(string="Is Request Submitted", default=False)
    mark_inactive = fields.Boolean(string="Mark Contract as Inactive")
    active = fields.Boolean('Active', default=True, tracking=True)


    @api.depends('company_id')
    def _compute_currency_id(self):
        for order in self:
            order.currency_id = order.company_id.currency_id

    @api.onchange('partner_id', 'department_id', 'create_date')
    def onchange_partner_service(self):
        seq = "TMAXDXB"
        if self.partner_id and self.create_date and self.department_id:
            if not self.partner_id.contract_seq:
                seq = "TMAXDXB"
                code = 'contracts.contracts.' + self.partner_id.name[0].lower()
                seqs = self.env['ir.sequence'].next_by_code(code)
                if seqs:
                    seq += '|' + seqs
                if self.create_date:
                    date_seq = self.create_date.date().strftime("%d%m%y")
                    seq += "|" + date_seq
                if self.department_id:
                    if self.department_id.seq_code:

                        seq += "|" + self.department_id.seq_code.upper()
                self.code = seq
                self.partner_id.contract_seq = seq
            else:
                self.code = self.partner_id.contract_seq

    def write(self, vals):
        new_stg_ref = self.env.ref("crm_contracts.contract_stage1").id
        doc = self.env.ref("crm_contracts.contract_stage2").id
        fm_approve = self.env.ref("crm_contracts.contract_stage10").id
        ctr_submit = self.env.ref("crm_contracts.contract_stage3").id
        appr = self.env.ref("crm_contracts.contract_stage8").id
        if 'stage_id' in vals:
            if vals['stage_id'] in [new_stg_ref, doc, fm_approve, appr]:
                ctx = self.env.context
                if 'allow_stage' not in ctx:
                    raise UserError("You cannot change the stage directly. Please discard changes.")
            if vals['stage_id'] == ctr_submit:
                if not self.visa_copy or not self.trade_license or not self.vat or not self.eid_copy or not self.passport_copy:
                    raise ValidationError("Please add all required Documents.")
        res = super(ContractContracts, self).write(vals)
        for data in self:
            if data.status == 'inactive':
                ctx = self.env.context
                if 'allow_stage' not in ctx:

                    raise UserError("You cannot update an Inactive Contract. Please discard changes.")

            if data.stage_id.id == fm_approve:
                ctx = self.env.context.copy()
                if 'allow_stage' not in ctx:
                    raise UserError("Contract is under Approval.")
            if data.approval_status == 'approve':
                ctx = self.env.context.copy()
                if 'change' not in ctx:
                    if 'stage_id' not in vals:
                        raise UserError("Please Create change requests. Please discard changes.")
            if data.stage_code in ['04', '07', '08']:
                ctx = self.env.context.copy()

                if data.approval_status != 'approve':
                    if 'change' not in ctx:
                        raise UserError("Contract is not approved.")
            if data.stage_code not in ['01', '02', '03', '05', '06']:
                ctx = self.env.context.copy()
                if 'change' not in ctx:
                    if 'stage_id' not in vals:
                        raise UserError("Please Create change requests")

        return res

    def submit_for_approval(self):
        if self.stage_code in ['02', '03']:
            if not self.visa_copy or not self.trade_license or not self.vat or not self.eid_copy or not self.passport_copy:
                raise ValidationError("Please add all required Documents.")
        action = self.env.ref('crm_contracts.action_contracts_contract').sudo().read()[0]
        stage = self.env['contract.stage'].sudo().search([('code', '=', '03')], limit=1)
        groups = self.env.ref('department_groups.group_finance_mgr').id
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        ctx['change'] = True
        self.env.context = ctx
        self.sudo().stage_id = stage
        self.sudo().assigned_groups_ids = False
        self.sudo().assigned_users_ids = False
        self.sudo().assigned_groups_ids = [Command.link(groups), Command.link(self.env.ref("base.group_erp_manager").id)]
        self.sudo().approval_status = False
        self.sudo().is_self_assign = False
        self.sudo().req_submitted = True
        template_id = self.env.ref('crm_contracts.email_contract_fm_approval', raise_if_not_found=False)
        if template_id:
            lang = self.partner_id.lang
            template = self.env['mail.template'].browse(template_id.id)
            template.send_mail(self.id, force_send=True)
        self.sudo().assigned_users_ids = [Command.link(user.id) for user in self.assigned_groups_ids.users]
        # return action

    def document_collection(self):
        stage = self.env['contract.stage'].sudo().search([('code', '=', '02')], limit=1)
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        self.env.context = ctx
        self.stage_id = stage

    def view_attachments(self):
        view_id = self.env.ref('crm_contracts.contracts_contract_attachments_view_form')
        ctx = self.env.context.copy()
        return {
            'name': _('Add Attachments'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'contract.contracts',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx
        }

    def view_td(self):
        doc = False
        context = self.env.context
        if 'td' in context or 'vat' in context or 'passport' in context or 'visa' in context or 'eid' in context:
            if 'td' in context:
                if not self.trade_license:
                    raise UserError("Please Add Trade License")
                doc = self.trade_license
            elif 'vat' in context:
                if not self.vat:
                    raise UserError("Please Add VAT Document")
                doc = self.vat
            elif 'passport' in context:
                if not self.passport_copy:
                    raise UserError("Please Add Passport Copy.")
                doc = self.passport_copy
            elif 'visa' in context:
                if not self.visa_copy:
                    raise UserError("Please Add Visa Copy.")
                doc = self.visa_copy
            elif 'eid' in context:
                if not self.eid_copy:
                    raise UserError("Please Add EID Copy.")
                doc = self.eid_copy
            if doc:
                value = self.env['docs.view'].sudo().create({'doc': doc})
                return {
                    'type': 'ir.actions.act_window',
                    'message': 'Message',
                    'res_model': 'docs.view',
                    'view_mode': 'form',
                    'target': 'new',
                    'res_id': value.id
                }

    @api.depends('assigned_users_ids', 'assigned_groups_ids')
    def compute_allowed_user_group(self):
        for rec in self:
            rec.is_user = False
            rec.is_group = False
            rec.is_admin = False
            rec.is_ack = False
            if self.env.user in rec.assigned_users_ids:
                rec.is_user = True
            if self.env.user in rec.assigned_groups_ids.users:
                rec.is_group = True
            if self.env.user.has_group("base.group_erp_manager"):
                rec.is_admin = True
                rec.is_user = True
                rec.is_ack = True
            if self.env.user == rec.ack_user_id:
                rec.is_ack = True
            if rec.stage_code == '05':
                if self.env.user == self.create_uid:
                    rec.is_user = True

    def approve(self):
        action = self.env.ref('crm_contracts.action_contracts_contract').sudo().read()[0]
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        ctx['change'] = True
        self.env.context = ctx
        self.sudo().approval_status = "approve"
        self.sudo().approved_on = datetime.datetime.now()
        self.sudo().ack_user_id = self.env.user
        self.sudo().approve_user_id = self.env.user
        self.sudo().assigned_groups_ids = False
        self.sudo().is_self_assign = False
        self.sudo().change_requested = False
        self.sudo().change_approved = False
        stage = self.env['contract.stage'].sudo().search([('code', '=', '05')], limit=1)
        if stage:
            self.sudo().stage_id = stage
        self.sudo().assigned_groups_ids = [
            Command.link(self.env.ref("department_groups.group_contarct_bids_exec").id),
            Command.link(self.env.ref("base.group_erp_manager").id),
            Command.link(self.env.ref("department_groups.group_contarct_bde").id)]
        template_id = self.env.ref('crm_contracts.email_contract_fm_approved', raise_if_not_found=False)
        if template_id:
            template = self.env['mail.template'].browse(template_id.id)
            template.send_mail(self.id, force_send=True)
        # self.sudo().assigned_users_ids = self.create_uid
        self.sudo().assigned_users_ids = self.user_id.ids

        # return action

    def submit_for_rework(self):

        view_id = self.env.ref('crm_contracts.change_requests_views_form')
        res = self.env['change.requests'].sudo().create({'contract_id': self.id})
        ctx = self.env.context.copy()
        return {
            'name': _('Rework Reason'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'change.requests',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': res.id,
            'context': ctx
        }

    def rework(self):
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        ctx['change'] = True
        self.env.context = ctx

        action = self.env.ref('crm_contracts.action_contracts_contract').sudo().read()[0]

        self.sudo().approval_status = "rework"
        self.sudo().ack_user_id = False
        self.sudo().assigned_groups_ids = False
        self.sudo().assigned_groups_ids = False
        self.sudo().is_self_assign = False
        self.sudo().assigned_groups_ids = [Command.link(self.env.ref("department_groups.group_contarct_bids_exec").id),
                                           Command.link(self.env.ref("base.group_erp_manager").id),
                                           Command.link(self.env.ref("department_groups.group_contarct_bde").id)]
        if self.rework_reason_sample:
            if self.rework_reason:
                self.sudo().rework_reason = str(self.rework_reason) + "\n" + str(
                    (datetime.datetime.utcnow() + datetime.timedelta(hours=4)).replace(microsecond=0)) + ' #' + str(
                    self.env.user.name) + ' - ' + str(self.rework_reason_sample)
            else:
                self.sudo().rework_reason = str(
                    (datetime.datetime.utcnow() + datetime.timedelta(hours=4)).replace(microsecond=0)) + ' #' + str(
                    self.env.user.name) + ' - ' + str(self.rework_reason_sample)
            self.sudo().rework_reason_sample = False
        template_id = self.env.ref('crm_contracts.email_contract_fm_approved', raise_if_not_found=False)
        if template_id:
            template = self.env['mail.template'].browse(template_id.id)
            template.send_mail(self.id, force_send=True)
        self.sudo().assigned_users_ids = self.create_uid
        # return action

    def acknowledge(self):
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        ctx['change'] = True
        self.env.context = ctx

        self.sudo().ack_user_id = self.env.user
        self.sudo().is_self_assign = True
        self.sudo().assigned_users_ids = False
        self.sudo().req_submitted = False

        self.sudo().assigned_users_ids = self.env.user.ids

    def send_contract(self):
        template_id = self.env.ref('crm_contracts.email_template_contract_sent', raise_if_not_found=False)
        if template_id:
            lang = self.partner_id.lang
            template = self.env['mail.template'].browse(template_id.id)
            template.send_mail(self.id, force_send=True)
            ctx = {
                'default_model': 'contract.contracts',
                'default_res_ids': self.ids,
                'default_template_id': template.id if template else None,
                'default_composition_mode': 'comment',
                'mark_so_as_sent': True,
                'default_email_layout_xmlid': 'mail.mail_notification_layout_with_responsible_signature',
                'force_email': True,
                'model_description': self.with_context(lang=lang).code,
                'allow_stage': True,
                'change': True
            }
            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'mail.compose.message',
                'views': [(False, 'form')],
                'view_id': False,
                'target': 'new',
                'context': ctx,
            }

    @api.onchange('jafza_req')
    def onchange_jafza_reqs(self):
        if self.jafza_req:
            if self.jafza_req == 'yes':
                self.jafza_req_date = datetime.datetime.now().date() + relativedelta.relativedelta(days=3)

    def _cron_send_jafza_notify(self):
        contracts = self.search([('jafza_req', '=', 'yes'), ('jafza_status', '=', 'ndone'),
                                 ('jafza_req_date', '=', datetime.datetime.now().date())])
        if contracts:
            for contract in contracts:
                template_id = self.env.ref('crm_contracts.email_template_jafza_notify_sent', raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(contract.id, force_send=True)
                    contract.last_notif_date = datetime.datetime.now()
                    contract.jafza_req_date = datetime.datetime.now().date() + relativedelta.relativedelta(days=3)

    def add_change_request(self):
        value = self.env['change.requests'].sudo().create({'contract_id': self.id})
        return {
            'type': 'ir.actions.act_window',
            'message': 'Message',
            'res_model': 'change.requests',
            'view_mode': 'form',
            'target': 'new',
            'res_id': value.id
        }

    def approve_change(self):
        action = self.env.ref('crm_contracts.action_contracts_contract').sudo().read()[0]
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        ctx['change'] = True
        self.env.context = ctx

        self.sudo().assigned_groups_ids = False
        self.sudo().assigned_users_ids = False
        self.sudo().assigned_users_ids = self.user_id.ids
        self.sudo().change_req_id.approval_status = 'approve'
        self.sudo().change_req_id.user_id = self.env.user
        self.sudo().change_requested = False
        self.sudo().change_approved = True
        self.sudo().approval_status = False
        if self.sudo().mark_inactive:
            if self.sudo().contract_lines_ids:
                for lines in self.sudo().contract_lines_ids:
                    lines.sudo().state = 'inactive'
                    lines.sudo().active = False
            self.sudo().status = 'inactive'
            self.sudo().active = False

        template_id = self.env.ref('crm_contracts.email_contract_fm_approved', raise_if_not_found=False)
        if template_id:
            lang = self.partner_id.lang
            template = self.env['mail.template'].browse(template_id.id)
            template.send_mail(self.id, force_send=True)
        # return action

    def reject_change(self):
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        ctx['change'] = True
        self.env.context = ctx
        action = self.env.ref('crm_contracts.action_contracts_contract').sudo().read()[0]
        self.sudo().assigned_groups_ids = False
        self.sudo().change_req_id.approval_status = 'reject'
        self.sudo().change_req_id.user_id = self.env.user
        self.sudo().assigned_users_ids = self.user_id.ids
        self.sudo().change_requested = False
        template_id = self.env.ref('crm_contracts.email_contract_change_fm_reject', raise_if_not_found=False)
        if template_id:
            lang = self.partner_id.lang
            template = self.env['mail.template'].browse(template_id.id)
            template.send_mail(self.id, force_send=True)
        # return action










