# -*- coding: utf-8 -*-
import datetime

from odoo import models, fields, api
from odoo.exceptions import UserError
from dateutil.relativedelta import relativedelta


class ContractLines(models.Model):
    _name = 'contract.lines'
    _description = 'Contract Lines'

    contract_id = fields.Many2one('contract.contracts', string="Contract ID")
    name = fields.Char(string="Description")
    product_id = fields.Many2one('product.product', string='Service')
    qty = fields.Float(string="Quantity")
    unit_price = fields.Float(string="Unit Rate")
    sub_total = fields.Float(string="Sub Total", compute='_compute_amount',
                             store=True, precompute=True)
    discount = fields.Float(string="Discount")
    total = fields.Float(string="Total", compute='_compute_amount',
                         store=True, precompute=True)
    lead_id = fields.Many2one('crm.lead', string="Lead")
    tax_id = fields.Many2many(comodel_name='account.tax')
    location_ids = fields.Many2many('hr.work.location', string="Location")
    day_count = fields.Integer(string="Number of days in a week")
    start_date = fields.Date(string="Start Date")
    mode = fields.Selection([('open', 'Open'), ('fixed', 'Fixed')], string="Mode")
    end_date = fields.Date(string="End Date")
    lpo_number = fields.Char(string="LPO Number")
    lpo_validity = fields.Integer(string="LPO Validity")
    lpo_attachment = fields.Binary(string="LPO Attachment")
    payroll_hours = fields.Float(string="Payroll Hours")
    invoice_hours = fields.Float(string="Normal Invoice Hours")
    deploy_date = fields.Date(string="Deployment Date")
    contract = fields.Binary(string="Contract")
    contract_details_ids = fields.One2many('contract.location.details', 'contract_line_id', string="Location Details")
    company_id = fields.Many2one(
        comodel_name='res.company',
        required=True, index=True,
        default=lambda self: self.env.company)
    currency_id = fields.Many2one(
        'res.currency', 'Currency', compute='_compute_currency_id')
    payroll_based = fields.Selection([('amount', 'Timesheet Amount'), ('hours', 'Timesheet Hours')], string="Based On")
    payroll_mode = fields.Selection([('fixed', 'Fixed'), ('percent', 'Percentage'), ('prorata', 'Prorata')],
                                    string="Payroll Mode")
    overtime_type_ids = fields.Many2many('overtime.type', string="Overtime Type")
    allowance_type_ids = fields.Many2many('allowance.type', string="Allowance Type")
    amount = fields.Float(string="Amount")
    percent = fields.Float(string="Percent")
    max_amount = fields.Float(string="Max Amount")
    billed_to = fields.Selection([('trumax', 'Trumax'), ('staff', 'Staff'), ('customer', 'Customer')],
                                 string="Billed To")
    percent_over_ids = fields.Many2many('hr.salary.rule', string="Percentage Over")
    bill_partner_id = fields.Many2one('res.partner', string="Invoice Address")
    inv_overtime_ids = fields.Many2many('overtime.type', 'overtime_type_contracts_lines_rel', 'overtime_id', 'line_id',
                                        string="Overtime Types")
    inv_allowance_ids = fields.Many2many('allowance.type', 'allowance_type_contracts_lines_rel', 'allowance_id',
                                         'line_id', string="Allowance Type")
    inv_based_on = fields.Selection(
        [('amount', 'Timesheet Amount'), ('hours', 'Timesheet Hours'), ('staff', 'Number of Staff in Timesheet')],
        string="Based On")
    inv_mode = fields.Selection([('fixed', 'Fixed'), ('percent', 'Percentage'), ('prorata', 'Prorata')],
                                string="Invoice Mode")
    inv_amount = fields.Float(string="Invoice Amount")
    inv_percent = fields.Float(string="Percent")
    inv_max_amount = fields.Float(string="Max Amount")
    inv_billed_to = fields.Selection([('trumax', 'Trumax'), ('staff', 'Staff'), ('customer', 'Customer')],
                                     string="Billed To")
    contract_inv_line_ids = fields.One2many('contract.invoice.lines', 'line_id', string="Invoice Details")
    contract_payroll_line_ids = fields.One2many('contract.payroll.lines', 'line_id', string="Payroll Details")
    state = fields.Selection([('active', 'Active'), ('inactive', 'Inactive')], string="Status", default='active')
    renewal_date = fields.Date(string="Renewal Date", compute='compute_renewal_date', store=True)
    contract_type = fields.Selection(
        [('hourly', 'Hourly'), ('daily', 'Daily'), ('monthly', 'Monthly'), ('fixed', 'Fixed Cost')],
        string="Contract Type")
    active = fields.Boolean('Active', default=True, tracking=True)


    @api.depends('start_date', 'end_date', 'mode')
    def compute_renewal_date(self):
        for vals in self:
            vals.renewal_date = datetime.datetime.now().date()
            if vals.start_date and vals.end_date and vals.mode:
                if vals.mode == 'open':
                    vals.renewal_date = vals.end_date - relativedelta(days=7)
                else:
                    vals.renewal_date = vals.end_date - relativedelta(months=1)

    def write(self, values):
        res = super(ContractLines, self).write(values)
        if 'payroll_mode' in values:
            if self.payroll_mode == 'fixed':
                if self.amount <= 0:
                    raise UserError("Please Add Amount.")
        return res

    @api.depends('company_id')
    def _compute_currency_id(self):
        for order in self:
            order.currency_id = order.company_id.currency_id

    @api.depends('qty', 'discount', 'unit_price', 'tax_id')
    def _compute_amount(self):
        for line in self:
            tax_results = self.env['account.tax']._compute_taxes([
                line._convert_to_tax_base_line_dict()
            ])
            totals = list(tax_results['totals'].values())[0]
            amount_untaxed = totals['amount_untaxed']
            amount_tax = totals['amount_tax']

            line.update({
                'sub_total': amount_untaxed,
                # 'price_tax': amount_tax,
                'total': amount_untaxed + amount_tax,
            })

    def _convert_to_tax_base_line_dict(self, **kwargs):
        self.ensure_one()
        return self.env['account.tax']._convert_to_tax_base_line_dict(
            self,
            partner=False,
            currency=self.currency_id,
            product=self.product_id,
            taxes=self.tax_id,
            price_unit=self.unit_price,
            quantity=self.qty,
            discount=self.discount,
            price_subtotal=self.total,
            **kwargs,
        )

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.name = self.product_id.name

    @api.onchange('start_date', 'end_date')
    def onchange_start_end_date(self):
        if self.start_date and self.end_date:
            if self.end_date < self.start_date:
                raise UserError("Contract End Date should be greater than Star date.")

    @api.onchange('mode', 'lpo_validity')
    def onchange_mode_validity(self):
        if self.mode == 'open':
            if self.lpo_validity <= 0:
                raise UserError("Please Add LPO Validity.")
            self.end_date = self.start_date + relativedelta(days=self.lpo_validity)



    @api.onchange('overtime_type_ids')
    def onchange_overtime_type_id(self):
        if self.overtime_type_ids:
            self.allowance_type_ids = False

    @api.onchange('payroll_mode', 'percent')
    def onchange_payroll_mode(self):
        if self.payroll_mode:
            if self.payroll_mode == 'percent':
                if self.percent <= 0:
                    raise UserError("Please Add Payroll Percentage Value.")

    @api.onchange('inv_mode', 'inv_percent')
    def onchange_inv_mode(self):
        if self.inv_mode:
            if self.inv_mode == 'percent':
                if self.inv_percent <= 0:
                    raise UserError("Please Add Invoice Percentage Value.")
