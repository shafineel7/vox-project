# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError


class ContractInvoiceLines(models.Model):
    _name = 'contract.invoice.lines'
    _description = 'Contract Invoice Lines'

    type = fields.Selection([('allowance', 'Allowance'), ('overtime', 'Overtime')], string="Type")
    overtime_type_id = fields.Many2one('overtime.type', string="Overtime Type")
    allowance_type_id = fields.Many2one('allowance.type', string="Allowance Type")
    inv_based_on = fields.Selection(
        [('amount', 'Timesheet Amount'), ('hours', 'Timesheet Hours'), ('staff', 'Number of Staff in Timesheet')],
        string="Based On")
    inv_mode = fields.Selection([('fixed', 'Fixed'), ('percent', 'Percentage'), ('prorata', 'Prorata')],
                                string="Invoice Mode")
    inv_amount = fields.Float(string="Invoice Amount")
    inv_percent = fields.Float(string="Percent")
    inv_max_amount = fields.Float(string="Max Amount")
    inv_billed_to = fields.Selection([('trumax', 'Trumax'), ('staff', 'Staff'), ('customer', 'Customer')],
                                     string="Billed To")
    line_id = fields.Many2one('contract.lines', string="Contract Lines")

    @api.onchange('type')
    def onchange_line_type(self):
        if self.type:
            if self.type == 'allowance':
                self.overtime_type_id = False
            else:
                self.allowance_type_id = False

    @api.onchange('inv_mode')
    def onchange_inv_mode(self):
        if self.inv_mode:
            if self.inv_mode == 'fixed':
                self.inv_percent = 0
            elif self.inv_mode == 'percent':
                self.inv_amount = 0





