# -*- coding: utf-8 -*-

from odoo import models, fields, api


class OvertimeType(models.Model):
    _name = 'overtime.type'
    _description = 'Overtime Types'
    _order = "id desc"
    _rec_name = 'name'

    name = fields.Char(string="Name")
    code = fields.Char(string="Code")

