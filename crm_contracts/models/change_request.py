# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError


class ContractChangeRequest(models.Model):
    _name = 'contract.change.request'
    _description = 'Contract Change Requests'

    name = fields.Text(string="Reason")
    date = fields.Date(string="Date")
    approval_status = fields.Selection([('approve', 'Approved'), ('reject', 'Reject'), ('rework', 'Rework')],
                                       string="FM Approval Status", tracking=1)
    contract_id = fields.Many2one('contract.contracts', string="Contract")
    user_id = fields.Many2one('res.users', string="Approved / Rejected By")



