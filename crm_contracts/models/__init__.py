# -*- coding: utf-8 -*-

from . import contract_stages
from . import contract_contracts
from . import contract_lines
from . import contract_location_details
from . import overtime_type
from . import allowance_type
from . import res_partner
from . import contract_invoice_lines
from . import contract_payroll_lines
from . import change_request


