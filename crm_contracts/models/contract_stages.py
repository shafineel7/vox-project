# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ContractStages(models.Model):
    _name = 'contract.stage'
    _description = 'Contract Stage'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    code = fields.Char(string="Code")
