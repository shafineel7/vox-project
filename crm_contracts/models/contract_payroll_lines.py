# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError


class ContractPayrollLines(models.Model):
    _name = 'contract.payroll.lines'
    _description = 'Contract Payroll Lines'

    type = fields.Selection([('allowance', 'Allowance'), ('overtime', 'Overtime')], string="Type")
    overtime_type_id = fields.Many2one('overtime.type', string="Overtime Type")
    allowance_type_id = fields.Many2one('allowance.type', string="Allowance Type")
    payroll_based_on = fields.Selection(
        [('amount', 'Timesheet Amount'), ('hours', 'Timesheet Hours'), ('staff', 'Number of Staff in Timesheet')],
        string="Based On")
    payroll_mode = fields.Selection([('fixed', 'Fixed'), ('percent', 'Percentage'), ('prorata', 'Prorata')],
                                string="Invoice Mode")
    amount = fields.Float(string="Invoice Amount")
    percent = fields.Float(string="Percent")
    max_amount = fields.Float(string="Max Amount")
    billed_to = fields.Selection([('trumax', 'Trumax'), ('staff', 'Staff'), ('customer', 'Customer')],
                                     string="Billed To")
    line_id = fields.Many2one('contract.lines', string="Contract Lines")
    percent_over_ids = fields.Many2many('hr.salary.rule', string="Percentage Over")

    @api.onchange('type')
    def onchange_line_type(self):
        if self.type:
            if self.type == 'allowance':
                self.overtime_type_id = False
            else:
                self.allowance_type_id = False

    @api.onchange('payroll_mode')
    def onchange_payroll_mode(self):
        if self.payroll_mode:
            if self.payroll_mode == 'fixed':
                self.percent = 0
                self.percent_over_ids = False
            elif self.payroll_mode == 'percent':
                self.amount = 0
