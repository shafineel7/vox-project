# -*- coding: utf-8 -*-
{
    'name': 'Employee Timesheet',
    'version': '17.0.1.0.1',
    'category': 'Human Resources',
    'summary': 'Track employee timesheets',
    'description': """
        This module allows you to track employee timesheet and create and edit employee timesheets from an external view.
    """,
    'author': 'Jishnu K',
    'website': 'https://www.voxtronme.com',
    'depends': ['project', 'employee', 'crm_contracts'],
    'external_dependencies': {
        'python': [
            'openpyxl',
        ],
    },
    'data': [
        'security/ir.model.access.csv',
        'views/employee_timesheet_views.xml',
        'wizard/import_confirm.xml',
        'wizard/employee_timesheet_view.xml',
        'views/employee_allowance_view.xml',
        'views/monthly_timesheet_views.xml',
        'views/designation_views.xml',
        'views/employee_timesheet_menu.xml',
        'views/timesheet_html_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,

}
