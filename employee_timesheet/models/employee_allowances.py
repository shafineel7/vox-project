# -*- coding: utf-8 -*-
from odoo import models, fields


class EmployeeAllowance(models.Model):
    _name = 'employee.allowance'
    _description = 'Employee Allowance'

    name = fields.Char('Name')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    employee_name = fields.Char(string='Employee')
    contract_name = fields.Char(string='Contract')
    designation = fields.Char(string='Designation')
    month = fields.Selection([('1', 'January'), ('2', 'February'), ('3', 'March'), ('4', 'April'),
                              ('5', 'May'), ('6', 'June'), ('7', 'July'), ('8', 'August'), ('9', 'September'),
                              ('10', 'October'), ('11', 'November'), ('12', 'December')], string='Month')
    year = fields.Char(string='Year')
    amount = fields.Float(string='Amount')
    monthly_timesheet_id = fields.Many2one('monthly.timesheet', string='Monthly Timesheet')
    product_id = fields.Many2one('product.product', string='Designation')
    allowance_type_id = fields.Many2one('allowance.type', 'Allowance')
