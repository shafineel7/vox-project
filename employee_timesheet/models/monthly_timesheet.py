# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime


class MonthlyTimesheet(models.Model):
    _name = 'monthly.timesheet'
    _description = 'Monthly Timesheet'

    MONTH_SELECTION = [
        ('1', 'January'),
        ('2', 'February'),
        ('3', 'March'),
        ('4', 'April'),
        ('5', 'May'),
        ('6', 'June'),
        ('7', 'July'),
        ('8', 'August'),
        ('9', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December'),
    ]

    excel_file = fields.Binary(string='Excel File', attachment=True)
    file_name = fields.Char()
    year = fields.Selection([(str(year), str(year)) for year in range(2020, datetime.now().year + 1)], string='Year')
    month = fields.Selection(MONTH_SELECTION, string='Month')
    monthly_allowance_ids = fields.One2many('employee.allowance', 'monthly_timesheet_id',
                                            string='Employee Allowance')
    employee_timesheet_ids = fields.One2many('employee.timesheet', 'monthly_timesheet_id',
                                             string='Employee Timesheet')

    project_id = fields.Many2one('project.project')
    display_name = fields.Char(compute='_compute_display_name')
    location_id = fields.Many2one('hr.work.location', 'Location')
    customer_contract_id = fields.Many2one('contract.contracts', 'Customer Contract')
    is_cafu_timesheet = fields.Boolean('CAFU Timesheet')

    @api.depends('month', 'year', 'location_id')
    def _compute_display_name(self):
        for rec in self:
            if rec.month and rec.year:
                rec.display_name = rec.month + ' / ' + rec.year + '' + rec.location_id.name if rec.location_id else ''
            else:
                rec.display_name = 'New'

    def create_edit_timesheet(self):
        return {
            'res_id': self.id,
            "url": "/emp_timesheet/ %s "%(self.id),
            "type": "ir.actions.act_url",
        }
