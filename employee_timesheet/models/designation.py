# -*- coding: utf-8 -*-
from odoo import api, fields, models


class Designation(models.Model):
    _inherit = 'designation'

    allowance_type_ids = fields.One2many('designation.allowances', 'designation_id',
                                         'Designation Allowance')
    payable_allowance_ids = fields.One2many('payable.allowance', 'designation_id', 'Payable Allowance')
    gross_salary = fields.Float('Gross Salary', compute='_compute_gross_salary')

    @api.depends('allowance_type_ids')
    def _compute_gross_salary(self):
        for rec in self:
            if rec.allowance_type_ids:
                rec.gross_salary = sum(rec.allowance_type_ids.mapped('amount')) + rec.wage
            else:
                rec.gross_salary = 0.0

