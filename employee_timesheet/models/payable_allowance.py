# -*- coding: utf-8 -*-
from odoo import fields, models


class PayableAllowance(models.Model):
    _name = 'payable.allowance'
    _description = 'Describes Payable allowances for a designation'

    designation_id = fields.Many2one('designation', 'Designation')
    allowance_type_id = fields.Many2one('allowance.type', 'Allowance', required=True)
    code = fields.Char('code', related='allowance_type_id.code', store=True)
    amount = fields.Float('Multiplication Factor')
    is_deduction = fields.Boolean('Is Deduction')
    is_needed_in_report = fields.Boolean('Needed In Report?', default=True)
