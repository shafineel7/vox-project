# -*- coding: utf-8 -*-
from odoo import models, fields


class EmployeeTimesheet(models.Model):
    _name = 'employee.timesheet'
    _description = 'Employee Timesheet'

    employee_name = fields.Char(string='Employee')
    employee_id = fields.Many2one('hr.employee', string="Employee ID")
    designation = fields.Char(string='Designation')
    product_id = fields.Many2one('product.product', 'Designation')
    contract_name = fields.Char(string='Contract')
    hour_spent = fields.Float(string="Hour Spent")
    hour_spent_by = fields.Char()
    date = fields.Date(string="Date", default=fields.Date.context_today)
    work_days = fields.Integer(string='Work Days')
    monthly_timesheet_id = fields.Many2one('monthly.timesheet', string='Employee Timesheet')
    hour_prefix = fields.Selection([('o', 'Weekly OT hours'), ('e', 'Ramadan OT Hours'), ('j', 'On Job Training Hours'),
                                    ('r', 'Reliever Hours'), ('h', 'Holiday overtime'),
                                    ('s', 'Special Shift'), ('i', 'Idle Hours')])
    cafu_id = fields.Char('CAFU ID')
