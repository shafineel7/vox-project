# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import base64
from io import BytesIO
import openpyxl
import calendar
from datetime import date
import re


class ImportConfirm(models.TransientModel):
    _name = 'import.confirm'
    _description = 'While importing timesheet Checking employee timesheet is already existing or not'

    cafu_import = fields.Boolean('CAFU Import')

    def import_confirm(self):
        try:
            active_id = self.env.context.get('active_id')
            employee_ts_import = self.env['employee.timesheet.import'].browse(active_id)
            monthly_timesheet = self.env['monthly.timesheet'].search([('year', '=', employee_ts_import.year),
                                                                      ('month', '=', employee_ts_import.month),
                                                                      ('location_id', '=',
                                                                       employee_ts_import.location_id.id)])
            wb = openpyxl.load_workbook(
                filename=BytesIO(base64.b64decode(employee_ts_import.excel_file)), read_only=True
            )
            ws = wb.active
            days_in_month = calendar.monthrange(int(monthly_timesheet.year), int(monthly_timesheet.month))[1]
            timesheet_obj = self.env['employee.timesheet']
            designations = []
            designation_product = self.env['product.product'].search([]).mapped(
                'name')
            for row in ws.iter_rows(min_row=2, max_row=None, min_col=5, max_col=5, values_only=True):
                designations.append(row[0])

            designation = []
            contract_designations = self.env['contract.contracts'].search(
                [('contract_id', '=', employee_ts_import.customer_contract_id.id)]).mapped('contract_lines_ids')
            contracts = []
            validate_contracts = []
            for contract in contract_designations:
                contracts.append(contract.product_id.name)

            for rec in designations:
                if rec not in designation_product and rec not in designation and rec is not None:
                    designation.append(rec)

                if rec not in contracts and rec not in validate_contracts and rec is not None:
                    validate_contracts.append(rec)
            if validate_contracts:
                raise ValidationError(
                    'Imported Excel containing %s designations. These designations are '
                    'not in the Contracts.Please configure and import again' % (validate_contracts))
            if designation:
                raise ValidationError('Imported Excel containing %s designations. These designations are '
                                      'not in the system.Please configure and import again' % (designation))
            if not employee_ts_import.timesheet_with_allowance:
                self.env['employee.timesheet'].search([('monthly_timesheet_id', '=', monthly_timesheet.id)]).unlink()
                for row in ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
                    keys = ['contract_id', 'contract_name', 'employee_id', 'employee_name', 'designation',
                            'spent_hour', ]
                    row_dict = dict(zip(keys, row))

                    dict_keys = ['worked_days']
                    work_day_col = days_in_month + 5
                    work_days = dict(zip(dict_keys, [row[work_day_col]]))
                    if row_dict['contract_id'] is not None:
                        pass
                    if row_dict['employee_id'] is not None:
                        employee = self.env['hr.employee'].search([('employee_sequence', '=', row_dict['employee_id'])],
                                                                  limit=1)
                        if not employee:
                            raise ValidationError(f"Employee  ID '{row_dict['employee_id']}' not found")
                    hour_prefix = ['o', 'O', 'e', 'E', 'j', 'J', 'r', 'R', 'h', 'H', 's', 'S']
                    for day in range(1, days_in_month + 1):
                        date_vals = date(int(monthly_timesheet.year), int(monthly_timesheet.month), day)
                        employee_hour_spent = self.env['employee.timesheet'].search([('employee_id', '=', employee.id),
                                                                                     ('date', '=', date_vals)]).mapped(
                            'hour_spent')
                        cell_value = row[day + 4]
                        if cell_value or cell_value == 0:
                            str_cell_value = str(cell_value)
                            hour_spent = ''.join(filter(lambda char: char.isdigit() or char == '.', str_cell_value))
                            hour_spent_by = ', '.join(re.findall(r'\b[a-zA-Z]\d+\b', str_cell_value))
                            if (sum(employee_hour_spent) + float(
                                    hour_spent)) <= self.env.user.company_id.max_working_hr_per_day:
                                if hour_spent_by:
                                    if hour_spent_by[0][0] in hour_prefix:
                                        if employee:
                                            timesheet_obj.create({
                                                'date': date_vals,
                                                'contract_name': row_dict['contract_name'],
                                                'employee_id': employee.id,
                                                'employee_name': row_dict['employee_name'],
                                                # 'designation': row_dict['designation'],
                                                'product_id': self.env['product.product'].search(
                                                    [('name', '=', row_dict['designation'])]).id,
                                                'hour_spent': hour_spent,
                                                'hour_spent_by': str_cell_value,
                                                'work_days': work_days.get('worked_days'),
                                                'monthly_timesheet_id': monthly_timesheet.id,
                                                'hour_prefix': hour_spent_by[0][0].lower()
                                            })
                                    else:
                                        raise ValidationError(
                                            "You can't import this excel because timesheet hour spent prefix "
                                            "only contains [o,e,j,r,h,s]")

                                else:
                                    if employee:
                                        timesheet_obj.create({
                                            'date': date_vals,
                                            'contract_name': row_dict['contract_name'],
                                            'employee_id': employee.id,
                                            'employee_name': row_dict['employee_name'],
                                            'product_id': self.env['product.product'].search([
                                                ('name', '=', row_dict['designation'])]).id,
                                            'hour_spent': hour_spent,
                                            'hour_spent_by': str_cell_value,
                                            'work_days': work_days.get('worked_days'),
                                            'monthly_timesheet_id': monthly_timesheet.id
                                        })
                            else:
                                raise ValidationError(
                                    "%s hour spent exceed %s Hours in %s. Please change hours." % (
                                    employee.employee_sequence,
                                    self.env.user.company_id.max_working_hr_per_day, date_vals)
                                )
            else:
                self.env['employee.timesheet'].search([('monthly_timesheet_id', '=', monthly_timesheet.id)]).unlink()
                self.env['employee.allowance'].search([('monthly_timesheet_id', '=', monthly_timesheet.id)]).unlink()
                allowance_heads = []
                for row in ws.iter_rows(min_row=1, max_row=1, min_col=None, max_col=None, values_only=True):
                    allowance_heads.extend(
                        ['contract_id', 'contract_name', 'employee_id', 'employee_name', 'designation'])
                    for day in range(1, days_in_month + 1):
                        allowance_heads.append(day)
                    for rec in row[days_in_month + 5:]:
                        allowance_heads.append(rec)
                    break
                for row in ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
                    keys = allowance_heads
                    row_dict = dict(zip(keys, row))
                    if row_dict['employee_id']:
                        employee = self.env['hr.employee'].search(
                            [('employee_sequence', '=', row_dict['employee_id'])], limit=1)
                        if not employee:
                            raise ValidationError(f"Employee  ID '{row_dict['employee_id']}' not found")

                        allowances = [allowance for allowance in list(row_dict)[days_in_month + 5:]]
                        allowance_types = self.env['allowance.type'].search([]).mapped('name')
                        contract_info = self.env['contract.lines'].search(
                            [('contract_id', '=', employee_ts_import.customer_contract_id.id)])
                        timesheet_allowance = []
                        contract_allowances = []
                        for al in allowances:
                            if al not in allowance_types:
                                timesheet_allowance.append(al)
                            if float(row_dict[al]) >= contract_info.mapped('contract_payroll_line_ids').filtered(
                                    lambda x: x.allowance_type_id.name == al).max_amount:
                                contract_allowances.append(al)
                        if timesheet_allowance:
                            raise ValidationError(
                                '%s allowances are not in Allowance Master, Need to create this allowances' % (
                                    timesheet_allowance))

                        if contract_allowances:
                            raise ValidationError(
                                '%s These allowances amount must be less than the amount configured in customer contracts' % (
                                    contract_allowances))

                        for allowance in list(row_dict)[days_in_month + 5:]:
                            self.env['employee.allowance'].create({
                                'month': employee_ts_import.month,
                                'year': employee_ts_import.year,
                                'contract_name': row_dict['contract_name'],
                                'employee_id': employee.id,
                                'employee_name': row_dict['employee_name'],
                                'product_id': self.env['product.product'].search([('name', '=', row_dict['designation'])]).id,
                                'monthly_timesheet_id': monthly_timesheet.id,
                                'name': allowance,
                                'amount': row_dict[allowance]
                            })

                for row in ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
                    keys = ['contract_id', 'contract_name', 'employee_id', 'employee_name', 'designation',
                            'spent_hour', ]
                    row_dict = dict(zip(keys, row))
                    if row_dict['contract_id'] is not None:
                        pass
                    if row_dict['employee_id'] is not None:
                        employee = self.env['hr.employee'].search(
                            [('employee_sequence', '=', row_dict['employee_id'])], limit=1)
                        if not employee:
                            raise ValidationError(f"Employee  ID '{row_dict['employee_id']}' not found")
                    hour_prefix = ['o', 'O', 'e', 'E', 'j', 'J', 'r', 'R', 'h', 'H', 's', 'S']
                    for day in range(1, days_in_month + 1):
                        date_vals = date(int(employee_ts_import.year), int(employee_ts_import.month), day)
                        employee_hour_spent = self.env['employee.timesheet'].search(
                            [('employee_id', '=', employee.id),
                             ('date', '=', date_vals)]).mapped('hour_spent')
                        cell_value = row[day + 4]
                        if cell_value or cell_value == 0:
                            str_cell_value = str(cell_value)
                            hour_spent = ''.join(filter(lambda char: char.isdigit() or char == '.', str_cell_value))
                            hour_spent_by = ', '.join(re.findall(r'\b[a-zA-Z]\d+\b', str_cell_value))
                            if (sum(employee_hour_spent) + float(
                                    hour_spent)) <= self.env.user.company_id.max_working_hr_per_day:
                                if hour_spent_by:
                                    if hour_spent_by[0][0] in hour_prefix:
                                        if employee:
                                            timesheet_obj.create({
                                                'date': date_vals,
                                                'contract_name': row_dict['contract_name'],
                                                'employee_id': employee.id,
                                                'employee_name': row_dict['employee_name'],
                                                # 'designation': self.env['product.product'].search([('name', '=', row_dict['designation'])]).id,
                                                'product_id': self.env['product.product'].search([('name', '=', row_dict['designation'])]).id,
                                                'hour_spent': hour_spent,
                                                'hour_spent_by': str_cell_value,
                                                'monthly_timesheet_id': monthly_timesheet.id,
                                                'hour_prefix': hour_spent_by[0][0].lower()
                                            })
                                    else:
                                        raise ValidationError(
                                            "You can't import this excel because timesheet hour spent prefix "
                                            "only contains [o,e,j,r,h,s]")

                                else:
                                    if employee:
                                        timesheet_obj.create({
                                            'date': date_vals,
                                            'contract_name': row_dict['contract_name'],
                                            'employee_id': employee.id,
                                            'employee_name': row_dict['employee_name'],
                                            # 'designation': row_dict['designation'],
                                            'product_id': self.env['product.product'].search(
                                                [('name', '=', row_dict['designation'])]).id,
                                            'hour_spent': hour_spent,
                                            'hour_spent_by': str_cell_value,
                                            'monthly_timesheet_id': monthly_timesheet.id
                                        })
                            else:
                                raise ValidationError(
                                    "%s hour spent exceed %s Hours in %s. Please change hours." % (
                                        employee.employee_sequence,
                                        self.env.user.company_id.max_working_hr_per_day, date_vals)
                                )
        except Exception as e:
            raise ValidationError(f"An error occurred: {str(e)}")

    def import_confirm_cafu(self):
        try:
            active_id = self.env.context.get('active_id')
            employee_ts_import = self.env['employee.timesheet.import'].browse(active_id)
            monthly_timesheet = self.env['monthly.timesheet'].search([('year', '=', employee_ts_import.year),
                                                                      ('month', '=', employee_ts_import.month),
                                                                      ('location_id', '=',
                                                                       employee_ts_import.location_id.id)])
            wb = openpyxl.load_workbook(
                filename=BytesIO(base64.b64decode(employee_ts_import.excel_file)), read_only=True
            )
            ws = wb.active
            days_in_month = calendar.monthrange(int(monthly_timesheet.year), int(monthly_timesheet.month))[1]
            timesheet_obj = self.env['employee.timesheet']
            designations = []
            designation_product = self.env['product.product'].search([]).mapped(
                'name')
            for row in ws.iter_rows(min_row=2, max_row=None, min_col=6, max_col=6, values_only=True):
                designations.append(row[0])

            designation = []
            contract_designations = self.env['contract.contracts'].search(
                [('contract_id', '=', self.contract_id.id)]).mapped('contract_lines_ids')
            contracts = []
            validate_contracts = []
            for contract in contract_designations:
                contracts.append(contract.product_id.name)

            for rec in designations:
                if rec not in designation_product and rec not in designation and rec is not None:
                    designation.append(rec)

                if rec not in contracts and rec is not None:
                    validate_contracts.append(rec)
            if validate_contracts:
                raise ValidationError(
                    'Imported Excel containing %s designations. These designations are '
                    'not in the Contracts.Please configure and import again' % (validate_contracts))
            if designation:
                raise ValidationError('Imported Excel containing %s designations. These designations are '
                                      'not in the system.Please configure and import again' % (designation))

            self.env['employee.timesheet'].search([('monthly_timesheet_id', '=', monthly_timesheet.id)]).unlink()
            self.env['employee.allowance'].search([('monthly_timesheet_id', '=', monthly_timesheet.id)]).unlink()
            allowance_heads = []
            for row in ws.iter_rows(min_row=1, max_row=1, min_col=None, max_col=None, values_only=True):
                allowance_heads.extend(
                    ['cafu_id', 'contract_id', 'contract_name', 'employee_id', 'employee_name', 'designation'])
                for day in range(1, days_in_month + 1):
                    allowance_heads.append(day)
                for rec in row[days_in_month + 6:]:
                    allowance_heads.append(rec)
                break
            for row in ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
                keys = allowance_heads
                row_dict = dict(zip(keys, row))
                if row_dict['employee_id']:
                    employee = self.env['hr.employee'].search(
                        [('employee_sequence', '=', row_dict['employee_id'])], limit=1)
                    if not employee:
                        raise ValidationError(f"Employee  ID '{row_dict['employee_id']}' not found")

                    allowances = [allowance for allowance in list(row_dict)[days_in_month + 6:]]
                    allowance_types = self.env['allowance.type'].search([]).mapped('name')
                    contract_info = self.env['contract.lines'].search(
                        [('contract_id', '=', employee_ts_import.customer_contract_id.id)])
                    timesheet_allowance = []
                    contract_allowances = []
                    for al in allowances:
                        if al not in allowance_types:
                            timesheet_allowance.append(al)
                        if float(row_dict[al]) >= contract_info.mapped('contract_payroll_line_ids').filtered(
                                lambda x: x.allowance_type_id.name == al).max_amount:
                            contract_allowances.append(al)
                    if timesheet_allowance:
                        raise ValidationError(
                            '%s allowances are not in Allowance Master, Need to create this allowances' % (
                                timesheet_allowance))

                    if contract_allowances:
                        raise ValidationError(
                            '%s These allowances amount must be less than the amount configured in customer contracts' % (
                                contract_allowances))

                    for allowance in list(row_dict)[days_in_month + 6:]:
                        self.env['employee.allowance'].create({
                            'month': employee_ts_import.month,
                            'year': employee_ts_import.year,
                            'contract_name': row_dict['contract_name'],
                            'employee_id': employee.id,
                            'employee_name': row_dict['employee_name'],
                            'product_id': self.env['product.product'].search(
                                [('name', '=', row_dict['designation'])]).id,
                            'monthly_timesheet_id': monthly_timesheet.id,
                            'name': allowance,
                            'amount': row_dict[allowance]
                        })

            for row in ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
                keys = ['cafu_id', 'contract_id', 'contract_name', 'employee_id', 'employee_name', 'designation',
                        'spent_hour', ]
                row_dict = dict(zip(keys, row))
                if row_dict['contract_id'] is not None:
                    pass
                if row_dict['employee_id'] is not None:
                    employee = self.env['hr.employee'].search(
                        [('employee_sequence', '=', row_dict['employee_id'])], limit=1)
                    if not employee:
                        raise ValidationError(f"Employee  ID '{row_dict['employee_id']}' not found")
                hour_prefix = ['o', 'O', 'e', 'E', 'j', 'J', 'r', 'R', 'h', 'H', 's', 'S']
                for day in range(1, days_in_month + 1):
                    date_vals = date(int(employee_ts_import.year), int(employee_ts_import.month), day)
                    employee_hour_spent = self.env['employee.timesheet'].search(
                        [('employee_id', '=', employee.id),
                         ('date', '=', date_vals)]).mapped('hour_spent')
                    cell_value = row[day + 5]
                    if cell_value or cell_value == 0:
                        str_cell_value = str(cell_value)
                        hour_spent = ''.join(filter(lambda char: char.isdigit() or char == '.', str_cell_value))
                        hour_spent_by = ', '.join(re.findall(r'\b[a-zA-Z]\d+\b', str_cell_value))
                        if (sum(employee_hour_spent) + float(
                                hour_spent)) <= self.env.user.company_id.max_working_hr_per_day:
                            if hour_spent_by:
                                if hour_spent_by[0][0] in hour_prefix:
                                    if employee:
                                        timesheet_obj.create({
                                            'date': date_vals,
                                            'contract_name': row_dict['contract_name'],
                                            'employee_id': employee.id,
                                            'employee_name': row_dict['employee_name'],
                                            # 'designation': self.env['product.product'].search([('name', '=', row_dict['designation'])]).id,
                                            'product_id': self.env['product.product'].search(
                                                [('name', '=', row_dict['designation'])]).id,
                                            'hour_spent': hour_spent,
                                            'hour_spent_by': str_cell_value,
                                            'monthly_timesheet_id': monthly_timesheet.id,
                                            'hour_prefix': hour_spent_by[0][0].lower(),
                                            'cafu_id': row_dict['cafu_id'] if row_dict['cafu_id'] else ''
                                        })
                                else:
                                    raise ValidationError(
                                        "You can't import this excel because timesheet hour spent prefix "
                                        "only contains [o,e,j,r,h,s]")

                            else:
                                if employee:
                                    timesheet_obj.create({
                                        'date': date_vals,
                                        'contract_name': row_dict['contract_name'],
                                        'employee_id': employee.id,
                                        'employee_name': row_dict['employee_name'],
                                        # 'designation': row_dict['designation'],
                                        'product_id': self.env['product.product'].search(
                                            [('name', '=', row_dict['designation'])]).id,
                                        'hour_spent': hour_spent,
                                        'hour_spent_by': str_cell_value,
                                        'monthly_timesheet_id': monthly_timesheet.id,
                                        'cafu_id': row_dict['cafu_id'] if row_dict['cafu_id'] else ''
                                    })
                        else:
                            raise ValidationError(
                                "%s hour spent exceed %s Hours in %s. Please change hours." % (
                                    employee.employee_sequence,
                                    self.env.user.company_id.max_working_hr_per_day, date_vals)
                            )

            monthly_timesheet.is_cafu_timesheet = True
        except Exception as e:
            raise ValidationError(f"An error occurred: {str(e)}")