from odoo import http
from odoo.http import request
from datetime import date, timedelta
import calendar
import json
from odoo.exceptions import ValidationError


class Main(http.Controller):
    def return_employee_data(self, timesheet_id):
        employee_timesheet = request.env['employee.timesheet'].search([('monthly_timesheet_id', '=',
                                                                        int(timesheet_id))])
        monthly_timesheet = request.env['monthly.timesheet'].search([('id', '=', int(timesheet_id))])
        employee_dict = {employee.employee_id.id: [] for employee in employee_timesheet}
        month = int(monthly_timesheet.month)
        year = int(monthly_timesheet.year)
        number_of_days = calendar.monthrange(year, month)[1]
        first_date = date(year, month, 1)
        last_date = date(year, month, number_of_days)
        delta = last_date - first_date
        date_list = []
        for i in range(delta.days + 1):
            day = first_date + timedelta(days=i)
            date_list.append(day)
        for emp in employee_timesheet:
            key = emp.employee_id.id
            employee_dict[key].append((emp.hour_spent_by, emp.id))
        return employee_dict

    def get_dates(self, month, year):
        number_of_days = calendar.monthrange(year, month)[1]
        first_date = date(year, month, 1)
        last_date = date(year, month, number_of_days)
        delta = last_date - first_date
        all_dates = [(first_date + timedelta(days=i)).strftime('%b %d') for i in range(delta.days + 1)]
        return all_dates

    def remove_prefix(self, text):
        # Check if the string is empty
        if not text:
            return None
        for char in text:
            if char.isdigit():
                # Return the substring starting from the first digit
                return text[text.index(char):]

    @http.route('/emp_timesheet/<string:serial>', type='http', auth='public')
    def index(self, serial, **kw):
        obj = request.env['monthly.timesheet'].sudo().search([('id', '=', int(serial))])
        employees = request.env['hr.employee'].sudo().search([])
        all_dates = self.get_dates(int(obj.month), int(obj.year))
        designations = request.env['product.product'].sudo().search([])
        return request.render("employee_timesheet.timesheet_view_id", {'timesheet_lines': self.return_employee_data(serial),
                                                                  'employees': employees, 'all_dates': all_dates,
                                                                  'project_id': obj, 'designation': designations})

    @http.route('/submit_form', type='http', auth='public', csrf=False, methods=['POST'])
    def submit_data(self):
        data = json.loads(request.httprequest.data)
        for timesheet in data:
            for key, value in timesheet.items():
                if key == 'editedData':
                    hour_prefix = ['o', 'O', 'e', 'E', 'j', 'J', 'r', 'R', 'h', 'H', 's', 'S']
                    edited_timesheet_entry = value[5:-1]
                    for time in edited_timesheet_entry:
                        employee_timesheet = request.env['employee.timesheet'].browse(int(time.split('-')[1]))
                        employee_timesheet.write({'hour_spent_by': time.split('-')[0]})
                        if employee_timesheet.hour_spent_by[0] in hour_prefix:
                            employee_timesheet.write({'hour_prefix': employee_timesheet.hour_spent_by[0].lower(),
                                                      'hour_spent': self.remove_prefix(time.split('-')[0])})
                        else:
                            employee_timesheet.write({'hour_spent': float(time.split('-')[0])})
                else:
                    month = value[3]
                    year = value[2]
                    monthly_timesheet_rec = request.env['monthly.timesheet'].search(
                        [('year', '=', int(year)), ('month', '=', int(month))], limit=1)
                    employee_timesheet = request.env['employee.timesheet']

                    number_of_days = calendar.monthrange(int(year), int(month))[1]
                    first_date = date(int(year), int(month), 1)
                    last_date = date(int(year), int(month), number_of_days)
                    delta = last_date - first_date
                    all_dates = [(first_date + timedelta(days=i)) for i in range(delta.days + 1)]
                    timesheet_entry = value[5:]
                    zip_time_entry = zip(all_dates, timesheet_entry)
                    for rec in zip_time_entry:
                        if value[0]:
                            if int(value[0]):
                                data = {
                                    'employee_id': int(value[0]),
                                    'product_id': request.env['product.product'].sudo().search([('id', '=',int(value[1]))], limit=1).id,
                                    'monthly_timesheet_id': monthly_timesheet_rec.id,
                                    'date': rec[0],
                                    'hour_spent_by': rec[1] if rec[1] else 0,
                                }
                                employee_timesheet.create(data)
