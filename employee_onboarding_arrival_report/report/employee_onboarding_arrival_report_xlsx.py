import base64
import io
from odoo import models, api
from datetime import datetime, timedelta
from operator import itemgetter
import re
import xlsxwriter
import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class EmployeeOnboardingArrivalReportXlsx(models.AbstractModel):
    _name = 'report.employee_onboarding_arrival_report.report_arrival_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, complaints):
        user = self.env.user
        user_tz = self.env.user.tz or pytz.utc
        services = []
        search_condition = []
        local = pytz.timezone(user_tz)

        if data:
            if data['form'][0]['from_date']:
                search_condition.append(('create_date', '>=', data['form'][0]['from_date']), )
            if data['form'][0]['to_date']:
                search_condition.append(('create_date', '<=', data['form'][0]['to_date']), )
            if data['form'][0].get('airport_ids'):
                search_condition.append(('airport_destination_id', '=', data['form'][0]['airport_ids']))
            if data['form'][0].get('terminal_ids'):
                search_condition.append(('terminal_id', '=', data['form'][0]['terminal_ids']))

        arrival_report = self.env['employee.onboarding'].search(search_condition)
        sheet = workbook.add_worksheet('Employee Arrival Report')
        merge_format = workbook.add_format({
            'bold': True,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Arial',
        })
        merge_format.set_font_size(15)
        bold = workbook.add_format({'bold': True})
        row = 0
        column = 0
        sheet.set_column(column, column, 15)
        sheet.set_column(column + 2, column + 2, 15)
        sheet.set_column(column + 4, column + 4, 15)
        sheet.set_column(column + 6, column + 6, 15)
        sheet.merge_range('A1:I1', 'Employee Arrival Report', merge_format)

        row += 1
        sheet.write(row, column + 0, 'Employee ID', bold)
        sheet.set_column(column + 1, column + 1, 15)
        sheet.write(row, column + 1, 'Name', bold)
        sheet.set_column(column + 2, column + 2, 15)
        sheet.write(row, column + 2, 'Airport', bold)
        sheet.set_column(column + 3, column + 3, 15)
        sheet.write(row, column + 3, 'Date of Departure', bold)
        sheet.set_column(column + 4, column + 4, 15)
        sheet.write(row, column + 4, 'Terminal', bold)
        sheet.set_column(column + 5, column + 5, 15)
        sheet.write(row, column + 5, 'Date of Arrival', bold)

        for record in arrival_report:
            if record.stage == "scheduled_arrivals":
                row += 1
                sheet.write(row, column + 0, record.employee_id.id if record.employee_id else '')
                sheet.write(row, column + 1, record.employee_id.name if record.employee_id else '')
                sheet.write(row, column + 2,
                            record.airport_destination_id.name if record.airport_destination_id.name else '')
                sheet.write(row, column + 3, record.date_of_journey.strftime(
                    '%d/%m/%Y') if record.date_of_journey else '')
                sheet.write(row, column + 4, record.terminal_id.name if record.terminal_id.name else '')
                sheet.write(row, column + 5, record.date_time_of_landing.strftime(
                    '%d/%m/%Y %H:%M:%S') if record.date_time_of_landing else '')
