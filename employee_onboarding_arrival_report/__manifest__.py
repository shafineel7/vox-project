{
    'name': 'Employee Onboarding Arrival Report',
    'version': '17.0.1.0.0',
    'summary': 'Employee Onboarding Scheduled Arrival Report',
    'description': """
    This module provides reports on the scheduled arrivals of employees and allows to filter and generate reports 
    in Xlsx format based on various criteria such as date-range, destination.
    """,
    'author': 'Aswin',
    'website': 'https://www.voxtronme.com',
    'depends': ['base', 'employee_onboarding', 'report_xlsx'],
    'data': [
        "security/ir.model.access.csv",
        "wizards/employee_arrival_wizard.xml",
        "views/employee_onboarding_menus.xml",
    ],
    'license': 'LGPL-3',
    'installable': True,
    'application': True,
    'auto_install': False,

}
