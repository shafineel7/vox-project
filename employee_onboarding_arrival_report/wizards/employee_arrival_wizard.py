from odoo import fields, models


class EmployeeArrival(models.TransientModel):
    _name = "employee.arrival"

    from_date = fields.Datetime(string="From")
    to_date = fields.Datetime(string="To")
    airport_ids = fields.Many2many("airport.destination", string="Airport")
    terminal_ids = fields.Many2many("terminal.terminal", string="Terminal")

    def print_xlsx(self):
        self.ensure_one()
        data = self.read()
        data = {
            'form': data,
        }
        return self.env.ref(
            'employee_onboarding_arrival_report.action_employee_arrival_report_xlsx').sudo().report_action(self,
                                                                                                           data=data)
