# -*- coding: utf-8 -*-
{
    'name': 'Employee Self Service',
    'version': '17.0.7.3.5',
    'category': 'Human Resources',
    'author': 'Anoopa paul, Muhammed Aslam',
    'summary': 'Employee Self Service module for passport custody management',
    'description': """
        Employee self-service management refers to the system and processes in place within an
        organization that allow employees to handle various administrative tasks and access information
        related to their employment autonomously.
        including Loan,Salary Certificate,NOC for consulate,Letter for Bank loan,Employee certificate,
        Passport Release,Compensatory off entitlement,Exit and Salary revision
    """,
    'depends': ['employee', 'mail', 'department_groups', 'reject_reason', 'account_accountant', 'employee_timesheet',
                'document_layout'],
    'external_dependencies': {
        'python': ['docxtpl'],
    },
    'data': [
        'security/employee_self_service_security.xml',
        'security/ir.model.access.csv',
        'data/employee_self_service_data.xml',
        'data/mail_template_data.xml',
        'data/ir_cron_data.xml',
        'report/employee_self_service_header_footer_templates.xml',
        'report/emp_self_ser_sal_cer_templates.xml',
        'report/emp_self_ser_sal_rev_templates.xml',
        'report/emp_self_ser_sal_rev_hr_action_form_templates.xml',
        'report/emp_self_ser_emp_cer_templates.xml',
        'report/emp_self_ser_letter_bank_loan_templates.xml',
        'report/emp_self_ser_noc_consulate_templates.xml',
        'report/employee_self_service_report.xml',
        'views/passport_custody_views.xml',
        'views/reject_reason_views.xml',
        'views/employee_views.xml',
        'views/emp_self_ser_loan_views.xml',
        'views/emp_self_ser_sal_cer_views.xml',
        'views/emp_self_ser_noc_consulate_views.xml',
        'views/emp_self_ser_letter_bank_loan_views.xml',
        'views/emp_self_ser_emp_cer_views.xml',
        'views/passport_rel_req_views.xml',
        'views/emp_self_ser_sal_rev_views.xml',
        'views/res_config_settings_views.xml',
        'views/employee_self_service_menus.xml',
        'wizard/aramex_warning_views.xml',
        'wizard/jafza_warning_views.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
