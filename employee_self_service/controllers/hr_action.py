# -*- coding: utf-8 -*-

from odoo import http
from odoo.http import request
import base64


class DownloadHrAction(http.Controller):
    @http.route('/download_cp/docx/<int:attachment_id>', type='http', auth="public")
    def download_docx_attachment(self, attachment_id, **kwargs):
        attachment = request.env['ir.attachment'].sudo().browse(attachment_id)

        if attachment and attachment.type == 'binary':
            file_content = base64.b64decode(attachment.datas)

            headers = [
                ('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
                ('Content-Disposition', f'attachment; filename="{attachment.name}"'),
            ]

            return request.make_response(file_content, headers)
        else:
            request.not_found()

class DownloadAramex(http.Controller):
    @http.route('/download_nfa/docx/<int:attachment_id>', type='http', auth="public")
    def download_docx_attachment(self, attachment_id, **kwargs):
        attachment = request.env['ir.attachment'].sudo().browse(attachment_id)

        if attachment and attachment.type == 'binary':
            file_content = base64.b64decode(attachment.datas)

            headers = [
                ('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
                ('Content-Disposition', f'attachment; filename="{attachment.name}"'),
            ]

            return request.make_response(file_content, headers)
        else:
            request.not_found()


class DownloadJafza(http.Controller):
    @http.route('/download_nfj/docx/<int:attachment_id>', type='http', auth="public")
    def download_docx_attachment(self, attachment_id, **kwargs):
        attachment = request.env['ir.attachment'].sudo().browse(attachment_id)

        if attachment and attachment.type == 'binary':
            file_content = base64.b64decode(attachment.datas)

            headers = [
                ('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
                ('Content-Disposition', f'attachment; filename="{attachment.name}"'),
            ]

            return request.make_response(file_content, headers)
        else:
            request.not_found()
