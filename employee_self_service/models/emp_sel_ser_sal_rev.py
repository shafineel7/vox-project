# -*- coding: utf-8 -*-
from odoo import models, fields, api, Command, _
from odoo.exceptions import ValidationError
from docxtpl import DocxTemplate
from datetime import datetime
import base64
import os
import xlsxwriter
from io import BytesIO


class EmployeeSelfServiceSalaryRevision(models.Model):
    _name = 'emp.self.ser.sal.rev'
    _description = 'Employee Self Service Salary Revision'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _get_default_groups(self):
        hr_asst = self.env.ref('department_groups.group_hr_asst').id
        return [Command.link(hr_asst)]

    name = fields.Char(string='Name', readonly=True, default='New', copy=False)
    employee_id = fields.Many2one('hr.employee', string='Employee ID', required=True)
    employee_name = fields.Char(string='Employee Name')
    designation_id = fields.Many2one('designation', string='Designation')
    salary_amount = fields.Float(string="Current Amount")
    suggest_designation_id = fields.Many2one('designation', string='Suggested Designation')
    rev_salary = fields.Float(string="Revised Salary")
    remarks = fields.Char(string="Remarks", tracking=True)
    effective_date = fields.Date(string="Effective From")
    status = fields.Selection([('new', 'New'),
                               ('hr_mgr_approval', 'HR Manager Approval'),
                               ('finance_mgr_approval', 'Finance Manager Approval'),
                               ('hr_asst_approval', 'HR Assistant Approval'),
                               ('closed', 'Closed'), ('reject', 'Rejected')], default='new', tracking=True)
    type = fields.Selection([
        ('direct', 'Direct'),
        ('indirect', 'Indirect'),
    ], string='Type')

    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    approval_hr_id = fields.Many2one('res.users', string="Approval HR")
    approval_finance_id = fields.Many2one('res.users', string="Approval Finance Manager")
    approval_asst_hr_id = fields.Many2one('res.users', string="Approval Assistant HR")
    is_self_assign = fields.Boolean(string="Self Assign")
    reject_reason = fields.Char(string="Reject Reason")
    is_approve_hr_asst = fields.Boolean(compute='_compute_is_approve_hr_asst')
    salary_rev_doc = fields.Binary(string="Salary Revision Letter")
    filename = fields.Char(string="File name")
    hr_action_form_doc = fields.Binary(string="HR Action Form")
    hr_action_filename = fields.Char(string="HR Action Filename")
    # report information fields
    division = fields.Char(string="Division")
    site = fields.Char(string="Site")
    justification = fields.Char(string="Justification")
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id, store=True)
    amount_words = fields.Char(string="Total(In words):", compute='_compute_amount_words', store=True)

    @api.depends('rev_salary', 'currency_id')
    def _compute_amount_words(self):
        for rec in self:
            rec.amount_words = rec.currency_id.amount_to_text(rec.rev_salary).replace(',', '')

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('name') or vals['name'] == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('emp.self.ser.sal.rev') or _('New')
        return super().create(vals_list)

    def _compute_is_approve_hr_asst(self):
        for rec in self:
            if rec.approval_asst_hr_id.id == self._uid:
                rec.is_approve_hr_asst = True
            else:
                rec.is_approve_hr_asst = False

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for rec in self:
            if rec.employee_id:
                emp = self.env['hr.employee'].search([('employee_sequence', '=', rec.employee_id.employee_sequence)],
                                                     limit=1)
                if emp:
                    if emp.type == 'direct':
                        if rec.user_has_groups(
                                'department_groups.group_staff_operations_super,'
                                'department_groups.group_construction_operations_super,'
                                'department_groups.group_soft_ser_operations_super'):
                            hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                            hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                            rec.sudo().write(
                                {'employee_name': emp.name, 'designation_id': emp.designation_id.id, 'type': emp.type,
                                 'salary_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,
                                 'assigned_group_ids': [Command.link(group.id) for group in
                                                        hr_manager_group],
                                 'assigned_user_ids': [Command.link(user.id) for user in hr_manager_users]})
                        else:
                            raise ValidationError(
                                "You don't have access to create direct employees salary revision")
                    else:
                        raise ValidationError("Indirect employees salary revision not found")

                else:
                    raise ValidationError("Employee not found")

    @api.onchange('suggest_designation_id')
    def _onchange_suggest_designation_id(self):
        for rec in self:
            if rec.suggest_designation_id:
                rec.write({'rev_salary': rec.suggest_designation_id.gross_salary})

    def action_self_assign(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in hr_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_hr_id': rec.env.user.id})

            elif rec.env.user.has_group('department_groups.group_finance_mgr'):
                fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                fin_manager_users = self.env['res.users'].search([('groups_id', 'in', fin_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in fin_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_finance_id': rec.env.user.id})
            else:
                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in hr_asst_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_asst_hr_id': rec.env.user.id})

    def action_submit(self):
        for rec in self:
            if rec.type == 'direct':
                if rec.user_has_groups(
                        'department_groups.group_staff_operations_super,'
                        'department_groups.group_construction_operations_super,'
                        'department_groups.group_soft_ser_operations_super'):
                    rec.status = 'hr_mgr_approval'
                    hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                    hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                    rec.assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                    rec.assigned_group_ids = [Command.link(hr_manager_group.id)]
                    email_template = self.env.ref('employee_self_service.email_template_salary_revision',
                                                  raise_if_not_found=False)
                    hr_manager_emails = [manager.login for manager in hr_manager_users]
                    if hr_manager_emails:
                        email_values = {'email_to': ','.join(hr_manager_emails)}
                        email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                        return True
                    else:
                        return False
                else:
                    raise ValidationError(
                        "You don't have access to submit direct employee's salary revision request")

    def action_approve_hr_manager(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                rec.sudo().status = 'finance_mgr_approval'
                rec.sudo().is_self_assign = False
                fin_mgr_group = self.env.ref('department_groups.group_finance_mgr')
                fin_mgr_users = self.env['res.users'].search([('groups_id', 'in', fin_mgr_group.id)])
                rec.assigned_user_ids = [Command.link(user.id) for user in fin_mgr_users]
                rec.assigned_group_ids = [Command.link(fin_mgr_group.id)]
                email_template = self.env.ref('employee_self_service.email_template_salary_revision',
                                              raise_if_not_found=False)
                hr_asst_emails = [manager.login for manager in fin_mgr_users]
                if hr_asst_emails:
                    email_values = {'email_to': ','.join(hr_asst_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_finance_manager(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_finance_mgr'):
                rec.sudo().status = 'hr_asst_approval'
                rec.sudo().is_self_assign = False
                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                rec.assigned_user_ids = [Command.link(user.id) for user in hr_asst_users]
                rec.assigned_group_ids = [Command.link(hr_asst_group.id)]
                email_template = self.env.ref('employee_self_service.email_template_salary_revision',
                                              raise_if_not_found=False)
                hr_asst_emails = [manager.login for manager in hr_asst_users]
                if hr_asst_emails:
                    email_values = {'email_to': ','.join(hr_asst_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_hr_assistant(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_asst'):
                if not (rec.salary_rev_doc or rec.hr_action_form_doc):
                    raise ValidationError(
                        "Please upload salary revision file or HR action form")
                rec.sudo().status = 'closed'

    def action_rejected(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def print_salary_revision(self):
        return self.env.ref("employee_self_service.action_report_salary_revision_report").report_action(self)

    def print_hr_action_form(self):
        for rec in self:
            if not (rec.division and rec.site and rec.justification):
                raise ValidationError(
                    "Please site,division and justification before print hr action form")
        return self.env.ref("employee_self_service.action_report_hr_action_form_report").report_action(self)

    def generate_docx_file(self):
        module_path = os.path.dirname(os.path.abspath(__file__))
        template_path = os.path.join(module_path, '..', 'document_template', 'HRAction.docx')
        document = DocxTemplate(template_path)

        designation_allowances = {allowance.code: allowance for allowance in self.designation_id.allowance_type_ids
                                  if allowance.amount > 0}
        suggest_designation_allowances = {allowance.code: allowance
                                          for allowance in self.suggest_designation_id.allowance_type_ids
                                          if allowance.amount > 0}

        allowances_data = []
        all_codes = set(designation_allowances.keys()).union(set(suggest_designation_allowances.keys()))
        total_amt_1 = 0
        total_amt_2 = 0
        for code in all_codes:
            amt_1 = suggest_designation_allowances[code].amount if code in suggest_designation_allowances else 0
            amt_2 = designation_allowances[code].amount if code in designation_allowances else 0

            allowance = designation_allowances.get(code) or suggest_designation_allowances.get(code)
            allowance_type_name = allowance.allowance_type_id.name if allowance else ''
            allowance_code = allowance.code if allowance and allowance.code else ''


            row = {
                'all_type_name': allowance_type_name,
                'code': allowance_code,
                'amt_1': amt_1 if amt_1 != 0 else '',
                'amt_2': amt_2 if amt_2 != 0 else ''
            }
            allowances_data.append(row)
            total_amt_1 += amt_1
            total_amt_2 += amt_2

        division = self.division if self.division else ""
        site = self.site if self.site else ""
        remarks = self.remarks if self.remarks else ""
        date_of_joining = self.employee_id.date_of_joining if self.employee_id.date_of_joining else ''
        justification = self.justification if self.justification else ''

        context = {
            'date_join': date_of_joining,
            'sal_sugg': self.suggest_designation_id.wage,
            'sal_des': self.designation_id.wage,
            'sugg_gross': self.suggest_designation_id.gross_salary,
            'des_gross': self.designation_id.gross_salary,
            'allowances': allowances_data,
            'date': datetime.today().date(),
            'name': self.name,
            'emp': self.employee_name,
            'division': division,
            'emp_no': self.employee_id.employee_sequence,
            'site': site,
            'des': self.designation_id.name,
            'effective_date': self.effective_date,
            'justification': justification,
            'prep_by': self.env.user.name,
            'remarks': remarks,
            'amt_words': self.amount_words,
            'gross_sal_1': self.rev_salary,
            'gross_sal_2': self.salary_amount,
        }

        document.render(context)

        temp_dir = os.path.join(module_path, '..', 'hr_action_docx')
        os.makedirs(temp_dir, exist_ok=True)
        temp_file_path = os.path.join(temp_dir, f'{self.name}.docx')

        document.save(temp_file_path)

        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()

        docx_base64 = base64.b64encode(docx_content)
        attachment_name = f"{self.employee_id.employee_sequence} - HR Action Form.docx"

        attachment = self.env['ir.attachment'].sudo().create({
            'name': attachment_name,
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'emp.self.ser.sal.rev',
            'res_id': self.id,
            'store_fname': f"{self.display_name}.docx"
        })

        os.remove(temp_file_path)

        url = '/download_cp/docx/%d' % attachment.id

        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self'
        }

    def _salary_revision_reminder(self):
        group_operations_super = self.env.ref('department_groups.group_staff_operations_super')
        operations_super_users = self.env['res.users'].search([('groups_id', 'in', group_operations_super.id)])
        operations_super_emails = [user.login for user in operations_super_users]

        group_hr_mgr = self.env.ref('department_groups.group_hr_mgr')
        hr_mgr_users = self.env['res.users'].search([('groups_id', 'in', group_hr_mgr.id)])
        hr_mgr_emails = [user.login for user in hr_mgr_users]
        all_hr_emails = operations_super_emails + hr_mgr_emails

        objs = self.search([('status', '=', 'closed')])
        if objs:
            output = BytesIO()
            workbook = xlsxwriter.Workbook(output)
            sheet = workbook.add_worksheet('Salary Revisions')
            bold = workbook.add_format({'bold': True})

            headers = ['Request No', 'Employee ID', 'Employee Name', 'Previous Designation', 'Gross Salary',
                       'Revised Designation', 'Revised Gross Salary']

            column_widths = [15, 12, 20, 20, 15, 20, 20]
            for col_num, width in enumerate(column_widths):
                sheet.set_column(col_num, col_num, width)

            for col_num, header in enumerate(headers):
                sheet.write(0, col_num, header, bold)

            row = 1
            for obj in objs:
                sheet.write(row, 0, obj.name)
                sheet.write(row, 1, obj.employee_id.name)
                sheet.write(row, 2, obj.employee_name)
                sheet.write(row, 3, obj.designation_id.name)
                sheet.write(row, 4, obj.salary_amount)
                sheet.write(row, 5, obj.suggest_designation_id.name)
                sheet.write(row, 6, obj.rev_salary)
                row += 1

            workbook.close()
            output.seek(0)
            attachment_name = f"Salary Revisions For {datetime.today().strftime('%B')}.xlsx"
            attachment = self.env['ir.attachment'].create({
                'name': attachment_name,
                'type': 'binary',
                'datas': base64.b64encode(output.read()).decode('utf-8'),
                'res_model': 'emp.self.ser.sal.rev',
                'res_id': False,
                'mimetype': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            })

            mail_template = self.env.ref('employee_self_service.email_template_employee_salary_revision',
                                         raise_if_not_found=False)
            if mail_template:
                mail_template.attachment_ids = [(5, 0, 0)]
                mail_template.attachment_ids = [(4, attachment.id)]

                email_values = {'email_to': ','.join(all_hr_emails)}
                mail_template.with_context(email_values).send_mail(self.id, force_send=True)
                return True

        return False
