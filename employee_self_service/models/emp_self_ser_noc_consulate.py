# -*- coding: utf-8 -*-
import base64
import os

from docxtpl import DocxTemplate, RichText

from odoo import models, fields, api, Command, _
from odoo.exceptions import ValidationError


class EmployeeSelfServiceNocConsulate(models.Model):
    _name = 'emp.self.ser.noc.consulate'
    _description = 'Employee Self Service Noc for Consulate'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _get_default_groups(self):
        hr_asst = self.env.ref('department_groups.group_hr_asst').id
        return [Command.link(hr_asst)]

    name = fields.Char(string='Name', readonly=True, default='New', copy=False)
    employee_id = fields.Many2one('hr.employee', string='Employee ID', required=True)
    employee_name = fields.Char(string='Employee Name')
    designation_id = fields.Many2one('designation', string='Designation')
    salary_amount = fields.Float(string="Salary Amount")
    country_id = fields.Many2one('res.country', string="Nationality")
    passport_no = fields.Char(string="Passport Number")
    remarks = fields.Char(string="Remarks", tracking=True)
    visiting_place = fields.Char(string="Visiting Place", tracking=True)
    status = fields.Selection([('new', 'New'),
                               ('hr_mgr_approval', 'HR Manager Approval'),
                               ('hr_asst_approval', 'HR Assistant Approval'),
                               ('closed', 'Closed'), ('reject', 'Rejected')], default='new', tracking=True)
    type = fields.Selection([
        ('direct', 'Direct'),
        ('indirect', 'Indirect'),
    ], string='Type')

    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    approval_hr_id = fields.Many2one('res.users', string="Approval HR")
    approval_asst_hr_id = fields.Many2one('res.users', string="Approval Assistant HR")
    is_self_assign = fields.Boolean(string="Self Assign")
    reject_reason = fields.Char(string="Reject Reason")
    is_approve_hr_asst = fields.Boolean(compute='_compute_is_approve_hr_asst')
    noc_doc = fields.Binary(string="NOC Doc")
    filename_noc = fields.Char(string="File name NOC")
    jafza_doc = fields.Binary(string="Jafza Doc")
    filename_jafza = fields.Char(string="File name Jafza")
    aramex_doc = fields.Binary(string="Aramex Doc")
    filename_aramex = fields.Char(string="File name Aramex")
    # report information fields
    consulate = fields.Char(string="consulate")
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id, store=True)
    amount_words = fields.Char(string="Total(In words):", compute='_compute_amount_words', store=True)
    issue_status = fields.Selection([('yes', 'Yes'), ('no', 'No')], string="Issue Status")
    issue_date = fields.Datetime(string="Issue Date")

    @api.depends('salary_amount', 'currency_id')
    def _compute_amount_words(self):
        for rec in self:
            rec.amount_words = rec.currency_id.amount_to_text(rec.salary_amount).replace(',', '')

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('name') or vals['name'] == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('emp.self.ser.noc.consulate') or _('New')
        return super().create(vals_list)

    def _compute_is_approve_hr_asst(self):
        for rec in self:
            if rec.approval_asst_hr_id.id == self._uid:
                rec.is_approve_hr_asst = True
            else:
                rec.is_approve_hr_asst = False

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for rec in self:
            if rec.employee_id:
                emp = self.env['hr.employee'].search([('employee_sequence', '=', rec.employee_id.employee_sequence)],
                                                     limit=1)
                if emp:
                    if emp.type == 'direct':
                        if rec.user_has_groups(
                                'department_groups.group_construction_operations_coord,'
                                'department_groups.group_staff_operations_coord,'
                                'department_groups.group_soft_ser_operations_coord'):
                            op_coordinator_staff = self.env.ref('department_groups.group_staff_operations_coord')
                            op_coordinator_construction = self.env.ref(
                                'department_groups.group_construction_operations_coord')
                            op_coordinator_soft_ser = self.env.ref('department_groups.group_soft_ser_operations_coord')
                            hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                            op_coordinator_hr_asst_groups = self.env['res.groups'].browse(
                                (op_coordinator_staff.id, op_coordinator_construction.id, op_coordinator_soft_ser.id,
                                 hr_asst_group.id))
                            op_coordinator_asst_hr_users = self.env['res.users'].search(
                                [('groups_id', 'in', op_coordinator_hr_asst_groups.ids)])
                            rec.sudo().write(
                                {'employee_name': emp.name, 'designation_id': emp.designation_id.id, 'type': emp.type,
                                 'salary_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,
                                 'country_id': emp.country_id.id,
                                 'passport_no': emp.passport_id,
                                 'assigned_group_ids': [Command.link(group.id) for group in
                                                        op_coordinator_hr_asst_groups],
                                 'assigned_user_ids': [Command.link(user.id) for user in op_coordinator_asst_hr_users]})
                        else:
                            raise ValidationError(
                                "You don't have access to create direct employees noc for consulate")
                    else:
                        if self.env.user.has_group('department_groups.group_hr_asst'):
                            hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                            hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                            rec.write(
                                {'employee_name': emp.name, 'designation_id': emp.designation_id.id, 'type': emp.type,
                                 'salary_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,
                                 'country_id': emp.country_id.id,
                                 'passport_no': emp.passport_id,
                                 'assigned_user_ids': [Command.link(user.id) for user in hr_asst_users],
                                 'assigned_group_ids': [Command.link(hr_asst_group.id)]})

                        elif self.env.user.employee_id != emp:
                            raise ValidationError("Can't create another indirect employee's noc for consulate request")
                        else:
                            hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                            hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                            rec.write(
                                {'employee_name': emp.name, 'designation_id': emp.designation_id.id, 'type': emp.type,
                                 'salary_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,
                                 'country_id': emp.country_id.id,
                                 'passport_no': emp.passport_id,
                                 'assigned_user_ids': [Command.link(user.id) for user in hr_asst_users],
                                 'assigned_group_ids': [Command.link(hr_asst_group.id)]})
                else:
                    raise ValidationError("Employee not found")

    def action_self_assign(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in hr_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_hr_id': rec.env.user.id})
            else:
                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in hr_asst_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_asst_hr_id': rec.env.user.id})

    def action_submit(self):
        for rec in self:
            if rec.type == 'direct':
                if rec.user_has_groups(
                        'department_groups.group_construction_operations_coord,'
                        'department_groups.group_staff_operations_coord,'
                        'department_groups.group_soft_ser_operations_coord'):
                    rec.status = 'hr_mgr_approval'
                    hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                    hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                    rec.assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                    rec.assigned_group_ids = [Command.link(hr_manager_group.id)]
                    email_template = self.env.ref('employee_self_service.email_template_noc_consulate',
                                                  raise_if_not_found=False)
                    hr_manager_emails = [manager.login for manager in hr_manager_users]
                    if hr_manager_emails:
                        email_values = {'email_to': ','.join(hr_manager_emails)}
                        email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                        return True
                    else:
                        return False
                else:
                    raise ValidationError(
                        "You don't have access to submit direct employee's salary certificate request")

            else:
                rec.status = 'hr_mgr_approval'
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                rec.assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                rec.assigned_group_ids = [Command.link(hr_manager_group.id)]
                email_template = self.env.ref('employee_self_service.email_template_noc_consulate',
                                              raise_if_not_found=False)
                hr_manager_emails = [manager.login for manager in hr_manager_users]
                if hr_manager_emails:
                    email_values = {'email_to': ','.join(hr_manager_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_hr_manager(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                rec.sudo().status = 'hr_asst_approval'
                rec.sudo().is_self_assign = False
                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                rec.assigned_user_ids = [Command.link(user.id) for user in hr_asst_users]
                rec.assigned_group_ids = [Command.link(hr_asst_group.id)]
                email_template = self.env.ref('employee_self_service.email_template_noc_consulate',
                                              raise_if_not_found=False)
                hr_asst_emails = [manager.login for manager in hr_asst_users]
                if hr_asst_emails:
                    email_values = {'email_to': ','.join(hr_asst_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_hr_assistant(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_asst'):
                if not (rec.noc_doc or rec.jafza_doc or rec.aramex_doc):
                    raise ValidationError(
                        "Please upload doc file")
                if rec.issue_status != 'yes':
                    raise ValidationError(
                        "Please change issue status to yes")
                rec.sudo().issue_date = fields.Datetime.now()
                rec.sudo().status = 'closed'

    def action_rejected(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def print_noc_consulate(self):
        return self.env.ref("employee_self_service.action_report_noc_consulate_report").report_action(self)

    def print_noc_jafza(self):
        return self.env.ref("employee_self_service.action_report_noc_jafza_report").report_action(self)

    def print_noc_aramex(self):
        return self.env.ref("employee_self_service.action_report_noc_aramex_report").report_action(self)

    @api.onchange('noc_doc', 'jafza_doc', 'aramex_doc')
    def _onchange_noc_docs(self):
        for rec in self:
            if rec.noc_doc or rec.jafza_doc or rec.aramex_doc:
                if not rec.env.user.has_group('department_groups.group_hr_asst'):
                    raise ValidationError(
                        "You don't have access to upload employee's noc docs")

    def action_generate_aramex(self):
        active_ids = self.env.context.get('active_ids', [])
        records = self.browse(active_ids)
        for rec in records:
            if rec.status != 'closed':
                return {
                    'name': 'NOC For Aramex',
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'noc.aramex.warning',
                    'target': 'new',
                    'res_id': self.id,
                }
        return self.generate_aramex_docx_file()

    def action_generate_jafza(self):
        active_ids = self.env.context.get('active_ids', [])
        records = self.browse(active_ids)
        for rec in records:
            if rec.status != 'closed':
                return {
                    'name': 'NOC For Jafza',
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'noc.jafza.warning',
                    'target': 'new',
                    'res_id': self.id,
                }
        return self.generate_jafza_docx_file()

    def generate_aramex_docx_file(self):
        active_ids = self.env.context.get('active_ids', [])
        records = self.browse(active_ids)
        module_path = os.path.dirname(os.path.abspath(__file__))
        template_path = os.path.join(module_path, '..', 'document_template', 'cafu_noc_for_aramex.docx')
        document = DocxTemplate(template_path)
        company = self.env.company
        company_data = {
            'name': RichText(company.name, bold=True, font='Arial'),
            'street': company.street,
            'state': company.state_id.name,
            'country': company.country_id.name,
            'zip': company.zip,
        }

        aramex_record = self.search([
            ('status', '=', 'closed'),
            ('id', 'in', active_ids)
        ])
        context = {
            'company_data': company_data,
            'records': enumerate(aramex_record),
            'date': fields.Date.today(),
            'hr_manager': aramex_record.approval_asst_hr_id.name,
            'designation': ' '.join(aramex_record.assigned_group_ids.mapped('name'))
        }
        document.render(context, autoescape=True)

        temp_dir = os.path.join(module_path, '..', 'temp')
        os.makedirs(temp_dir, exist_ok=True)
        temp_file_path = os.path.join(temp_dir, f'{self.name}.docx')

        document.save(temp_file_path)

        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()

        docx_base64 = base64.b64encode(docx_content)
        attachment = self.env['ir.attachment'].sudo().create({
            'name': "NOC for Aramex.docx",
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'emp.self.ser.noc.consulate',
            'res_id': self.id,
            'store_fname': "NOC for Aramex.docx"
        })

        os.remove(temp_file_path)
        url = '/download_nfa/docx/%d' % attachment.id
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self'
        }

    def generate_jafza_docx_file(self):
        active_ids = self.env.context.get('active_ids', [])
        records = self.browse(active_ids)
        module_path = os.path.dirname(os.path.abspath(__file__))
        template_path = os.path.join(module_path, '..', 'document_template', 'noc_for_jafza.docx')
        document = DocxTemplate(template_path)
        company = self.env.company
        company_data = {
            'name': RichText(company.name, bold=True, font='Arial'),
            'street': company.street,
            'state': company.state_id.name,
            'country': company.country_id.name,
            'zip': company.zip,
        }

        jafza_record = self.search([('status', '=', 'closed'), ('id', 'in', active_ids)])
        context = {
            'company_data': company_data,
            'records': enumerate(jafza_record),
            'date': fields.Date.today(),
            'hr_manager': jafza_record.approval_asst_hr_id.name,
            'designation': ' '.join(jafza_record.assigned_group_ids.mapped('name'))
        }
        document.render(context, autoescape=True)

        temp_dir = os.path.join(module_path, '..', 'temp')
        os.makedirs(temp_dir, exist_ok=True)
        temp_file_path = os.path.join(temp_dir, f'{self.name}.docx')
        document.save(temp_file_path)

        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()

        docx_base64 = base64.b64encode(docx_content)
        attachment = self.env['ir.attachment'].sudo().create({
            'name': "NOC for Jafza.docx",
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'emp.self.ser.noc.consulate',
            'res_id': self.id,
            'store_fname': "NOC for Jafza.docx"
        })

        os.remove(temp_file_path)
        url = '/download_nfj/docx/%d' % attachment.id
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self'
        }
