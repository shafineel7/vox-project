# -*- coding: utf-8 -*-
from . import passport_custody
from . import employee
from . import emp_self_ser_loan
from . import emp_self_ser_sal_cer
from . import emp_self_ser_noc_consulate
from . import emp_self_ser_letter_bank_loan
from . import emp_self_ser_emp_cer
from . import passport_release_request
from . import emp_sel_ser_sal_rev
from . import res_config_settings
from . import loan_installment