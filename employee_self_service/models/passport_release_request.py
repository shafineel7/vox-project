# -*- coding: utf-8 -*-
from datetime import timedelta

from odoo import models, fields, api, Command, _
from odoo.exceptions import ValidationError


class PassportReleaseRequest(models.Model):
    _name = 'passport.rel.req'
    _description = 'Passport Release Request'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _get_default_groups(self):
        hr_asst = self.env.ref('department_groups.group_hr_asst').id
        return [Command.link(hr_asst)]

    name = fields.Char(string='Name', readonly=True, default='New', copy=False)
    employee_id = fields.Many2one('hr.employee', string='Employee ID', required=True)
    employee_name = fields.Char(string='Employee Name')
    dept_id = fields.Many2one('hr.department', string='Department')
    issue_date = fields.Date(string='Issue Date')
    reason = fields.Char(string='Reason for Request', required=True)
    planned_return = fields.Date(string='Planned Return', required=True)
    actual_return = fields.Date(string='Actual Return')
    passport_custody_status = fields.Selection([
        ('with_employee', 'With Employee'),
        ('in_custody', 'In Custody'),
    ], string='Passport Custody Status')
    dept_head_employee_ids = fields.Many2many('hr.employee', related='employee_id.department_id.department_head_ids')
    dept_head_user_ids = fields.Many2many('res.users', 'user_passport_rel', 'user_id', 'passport_rel_req_id',
                                          compute='_compute_dept_head_user_ids', store=True)
    asset_collection_status = fields.Selection([
        ('asset_collected', 'Asset Collected'),
        ('asset_released', 'Asset Released')
    ], string='Asset Collection Status', track_visibility='onchange')
    remarks = fields.Char(string="Remarks", tracking=True)
    status = fields.Selection([('new', 'New'),
                               ('operation_supervisor_approval', 'Operation Supervisor Approval'),
                               ('department_head_approval', 'Department Head Approval'),
                               ('waiting_for_hr_approval', 'HR Manager Approval'),
                               ('store_keeper_approval', 'Storekeeper Approval'),
                               ('assigned_to_hr_assistant', 'Assigned to HR Assistant'),
                               ('submitted_in_hr_assistant', 'Submitted in HR Assistant'),
                               ('issued_to_employee', 'Issued to Employee'),
                               ('closed', 'Closed'), ('reject', 'Rejected')], default='new', tracking=True)
    type = fields.Selection([
        ('direct', 'Direct'),
        ('indirect', 'Indirect'),
    ], string='Type')

    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    approval_dept_head_id = fields.Many2one('res.users', string="Approval Dept Head")
    approval_hr_id = fields.Many2one('res.users', string="Approval HR")
    approval_asst_hr_id = fields.Many2one('res.users', string="Approval Assistant HR")
    approval_op_super_id = fields.Many2one('res.users', string="Approval Operation Supervisor")
    approval_store_keep_id = fields.Many2one('res.users', string="Approval Storekeeper")

    is_self_assign = fields.Boolean(string="Self Assign")
    reject_reason = fields.Char(string="Reject Reason")
    is_approve_hr_asst = fields.Boolean(compute='_compute_is_approve_hr_asst')
    is_dept_head = fields.Boolean(compute='_compute_is_dept_head')

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('name') or vals['name'] == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('passport.rel.req') or _('New')
        return super().create(vals_list)

    def _compute_is_dept_head(self):
        for rec in self:
            if self._uid in rec.dept_head_user_ids.ids:
                rec.is_dept_head = True
            else:
                rec.is_dept_head = False

    def _compute_is_approve_hr_asst(self):
        for rec in self:
            if rec.approval_asst_hr_id.id == self._uid:
                rec.is_approve_hr_asst = True
            else:
                rec.is_approve_hr_asst = False

    @api.depends('employee_id', 'employee_id.department_id.department_head_ids')
    def _compute_dept_head_user_ids(self):
        user_list = []
        for rec in self:
            if rec.dept_head_employee_ids:
                for head in rec.dept_head_employee_ids:
                    if head.user_id:
                        user_list.append(head.user_id.id)
            rec.dept_head_user_ids = user_list

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for rec in self:
            if rec.employee_id:
                emp = self.env['hr.employee'].search([('employee_sequence', '=', rec.employee_id.employee_sequence)],
                                                     limit=1)
                if emp:
                    passport_release_req = self.search_count(
                        [('employee_id', '=', emp.id), ('status', 'not in', ('closed', 'reject'))])
                    if passport_release_req:
                        raise ValidationError("Multiple passport release request can't be create for same employee")
                    if emp.passport_custody_status != 'in_custody':
                        raise ValidationError('The Passport Custody Status must be "In Custody".')
                    else:
                        if emp.type == 'direct':
                            if rec.user_has_groups(
                                    'department_groups.group_hr_asst,'
                                    'department_groups.group_hr_welfare_officer,'
                                    'department_groups.group_construction_operations_coord,'
                                    'department_groups.group_staff_operations_coord,'
                                    'department_groups.group_soft_ser_operations_coord'):
                                op_coordinator_staff = self.env.ref('department_groups.group_staff_operations_coord')
                                op_coordinator_construction = self.env.ref(
                                    'department_groups.group_construction_operations_coord')
                                op_coordinator_soft_ser = self.env.ref(
                                    'department_groups.group_soft_ser_operations_coord')
                                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                                welfare_group = self.env.ref('department_groups.group_hr_welfare_officer')
                                direct_emp_groups = self.env['res.groups'].browse(
                                    (
                                        op_coordinator_staff.id, op_coordinator_construction.id,
                                        op_coordinator_soft_ser.id,
                                        hr_asst_group.id, welfare_group.id))
                                op_coordinator_asst_hr_users = self.env['res.users'].search(
                                    [('groups_id', 'in', direct_emp_groups.ids)])
                                rec.sudo().write(
                                    {'employee_name': emp.name, 'dept_id': emp.department_id.id, 'type': emp.type,
                                     'passport_custody_status': emp.passport_custody_status,
                                     'assigned_group_ids': [Command.link(group.id) for group in
                                                            direct_emp_groups],
                                     'assigned_user_ids': [Command.link(user.id) for user in
                                                           op_coordinator_asst_hr_users]})
                            else:
                                raise ValidationError(
                                    "You don't have access to create direct employees passport release request")
                        else:
                            if self.env.user.has_group('department_groups.group_hr_asst'):
                                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                                hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                                rec.sudo().write(
                                    {'employee_name': emp.name, 'dept_id': emp.department_id.id, 'type': emp.type,
                                     'passport_custody_status': emp.passport_custody_status,
                                     'assigned_group_ids': [Command.link(group.id) for group in
                                                            hr_asst_group],
                                     'assigned_user_ids': [Command.link(user.id) for user in hr_asst_users]})

                            elif self.env.user.employee_id != emp:
                                raise ValidationError(
                                    "Can't create another indirect employee's passport release request")
                            else:
                                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                                hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                                rec.sudo().write(
                                    {'employee_name': emp.name, 'dept_id': emp.department_id.id, 'type': emp.type,
                                     'passport_custody_status': emp.passport_custody_status,
                                     'assigned_group_ids': [Command.link(group.id) for group in
                                                            hr_asst_group],
                                     'assigned_user_ids': [Command.link(user.id) for user in hr_asst_users]})

                else:
                    raise ValidationError("Employee not found")

    def action_self_assign(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in hr_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_hr_id': rec.env.user.id})

            elif rec.user_has_groups(
                    'department_groups.group_staff_operations_super,'
                    'department_groups.group_construction_operations_super,'
                    'department_groups.group_soft_ser_operations_super'):

                construction_op_super = self.env.ref('department_groups.group_construction_operations_super')
                soft_ser_op_super = self.env.ref('department_groups.group_soft_ser_operations_super')
                staff_op_super = self.env.ref('department_groups.group_staff_operations_super')
                super_groups = self.env['res.groups'].browse(
                    (
                        construction_op_super.id, soft_ser_op_super.id,
                        staff_op_super.id))
                super_users = self.env['res.users'].search(
                    [('groups_id', 'in', super_groups.ids)])

                rec.assigned_user_ids = [Command.unlink(user.id) for user in super_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_op_super_id': rec.env.user.id})

            elif rec.env.user.has_group('department_groups.group_finance_store_keeper'):
                storekeeper_group = self.env.ref('department_groups.group_finance_store_keeper')
                storekeeper_users = self.env['res.users'].search([('groups_id', 'in', storekeeper_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in storekeeper_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_store_keep_id': rec.env.user.id})

            elif rec.env.user.has_group('department_groups.group_hr_asst'):
                rec.write({'is_self_assign': True, 'approval_asst_hr_id': rec.env.user.id})

            else:
                construction_dept_head = self.env.ref('department_groups.group_construction_dept_head')
                soft_ser_dept_head = self.env.ref('department_groups.group_soft_ser_dept_head')
                staff_dept_head = self.env.ref('department_groups.group_staff_dept_head')
                dept_head_groups = self.env['res.groups'].browse(
                    (
                        construction_dept_head.id, soft_ser_dept_head.id,
                        staff_dept_head.id))
                dept_head_users = self.env['res.users'].search(
                    [('groups_id', 'in', dept_head_groups.ids)])

                rec.assigned_user_ids = [Command.unlink(user.id) for user in dept_head_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_dept_head_id': rec.env.user.id})

    #
    def action_submit(self):
        for rec in self:
            if rec.type == 'direct':
                if rec.user_has_groups(
                        'department_groups.group_hr_asst,'
                        'department_groups.group_hr_welfare_officer,'
                        'department_groups.group_construction_operations_coord,'
                        'department_groups.group_staff_operations_coord,'
                        'department_groups.group_soft_ser_operations_coord'):
                    rec.status = 'operation_supervisor_approval'
                    construction_op_super = self.env.ref('department_groups.group_construction_operations_super')
                    soft_ser_op_super = self.env.ref('department_groups.group_soft_ser_operations_super')
                    staff_op_super = self.env.ref('department_groups.group_staff_operations_super')
                    super_users = self.env['res.users'].search(
                        [('groups_id', 'in',
                          [construction_op_super.id, soft_ser_op_super.id, staff_op_super.id,
                           ])])
                    super_groups = self.env['res.groups'].browse(
                        (
                            construction_op_super.id, soft_ser_op_super.id,
                            staff_op_super.id))
                    rec.assigned_user_ids = [Command.link(user.id) for user in super_users]
                    rec.assigned_group_ids = [Command.link(group.id) for group in super_groups]
                    email_template = self.env.ref('employee_self_service.email_template_passport_release_request',
                                                  raise_if_not_found=False)
                    super_emp_emails = [manager.login for manager in super_users]
                    if super_emp_emails:
                        email_values = {'email_to': ','.join(super_emp_emails)}
                        email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                        return True
                    else:
                        return False
                else:
                    raise ValidationError(
                        "You don't have access to submit direct employee's passport release request")

            else:
                rec.status = 'department_head_approval'
                construction_dept_head = self.env.ref('department_groups.group_construction_dept_head')
                soft_ser_dept_head = self.env.ref('department_groups.group_soft_ser_dept_head')
                staff_dept_head = self.env.ref('department_groups.group_staff_dept_head')
                dept_head_groups = self.env['res.groups'].browse(
                    (
                        construction_dept_head.id, soft_ser_dept_head.id,
                        staff_dept_head.id))
                dept_head_users = self.env['res.users'].search(
                    [('groups_id', 'in', dept_head_groups.ids)])
                rec.assigned_user_ids = [Command.link(user.id) for user in dept_head_users]
                rec.assigned_group_ids = [Command.link(group.id) for group in dept_head_groups]
                email_template = self.env.ref('employee_self_service.email_template_passport_release_request',
                                              raise_if_not_found=False)
                dept_head_emails = [manager.login for manager in dept_head_users]
                if dept_head_emails:
                    email_values = {'email_to': ','.join(dept_head_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_dept_head(self):
        for rec in self:
            rec.sudo().status = 'waiting_for_hr_approval'
            rec.sudo().is_self_assign = False
            hr_mgr_group = self.env.ref('department_groups.group_hr_mgr')
            hr_mgr_users = self.env['res.users'].search([('groups_id', 'in', hr_mgr_group.id)])
            rec.assigned_user_ids = [Command.link(user.id) for user in hr_mgr_users]
            rec.assigned_group_ids = [Command.link(hr_mgr_group.id)]
            email_template = self.env.ref('employee_self_service.email_template_passport_release_request',
                                          raise_if_not_found=False)
            hr_mgr_emails = [manager.login for manager in hr_mgr_users]
            if hr_mgr_emails:
                email_values = {'email_to': ','.join(hr_mgr_emails)}
                email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                return True
            else:
                return False

    def action_approve_op_super(self):
        for rec in self:
            if rec.user_has_groups(
                    'department_groups.group_staff_operations_super,'
                    'department_groups.group_construction_operations_super,'
                    'department_groups.group_staff_operations_super'):
                rec.sudo().status = 'store_keeper_approval'
                rec.sudo().is_self_assign = False
                storekeeper_group = self.env.ref('department_groups.group_finance_store_keeper')
                storekeeper_users = self.env['res.users'].search([('groups_id', 'in', storekeeper_group.id)])
                rec.sudo().assigned_user_ids = [Command.link(user.id) for user in storekeeper_users]
                rec.sudo().assigned_group_ids = [Command.link(storekeeper_group.id)]
                email_template = self.env.ref('employee_self_service.email_template_passport_release_request',
                                              raise_if_not_found=False)
                storekeeper_emails = [manager.login for manager in storekeeper_users]
                if storekeeper_emails:
                    email_values = {'email_to': ','.join(storekeeper_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_storekeeepr(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_finance_store_keeper'):
                rec.sudo().status = 'assigned_to_hr_assistant'
                rec.sudo().is_self_assign = False
                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                rec.assigned_user_ids = [Command.link(user.id) for user in hr_asst_users]
                rec.assigned_group_ids = [Command.link(hr_asst_group.id)]
                email_template = self.env.ref('employee_self_service.email_template_passport_release_request',
                                              raise_if_not_found=False)
                hr_asst_emails = [manager.login for manager in hr_asst_users]
                if hr_asst_emails:
                    email_values = {'email_to': ','.join(hr_asst_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_hr_manager(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                rec.sudo().status = 'assigned_to_hr_assistant'
                rec.sudo().is_self_assign = False
                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                rec.assigned_user_ids = [Command.link(user.id) for user in hr_asst_users]
                rec.assigned_group_ids = [Command.link(hr_asst_group.id)]
                email_template = self.env.ref('employee_self_service.email_template_passport_release_request',
                                              raise_if_not_found=False)
                hr_asst_emails = [manager.login for manager in hr_asst_users]
                if hr_asst_emails:
                    email_values = {'email_to': ','.join(hr_asst_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_rejected(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_issue_to_employee(self):
        for rec in self:
            if rec.status == 'assigned_to_hr_assistant':
                if rec.employee_id:
                    vals = {
                        'issue_date': fields.date.today(),
                        'status': 'issued_to_employee',
                        'passport_custody_status': 'with_employee',
                    }
                    rec.write(vals)
                    rec.employee_id.write({'reason': rec.reason, 'passport_custody_status': 'with_employee',
                                           'issue_date': fields.date.today(),
                                           'planned_return_date': rec.planned_return})
                    email_template = self.env.ref(
                        'employee_self_service.email_template_passport_issued_notification',
                        raise_if_not_found=False)
                    email_values = {'email_to': rec.employee_id.work_email}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_receive_passport(self):
        for record in self:
            if record.status == 'issued_to_employee':
                if record.asset_collection_status != 'asset_collected':
                    raise ValidationError('Please update the asset collection status')
                record.write({'status': 'closed', 'passport_custody_status': 'in_custody'})
                if record.employee_id:
                    record.employee_id.write({
                        'passport_custody_status': 'in_custody',
                        'actual_return_date': record.actual_return,
                    })
            else:
                raise ValidationError('Invalid status for receiving the passport.')

    @api.onchange('asset_collection_status')
    def _onchange_asset_collection_status(self):
        for rec in self:
            if rec.asset_collection_status == 'asset_collected' and rec.status == 'store_keeper_approval':
                raise ValidationError('Asset collection status should be asset released')

    def _passport_return_reminder(self):
        today = fields.Date.today()
        hr_group = self.env.ref('department_groups.group_hr_asst')
        group_hr_mgr = self.env.ref('department_groups.group_hr_mgr')

        hr_users = self.env['res.users'].search([('groups_id', 'in', hr_group.id)])
        hr_emails = [user.login for user in hr_users]

        hr_mgr_users = self.env['res.users'].search([('groups_id', 'in', group_hr_mgr.id)])
        hr_mgr_emails = [user.login for user in hr_mgr_users]
        all_hr_emails = hr_emails + hr_mgr_emails

        if not hr_emails:
            return False
        records_7_days = self.search(
            [('planned_return', '=', today + timedelta(days=7)), ('passport_custody_status', '=', 'with_employee')])
        records_60_days = self.search([('passport_custody_status', '=', 'with_employee')])

        emails_sent = False
        for rec in records_7_days:
            email_template = self.env.ref(
                'employee_self_service.email_hr_assistant_passport_return_notification',
                raise_if_not_found=False
            )
            if email_template:
                email_values = {'email_to': ','.join(hr_emails)}
                email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                emails_sent = True

        for rec in records_60_days:
            return_date = rec.planned_return + timedelta(days=60)
            if return_date == today:
                email_template_60_days = self.env.ref(
                    'employee_self_service.email_hr_assistant_passport_60_days_return_notification',
                    raise_if_not_found=False
                )
                if email_template_60_days:
                    email_values = {'email_to': ','.join(all_hr_emails)}
                    email_template_60_days.with_context(email_values).send_mail(rec.id, force_send=True)
                    emails_sent = True

        return emails_sent
