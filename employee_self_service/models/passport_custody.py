# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import ValidationError
from odoo.fields import Datetime


class PassportCustody(models.Model):
    _name = 'passport.custody'
    _description = 'Passport Custody Management'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    employee_id = fields.Many2one('hr.employee', string='Employee ID', required=True)
    name = fields.Char(string='Name', related='employee_id.name')
    passport_request_sequence = fields.Char(string='Reference', readonly=True, copy=False)
    department_id = fields.Many2one('hr.department', string='Department', related='employee_id.department_id')
    issue_date = fields.Date(string='Issue Date')
    reason = fields.Char(string='Reason for Request', required=True)
    planned_return = fields.Date(string='Planned Return', required=True)
    actual_return = fields.Date(string='Actual Return')
    user_id = fields.Many2one("res.users", default=lambda self: self.env.user, string="User")
    passport_custody_status = fields.Selection(string='Passport Custody Status', related='employee_id.passport_custody_status', readonly=True)
    dept_user_id = fields.Many2one("res.users", string="Dept Head User", )
    reject_reason = fields.Char(string='Reject Reason')
    reject_user_id = fields.Many2one('res.users', string='Rejected By', readonly=True)
    reject_date = fields.Datetime(string='Rejected On', readonly=True)
    hr_user_id = fields.Many2one("res.users", string="Hr User")
    status = fields.Selection([
        ('new', 'New'),
        ('operation_supervisor_approval', 'Operation Supervisor Approval'),
        ('department_head_approval', 'Department Head Approval'),
        ('waiting_for_hr_approval', 'Waiting for Hr Approval'),
        ('store_keeper_approval', 'Store Keeper Approval'),
        ('assigned_to_hr_assistant', 'Assigned to HR Assistant'),
        ('submitted_in_hr_assistant', 'Submitted in HR Assistant'),
        ('issued_to_employee', 'Issued to Employee'),
        ('closed', 'Closed'),
        ('rejected', 'Rejected'),
    ], string='Request Status', required=True, track_visibility='onchange', default='new', copy=False)
    type = fields.Selection(string='Type', related='employee_id.type')
    asset_collection_status = fields.Selection([
        ('asset_collected', 'Asset Collected'),
        ('asset_released', 'Asset Released')
    ], string='Asset Collection Status', track_visibility='onchange')
    is_department_head = fields.Boolean(string="Is Department Head", compute="_compute_is_department_head", store=True)
    is_button = fields.Boolean(default=False)

    @api.constrains('status', 'passport_custody_status')
    def _check_passport_custody_status(self):
        self.issue_date = Datetime.today()
        self.is_button = True
        for record in self:
            if record.status == 'new' and record.passport_custody_status != 'in_custody':
                raise exceptions.ValidationError('The Passport Custody Status must be "In Custody".')

    def action_submit_for_hr_approval(self):
        for record in self:
            if record.status == 'new' and record.type == 'indirect':
                record.status = 'waiting_for_hr_approval'

    def action_approve(self):
        for record in self:
            if record.type == 'indirect' and record.status == 'waiting_for_hr_approval':
                record.write({'status': 'assigned_to_hr_assistant'})
                # Send email for assigned to HR assistant
                template_id = self.env.ref('employee_self_service.email_hr_assistant_approval', raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(record.id, force_send=True)
                else:
                    raise exceptions.ValidationError('Email template not found. Cannot proceed.')

            elif record.type == 'direct' and record.status == 'operation_supervisor_approval':
                record.write({'status': 'department_head_approval'})
                # Send email for department head approval
                template_id = self.env.ref('employee_self_service.email_pending_approval', raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(record.id, force_send=True)
                else:
                    raise exceptions.ValidationError('Email template not found. Cannot proceed.')

            elif record.type == 'direct' and record.status == 'department_head_approval':
                if record.is_department_head:
                    record.write({'status': 'store_keeper_approval'})
                    # Send email for store keeper approval
                    template_id = self.env.ref('employee_self_service.email_pending_approval', raise_if_not_found=False)
                    if template_id:
                        template = self.env['mail.template'].browse(template_id.id)
                        template.send_mail(record.id, force_send=True)
                    else:
                        raise exceptions.ValidationError('Email template not found. Cannot proceed.')
                else:
                    raise exceptions.ValidationError('You are not authorized to approve this request.')

            elif record.type == 'direct' and record.status == 'store_keeper_approval':
                if not record.asset_collection_status:
                    raise exceptions.ValidationError(
                        'Asset collection status is required.')
                record.write({'status': 'assigned_to_hr_assistant'})
                # Send email for assigned to HR assistant
                template_id = self.env.ref('employee_self_service.email_hr_assistant_approval', raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(record.id, force_send=True)
                else:
                    raise exceptions.ValidationError('Email template not found. Cannot proceed.')

    def action_reject(self):
        for record in self:
            reject_reason = self.env.ref('employee_self_service.reject_reason_view_form')
            template_id = self.env.ref('employee_self_service.email_rejection_notification', raise_if_not_found=False)
            if template_id:
                template = self.env['mail.template'].browse(template_id.id)
                template.send_mail(record.id, force_send=True)
            else:
                raise ValidationError('Email template not found. Cannot proceed.')

            return {
                'name': 'Reject Reason',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'passport.custody',
                'views': [(reject_reason.id, 'form')],
                'view_id': reject_reason.id,
                'target': 'new',
                'res_id': self.id,
            }

    def action_reject_view(self):
        for record in self:
            record.write({
                'reject_reason': record.reject_reason,
                'reject_user_id': record.env.user.id,
                'reject_date': fields.datetime.today(),
                'status': 'rejected'
            })

    def action_submit_operation_supervisor(self):
        for record in self:
            if record.status == 'new' and record.type == 'direct':
                record.status = 'operation_supervisor_approval'

    def action_submit_department_head(self):
        for record in self:
            if record.status == 'department_head_approval':
                record.write({'status': 'store_keeper_approval'})

    def action_assign_to_hr_assistant(self):
        for record in self:
            if record.status == 'store_keeper_approval':
                record.status = 'assigned_to_hr_assistant'

    def action_receive_passport(self):
        for record in self:
            if record.status == 'issued_to_employee':
                if not record.actual_return:
                    raise exceptions.ValidationError('Actual return date is required before receiving the passport.')
                record.write({'status': 'closed'})
                if record.employee_id:
                    record.employee_id.write({
                        'passport_custody_status': 'in_custody',
                        'actual_return_date': record.actual_return
                    })
            else:
                raise exceptions.ValidationError('Invalid status for receiving the passport.')

    def action_issue_to_employee(self):
        for record in self:
            if record.status == 'assigned_to_hr_assistant':
                try:
                    record.status = 'issued_to_employee'
                    record.employee_id.passport_custody_status = 'with_employee'
                    if record.employee_id:
                        vals = {
                            'issue_date': record.issue_date,
                            'reason': record.reason,
                            'planned_return_date': record.planned_return
                        }
                        record.employee_id.write(vals)
                        template_id = self.env.ref('employee_self_service.email_passport_issued_notification',
                                                   raise_if_not_found=False)
                        if template_id:
                            template = self.env['mail.template'].browse(template_id.id)
                            template.send_mail(record.id, force_send=True)
                        else:
                            raise exceptions.ValidationError('Email template not found. Cannot proceed.')
                except Exception as e:
                    raise exceptions.ValidationError('An error occurred: %s' % str(e))

    @api.onchange('issue_date')
    def _onchange_issue_date(self):
        for record in self:
            if record.status == 'issued_to_employee':
                if record.employee_id:
                    record.employee_id.write({'issue_date': record.issue_date})

    @api.depends('employee_id')
    def _compute_is_department_head(self):
        for record in self:
            if record.employee_id and record.employee_id.department_id and record.employee_id.department_id.department_head_ids:
                record.is_department_head = True
            else:
                record.is_department_head = False

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('passport_request_sequence') or vals['passport_request_sequence'] == _('New'):
                vals['passport_request_sequence'] = self.env['ir.sequence'].next_by_code('passport_request_sequence') or _('New')
        return super().create(vals_list)
