# -*- coding: utf-8 -*-

from odoo import  fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    employee_loan_ac_id = fields.Many2one('account.account', string="Employee Loan Account",
                                          config_parameter='employee_self_service.employee_loan_ac_id')
    bank_ac_id = fields.Many2one('account.account', string="Bank Account", config_parameter='employee_self_service.bank_ac_id')
    bank_journal_id = fields.Many2one('account.journal', string="Bank Journal",
                                      config_parameter='employee_self_service.bank_journal_id')
