# -*- coding: utf-8 -*-

from odoo import fields, models


class Employee(models.Model):
    _inherit = "hr.employee"

    passport_rel_req_ids = fields.One2many('passport.rel.req', 'employee_id', string='Passport Custody')
    emp_self_ser_sal_rev_ids = fields.One2many('emp.self.ser.sal.rev', 'employee_id', string="Salary Revision")


