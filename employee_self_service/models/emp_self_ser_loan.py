# -*- coding: utf-8 -*-
from odoo import models, fields, api, Command, _
from odoo.exceptions import ValidationError
from dateutil.relativedelta import relativedelta
import calendar


class EmployeeSelfServiceLoan(models.Model):
    _name = 'emp.self.ser.loan'
    _description = 'Employee Self Service Loan'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _get_default_groups(self):
        hr_asst = self.env.ref('department_groups.group_hr_asst').id
        return [Command.link(hr_asst)]

    name = fields.Char(string='Name', readonly=True, default='New', copy=False)
    employee_name = fields.Char(string='Employee Name')
    employee_id = fields.Many2one('hr.employee', string='Employee ID', required=True)
    designation_id = fields.Many2one('designation', string='Designation')
    salary_amount = fields.Char(string="Salary Amount")
    req_loan_amount = fields.Float(string="Requested Amount", required=True, tracking=True)
    approve_loan_amount = fields.Float(string="Approved Amount", tracking=True)
    no_of_installments = fields.Integer(string="No.of Installments", tracking=True, default=6)
    remarks = fields.Char(string="Remarks", tracking=True)
    status = fields.Selection([('new', 'New'), ('hr_approval', 'HR Approval'),
                               ('finance_mgr_approval', 'Finance Manager Approval'), ('md_approval', 'MD Approval'),
                               ('accounts_approval', 'Accounts Approval'),
                               ('closed', 'Closed'), ('reject', 'Rejected')], default='new', tracking=True)
    type = fields.Selection([
        ('direct', 'Direct'),
        ('indirect', 'In Direct'),
    ], string='Type')
    approval_hr_id = fields.Many2one('res.users', string="Approval HR")
    approval_finance_id = fields.Many2one('res.users', string="Approval Finance")
    approval_md_id = fields.Many2one('res.users', string="Approval MD")
    approval_accounts_id = fields.Many2one('res.users', string="Approval Accounts")
    is_self_assign = fields.Boolean(string="Self Assign")
    reject_reason = fields.Char(string="Reject Reason")
    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    loan_installment_ids = fields.One2many('loan.installment', 'loan_id', string="Installments")
    move_id = fields.Many2one('account.move', string='Journal Entry')

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if vals.get('name', _('New')) == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('emp.self.ser.loan')
        return super().create(vals_list)

    @api.onchange('approve_loan_amount', 'no_of_installments')
    def _onchange_loan_details(self):
        for rec in self:
            if rec.employee_id and rec.approve_loan_amount or rec.no_of_installments != 6:
                if not rec.user_has_groups(
                        'department_groups.group_hr_asst,'
                        'department_groups.group_hr_mgr'):
                    raise ValidationError(
                        "You don't have access to update approved loan amount or no.of installments")
                else:
                    if rec.no_of_installments > 6:
                        raise ValidationError(
                            "Maximum no.of installments is limited to 6")
                    if rec.type == 'direct':
                        if not rec.approve_loan_amount <= 2 * rec.employee_id.designation_id.gross_salary:
                            raise ValidationError(
                                "Loan amount is greater than two months salary; can't be proceed")
                    else:
                        if not rec.approve_loan_amount <= rec.employee_id.designation_id.gross_salary:
                            raise ValidationError(
                                "Loan amount is greater than one month salary; can't be proceed")

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for rec in self:
            if rec.employee_id:
                emp = self.env['hr.employee'].search([('employee_sequence', '=', rec.employee_id.employee_sequence)],
                                                     limit=1)
                if emp:
                    if emp.type == 'indirect':
                        if emp and not emp.designation_id:
                            raise ValidationError('Please update the employee designation')
                        if emp and emp.designation_id and emp.designation_id.gross_salary == 0.0:
                            raise ValidationError('Please update the employee salary')
                        if rec.env.user.has_group('department_groups.group_hr_asst'):
                            rec.write(
                                {'employee_name': emp.name, 'designation_id': emp.designation_id.id, 'type': emp.type,
                                 'salary_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,
                                 'req_loan_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,})
                        else:
                            if rec.env.user.employee_id != emp:
                                raise ValidationError("Can't create another employee's loan request")
                            else:
                                rec.write(
                                    {'employee_name': emp.name, 'designation_id': emp.designation_id.id,
                                     'type': emp.type,
                                     'salary_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,
                                     'req_loan_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,})
                    else:
                        if emp and not emp.designation_id:
                            raise ValidationError('Please update the employee designation')

                        if emp and emp.designation_id and emp.designation_id.gross_salary == 0.0:
                            raise ValidationError('Please update the employee salary')

                        if rec.env.user.has_group('department_groups.group_hr_asst'):
                            rec.write(
                                {'employee_name': emp.name, 'designation_id': emp.designation_id.id, 'type': emp.type,
                                 'salary_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,
                                 'req_loan_amount': emp.designation_id.gross_salary if emp.designation_id else 0.0,})

                        else:
                            raise ValidationError("Can't create another employee's loan request")



                else:
                    raise ValidationError('Employee ID not found')

    def action_submit(self):
        for rec in self:
            if rec.req_loan_amount <= 0.0:
                raise ValidationError('Requested loan amount should be greater than 0.0')
            if rec.env.user.has_group('department_groups.group_hr_asst'):
                if not rec.approve_loan_amount:
                    raise ValidationError('Approved loan amount should not be blank')
            rec.status = 'hr_approval'
            hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
            hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
            rec.assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
            rec.assigned_group_ids = [Command.link(hr_manager_group.id)]
            email_template = self.env.ref('employee_self_service.email_template_loan_approval',
                                          raise_if_not_found=False)
            hr_manager_emails = [manager.login for manager in hr_manager_users]
            if hr_manager_emails:
                email_values = {'email_to': ','.join(hr_manager_emails)}
                email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                return True
            else:
                return False

    def action_approve_hr_manager(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                if rec.approve_loan_amount <= 0.0:
                    raise ValidationError('Approved loan amount should not be blank')
                rec.sudo().status = 'finance_mgr_approval'
                rec.sudo().is_self_assign = False
                fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                fin_manager_users = self.env['res.users'].search([('groups_id', 'in', fin_manager_group.id)])
                rec.assigned_user_ids = [Command.link(user.id) for user in fin_manager_users]
                rec.assigned_group_ids = [Command.link(fin_manager_group.id)]
                email_template = self.env.ref('employee_self_service.email_template_loan_approval',
                                              raise_if_not_found=False)
                fin_manager_emails = [manager.login for manager in fin_manager_users]
                if fin_manager_emails:
                    email_values = {'email_to': ','.join(fin_manager_emails)}
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def action_approve_finance_manager(self):
        for rec in self:
            if rec.type == 'direct':
                if rec.env.user.has_group('department_groups.group_finance_mgr'):
                    rec.status = 'accounts_approval'
                    rec.sudo().is_self_assign = False
                    fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                    acc_payroll_group = self.env.ref('department_groups.group_finance_accountant_payroll')
                    asst_mgr_accountant = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                    acc_fin_accountant = self.env.ref('department_groups.group_finance_accountant')
                    acc_payroll_asst = self.env.ref('department_groups.group_finance_payroll_asst')
                    acc_pur_exe = self.env.ref('department_groups.group_finance_purchase_exec')
                    acc_store_keep = self.env.ref('department_groups.group_finance_store_keeper')
                    acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                    [fin_manager_group.id, acc_payroll_group.id,
                                                                     asst_mgr_accountant.id, acc_fin_accountant.id,
                                                                     acc_payroll_asst.id, acc_pur_exe.id,
                                                                     acc_store_keep.id, acc_pur_exe.id])])
                    acc_team_groups = self.env['res.groups'].browse(
                        (fin_manager_group.id, acc_payroll_group.id, asst_mgr_accountant.id,
                         acc_fin_accountant.id, acc_payroll_asst.id, acc_pur_exe.id, acc_store_keep.id))
                    rec.assigned_user_ids = [Command.link(user.id) for user in acc_team_users]
                    rec.assigned_group_ids = [Command.link(group.id) for group in acc_team_groups]

                    email_template = self.env.ref('employee_self_service.email_template_loan_approval',
                                                  raise_if_not_found=False)
                    acc_team_emails = [manager.login for manager in acc_team_users]
                    if acc_team_emails:
                        email_values = {'email_to': ','.join(acc_team_emails)}
                        email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                        return True
                    else:
                        return False
            else:
                if rec.env.user.has_group('department_groups.group_finance_mgr'):
                    rec.status = 'md_approval'
                    rec.sudo().is_self_assign = False

                    md_group = self.env.ref('department_groups.group_mngmt_md')
                    md_users = self.env['res.users'].search([('groups_id', 'in', md_group.id)])
                    rec.assigned_user_ids = [Command.link(user.id) for user in md_users]
                    rec.assigned_group_ids = [Command.link(md_group.id)]
                    email_template = self.env.ref('employee_self_service.email_template_loan_approval',
                                                  raise_if_not_found=False)
                    md_emails = [manager.login for manager in md_users]
                    if md_emails:
                        email_values = {'email_to': ','.join(md_emails)}
                        email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                        return True
                    else:
                        return False

    def action_approve_md(self):
        for rec in self:
            if rec.type == 'indirect':
                if rec.env.user.has_group('department_groups.group_mngmt_md'):
                    rec.sudo().status = 'accounts_approval'
                    rec.sudo().is_self_assign = False
                    fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                    acc_payroll_group = self.env.ref('department_groups.group_finance_accountant_payroll')
                    asst_mgr_accountant = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                    acc_fin_accountant = self.env.ref('department_groups.group_finance_accountant')
                    acc_payroll_asst = self.env.ref('department_groups.group_finance_payroll_asst')
                    acc_pur_exe = self.env.ref('department_groups.group_finance_purchase_exec')
                    acc_store_keep = self.env.ref('department_groups.group_finance_store_keeper')
                    acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                    [fin_manager_group.id, acc_payroll_group.id,
                                                                     asst_mgr_accountant.id, acc_fin_accountant.id,
                                                                     acc_payroll_asst.id, acc_pur_exe.id,
                                                                     acc_store_keep.id, acc_pur_exe.id])])

                    acc_team_groups = self.env['res.groups'].browse(
                        (fin_manager_group.id, acc_payroll_group.id, asst_mgr_accountant.id,
                         acc_fin_accountant.id, acc_payroll_asst.id, acc_pur_exe.id, acc_store_keep.id))
                    rec.assigned_user_ids = [Command.link(user.id) for user in acc_team_users]
                    rec.assigned_group_ids = [Command.link(group.id) for group in acc_team_groups]
                    email_template = self.env.ref('employee_self_service.email_template_loan_approval',
                                                  raise_if_not_found=False)
                    acc_team_emails = [manager.login for manager in acc_team_users]
                    if acc_team_emails:
                        email_values = {'email_to': ','.join(acc_team_emails)}
                        email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                        return True
                    else:
                        return False

    def action_disperse(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_finance_mgr') or rec.env.user.has_group(
                    'department_groups.group_finance_accountant') or rec.env.user.has_group(
                'department_groups.group_finance_accountant_payroll') or rec.env.user.has_group(
                'department_groups.group_finance_payroll_asst') or rec.env.user.has_group(
                'department_groups.group_finance_purchase_exec') or rec.env.user.has_group(
                'department_groups.group_finance_asst_mgr_accountant') or rec.env.user.has_group(
                'department_groups.group_finance_store_keeper'):
                loan_ac = self.env['ir.config_parameter'].sudo().get_param(
                    'employee_self_service.employee_loan_ac_id')
                bank_ac = self.env['ir.config_parameter'].sudo().get_param(
                    'employee_self_service.bank_ac_id')

                bank_journal = self.env['ir.config_parameter'].sudo().get_param(
                    'employee_self_service.bank_journal_id')
                if not all((loan_ac, bank_ac, bank_journal)):
                    raise ValidationError(
                        "Please add loan account and bank account and bank journal in configuration; please contact Administrator")

                move = self.env['account.move'].create({
                    'move_type': 'entry',
                    'date': fields.Date.today(),
                    'ref': rec.name,
                    'line_ids': [
                        Command.create({
                            'debit': rec.approve_loan_amount,
                            'credit': 0.0,
                            'account_id': loan_ac,
                            'partner_id': rec.employee_id.work_contact_id.id,
                            'name': rec.name,
                        }),
                        Command.create({
                            'debit': 0.0,
                            'credit': rec.approve_loan_amount,
                            'account_id': bank_ac,
                            'partner_id': rec.employee_id.work_contact_id.id,
                            'name': '/'
                        }),
                    ],
                })
                rec.move_id = move.id
                rec.sudo().status = 'closed'
                rec.sudo().is_self_assign = False

    def action_self_assign(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in hr_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_hr_id': rec.env.user.id})
            elif rec.env.user.has_group('department_groups.group_mngmt_md'):
                md_group = self.env.ref('department_groups.group_mngmt_md')
                md_users = self.env['res.users'].search([('groups_id', 'in', md_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in md_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]

                rec.write({'is_self_assign': True, 'approval_md_id': rec.env.user.id})
            elif rec.status == 'finance_mgr_approval':
                fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                fin_manager_users = self.env['res.users'].search([('groups_id', 'in', fin_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in fin_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_finance_id': rec.env.user.id})
            else:
                fin_manager_group = self.env.ref('department_groups.group_finance_mgr')
                acc_payroll_group = self.env.ref('department_groups.group_finance_accountant_payroll')
                asst_mgr_accountant = self.env.ref('department_groups.group_finance_asst_mgr_accountant')
                acc_fin_accountant = self.env.ref('department_groups.group_finance_accountant')
                acc_payroll_asst = self.env.ref('department_groups.group_finance_payroll_asst')
                acc_pur_exe = self.env.ref('department_groups.group_finance_purchase_exec')
                acc_store_keep = self.env.ref('department_groups.group_finance_store_keeper')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [fin_manager_group.id, acc_payroll_group.id,
                                                                 asst_mgr_accountant.id, acc_fin_accountant.id,
                                                                 acc_payroll_asst.id, acc_pur_exe.id,
                                                                 acc_store_keep.id, acc_pur_exe.id])])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in acc_team_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.assigned_user_ids = [Command.link(rec.approval_finance_id.id)]
                rec.write({'is_self_assign': True, 'approval_accounts_id': rec.env.user.id})

    def action_rejected(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_compute_installment(self):
        vals = []
        for rec in self:
            rec.loan_installment_ids = False
            for install in range(rec.no_of_installments):
                next_installment_date = fields.Date.today().replace(day=1) + relativedelta(months=install + 1)
                vals += [{
                    'sl_no': install + 1,
                    'date_from': next_installment_date.replace(day=1),
                    'date_to': next_installment_date.replace(
                        day=calendar.monthrange(next_installment_date.year, next_installment_date.month)[1]),
                    'amount': rec.approve_loan_amount / rec.no_of_installments,
                    'rec_amount': '',
                    'state': 'unpaid',
                    'loan_id': rec.id
                }]

        self.env['loan.installment'].create(vals)
