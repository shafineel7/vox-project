# -*- coding: utf-8 -*-
from odoo import models, fields


class LoanInstallment(models.Model):
    _name = 'loan.installment'
    _description = 'Loan Installment'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'sl_no'

    sl_no = fields.Integer(string="Number")
    date_from = fields.Date(string="Date From")
    date_to = fields.Date(string="Date To")
    amount = fields.Float(string="Amount")
    rec_amount = fields.Float(string="Received Amount")
    state = fields.Selection([('paid', 'Paid'), ('unpaid', 'Unpaid'), ('partial', 'Partial')], default='unpaid',
                             string="State")
    loan_id = fields.Many2one('emp.self.ser.loan', string="Loan")
