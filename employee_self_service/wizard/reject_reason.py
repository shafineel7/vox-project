from odoo import models
from odoo.exceptions import ValidationError


class RejectReason(models.TransientModel):
    _inherit = "reject.reason"

    def confirm_action(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model == 'emp.self.ser.sal.cer' and active_id:
            salary_certificate = self.env[active_model].browse(active_id)
            for rec in self:
                if rec.env.user.has_group('department_groups.group_hr_mgr'):
                    salary_certificate.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_hr_id': rec.env.user.id})

                else:
                    salary_certificate.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_asst_hr_id': rec.env.user.id})

                template_id = self.env.ref('employee_self_service.email_template_salary_certificate_rejection',
                                           raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(salary_certificate.id, force_send=True)
                else:
                    raise ValidationError('Email template not found')

        elif active_model == 'emp.self.ser.noc.consulate' and active_id:
            noc_consulate = self.env[active_model].browse(active_id)
            for rec in self:
                if rec.env.user.has_group('department_groups.group_hr_mgr'):
                    noc_consulate.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_hr_id': rec.env.user.id})

                else:
                    noc_consulate.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_asst_hr_id': rec.env.user.id})

                template_id = self.env.ref('employee_self_service.email_template_noc_consulate_rejection',
                                           raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(noc_consulate.id, force_send=True)
                else:
                    raise ValidationError('Email template not found')

        elif active_model == 'emp.self.ser.letter.bank.loan' and active_id:
            letter_bank_loan = self.env[active_model].browse(active_id)
            for rec in self:
                if rec.env.user.has_group('department_groups.group_hr_mgr'):
                    letter_bank_loan.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_hr_id': rec.env.user.id})

                else:
                    letter_bank_loan.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_asst_hr_id': rec.env.user.id})

                template_id = self.env.ref('employee_self_service.email_template_letter_bank_loan_rejection',
                                           raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(letter_bank_loan.id, force_send=True)
                else:
                    raise ValidationError('Email template not found')

        elif active_model == 'emp.self.ser.emp.cer' and active_id:
            employee_certificate = self.env[active_model].browse(active_id)
            for rec in self:
                if rec.env.user.has_group('department_groups.group_hr_mgr'):
                    employee_certificate.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_hr_id': rec.env.user.id})

                else:
                    employee_certificate.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_asst_hr_id': rec.env.user.id})

                template_id = self.env.ref('employee_self_service.email_template_employee_certificate_rejection',
                                           raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(employee_certificate.id, force_send=True)
                else:
                    raise ValidationError('Email template not found')

        elif active_model == 'emp.self.ser.loan' and active_id:
            loan = self.env['emp.self.ser.loan'].browse(active_id)
            for rec in self:
                if rec.env.user.has_group('department_groups.group_hr_mgr'):
                    loan.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_hr_id': rec.env.user.id})

                elif rec.env.user.has_group('department_groups.group_mngmt_md'):
                    loan.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_md_id': rec.env.user.id})

                elif loan.status == 'finance_mgr_approval':
                    loan.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_finance_id': rec.env.user.id})
                else:
                    loan.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_accounts_id': rec.env.user.id})

                template_id = self.env.ref('employee_self_service.email_template_loan_rejection',
                                           raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(loan.id, force_send=True)
                else:
                    raise ValidationError('Email template not found')

        elif active_model == 'passport.rel.req' and active_id:
            passport_rel_req = self.env['passport.rel.req'].browse(active_id)
            for rec in self:
                if rec.env.user.has_group('department_groups.group_hr_mgr'):
                    passport_rel_req.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_hr_id': rec.env.user.id})

                elif rec.env.user.has_group('department_groups.group_finance_store_keeper'):
                    passport_rel_req.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_store_keep_id': rec.env.user.id})

                elif rec.user_has_groups(
                        'department_groups.group_staff_operations_super,'
                        'department_groups.group_construction_operations_super,'
                        'department_groups.group_soft_ser_operations_super'):
                    passport_rel_req.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_dept_head_id': rec.env.user.id})
                else:
                    passport_rel_req.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_dept_head_id': rec.env.user.id})
                template_id = self.env.ref('employee_self_service.email_template_passport_release_request_rejection',
                                           raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(passport_rel_req.id, force_send=True)
                else:
                    raise ValidationError('Email template not found')

        elif active_model == 'emp.self.ser.sal.rev' and active_id:
            salary_revision = self.env[active_model].browse(active_id)
            for rec in self:
                if rec.env.user.has_group('department_groups.group_hr_mgr'):
                    salary_revision.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_hr_id': rec.env.user.id})

                elif rec.env.user.has_group('department_groups.group_finance_mgr'):
                    salary_revision.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_finance_id': rec.env.user.id})

                else:
                    salary_revision.sudo().write(
                        {'reject_reason': rec.name, 'status': 'reject', 'approval_asst_hr_id': rec.env.user.id})

                template_id = self.env.ref('employee_self_service.email_template_salary_revision_rejection',
                                           raise_if_not_found=False)
                if template_id:
                    template = self.env['mail.template'].browse(template_id.id)
                    template.send_mail(salary_revision.id, force_send=True)
                else:
                    raise ValidationError('Email template not found')

        return super().confirm_action()
