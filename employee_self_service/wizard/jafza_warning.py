# -*- coding: utf-8 -*-
import base64
import os

from docxtpl import DocxTemplate, RichText

from odoo import models, fields


class AramexWarning(models.TransientModel):
    _name = "noc.jafza.warning"

    def generate_jafza_docx_file(self):
        active_ids = self.env.context.get('active_ids', [])
        records = self.browse(active_ids)
        module_path = os.path.dirname(os.path.abspath(__file__))
        template_path = os.path.join(module_path, '..', 'document_template', 'noc_for_jafza.docx')
        document = DocxTemplate(template_path)
        company = self.env.company
        company_data = {
            'name': RichText(company.name, bold=True, font='Arial'),
            'street': company.street,
            'state': company.state_id.name,
            'country': company.country_id.name,
            'zip': company.zip,
        }

        aramex_record = self.env['emp.self.ser.noc.consulate'].search([('status', '=', 'closed'), ('id', 'in', active_ids)])
        context = {
            'company_data': company_data,
            'records': enumerate(aramex_record),
            'date': fields.Date.today(),
            'hr_manager': aramex_record.approval_asst_hr_id.name,
            'designation': ' '.join(aramex_record.assigned_group_ids.mapped('name'))
        }
        document.render(context, autoescape=True)

        temp_dir = os.path.join(module_path, '..', 'temp')
        os.makedirs(temp_dir, exist_ok=True)
        temp_file_path = os.path.join(temp_dir, 'NOC for Jafza.docx')

        document.save(temp_file_path)

        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()

        docx_base64 = base64.b64encode(docx_content)
        attachment = self.env['ir.attachment'].sudo().create({
            'name': "NOC for Jafza.docx",
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'emp.self.ser.noc.consulate',
            'res_id': self.id,
            'store_fname': "NOC for Jafza.docx"
        })

        os.remove(temp_file_path)
        url = '/download_nfj/docx/%d' % attachment.id
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self'
        }
