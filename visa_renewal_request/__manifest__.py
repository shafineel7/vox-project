# -*- coding: utf-8 -*-
{
    'name': 'Visa Renewal Request',
    'version': '17.0.1.0.0',
    'category': 'Human Resources',
    'summary': 'Visa Renewal',
    'description': """
        This module allows you to track employee visa renewal.
    """,

    'author': 'HILSHA P H',
    'website': 'https://www.voxtronme.com',
    'depends': ['employee', 'department_groups', 'recruitment','reject_reason','employee_onboarding','employee_exit_management'],
    'data': [
        'security/ir.model.access.csv',
        'security/visa_renewal_request_security.xml',
        'security/visa_renewal_onboarding_security.xml',
        'data/visa_renewal_cron.xml',
        'data/visa_renewal_request_sequence_data.xml',
        'data/mail_template_data.xml',
        'views/visa_renewal_request_views.xml',
        'views/hr_employee_views.xml',
        'views/visa_renewal_onboarding_views.xml',
        'views/visa_renewal_request_menus.xml',
    ],
    'license': 'LGPL-3',
    'installable': True,
    'application': True,
    'auto_install': False,

}
