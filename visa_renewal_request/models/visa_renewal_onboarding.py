# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, Command
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta,date
from odoo.exceptions import UserError, ValidationError
import os


class VisaRenewalOnboarding(models.Model):
    _name = 'visa.renewal.onboarding'
    _description = 'Visa Renewal Onboarding'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char('Name', copy=False)
    partner_name = fields.Char('Applicant Name',realted='employee_id.employee_onboarding_id.partner_name',store=True)
    application = fields.Char('Application',realted='employee_id.employee_onboarding_id.application',store=True)
    email = fields.Char('E-mail',realted='employee_id.employee_onboarding_id.email',store=True)
    department_id = fields.Many2one('hr.department', string='Department',realted='employee_id.employee_onboarding_id.department_id',store=True)
    employee_sequence = fields.Char('Employee ID',realted='employee_id.employee_onboarding_id.employee_sequence',store=True)
    onboarding_type = fields.Selection([('local', 'Local'), ('overseas', 'Overseas')], string='Onboarding Type',realted='employee_id.employee_onboarding_id.onboarding_type',store=True)
    role = fields.Char(string='Role',realted='employee_id.employee_onboarding_id.role',store=True)
    skills_ids = fields.Many2many('hr.skill', string='Skills',realted='employee_id.employee_onboarding_id.skills_ids',store=True)
    type = fields.Selection([('direct', 'Direct'), ('indirect', 'Indirect')], string='Type',realted='employee_id.employee_onboarding_id.type',store=True)
    qualification_ids = fields.Many2many('qualification', 'renewal_onboarding_qualification_relation', 'renewal_onboarding_id',
                                         'renewal_qualification_id', string="Qualifications",realted='employee_id.employee_onboarding_id.qualification_ids',store=True)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female'), ('others', 'Others')], string='Gender',realted='employee_id.employee_onboarding_id.gender',store=True)
    dob = fields.Date(string='Date Of Birth',realted='employee_id.employee_onboarding_id.dob',store=True)
    marital_status = fields.Selection([('single', 'Single'), ('married', 'Married')], string='Marital Status',realted='employee_id.employee_onboarding_id.marital_status',store=True)
    phone = fields.Char(string='Phone',realted='employee_id.employee_onboarding_id.phone',store=True)
    mobile = fields.Char(string='Mobile',realted='employee_id.employee_onboarding_id.mobile',store=True)
    document = fields.Binary(string="Supporting Documents")
    agent_id = fields.Many2one('agent.agent', string='Agent')
    user_id = fields.Many2one("res.users", default=lambda self: self.env.user, string="User")
    nationality_id = fields.Many2one('res.country', string='Nationality',realted='employee_id.employee_onboarding_id.nationality_id',store=True)


    def _get_default_groups(self):
        onboarding = self.env.ref('department_groups.group_hr_mgr').id
        return [Command.link(onboarding)]

    user_ids = fields.Many2many('res.users', string="Assigned Users", default=lambda self: self.env.user,
                                track_visibility='always')
    group_ids = fields.Many2many('res.groups', string='Assigned Groups', default=_get_default_groups)

    is_self_assign = fields.Boolean(string="Self Assign")
    self_assignee_ids = fields.Many2many('res.users', 'renewal_onbaording_self_assign_rel',
                                         'renewal_onboarding_id', 'renewal_self_assign_id', string="Assignees")

    stage = fields.Selection([('new', 'New'), ('ministry_of_labor_application', 'Ministry of Labor Application'),
                              ('visa_processing', 'Visa processing'),
                              ('medical_fitness', 'Medical Fitness'),
                              ('eid_application_insurance', 'EID application & Insurance'),
                              ('visa_stamping', 'Visa stamping'), ('employee_onboarding', 'Employee Onboarding'),
                              ('on_hold', 'On Hold')], group_expand='_group_expand_states', copy=False, default="new",
                             string='Stage',track_visibility='onchange')

    employee_id = fields.Many2one('hr.employee', string='Employee')


    def _group_expand_states(self, states, domain, order):
        return [key for key, val in type(self).stage.selection]

    mol_stage = fields.Selection([('applied', 'MOL Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
                                  ('approved', 'MOL Approved')], string='MOL Status')
    mol_offer_letter_date = fields.Date(string='MOL Offer Letter Date')
    st_and_mb = fields.Binary(string='ST and MB', attachment=True)
    signed_mol = fields.Binary(string='Signed MOL', attachment=True)
    mol_onhold_remarks = fields.Char(string='Remarks')
    mol_reject_remarks = fields.Char(string='Remarks')
    mol_applied_date = fields.Date(string='MOL Applied Date')
    signed_mol_date = fields.Date(string='Signed MOL Offer')

    visa_processing_stage = fields.Selection(
        [('applied', 'E-Visa Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('approved', 'E-Visa Approved')], string='E-Visa Status')
    insurance_payment = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Insurance Payment')
    labour_card_payment = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Labour Card Payment')
    entry_permit_payment = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Entry Permit Payment')
    uid_number = fields.Char(string='UID number')
    file_number = fields.Char(string='File number')
    evisa_date = fields.Date(string='E-Visa date')
    temporary_labour_card_number = fields.Char(string='Temporary Labour card number')
    temporary_labour_card_expiry_date = fields.Date(string='Temporary Labour card Expiry date')
    evisa_copy = fields.Binary(string='E Visa copy', attachment=True)
    evisa_reject_remarks = fields.Char(string='E-Visa Remarks')


    medical_attendance_status = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Medical Attendance Status')
    medical_center_id = fields.Many2one('medical.center', string='Medical Center')
    medical_appointment_date = fields.Date(string='Medical Appointment Date')
    medical_receipt_copy = fields.Binary(string='Mediacl Receipt copy', attachment=True)
    medical_status = fields.Selection(
        [('appointment_scheduled', 'Appointment scheduled'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('completed', 'Completed'),
         ('appointment_rescheduled', 'Appointment rescheduled'), ], string='Medical status')

    medical_detail_status = fields.Selection([
        ('done', 'Done'),
        ('not_done', 'Not-Done'),
    ], string='Medical Detail Status')

    medical_application_no = fields.Char(string='Medical Application Number')
    medical_application_date = fields.Date(string='Medical Application Date')
    medical_completed_date = fields.Date(string='Medical Completed Date')

    tawjeeh_attendance_status = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Tawjeeh Attendance Status')
    tawjeeh_location = fields.Char(string='Tawjeeh Location')
    tawjeeh_center_id = fields.Many2one('tawjeeh.center', string='Tawjeeh Center')
    tawjeeh_appointment_date = fields.Date(string='Tawjeeh Appointment Date')
    tawjeeh_receipt_copy = fields.Binary(string='Tawjeeh Receipt copy', attachment=True)
    tawjeeh_status = fields.Selection(
        [('appointment_scheduled', 'Appointment scheduled'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('completed', 'Completed'),
         ('appointment_rescheduled', 'Appointment rescheduled'), ], string='Tawjeeh status')

    tawjeeh_application_no = fields.Char(string='Tawjeeh Application Number')
    tawjeeh_detail_status = fields.Selection([
        ('done', 'Done'),
        ('not_done', 'Not Done'),
    ], string='Tawjeeh Detail Status')

    eid_status = fields.Selection(
        [('eid_applied', 'EID Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('eid_approved', 'EID Approved'), ], string='EID status')
    eid_application_number = fields.Char(string='EID application number')
    biometric = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Biometric')
    eid_application_receipt = fields.Binary(string='EID Application & receipt', attachment=True)
    biometric_date = fields.Date(string='Biometric Date')
    biometric_location = fields.Char(string='Biometric Location')
    biometric_center_id = fields.Many2one('biometric.center', string='Biometric Center')
    biometric_receipt_copy = fields.Binary(string='Biometric Receipt copy', attachment=True)
    biometric_attendance_status = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ],
                                                   string='Biometric Attendance Status')

    insurance_status = fields.Selection(
        [('insurance_applied', 'Insurance Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('insurance_approved', 'Insurance Approved'), ], string='Insurance status')
    group_code = fields.Char(string='Group code')
    insurance_number = fields.Char(string='Insurance Number')
    authority_number = fields.Char(string='Authority number ID')
    insurance_card = fields.Binary(string='Insurance Card', attachment=True)
    health_insurance_date = fields.Date(string='Health Insurance Application Date')
    premium_amount = fields.Float(string='Premium Amount')
    insurance_document = fields.Binary(string='Insurance Document', attachment=True)
    insurance_application_cancel_date = fields.Date(string='Insurance Application Cancellation Date')
    insurance_cancel_date = fields.Date(string='Insurance Cancellation Date')

    visa_stamping_stage = fields.Selection(
        [('applied', 'Applied'), ('on_hold', 'On Hold'), ('rejected', 'Rejected'),
         ('completed', 'Completed')], string='Visa Stamping Status')
    visa_application_number = fields.Char(string='Visa Application Number')
    visa_expiry_date = fields.Date(string='Visa Expiry Date')
    visa_copy = fields.Binary(string='visa copy', attachment=True)
    collection_of_passport = fields.Boolean(string='Collection of passport', default=False)
    collection_date = fields.Date(string='Collection Date')
    passport_custody_status = fields.Selection(
        [('with_employee', 'With Employee'), ('in_custody', 'In Custody')], string='Passport Custody Status')

    release_status = fields.Char(string="Release Status", default='new')
    onhold_reason_note = fields.Char(string="On Hold Reason Note")
    onhold_reason = fields.Char(string="On Hold Reason")

    approval_hr_id = fields.Many2one('res.users', string="Approval HR")
    approval_talent_id = fields.Many2one('res.users', string="Approval Talent acquisition")
    approval_onboarding_id = fields.Many2one('res.users', string="Approval Onboarding")

    onboarding_approval_date = fields.Date(string='Onboarding Approval Date')
    mol_expense_submit_date = fields.Date(string='MOL Expense Date')
    visa_processing_date = fields.Date(string='E-visa Process Date')
    evisa_applied_date = fields.Date(string='E-visa Applied Date')
    # changing_status_approved_date = fields.Date(string='Change Status approved date')
    eid_application_date = fields.Date(string='Eid Application date')
    eid_biometric_attendance_status_date = fields.Date(string='Eid Biometric attendance status date')
    visa_stamping_stage_date = fields.Date(string='Visa Stamping Stage date')
    visa_passport_status_date = fields.Date(string='Schedule Stage date')
    medical_fitness_date = fields.Date(string='Medical Stage date')
    medical_attendance_status_date = fields.Date(string='Medical Attendance date')
    tawjeeh_attendance_status_date = fields.Date(string='Tawjeeh attendance date')

    mol_expense_amount = fields.Float(string="MOL expense Amount", copy=False, store=True,
                                      default=lambda self: self.env['product.product'].search(
                                          [('is_mol_expense', '=', True)], limit=1).standard_price)

    @api.onchange('mol_stage')
    def _onchange_mol_stage(self):
        for rec in self:
            if rec.mol_stage == 'applied':
                rec.mol_expense_amount = self.env['product.product'].search([('is_mol_expense', '=', True)],
                                                                            limit=1).standard_price

    expense_sheet_id = fields.Many2one('hr.expense.sheet', store=True, copy=False)
    expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    visa_processing_amount = fields.Float(string="Visa Processing Amount", copy=False, store=True,
                                          default=lambda self: self.env['product.product'].search(
                                              [('is_visa_processing', '=', True)], limit=1).standard_price)

    @api.onchange('visa_processing_stage')
    def _onchange_visa_processing_stage(self):
        for rec in self:
            if rec.visa_processing_stage == 'applied':
                rec.visa_processing_amount = self.env['product.product'].search([('is_visa_processing', '=', True)],
                                                                                limit=1).standard_price

    visa_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    visa_expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    change_status_amount = fields.Float(string="Change status Amount", copy=False, store=True,
                                        default=lambda self: self.env['product.product'].search(
                                            [('is_change_status', '=', True)], limit=1).standard_price)

    @api.onchange('change_status')
    def _onchange_change_status(self):
        for rec in self:
            if rec.change_status == 'change_status_applied':
                rec.change_status_amount = self.env['product.product'].search([('is_change_status', '=', True)],
                                                                              limit=1).standard_price

    change_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', store=True, copy=False, )
    change_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    medical_status_amount = fields.Float(string="Medical status Amount", copy=False, store=True,
                                         default=lambda self: self.env['product.product'].search(
                                             [('is_medical_status', '=', True)], limit=1).standard_price)

    @api.onchange('medical_status')
    def _onchange_medical_status(self):
        for rec in self:
            if rec.medical_status == 'appointment_scheduled' or rec.medical_status == 'appointment_rescheduled':
                rec.medical_status_amount = self.env['product.product'].search([('is_medical_status', '=', True)],
                                                                               limit=1).standard_price

    medical_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    medical_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    schedule_status_amount = fields.Float(string="Schedule status Amount", copy=False, store=True,
                                          default=lambda self: self.env['product.product'].search(
                                              [('is_schedule_status', '=', True)], limit=1).standard_price)

    @api.onchange('schedule_status')
    def _onchange_schedule_status(self):
        for rec in self:
            if rec.schedule_status == 'reached':
                rec.schedule_status_amount = self.env['product.product'].search([('is_schedule_status', '=', True)],
                                                                                limit=1).standard_price

    schedule_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    schedule_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    tawjeeh_status_amount = fields.Float(string="Tawjeeh status Amount", copy=False, store=True,
                                         default=lambda self: self.env['product.product'].search(
                                             [('is_tawjeeh_status', '=', True)], limit=1).standard_price)

    @api.onchange('tawjeeh_status')
    def _onchange_tawjeeh_status(self):
        for rec in self:
            if rec.tawjeeh_status == 'appointment_scheduled' or rec.tawjeeh_status == 'appointment_rescheduled':
                rec.tawjeeh_status_amount = self.env['product.product'].search([('is_tawjeeh_status', '=', True)],
                                                                               limit=1).standard_price

    tawjeeh_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    tawjeeh_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    eid_status_amount = fields.Float(string="Eid status Amount", copy=False, store=True,
                                     default=lambda self: self.env['product.product'].search(
                                         [('is_eid_status', '=', True)], limit=1).standard_price)

    @api.onchange('eid_status')
    def _onchange_eid_status(self):
        for rec in self:
            if rec.eid_status == 'eid_applied':
                rec.eid_status_amount = self.env['product.product'].search([('is_eid_status', '=', True)],
                                                                           limit=1).standard_price

    eid_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    eid_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    insurance_status_amount = fields.Float(string="Insurance status Amount", copy=False, store=True,
                                           default=lambda self: self.env['product.product'].search(
                                               [('is_insurance_status', '=', True)], limit=1).standard_price)

    @api.onchange('insurance_status')
    def _onchange_insurance_status(self):
        for rec in self:
            if rec.insurance_status == 'insurance_applied':
                rec.insurance_status_amount = self.env['product.product'].search([('is_insurance_status', '=', True)],
                                                                                 limit=1).standard_price

    insurance_status_expense_sheet_id = fields.Many2one('hr.expense.sheet', copy=False, store=True)
    insurance_status_expense_id = fields.Many2one('hr.expense', store=True, copy=False)

    def _finance_manager_approval_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'ministry_of_labor_application' and rec.expense_sheet_id != False:
                if rec.expense_sheet_id.state not in ('approve', 'post'):
                    if rec.onboarding_approval_date:
                        exp_date = (date_now - rec.onboarding_approval_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 3:
                            template_id = self.env.ref(
                                'employee_recruitment_onboarding.email_template_finance_manager_approval')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

    def action_mol_post(self):
        expense_ids = []
        mol_expense = self.env['product.product'].search([('is_mol_expense', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.mol_expense_amount
            if rec.mol_stage == 'applied':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = rec.employee_id.department_id.id
                analytic_name = rec.employee_id.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': mol_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.employee_id.employee_onboarding_id.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > mol_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'expense_sheet_id': expense.sheet_id.id,
                        'expense_id': expense.id,

                    }
                )
            return expense_ids

    def action_visa_post(self):
        expense_ids = []
        visa_processing_expense = self.env['product.product'].search([('is_visa_processing', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.visa_processing_amount
            if rec.visa_processing_stage == 'applied':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': visa_processing_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.employee_id.employee_onboarding_id.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > visa_processing_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'visa_expense_sheet_id': expense.sheet_id.id,
                        'visa_expense_id': expense.id,

                    }
                )
            return expense_ids


    def action_medical_status_post(self):
        expense_ids = []
        medical_status_expense = self.env['product.product'].search([('is_medical_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.medical_status_amount
            if rec.medical_status == 'appointment_scheduled':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': medical_status_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.employee_id.employee_onboarding_id.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > medical_status_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'medical_status_expense_sheet_id': expense.sheet_id.id,
                        'medical_status_expense_id': expense.id,

                    }
                )
            return expense_ids

    def action_tawjeeh_post(self):
        expense_ids = []
        tawjeeh_status_expense = self.env['product.product'].search([('is_tawjeeh_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.tawjeeh_status_amount
            if rec.tawjeeh_status == 'appointment_scheduled':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': tawjeeh_status_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.employee_id.employee_onboarding_id.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > tawjeeh_status_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'tawjeeh_status_expense_sheet_id': expense.sheet_id.id,
                        'tawjeeh_status_expense_id': expense.id,

                    }
                )
            return expense_ids

    def action_eid_post(self):
        expense_ids = []
        eid_status_expense = self.env['product.product'].search([('is_eid_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.eid_status_amount
            if rec.eid_status == 'eid_applied':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': eid_status_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.employee_id.employee_onboarding_id.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > eid_status_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'eid_status_expense_sheet_id': expense.sheet_id.id,
                        'eid_status_expense_id': expense.id,

                    }
                )
            return expense_ids

    def action_insurance_post(self):
        expense_ids = []
        insurance_status_expense = self.env['product.product'].search([('is_insurance_status', '=', True)], limit=1)
        for rec in self:
            expense_value = rec.insurance_status_amount
            if rec.insurance_status == 'insurance_applied':
                analytic_accounts = {}
                employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)], limit=1)
                department_id = employee.department_id.id
                analytic_name = employee.department_id.name
                expense = self.env['hr.expense'].create({
                    'employee_id': employee.id,
                    'date': fields.Date.today(),
                    'name': rec.name,
                    'payment_mode': 'own_account',
                    'department_id': employee.department_id.id,
                    'total_amount': expense_value,
                    'product_id': insurance_status_expense.id,
                    'onboarding_employee_id': rec.employee_id.id,
                    'employee_onboarding_id': rec.employee_id.employee_onboarding_id.id,
                    'quantity': 1
                })
                expense_ids.append(expense.id)

                if expense_value > insurance_status_expense.maximum_allowed_amount:
                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                else:

                    expense.action_submit_expenses()
                    expense.sheet_id.sudo().action_submit_sheet()
                    expense.sheet_id.sudo().action_approve_expense_sheets()
                    expense.sheet_id.sudo().action_approve_finance_manager()
                rec.sudo().write(
                    {
                        'insurance_status_expense_sheet_id': expense.sheet_id.id,
                        'insurance_status_expense_id': expense.id,

                    }
                )
            return expense_ids


    def write(self, vals):
        # onboarding_users = self.env.user.has_group('department_groups.group_hr_asst_onboard')
        pro_manager = self.env.user.has_group('department_groups.group_pro_mgr')
        pro_assistant = self.env.user.has_group('department_groups.group_pro_asst')
        agent_users = self.env.user.has_group('department_groups.group_agent_agent')
        hr_aast = self.env.user.has_group('department_groups.group_hr_asst')
        hr_mgr = self.env.user.has_group('department_groups.group_hr_mgr')
        welfare_users = self.env.user.has_group('department_groups.group_hr_welfare_officer')
        camp_manager = self.env.user.has_group('department_groups.group_construction_camp_mgr')
        staff_camp_manager = self.env.user.has_group('department_groups.group_staff_camp_mgr')
        soft_camp_manager = self.env.user.has_group('department_groups.group_soft_ser_camp_mgr')
        res = super(VisaRenewalOnboarding, self).write(vals)
        if self.stage == 'new':
            if not hr_mgr:
                raise UserError("You cannot edit The data in new stage")
        if self.stage == 'ministry_of_labor_application':
            if not (hr_mgr or pro_manager or pro_assistant or agent_users):
                raise UserError("You cannot edit The data in new stage")
            if pro_assistant or pro_manager:
                if 'signed_mol' in vals:
                    raise UserError("You cannot edit The data")
            if agent_users:
                if 'mol_stage' in vals:
                    raise UserError("You cannot edit The data")
                if 'st_and_mb' in vals:
                    raise UserError("You cannot edit The data")
            if hr_mgr:
                if 'mol_expense_amount' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'visa_processing':
            if not (hr_mgr or pro_manager or pro_assistant):
                raise UserError("You cannot edit The data in new stage")
            if hr_mgr:
                if 'visa_processing_amount' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'visa_stamping':
            if not (hr_mgr or pro_manager or pro_assistant or hr_aast):
                raise UserError("You cannot edit The data in new stage")
            if pro_manager or pro_assistant:
                if 'collection_of_passport' in vals:
                    raise UserError("You cannot edit The data")
                if 'collection_date' in vals:
                    raise UserError("You cannot edit The data")
                if 'passport_custody_status' in vals:
                    raise UserError("You cannot edit The data")
            if hr_aast:
                if 'visa_stamping_stage' in vals:
                    raise UserError("You cannot edit The data")
                if 'visa_application_number' in vals:
                    raise UserError("You cannot edit The data")
                if 'visa_expiry_date' in vals:
                    raise UserError("You cannot edit The data")
                if 'visa_copy' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'eid_application_insurance' and self.env.context.get('insurance_value') == False:
            if not (hr_mgr or pro_manager or pro_assistant or hr_aast or welfare_users):
                raise UserError("You cannot edit The data in new stage")
            if hr_mgr:
                if 'insurance_status_amount' in vals:
                    raise UserError("You cannot edit The data")
                if 'eid_status_amount' in vals:
                    raise UserError("You cannot edit The data")
            if not (hr_aast or hr_mgr):
                if 'insurance_status' in vals:
                    raise UserError("You cannot edit The data")
                if 'group_code' in vals:
                    raise UserError("You cannot edit The data")
                if 'insurance_number' in vals:
                    raise UserError("You cannot edit The data")
                if 'authority_number' in vals:
                    raise UserError("You cannot edit The data")
                if 'insurance_card' in vals:
                    raise UserError("You cannot edit The data")
                if 'insurance_document' in vals:
                    raise UserError("You cannot edit The data")
            if not (welfare_users):
                if 'biometric_attendance_status' in vals:
                    raise UserError("You cannot edit The data")
            if not (pro_manager or pro_assistant):
                if 'eid_status_amount' in vals:
                    raise UserError("You cannot edit The data")

            if not (pro_manager or pro_assistant or hr_mgr):
                if 'eid_status' in vals:
                    raise UserError("You cannot edit The data")
                if 'eid_application_number' in vals:
                    raise UserError("You cannot edit The data")
                if 'biometric' in vals:
                    raise UserError("You cannot edit The data")
                if 'eid_application_receipt' in vals:
                    raise UserError("You cannot edit The data")
                if 'biometric_date' in vals:
                    raise UserError("You cannot edit The data")
                if 'biometric_location' in vals:
                    raise UserError("You cannot edit The data")

        if self.stage == 'medical_fitness':
            if not (hr_mgr or welfare_users):
                raise UserError("You cannot edit The data in new stage")
            if not welfare_users:
                if 'medical_attendance_status' in vals:
                    raise UserError("You cannot edit The data")
                if 'tawjeeh_attendance_status' in vals:
                    raise UserError("You cannot edit The data")

            if not hr_mgr:
                if 'medical_status_amount' in vals:
                    raise UserError("You cannot edit The data")
                if 'tawjeeh_status_amount' in vals:
                    raise UserError("You cannot edit The data")
                if 'medical_appointment_date' in vals:
                    raise UserError("You cannot edit The data")
                if 'medical_receipt_copy' in vals:
                    raise UserError("You cannot edit The data")
                if 'medical_center_id' in vals:
                    raise UserError("You cannot edit The data")

        return res

    @api.onchange('visa_stamping_stage')
    def _onchange_visa_stamping_stage(self):
        for rec in self:
            if rec.visa_stamping_stage == 'applied':
                rec.visa_stamping_stage_date = fields.date.today()


    @api.onchange('biometric_attendance_status')
    def _onchange_biometric_attendance_status(self):
        for rec in self:
            if rec.biometric_attendance_status == 'no':
                rec.eid_biometric_attendance_status_date = fields.date.today()
    def action_self_assign(self):
        for onboarding in self:
            if onboarding.stage == 'new':
                onboarding.stage = 'ministry_of_labor_application'
                onboarding.user_ids = False
                onboarding.user_ids = [Command.link(onboarding.env.user.id)]
                onboarding.write({'is_self_assign': True, 'self_assignee_ids': [Command.link(onboarding.env.user.id)], 'approval_onboarding_id': onboarding.env.user.id})
            else:
                onboarding.self_assignee_ids = False
                onboarding.user_ids = False
                onboarding.user_ids = [Command.link(onboarding.env.user.id)]
                onboarding.write({'is_self_assign': True, 'self_assignee_ids': [Command.link(onboarding.env.user.id)], 'approval_onboarding_id': onboarding.env.user.id})


    def action_submit(self):
        for rec in self:
            if rec.onhold_reason_note:
                if rec.onhold_reason:
                    rec.onhold_reason = str(rec.onhold_reason) + "\n" + str(
                        (datetime.utcnow() + timedelta(hours=4)).replace(microsecond=0)) + ' #' + str(
                        self.env.user.name) + ' - ' + str(self.onhold_reason_note)
                else:
                    rec.onhold_reason = str(
                        (datetime.utcnow() + timedelta(hours=4)).replace(microsecond=0)) + ' #' + str(
                        self.env.user.name) + ' - ' + str(rec.onhold_reason_note)
                rec.onhold_reason_note = False
            rec.sudo().write(
                {'onhold_reason': rec.onhold_reason,
                 'stage': 'on_hold',
                 'is_self_assign': False
                 })

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('name') or vals['name'] == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('visa_renewal_onboarding_name') or _('New')
        return super().create(vals_list)



    def onboarding_team_hold(self):

        view_id = self.env.ref('visa_renewal_request.visa_renewal_onboarding_view_form_onhold')
        ctx = self.env.context.copy()
        self.approval_onboarding_id = self.env.user.id
        return {
            'name': _('On Hold Reason'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'employee.onboarding',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx,
        }

    def mol_completed(self):
        for onboarding in self:
            onboarding.visa_processing_date = fields.Date.today()
            if not (
                    onboarding.mol_stage and onboarding.st_and_mb and onboarding.signed_mol and onboarding.mol_offer_letter_date):
                raise ValidationError("Please fill the MOL Details Before Approval")
            pro_manager = self.env.ref('department_groups.group_pro_mgr')
            pro_assistant = self.env.ref('department_groups.group_pro_asst')
            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                            [pro_manager.id, pro_assistant.id])])
            acc_team_groups = self.env['res.groups'].browse((pro_manager.id, pro_assistant.id))

            onboarding.write({
                'stage': 'visa_processing',
                'release_status': 'visa_processing',
                'user_ids': [(6, 0, acc_team_users.ids)],
                'group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False

            })
            onboarding.employee_id.write({
                'mol_offer_letter_date': onboarding.mol_offer_letter_date,
                'mol_status': onboarding.mol_stage,
                'mol_offer_letter': onboarding.signed_mol,
                'st_document': onboarding.st_and_mb,

            })

    def _evisa_send_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'visa_processing':
                if rec.visa_processing_date:
                    if (not (rec.insurance_payment or rec.labour_card_payment
                            or rec.entry_permit_payment or rec.uid_number or rec.file_number or rec.evisa_date or rec.temporary_labour_card_number
                    or rec.temporary_labour_card_expiry_date or rec.evisa_copy)):

                        exp_date =(date_now - rec.visa_processing_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=6:
                            template_id = self.env.ref('employee_onboarding.email_template_evisa_fields')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.evisa_applied_date:
                    if rec.visa_processing_stage == 'applied':
                        exp_date = (date_now - rec.evisa_applied_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=3:
                            template_id = self.env.ref('employee_onboarding.email_template_evisa_approval')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

    def _medical_status_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'medical_fitness':
                if rec.medical_fitness_date:
                    if rec.medical_status != 'appointment_scheduled' or rec.tawjeeh_status != 'appointment_scheduled':

                        exp_date =(date_now - rec.medical_fitness_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_hr_mgr')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.medical_attendance_status_date:
                    if rec.medical_attendance_status == 'no':

                        exp_date =(date_now - rec.medical_attendance_status_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_hr_mgr')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.tawjeeh_attendance_status_date:
                    if rec.tawjeeh_attendance_status == 'no':

                        exp_date =(date_now - rec.tawjeeh_attendance_status_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_hr_mgr')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)



    def _mol_send_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'ministry_of_labor_application':
                if rec.mol_stage == 'applied' and not rec.st_and_mb:
                    if rec.mol_applied_date:
                        exp_date = (date_now - rec.mol_applied_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=3:
                            template_id = self.env.ref('employee_onboarding.email_template_mol_attachments')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

                if rec.mol_stage == 'approved' and not rec.signed_mol:
                    if rec.mol_applied_date:
                        exp_date = (date_now - rec.mol_applied_date).days
                        exp_date = abs(exp_date)
                        if exp_date<=3:
                            if rec.onboarding_type == 'local':
                                template_id = self.env.ref('employee_onboarding.email_template_mol_attachments')
                                onboarding_users = self.env.ref('department_groups.group_hr_mgr')
                                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                                [onboarding_users.id])])
                            else:
                                template_id = self.env.ref('employee_onboarding.email_template_mol_attachments')
                                onboarding_users = self.env.ref('department_groups.group_agent_agent')
                                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                                [onboarding_users.id])])

                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

    def _eid_status_send_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'eid_application_insurance':
                if rec.eid_application_date:
                    if not(rec.eid_status or rec.eid_application_number or rec.biometric or rec.eid_application_receipt) or (rec.biometric=='yes' and not(rec.biometric_date or rec.biometric_location)):
                        exp_date = (date_now - rec.eid_application_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 3:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.eid_biometric_attendance_status_date:
                    if rec.biometric_attendance_status == 'no':
                        exp_date = (date_now - rec.eid_biometric_attendance_status_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)


    def _visa_status_send_reminder(self):
        date_now = date.today()
        match = self.search([])
        for rec in match:
            if rec.stage == 'visa_stamping':
                if rec.visa_stamping_stage_date:
                    if rec.visa_stamping_stage=='applied' and not rec.visa_application_number:
                        exp_date = (date_now - rec.visa_stamping_stage_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 2:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.visa_passport_status_date:
                    if rec.collection_of_passport == True and not (rec.passport_custody_status == 'in_custody' or rec.collection_date):
                        exp_date = (date_now - rec.visa_passport_status_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 30:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_hr_asst')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)
                if rec.visa_stamping_stage != 'completed':
                    if rec.visa_expiry_date :
                        exp_date = (date_now - rec.visa_expiry_date).days
                        exp_date = abs(exp_date)
                        if exp_date <= 15:
                            template_id = self.env.ref('employee_onboarding.email_template_change_status')
                            pro_manager = self.env.ref('department_groups.group_pro_mgr')
                            pro_assistant = self.env.ref('department_groups.group_pro_asst')
                            hr_mgr = self.env.ref('department_groups.group_hr_mgr')
                            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                            [pro_manager.id, pro_assistant.id,hr_mgr.id])])
                            mail_to = []
                            for user in acc_team_users:
                                if user.partner_id.email:
                                    mail_to.append(user.partner_id.email)
                            reorder_qty_mail_to = ",".join(mail_to)
                            if template_id and rec.id and reorder_qty_mail_to:
                                template_id.write({'email_to': reorder_qty_mail_to})
                                template_id.with_context({'email_to': reorder_qty_mail_to}). \
                                    send_mail(rec.id, force_send=True, raise_exception=False)

    @api.onchange('visa_processing_stage', )
    def _onchange_visa_processing_stage(self):
        for line in self:
            if line.visa_processing_stage == 'applied':
                line.evisa_applied_date = fields.Date.today()

            if line.visa_processing_stage == 'rejected':
                template_id = self.env.ref('employee_onboarding.email_template_mol_reject_notification')

                onboarding_users = self.env.ref('department_groups.group_hr_mgr')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [onboarding_users.id])])
                acc_team_groups = self.env['res.groups'].browse((onboarding_users.id))

                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (line.id or line._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                        line.id or line._origin.id, force_send=True, raise_exception=False)



    @api.onchange('medical_status')
    def _onchange_medical_status(self):
        for line in self:
            if line.medical_status == 'appointment_scheduled' or line.medical_status == 'appointment_rescheduled':
                template_id = self.env.ref('employee_onboarding.email_template_medical_fitness_notification')

                onboarding_users = self.env.ref('department_groups.group_hr_welfare_officer')

                acc_team_users = self.env['res.users'].search([('groups_id', 'in', [onboarding_users.id])])
                acc_team_groups = self.env['res.groups'].browse((onboarding_users.id))

                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (line.id or line._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                        line.id or line._origin.id, force_send=True, raise_exception=False)

    @api.onchange('tawjeeh_status')
    def _onchange_tawjeeh_status(self):
        for line in self:
            if line.tawjeeh_status == 'appointment_scheduled' or line.tawjeeh_status == 'appointment_rescheduled':
                template_id = self.env.ref('employee_onboarding.email_template_tawjee_notification')

                onboarding_users = self.env.ref('department_groups.group_hr_welfare_officer')

                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [onboarding_users.id])])
                acc_team_groups = self.env['res.groups'].browse((onboarding_users.id))

                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (line.id or line._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                        line.id or line._origin.id, force_send=True, raise_exception=False)

    @api.onchange('biometric')
    def _onchange_biometric(self):
        for line in self:
            if line.biometric == 'yes':
                template_id = self.env.ref('employee_onboarding.email_template_biomatric_notification')

                onboarding_users = self.env.ref('department_groups.group_hr_welfare_officer')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [onboarding_users.id])])
                acc_team_groups = self.env['res.groups'].browse((onboarding_users.id))
                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (line.id or line._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                        line.id or line._origin.id, force_send=True, raise_exception=False)

    def evisa_completed(self):
        for onboarding in self:
            if not (onboarding.evisa_date and
                    onboarding.visa_processing_stage and onboarding.insurance_payment and onboarding.labour_card_payment and onboarding.entry_permit_payment and onboarding.uid_number and onboarding.file_number and onboarding.temporary_labour_card_number and onboarding.temporary_labour_card_expiry_date and onboarding.evisa_copy):
                raise ValidationError("Please fill the Visa Processing Details Before Approval")
            onboarding.employee_id.write({
                'e_visa_date': onboarding.evisa_date,
                'e_visa_copy': onboarding.evisa_copy,
                'visa_status': onboarding.visa_processing_stage,

            })


            onboarding_team = self.env.ref('department_groups.group_hr_mgr')
            welfare_team = self.env.ref('department_groups.group_hr_welfare_officer')
            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                            [onboarding_team.id, welfare_team.id])])
            acc_team_groups = self.env['res.groups'].browse((onboarding_team.id, welfare_team.id))

        onboarding.write({
            'stage': 'medical_fitness',
            'release_status': 'medical_fitness',
            'user_ids': [(6, 0, acc_team_users.ids)],
            'group_ids': [(6, 0, acc_team_groups.ids)],
            'is_self_assign': False,
            'medical_fitness_date':fields.date.today()

        })


    @api.onchange('medical_attendance_status')
    def _onchange_medical_attendance_status(self):
        for rec in self:
            if rec.medical_attendance_status == 'no':
                rec.medical_attendance_status_date = fields.date.today()


    @api.onchange('tawjeeh_attendance_status')
    def _onchange_tawjeeh_attendance_status(self):
        for rec in self:
            if rec.tawjeeh_attendance_status == 'no':
                rec.tawjeeh_attendance_status_date = fields.date.today()

    def medical_tawjeeh_completed(self):
        for onboarding in self:
            if not (
                    onboarding.medical_application_no and onboarding.medical_application_date and onboarding.medical_completed_date and onboarding.medical_detail_status and
                    onboarding.medical_center_id and onboarding.medical_appointment_date and onboarding.medical_attendance_status and onboarding.medical_receipt_copy and onboarding.medical_status
                    and onboarding.tawjeeh_center_id and onboarding.tawjeeh_appointment_date and onboarding.tawjeeh_attendance_status and onboarding.tawjeeh_receipt_copy and onboarding.tawjeeh_status
                    and onboarding.tawjeeh_application_no and onboarding.tawjeeh_detail_status):
                raise ValidationError("Please fill the Medical and Tawjeeh Details Before Approval")
            onboarding.employee_id.write({
                'medical_application_no': onboarding.medical_application_no,
                'medical_center_id': onboarding.medical_center_id.id,
                'medical_application_date': onboarding.medical_application_date,
                'receipt_attachment': onboarding.medical_receipt_copy,
                'medical_completed_date': onboarding.medical_completed_date,
                'medical_status': onboarding.medical_detail_status,

                'tawjeeh_application_no': onboarding.tawjeeh_application_no,
                'tawjeeh_status': onboarding.tawjeeh_detail_status,
                'tawjeeh_appointment_date': onboarding.tawjeeh_appointment_date,
                'tawjeeh_receipt_attachment': onboarding.tawjeeh_receipt_copy,
                'tawjeeh_center_id': onboarding.tawjeeh_center_id.id,

            })

            template_id = self.env.ref('employee_onboarding.email_template_eid_insurance_notification')

            onboarding_users = self.env.ref('department_groups.group_hr_welfare_officer')

            hr_assistant = self.env.ref('department_groups.group_hr_asst')
            pro_assistant = self.env.ref('department_groups.group_pro_asst')
            pro_manager = self.env.ref('department_groups.group_pro_mgr')

            acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                            [onboarding_users.id, hr_assistant.id, pro_manager.id,
                                                             pro_assistant.id])])
            acc_team_groups = self.env['res.groups'].browse(
                (onboarding_users.id, hr_assistant.id, pro_manager.id, pro_assistant.id))

            mail_to = []
            for user in acc_team_users:
                if user.partner_id.email:
                    mail_to.append(user.partner_id.email)
            reorder_qty_mail_to = ",".join(mail_to)

            if template_id and (onboarding.id or onboarding._origin.id) and reorder_qty_mail_to:
                template_id.write({'email_to': reorder_qty_mail_to})
                template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(
                    onboarding.id or onboarding._origin.id, force_send=True, raise_exception=False)
            # date_now = date.today()
            onboarding.write({
                'stage': 'eid_application_insurance',
                'release_status': 'eid_application_insurance',
                'user_ids': [(6, 0, acc_team_users.ids)],
                'group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False,
                'eid_application_date':  fields.Date.today()

            })

    def eid_application_insurance_completed(self):
        for onboarding in self:
            if not onboarding.biometric:
                raise ValidationError("Please fill the EID and Insurance Details Before Approval")

            if onboarding.biometric == 'yes':
                if not (
                        onboarding.biometric and onboarding.biometric_date and onboarding.biometric_center_id and onboarding.biometric_receipt_copy and onboarding.eid_application_number and onboarding.eid_application_receipt and onboarding.biometric_attendance_status and onboarding.eid_status and onboarding.group_code and onboarding.insurance_number and onboarding.authority_number and onboarding.insurance_card and onboarding.insurance_status and onboarding.health_insurance_date and onboarding.premium_amount and onboarding.insurance_document):
                    raise ValidationError("Please fill the EID and Insurance Details Before Approval")
            if onboarding.biometric == 'no':
                if not (
                        onboarding.biometric and onboarding.eid_application_number and onboarding.eid_application_receipt and onboarding.biometric_attendance_status and onboarding.eid_status and onboarding.group_code and onboarding.insurance_number and onboarding.authority_number and onboarding.insurance_card and onboarding.insurance_status and onboarding.health_insurance_date and onboarding.premium_amount and onboarding.insurance_document):
                    raise ValidationError("Please fill the EID and Insurance Details Before Approval")

            onboarding.employee_id.write({
                'eid_application_no': onboarding.eid_application_number,
                'eid_application_copy': onboarding.eid_application_receipt,
                'eid_status': onboarding.eid_status,
                'biometric_date': onboarding.biometric_date,
                'biometric_center_id': onboarding.biometric_center_id.id,
                'biometric_status': onboarding.biometric_attendance_status,
                'health_insurance_date': onboarding.health_insurance_date,
                'health_insurance_no': onboarding.insurance_number,
                'group_code': onboarding.group_code,
                'insurance_authority': onboarding.authority_number,
                'premium_amount': onboarding.premium_amount,
                'insurance_card': onboarding.insurance_card,
                'insurance_document': onboarding.insurance_document,
                'insurance_application_cancel_date': onboarding.insurance_application_cancel_date,
                'insurance_cancel_date': onboarding.insurance_cancel_date,
                'health_insurance_status': onboarding.insurance_status,

            })

            onboarding.write({
                'stage': 'visa_stamping',
                'release_status': 'visa_stamping',
                'is_self_assign': False,
                'visa_passport_status_date':fields.date.today()

            })

    def visa_stamping_completed(self):
        for onboarding in self:
            if not (
                    onboarding.visa_stamping_stage and onboarding.visa_application_number and onboarding.visa_expiry_date and onboarding.visa_copy and onboarding.collection_of_passport and onboarding.collection_date and onboarding.passport_custody_status):
                raise ValidationError("Please fill the Visa Stamping Details Before Approval")

            onboarding.write({
                'stage': 'employee_onboarding',
                'release_status': 'employee_onboarding',
                'is_self_assign': False

            })

            onboarding.employee_id.write({
                'visa_expire': onboarding.visa_expiry_date

            })

    def release_status_stage(self):
        for onboarding in self:
            onboarding.write({
                'stage': onboarding.release_status

            })
