# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, Command
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta, date
import os
from docxtpl import DocxTemplate
import base64
from odoo.exceptions import UserError


class VisaRenewalRequest(models.Model):
    _name = 'visa.renewal.request'
    _description = 'Visa Renewal Request'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _get_default_groups(self):
        for rec in self:
            if rec.employee_id.department_id:
                department_head_ids = rec.employee_id.department_id.department_head_ids
        manager = self.env.ref('department_groups.group_hr_mgr').id
        welfare = self.env.ref('department_groups.group_hr_welfare_officer').id
        return [Command.link(manager), Command.link(welfare)]

    name = fields.Char('Name', copy=False)
    employee_id = fields.Many2one('hr.employee', string='Employee')
    employee_sequence = fields.Char('Employee ID', related='employee_id.employee_sequence')
    department_id = fields.Many2one('hr.department', string='Department')
    employee_type = fields.Selection([('local', 'Local'), ('overseas', 'Overseas')], string='Type')
    type = fields.Selection([('direct', 'Direct'), ('indirect', 'Indirect')], string='Type')
    document = fields.Binary(string="Supporting Documents")
    agent_id = fields.Many2one('agent.agent', string='Agent')
    user_id = fields.Many2one("res.users", default=lambda self: self.env.user, string="User")
    visa_renewal_confirmation_status = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Renewal status')
    visa_renewal_letter = fields.Binary(string='Visa Renewal letter', attachment=True)
    visa_renewal_letter_file_name = fields.Char('Visa Renewal letter Filename')
    signed_copy = fields.Binary(string='Signed Copy', attachment=True)
    signed_copy_file_name = fields.Char('Signed Copy Filename')
    assigned_user_ids = fields.Many2many('res.users', string='Assigned User', default=lambda self: self.env.user)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    is_self_assign = fields.Boolean(string="Self Assign")
    stage = fields.Selection([('new', 'New'),
                              ('department_head_approval', 'Department Head Approval'),
                              ('operation_supervisor_approval', 'Operation Supervisor Approval'),
                              ('hr_manager_approval', 'HR Manager Approval'),
                              ('closed', 'Closed'),  ('visa_cancellation', 'Visa Cancellation'), ('reject', 'Rejected'),
                              ('on_hold', 'On Hold')], copy=False, default="new",
                             string='Stage', group_expand='_group_expand_states', track_visibility='onchange')
    is_department_head = fields.Boolean(string="Is Department Head", compute="_compute_is_department_head")
    is_operational_supervisor = fields.Boolean(string="Is Operational Supervisor",
                                               compute="_compute_is_operational_supervisor")

    dept_head_ids = fields.Many2many('hr.employee', related='employee_id.department_id.department_head_ids')
    dept_head_user_ids = fields.Many2many('res.users', 'user_visa_renewal_rel', 'user_id', 'visa_renewal_rel_req_id',
                                          compute='_compute_dept_head_user_ids', store=True)

    operations_supervisor_ids = fields.Many2many('hr.employee',
                                                 related='employee_id.department_id.operations_supervisor_ids',
                                                 string='Operations Supervisor')
    operations_supervisor_user_ids = fields.Many2many('res.users', 'user_visa_renewal_supervisor_rel', 'user_id',
                                                      'visa_renewal_supervisor_rel_req_id',
                                                      compute='_compute_operations_supervisor_user_ids', store=True)
    # renewal approval user
    department_user_approval_id = fields.Many2one('res.users', string="Department Approval")
    approval_operations_supervisor_id = fields.Many2one('res.users', string="Approval Operations Supervisor")
    hr_manager_approval_id = fields.Many2one('res.users', string="Approval HR Manager")
    department_approval_date = fields.Date(string="Department Approval Date")
    employee_onboarding_id = fields.Many2one('employee.onboarding')
    reject_reason = fields.Text(string="Reject Reason")

    @api.depends('employee_id', 'employee_id.department_id', 'employee_id.type',
                 'employee_id.department_id.operations_supervisor_ids', )
    def _compute_operations_supervisor_user_ids(self):
        user_list = []
        for rec in self:
            if rec.operations_supervisor_ids:
                for head in rec.operations_supervisor_ids:
                    if head.user_id:
                        user_list.append(head.user_id.id)
            rec.operations_supervisor_user_ids = user_list

    @api.depends('employee_id', 'employee_id.department_id.department_head_ids', 'employee_id.type')
    def _compute_dept_head_user_ids(self):
        user_list = []
        for rec in self:
            if rec.dept_head_ids:
                for head in rec.dept_head_ids:
                    if head.user_id:
                        user_list.append(head.user_id.id)
            rec.dept_head_user_ids = user_list

    # @api.depends('employee_id','employee_id.department_id','employee_id.department_id.department_head_ids','dept_head_user_ids','user_id')
    def _compute_is_department_head(self):
        for record in self:
            if self.env.user.id in record.employee_id.department_id.department_head_ids.user_id.ids or self.env.user.id in record.dept_head_user_ids.ids:
                record.is_department_head = True
            else:
                record.is_department_head = False

    # @api.depends('employee_id', 'employee_id.department_id', 'employee_id.department_id.operations_supervisor_ids','operations_supervisor_user_ids','user_id')
    def _compute_is_operational_supervisor(self):
        for record in self:
            if self.env.user.id in record.employee_id.department_id.operations_supervisor_ids.user_id.ids or self.env.user.id in record.operations_supervisor_user_ids.ids:
                record.is_operational_supervisor = True
            else:
                record.is_operational_supervisor = False

    def _group_expand_states(self, states, domain, order):
        return [key for key, val in type(self).stage.selection]

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('name') or vals['name'] == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('visa_renewal_name') or _('New')
        return super().create(vals_list)

    def action_rejected(self):
        self.ensure_one()
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_self_assign(self):

        for record in self:
            if record.stage == 'new':
                record.stage = 'department_head_approval'
                record.assigned_user_ids = False
                record.assigned_user_ids = [Command.link(record.env.user.id)]
                record.write({'is_self_assign': True, 'department_user_approval_id': record.env.user.id})
            elif record.stage == 'operation_supervisor_approval':
                record.assigned_user_ids = False
                record.assigned_user_ids = [Command.link(record.env.user.id)]
                record.write({'is_self_assign': True, 'approval_operations_supervisor_id': record.env.user.id})
            elif record.stage == 'hr_manager_approval':
                record.assigned_user_ids = False
                record.assigned_user_ids = [Command.link(record.env.user.id)]
                record.write({'is_self_assign': True, 'hr_manager_approval_id': record.env.user.id})
            if record.env.user.has_group('department_groups.group_hr_mgr'):
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                record.assigned_user_ids = [Command.unlink(user.id) for user in hr_manager_users]
                record.assigned_user_ids = [Command.link(record.env.user.id)]
                record.write({'is_self_assign': True, 'hr_manager_approval_id': record.env.user.id})

    def action_department_head_approval(self):
        operational_supervisor = []
        for rec in self:
            rec.stage = 'operation_supervisor_approval'
            rec.sudo().is_self_assign = False
            rec.sudo().department_approval_date = fields.date.today()
            renewal_letter = rec.sudo().action_generate_visa_renewal_letter()
            rec.visa_renewal_letter = renewal_letter.datas
            if rec.employee_id.department_id:
                operational_supervisor_ids = rec.employee_id.department_id.operations_supervisor_ids
                operational_supervisor = [hod.user_id.partner_id.email for hod in operational_supervisor_ids if
                                          hod.user_id.partner_id.email]
            rec.assigned_user_ids = rec.operations_supervisor_user_ids
            user = self.env['res.users'].browse(rec.assigned_user_ids.ids)
            group_ids = user.groups_id.ids
            groups = self.env['res.groups'].browse(group_ids)
            rec.assigned_group_ids = [Command.link(group.id) for group in groups]
            email_template = self.env.ref('visa_renewal_request.mail_template_visa_renewal_reminder')
            if operational_supervisor:
                email_values = {'email_to': ','.join(operational_supervisor)}
                email_template.write({
                    'email_to': ",".join(operational_supervisor),
                    'email_from': self.env.user.email,
                })
                email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                return True
            else:
                return False

    @api.onchange('visa_renewal_confirmation_status')
    def _onchange_visa_renewal_confirmation_status(self):
        operational_supervisor = []
        for rec in self:
            if rec.visa_renewal_confirmation_status in ('yes', 'no'):
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                rec.assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                rec.assigned_group_ids = [Command.link(hr_manager_group.id)]
                email_template = self.env.ref('visa_renewal_request.mail_template_visa_renewal_reminder')
                hr_manager_emails = [manager.login for manager in hr_manager_users]
                if hr_manager_emails:
                    email_values = {'email_to': ','.join(hr_manager_emails)}
                    email_template.write({
                        'email_to': ",".join(operational_supervisor),
                        'email_from': self.env.user.email,
                    })
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)

    # def action_operation_supervisor_approval(self):
    def action_operation_hr_manager_approval(self):
        operational_supervisor = []
        for rec in self:
            if rec.visa_renewal_confirmation_status in ('no',):
                rec.stage = 'visa_cancellation'
            else:
                rec.stage = 'closed'
            rec.sudo().is_self_assign = False

    # def action_operation_hr_manager_approval(self):
    def action_operation_supervisor_approval(self):
        pro_team = []
        for rec in self:
            rec.stage = 'hr_manager_approval'
            if rec.visa_renewal_confirmation_status in ('no',):
                rec.sudo().is_self_assign = False

                rec.sudo().is_self_assign = False
                hr_manager = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search(
                    [('groups_id', 'in', [hr_manager.id])])
                pro_groups = self.env['res.groups'].browse((hr_manager.id))
                rec.assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                rec.assigned_group_ids = [Command.link(user.id) for user in pro_groups]
                email_template = self.env.ref('visa_renewal_request.mail_template_visa_renewal_reminder')
                hr_manager_emails = [manager.login for manager in hr_manager_users]
                if hr_manager_emails:
                    email_values = {'email_to': ','.join(hr_manager_emails)}
                    email_template.write({
                        'email_to': ",".join(pro_team),
                        'email_from': self.env.user.email,
                    })
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                allowed_groups = []
                if self.env.ref(
                        'department_groups.group_hr_mgr') in self.env.user.groups_id:
                    allowed_groups.append(
                        self.env.ref('department_groups.group_hr_mgr').id)
                if self.env.ref(
                        'department_groups.group_hr_asst') in self.env.user.groups_id:
                    allowed_groups.append(self.env.ref('department_groups.group_hr_asst').id)
                assigned_group = [Command.link(group) for group in allowed_groups]
                exit_reason = self.env['exit.reasons'].search([('type', '=', 'contract_expiry')], limit=1)
                exit_id = self.env['employee.exit.management'].sudo().create({
                    'employee_id': rec.employee_id.id,
                    'exit_reason_id': exit_reason.id,
                    'ticket_eligibility': exit_reason.ticket_eligibility,
                    'assigned_group_ids': assigned_group,
                    'is_auto_created': True,
                })

            else:

                rec.sudo().is_self_assign = False
                hr_manager = self.env.ref('department_groups.group_hr_mgr')
                pro_manager = self.env.ref('department_groups.group_pro_mgr')
                pro_assistant = self.env.ref('department_groups.group_pro_asst')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', [pro_manager.id, pro_assistant.id,hr_manager.id])])
                pro_groups = self.env['res.groups'].browse((pro_manager.id, pro_assistant.id,hr_manager.id))
                rec.assigned_user_ids = [Command.link(user.id) for user in hr_manager_users]
                rec.assigned_group_ids = [Command.link(user.id) for user in pro_groups]
                email_template = self.env.ref('visa_renewal_request.mail_template_visa_renewal_reminder')
                hr_manager_emails = [manager.login for manager in hr_manager_users]

                visa_renewal_onboarding = self.env['visa.renewal.onboarding'].sudo().create({
                    'employee_id': rec.employee_id.id,
                    'employee_sequence': rec.employee_id.employee_sequence,
                    'department_id': rec.department_id.id,
                    'stage': 'new',
                    'agent_id': rec.agent_id.id,
                    'onboarding_type': rec.employee_type,

                })
                if hr_manager_emails:
                    email_values = {'email_to': ','.join(hr_manager_emails)}
                    email_template.write({
                        'email_to': ",".join(pro_team),
                        'email_from': self.env.user.email,
                    })
                    email_template.with_context(email_values).send_mail(rec.id, force_send=True)
                    return True
                else:
                    return False

    def write(self, vals):
        # onboarding_users = self.env.user.has_group('department_groups.group_hr_asst_onboard')
        res = super(VisaRenewalRequest, self).write(vals)
        if self.stage == 'operation_supervisor_approval':
            if self.env.user.id not in self.operations_supervisor_user_ids.ids:
                # if self.is_operational_supervisor == True:
                if 'visa_renewal_confirmation_status' in vals:
                    raise UserError("You cannot edit The data")
                if 'visa_renewal_letter' in vals:
                    raise UserError("You cannot edit The data")
                if 'signed_copy' in vals:
                    raise UserError("You cannot edit The data")

        return res

    def action_generate_visa_renewal_letter(self):
        module_path = os.path.dirname(os.path.abspath(__file__))
        template_path = os.path.join(module_path, '..', 'visa_renewal_request_template',
                                     'visa_renewal_request.docx')
        document = DocxTemplate(template_path)

        context = {
            'employee_sequence': self.employee_sequence if self.employee_sequence else " ",
            'department_id': self.department_id.name if self.department_id.name else "",
            'name': self.employee_id.display_name if self.employee_id.display_name else " ",
            'visa_expiry_date': self.employee_id.visa_expire.strftime('%d-%m-%Y'),
            'visa_expiry_month': self.employee_id.visa_expire.strftime('%B'),
            'app_date': self.department_approval_date.strftime('%d-%m-%Y'),
        }

        document.render(context, autoescape=True)
        temp_dir = os.path.join(module_path, '..', 'temp')
        os.makedirs(temp_dir, exist_ok=True)
        temp_file_path = os.path.join(temp_dir, f'{self.name}.docx')
        document.save(temp_file_path)
        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()
        docx_base64 = base64.b64encode(docx_content)
        attachment = self.env['ir.attachment'].sudo().create({
            'name': f"Visa Renewal Request - {self.name}.docx",
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'visa.renewal.request',
            'res_id': self.id,
            'store_fname': f"{self.name}.docx"
        })
        os.remove(temp_file_path)
        return attachment
        # url = '/download_rsa/docx/%d' % attachment.id
        # return {
        #     'type': 'ir.actions.act_url',
        #     'url': url,
        #     'target': 'self'
        # }

    def _visa_renewal_creation_reminder(self):
        today = datetime.now().date()
        sixty_days_ago = today - timedelta(days=60)
        employees = self.env['hr.employee'].search([])
        hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
        hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
        hr_managers_emails = [user.partner_id.email for user in hr_manager_users]
        welfare_group = self.env.ref('department_groups.group_hr_welfare_officer')
        welfare_users = self.env['res.users'].search([('groups_id', 'in', welfare_group.id)])
        welfare_emails = [user.partner_id.email for user in welfare_users if user.partner_id.email]
        hod_emails = []
        visa_expire_employees = employees.search(
            [('visa_expire', '=', sixty_days_ago), ('is_visa_renewal', '=', False)])
        department_list = []
        supervisor_list = []
        for vehicle in visa_expire_employees:
            if vehicle.department_id:
                if vehicle.department_id.department_head_ids:
                    for head in vehicle.department_id.department_head_ids:
                        if head.user_id:
                            department_list.append(head.user_id.id)
                if vehicle.department_id.operations_supervisor_ids:
                    for sr_head in vehicle.department_id.operations_supervisor_ids:
                        if sr_head.user_id:
                            supervisor_list.append(sr_head.user_id.id)
                department_head_ids = vehicle.department_id.department_head_ids
                hod_emails = [hod.user_id.partner_id.email for hod in department_head_ids if
                              hod.user_id.partner_id.email]

            visa_renewal = self.env['visa.renewal.request'].sudo().create({
                'employee_id': vehicle.id,
                'employee_sequence': vehicle.employee_sequence,
                'department_id': vehicle.department_id.id,
                'type': vehicle.type,
                'stage': 'new',
                'agent_id': vehicle.employee_onboarding_id.agent_id.id,
                'employee_type': vehicle.employee_onboarding_id.onboarding_type,
                'dept_head_ids': vehicle.department_id.department_head_ids,
                'operations_supervisor_ids': vehicle.department_id.operations_supervisor_ids,
                'operations_supervisor_user_ids': [(6, 0, supervisor_list)],
                'dept_head_user_ids': [(6, 0, department_list)],
                'employee_onboarding_id': vehicle.employee_onboarding_id.id

            })
            vehicle.write({
                'visa_renewal_request_id': visa_renewal.id,
                'is_visa_renewal': True
            })
            all_emails = hr_managers_emails + welfare_emails + hod_emails
            email_to = ",".join(all_emails)
            mail_template = self.env.ref('visa_renewal_request.mail_template_visa_renewal_reminder')
            mail_template.write({
                'email_to': email_to,
                'email_from': self.env.user.email,
            })
            mail_template.send_mail(visa_renewal.id, force_send=True)
