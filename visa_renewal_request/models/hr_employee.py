# -*- coding: utf-8 -*-

from odoo import api, fields, models,_


class Employee(models.Model):
    _inherit = "hr.employee"

    visa_renewal_request_id = fields.Many2one('visa.renewal.request', string="Visa Renewal Request")
    is_visa_renewal = fields.Boolean(string="Visa Renewal")

