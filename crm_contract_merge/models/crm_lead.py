# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError
from dateutil.relativedelta import relativedelta


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    contract_id = fields.Many2one('contract.contracts', string="Contract ID")
    contract_start_date = fields.Date(string="Contract Start Date")
    contract_end_date = fields.Date(string="Contract End Date")
    mode = fields.Selection([('open', 'Open'), ('fixed', 'Fixed')], string="Mode")
    lpo_number = fields.Char(string="LPO Number")
    lpo_validity = fields.Integer(string="LPO Validity")
    lpo_attachment = fields.Binary(string="LPO Attachment")
    contract_value = fields.Float(string="Contract Value")

    @api.onchange('mode')
    def onchange_contract_mode(self):
        if self.mode == 'open':
            if not self.lpo_number or self.lpo_validity <= 0 or not self.contract_start_date:
                raise UserError("Please Add  Contract Start date, LPO Number and LPO Validity")
        elif self.mode == 'fixed':
            if not self.contract_start_date or not self.contract_end_date:
                raise UserError("Please Add Contract Start and End Dates")

    @api.onchange('contract_start_date', 'contract_end_date', 'lpo_validity')
    def onchange_contract_start_end(self):
        if self.contract_start_date and self.contract_end_date:
            if self.contract_start_date >= self.contract_end_date:
                raise UserError("End Date should be greater that Start Date")
        if self.contract_start_date and self.lpo_validity:
            self.contract_end_date = self.contract_start_date + relativedelta(days=self.lpo_validity)

    def action_mark_won(self):
        if self.lead_estimation_ids:
            self.contract_value = sum(self.lead_estimation_ids.mapped('total'))
            self.expected_revenue = self.contract_value
        view_id = self.env.ref('crm_contract_merge.crm_lead_contract_details_form')
        ctx = self.env.context.copy()
        return {
            'name': _('Contract Details'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'crm.lead',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx
        }

    def action_set_won_rainbowman(self):
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        self.env.context = ctx
        res = super(CrmLead, self).action_set_won_rainbowman()
        for vals in self:
            if vals.lead_type == 'new':
                contract_vals = {
                }
                if not vals.partner_id.is_account:
                    seq = "TMAXDXB"
                    code = 'contracts.contracts.' + self.partner_id.name[0].lower()
                    seqs = self.env['ir.sequence'].next_by_code(code)
                    if seqs:
                        seq += '|' + seqs
                        date_seq = datetime.now().date().strftime("%d%m%y")
                        seq += "|" + date_seq
                    if vals.department_id:
                        if vals.department_id.seq_code:
                            seq += "|" + vals.department_id.seq_code.upper()
                    code = seq
                    contract_vals["code"] = code
                else:
                    contract_vals["code"] = vals.partner_id.contract_seq

                contract_vals["partner_id"] = vals.partner_id.id
                contract_vals["name"] = vals.name
                contract_vals['phone'] = vals.phone
                contract_vals["email"] = vals.email_from
                contract_vals["type"] = vals.lead_type
                contract_vals["service_type"] = vals.service_type
                contract_vals["user_id"] = vals.user_id.id
                contract_vals['contract_start_date'] = vals.contract_start_date
                contract_vals['contract_end_date'] = vals.contract_end_date
                contract_vals['mode'] = vals.mode
                contract_vals['lpo_number'] = vals.lpo_number
                contract_vals['lpo_validity'] = vals.lpo_validity
                contract_vals['lpo_attachment'] = vals.lpo_attachment
                contract_vals['contract_value'] = vals.contract_value
                contract_vals['department_id'] = vals.department_id.id
                contract_vals['lead_id'] = vals.id
                contract = self.env['contract.contracts'].create(contract_vals)
                vals.contract_id = contract
                contract.assigned_users_ids = vals.assigned_user_ids
                contract.assigned_groups_ids = vals.assigned_group_ids
                if vals.lead_estimation_ids:
                    for est in vals.lead_estimation_ids:
                        line_vals = {
                            'contract_id': contract.id,
                            'product_id': est.product_id.id,
                            'name': est.description,
                            'start_date': vals.contract_start_date,
                            'end_date': vals.contract_end_date,
                            'mode': vals.mode,
                            'lpo_number': vals.lpo_number,
                            'lpo_validity': vals.lpo_validity,
                            'lpo_attachment': vals.lpo_attachment,
                            'qty': est.qty,
                            'unit_price': est.unit_price,
                            'contract_type': est.contract_type
                        }
                        self.env['contract.lines'].sudo().create(line_vals)
            elif vals.lead_type in ['addendum', 'renewal']:
                if vals.lead_estimation_ids:
                    for est in vals.lead_estimation_ids:
                        line_vals = {
                            'contract_id': vals.contract_id.id,
                            'product_id': est.product_id.id,
                            'name': est.description,
                            'start_date': vals.contract_start_date,
                            'end_date': vals.contract_end_date,
                            'mode': vals.mode,
                            'lpo_number': vals.lpo_number,
                            'lpo_validity': vals.lpo_validity,
                            'lpo_attachment': vals.lpo_attachment,
                            'qty': est.qty,
                            'unit_price': est.unit_price,
                            'contract_type': est.contract_type

                        }
                        if vals.lead_type == 'addendum':
                            line_vals["state"] = 'active'
                        else:
                            line_vals["state"] = 'inactive'
                        self.env['contract.lines'].sudo().create(line_vals)
        ctx = self.env.context.copy()
        ctx['allow_stage'] = True
        self.env.context = ctx
        return res

    def check_exist(self):
        if self.lead_type == 'new':
            contract = self.env['contract.contracts'].sudo().search(
                [('department_id', '=', self.department_id.id), ('partner_id', '=', self.partner_id.id),
                 ('contract_start_date', '>=', self.contract_start_date),
                 ('contract_end_date', '<=', self.contract_end_date), ('status', '=', 'active')])
            if contract:
                codes = contract.mapped('code')
                code_msg = ','.join(codes)
                msg = """Other Active Contracts %s exists for same customer. Click Update to continue.""" % code_msg
                value = self.env['display.conf.message'].sudo().create({'message': msg, 'lead_id': self.id})
                return {
                    'type': 'ir.actions.act_window',
                    'message': 'Message',
                    'res_model': 'display.conf.message',
                    'view_mode': 'form',
                    'target': 'new',
                    'res_id': value.id
                }
            else:
                ctx = self.env.context.copy()
                ctx['allow_stage'] = True
                self.env.context = ctx

                return self.action_set_won_rainbowman()
        else:
            return self.action_set_won_rainbowman()

    def action_show_related_contracts(self):
        self.ensure_one()
        return {
            'name': _('Contracts'),
            'view_mode': 'form',
            'res_model': 'contract.contracts',
            'res_id': self.contract_id.id,
            'view_id': self.env.ref('crm_contracts.contracts_contract_view_form').id,
            'type': 'ir.actions.act_window',
        }
