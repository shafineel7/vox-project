# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError
from dateutil.relativedelta import relativedelta


class ContractContracts(models.Model):
    _inherit = 'contract.contracts'

    lead_id = fields.Many2one('crm.lead', string="Opportunity")
    analytic_account_id = fields.Many2one('account.analytic.account', string="Analytic Account")

    @api.model_create_multi
    def create(self, vals_list):
        res = super(ContractContracts, self).create(vals_list)
        plan = self.env.ref('crm_contract_merge.analytic_plan_contract').id
        for vals in res:
            analytic_name = ""
            if vals.code:
                analytic_name = vals.code
            elif vals.name:
                analytic_name = vals.name
            if analytic_name:
                analytic_account = self.env['account.analytic.account'].sudo().create({
                    'name': analytic_name,
                    'plan_id': plan,
                    'partner_id': res.partner_id.id if res.partner_id else False,
                })
                if analytic_account:
                    vals.analytic_account_id = analytic_account.id
        return res

    def _cron_add_crm_renewal(self):
        contract_line = self.env['contract.lines'].sudo().search([('renewal_date', '=', datetime.now().date()),('state', '=', 'active'),('contract_id.stage_code', 'not in',['09', '10'] )])
        if contract_line:
            contracts = contract_line.mapped('contract_id')
            if contracts:
                for contract in contracts:
                    lines = contract.contract_lines_ids.filtered(lambda x: x in contract_line)
                    if lines:
                        opportunity = self.env['crm.lead'].sudo().create({
                            'name': contract.code + "- Renewal",
                            'lead_type': 'renewal',
                            'partner_id': contract.partner_id.id,
                            'email_from': contract.partner_id.email,
                            'phone': contract.partner_id.phone,
                            'mobile': contract.partner_id.mobile,
                            'user_id': contract.user_id.id,
                            'department_id': contract.department_id.id,
                            'contract_id': contract.id,
                            'type': 'opportunity'
                        })
                        opportunity.sudo().assigned_user_ids = contract.user_id.ids
                        for line in lines:
                            opportunity_line = self.env['crm.lead.estimation'].sudo().create({
                                'product_id': line.product_id.id,
                                'description': line.name,
                                'contract_type': 'hourly',
                                'qty': line.qty,
                                'unit_price': line.unit_price,
                                'lead_id': opportunity.id
                            })

