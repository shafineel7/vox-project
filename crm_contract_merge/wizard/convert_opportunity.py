# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Lead2OpportunityPartner(models.TransientModel):
    _inherit = 'crm.lead2opportunity.partner'

    contract_id = fields.Many2one('contract.contracts', string="Contract")

    @api.onchange('lead_type')
    def onchange_lead_type(self):
        super(Lead2OpportunityPartner, self).onchange_lead_type()
        if self.lead_type:
            if self.lead_type == 'new':
                self.contract = False

    def _convert_and_allocate(self, leads, user_ids, team_id=False):
        for lead in leads:
            lead.contract_id = self.contract_id
        return super(Lead2OpportunityPartner, self)._convert_and_allocate(leads, user_ids, team_id)
