# -*- coding: utf-8 -*-

from odoo import models, fields


class DisplayConfirmationMessage(models.TransientModel):
    _inherit = 'display.conf.message'

    lead_id = fields.Many2one('crm.lead', string="Lead")

    def continue_action(self):
        if not self.lead_id:
            return self.job_est_id.update_est_heads()
        else:
            self.lead_id.action_set_won_rainbowman()

