# -*- coding: utf-8 -*-
{
    'name': 'CRM - Contract Management',
    'version': '17.0.1.0.1',
    'category': 'crm',
    'summary': 'This module manages the creation of contract from CRM',
    'description': 'This module manages the creation of contract from CRM',
    'author': 'Neenu R',
    'website': 'http://www.voxtronme.com',
    'depends': ['crm_contract_mgmt', 'crm_contracts'],
    'data': [
        'data/ir_cron.xml',
        'data/analytic_plan_data.xml',
        'wizard/convert_opportunity.xml',
        'views/crm_lead_views.xml',
        'views/contract_views.xml'
    ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'AGPL-3',
    'images': [],
}
