# -*- coding: utf-8 -*-

{
    'name': 'Journal Entries Document',
    'author': 'Devika p',
    'version': '17.0.1.0.0',
    'summary': 'Module for Downloading Journal Entries PDF report',
    'category': 'Report',
    'depends': ['account_accountant'],
    'description': """This module is for creating Journal Entries PDF report""",
    'data': [
        'report/journal_entries_header.xml',
        'report/journal_entries_report.xml',
        'report/journal_entries_report_template.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
