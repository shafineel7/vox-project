from odoo import models, fields, api


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    fuel_log_entry_id = fields.Many2one('fuel.log.entries', string='Fuel Log Entry')
