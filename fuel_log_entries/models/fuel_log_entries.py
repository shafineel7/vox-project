from odoo import models, fields, api, Command
from odoo.exceptions import UserError

FUEL_TYPES = [
    ('diesel', 'Diesel'),
    ('gasoline', 'Gasoline'),
    ('full_hybrid', 'Full Hybrid'),
    ('plug_in_hybrid_diesel', 'Plug-in Hybrid Diesel'),
    ('plug_in_hybrid_gasoline', 'Plug-in Hybrid Gasoline'),
    ('cng', 'CNG'),
    ('lpg', 'LPG'),
    ('hydrogen', 'Hydrogen'),
    ('electric', 'Electric'),
]


class FuelLogEntries(models.Model):
    _name = "fuel.log.entries"
    _description = "Fuel Log Entries"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _rec_name = "model_type_id"

    model_type_id = fields.Many2one('fleet.vehicle', string="Model/Type of the vehicle")
    camp_name = fields.Char(string="Camp/Project Name")
    fleet_category = fields.Selection(string='Fleet Category',
                                      selection=[("permanent", "Permanent"), ("temporary", "Temporary")])
    date = fields.Date(string="Date")
    gallon_liters = fields.Integer(string="Gallons/Liters")
    assigned_to_id = fields.Many2one('res.users', string="Assigned To")
    fuel_type = fields.Selection(FUEL_TYPES, string='Fuel Type')
    sl_no = fields.Char(string="Reference", readonly=True)
    is_purchase_order = fields.Boolean(string="Is Purchase order", default=False)
    company_id = fields.Many2one(
        comodel_name='res.company',
        string="Company",
        required=True,
        readonly=True,
        default=lambda self: self.env.company,
    )
    tax_ids = fields.Many2many(
        comodel_name='account.tax',
        string="Tax",
    )
    currency_id = fields.Many2one("res.currency", string="Currency")
    total_amount = fields.Monetary(
        string="Total Amount",
        currency_field='currency_id',
        tracking=True,
    )
    purchase_order_id = fields.Many2one('purchase.order', string='Purchase Order')
    is_self_assign = fields.Boolean(string="Self Assign", default=False)
    self_assignee_ids = fields.Many2many('res.users', 'fuel_log_entries_self_assign_rel',
                                         'fuel_log_entries_id', 'self_assign_id', string="Assignees")

    user_ids = fields.Many2many('res.users', string="Assigned Users", default=lambda self: self.env.user,
                                )

    fleet_driver_id = fields.Many2one('hr.employee', string='Driver')

    def _get_default_groups(self):
        group = self.env['res.groups']
        fleet_manager = self.env.ref('department_groups.group_fleet_manager')
        if fleet_manager:
            group += fleet_manager
        if group:
            return group.ids
        else:
            return False

    group_ids = fields.Many2many('res.groups', string='Assigned Groups', default=_get_default_groups)
    active = fields.Boolean(string="Active", default="True")

    @api.onchange("model_type_id")
    def _onchange_model_type_id(self):
        for record in self:
            if record.model_type_id:
                record.write({
                    'fleet_category': record.model_type_id.fleet_category,
                    'fleet_driver_id': record.model_type_id.drivers_id,
                    'fuel_type': record.model_type_id.fuel_type,
                    'camp_name': record.model_type_id.site_or_project
                })

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if 'sl_no' not in vals:
                vals['sl_no'] = self.env['ir.sequence'].next_by_code('sl.no') or 'New'

        fuel_log_entries = super(FuelLogEntries, self).create(vals_list)

        group_templates = {
            "group_finance_accountant": "mail_template_fin_executive",
            "group_finance_asst_mgr_accountant": "mail_template_asst_mgr",
            "group_finance_mgr": "mail_template_fin_mgr",
            "group_finance_purchase_exec": "mail_template_purchase_executive",
        }

        email_recipients = []

        for group_name, template_name in group_templates.items():
            user_group = self.env.ref("department_groups." + group_name)
            email_list = [usr.partner_id.email for usr in user_group.users if usr.partner_id.email]
            email_recipients.extend(email_list)

        email_to = ",".join(email_recipients)
        mail_template = self.env.ref('fuel_log_entries.mail_template_create')

        for fuel_log_entry in fuel_log_entries:
            mail_template.with_context(lang=fuel_log_entry.env.user.lang).send_mail(
                fuel_log_entry.id, force_send=True, email_values={
                    'email_to': email_to,
                    'email_from': self.env.user.email,
                }
            )

        for vals in vals_list:
            if 'model_type_id' in vals:
                fleet_vehicle_record = self.env['fleet.vehicle'].browse(vals['model_type_id'])
                if fleet_vehicle_record:
                    fleet_vehicle_record.write({
                        'fuel_log_entries_ids': [(4, fuel_log_entry.id) for fuel_log_entry in fuel_log_entries]
                    })

        return fuel_log_entries

    def action_self_assign(self):
        for fuel_log_entry in self:
            fuel_log_entry.write({'is_self_assign': True,
                                  'self_assignee_ids': [Command.link(fuel_log_entry.env.user.id)],
                                  'user_ids': fuel_log_entry.env.user})

    def action_submitted(self):
        product = self.env['product.template'].browse(
            self.env.ref('fuel_log_entries.product_fuel_1').id)
        fuel_product = self.env['product.product'].search([('product_tmpl_id', '=', product.id)], limit=1)

        partner_id = False
        if fuel_product.seller_ids:
            partner_id = fuel_product.seller_ids[0].partner_id.id

        quotation_lines = [(0, 0, {
            'product_id': fuel_product.id,
            'name': self.model_type_id.name,
            'product_qty': self.gallon_liters,
            'price_unit': self.total_amount,
            'taxes_id': self.tax_ids
        })]

        quotation_values = {
            'partner_id': partner_id,
            'order_line': quotation_lines,
        }
        purchase_order = self.env['purchase.order'].create(quotation_values)
        self.purchase_order_id = purchase_order.id
        self.purchase_order_id.fuel_log_entry_id = self.id
        self.is_purchase_order = True

        group_templates = {
            "group_finance_accountant": "mail_template_fin_executive",
            "group_finance_asst_mgr_accountant": "mail_template_asst_mgr",
            "group_finance_mgr": "mail_template_fin_mgr",
            "group_finance_purchase_exec": "mail_template_purchase_executive",

        }

        email_recipients = []

        for group_name, template_name in group_templates.items():
            user_group = self.env.ref("department_groups." + group_name)
            email_list = [usr.partner_id.email for usr in user_group.users if usr.partner_id.email]
            email_recipients.extend(email_list)

        email_to = ",".join(email_recipients)
        mail_template = self.env.ref('fuel_log_entries.mail_template_purchase_order')

        mail_template.write({
            'email_to': email_to,
            'email_from': self.env.user.email,
        })

        mail_template.send_mail(self.id, force_send=True)

    def action_view_purchase_order(self):
        if self.purchase_order_id:
            action = self.env.ref('purchase.purchase_form_action').sudo().read()[0]
            action['domain'] = [('fuel_log_entry_id', '=', self.id)]
            return action

        else:
            raise UserError('No purchase order has been created for this vehicle request yet.')
