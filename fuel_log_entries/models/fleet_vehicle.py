from odoo import models, fields, api


class FleetVehicle(models.Model):
    _inherit = 'fleet.vehicle'

    fuel_log_entries_ids = fields.One2many('fuel.log.entries', 'model_type_id', string="Fuel Log Entries")
