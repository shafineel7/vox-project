# -*- coding: utf-8 -*-

from odoo import models, fields, _


class ResCompany(models.Model):
    _inherit = 'res.company'

    normal_working_hours = fields.Float('Normal Working Hours')
    holidays = fields.Selection([('sunday', 'Sunday'), ('monday', 'Monday'), ('tuesday', 'Tuesday'),
                                 ('wednesday', 'Wednesday'), ('thursday', 'Thursday'), ('friday', 'Friday'),
                                 ('saturday', 'Saturday')])
    max_working_hr_per_day = fields.Float('Maximum Working Hr/Day')
