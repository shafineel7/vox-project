# -*- coding: utf-8 -*-

from odoo import fields, models, _
import xlsxwriter
import base64
import calendar
from datetime import datetime
import itertools


class MonthlyTimesheet(models.Model):
    _inherit = 'monthly.timesheet'

    payslip_count = fields.Integer(compute='_compute_payslip_count')
    file_name = fields.Binary('Excel Report File')
    document_frame = fields.Char('File To Download')
    company_id = fields.Many2one('res.company', string='Company', index=True, default=lambda self: self.env.company)
    status = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirm')], default='draft')

    def get_dates_by_day_name(self, year, month, day_name):
        # Get the day number corresponding to the given day name (0 = Monday, 1 = Tuesday, ..., 6 = Sunday)
        target_day = list(calendar.day_name).index(day_name)

        # Initialize a list to store dates
        dates = []

        # Get the total number of days in the given month
        num_days_in_month = calendar.monthrange(year, month)[1]

        # Iterate through each day in the month
        for day in range(1, num_days_in_month + 1):
            # Create a datetime object for the current day
            current_date = datetime(year, month, day)
            # Check if the current day is the desired day of the week
            if current_date.weekday() == target_day:
                dates.append(current_date)

        return dates, num_days_in_month

    def confirm_timesheet_entry(self):
        self.ensure_one()
        self.status = 'confirm'

    def reset_draft(self):
        self.ensure_one()
        self.status = 'draft'

    def action_open_payslips(self):
        self.ensure_one()
        return {
            "type": "ir.actions.act_window",
            "res_model": "hr.payslip",
            "views": [[False, "tree"], [False, "form"]],
            "domain": [['monthly_timesheet_id', '=', self.id]],
            "name": "Payslips",
        }

    def _compute_payslip_count(self):
        for payslip in self:
            payslips = self.env['hr.payslip'].search([('monthly_timesheet_id', '=', self.id)])
            payslip.payslip_count = len(payslips.ids)

    def salary_register_report(self):
        self.ensure_one()
        file_path = 'Salary register report' + '.xlsx'
        workbook = xlsxwriter.Workbook('/tmp/' + file_path)

        title_format = workbook.add_format(
            {'border': 1, 'bold': True, 'valign': 'vcenter', 'align': 'center', 'font_size': 11, 'bg_color': '#D8D8D8'})

        formate_1 = workbook.add_format({'bold': True, 'align': 'center', 'font_size': 8, 'bg_color':'#f79586'})
        formate_2 = workbook.add_format({'bold': True, 'align': 'center', 'font_size': 8, 'bg_color': '#f2da91'})
        date_style = workbook.add_format({'text_wrap': True, 'num_format': 'dd-mm-yyyy'})

        sheet = workbook.add_worksheet('Salary Register Report')

        row = 2
        col = 0
        sheet.set_column(0, 75, 15)
        row_t = 0
        col_t = 0
        data_row = 3
        data_col = 0
        if not self.is_cafu_timesheet:
            sheet.merge_range(row_t, col_t, row_t, col_t + 3, "Site: %s" %(self.location_id.name), title_format)
            sheet.merge_range(row_t+1, col_t, row_t+1, col_t + 3, "Direct Staff Salary for %s / %s" %(self.month, self.year), title_format)
            sheet.merge_range(row_t, col_t+4, row_t + 1, col_t + 35, "Salary Register Report For %s - %s" % (self.month, self.year), title_format)
            sheet.write(row, col, 'Employee ID', formate_2)
            sheet.write(row, col+1, 'Employee Name', formate_2)
            sheet.write(row, col+2, 'Project Normal Working Hours', formate_2)
            sheet.write(row, col+3, 'Original Basic', formate_2)
            sheet.write(row, col+4, 'Payable Basic', formate_2)
            sheet.write(row, col+5, 'Original HRA', formate_2)
            sheet.write(row, col+6, 'Payable HRA', formate_2)
            sheet.write(row, col+7, 'Original Other Allowance', formate_2)
            sheet.write(row, col+8, 'Payable Other Allowance', formate_2)
            sheet.write(row, col+9, 'Original Food Allowance', formate_2)
            sheet.write(row, col+10, 'Payable Food Allowance', formate_2)
            sheet.write(row, col+11, 'Original Site Allowance', formate_2)
            sheet.write(row, col+12, 'Payable Site Allowance', formate_2)
            sheet.write(row, col+13, 'Original Supervisory Allowance', formate_2)
            sheet.write(row, col+14, 'Payable Supervisory Allowance', formate_2)
            sheet.write(row, col+15, 'Original Transport Allowance', formate_2)
            sheet.write(row, col+16, 'Payable Transport Allowance', formate_2)
            sheet.write(row, col+17, 'Fixed OT Allowance', formate_2)
            sheet.write(row, col+18, 'Monthly Fixed OT Allowance', formate_2)
            sheet.write(row, col+19, 'Monthly Air Ticket', formate_2)
            sheet.write(row, col+20, 'Gross Salary', formate_2)
            sheet.write(row, col+21, 'Gross Payable', formate_1)
            sheet.write(row, col+22, 'Arrears', formate_2)
            sheet.write(row, col+23, 'Other Salary', formate_2)
            sheet.write(row, col+24, 'Normal OT', formate_2)
            sheet.write(row, col+25, 'Payable Normal OT', formate_2)
            sheet.write(row, col+26, 'Holiday OT Hours', formate_2)
            sheet.write(row, col+27, 'Holiday OT Payable- As per Company policy', formate_2)
            # newly added
            sheet.write(row, col+28, 'Holiday OT Payable- As per labor law', formate_2)
            sheet.write(row, col+29, 'Friday OT Hours', formate_2)
            sheet.write(row, col+30, 'Friday OT Payable', formate_2)
            sheet.write(row, col+31, 'Non Performed PH OT Days', formate_2)
            sheet.write(row, col+32, 'Non-performed PH OT - Payable', formate_2)
            sheet.write(row, col+33, 'Ramadan OT Hours', formate_2)
            sheet.write(row, col+34, 'Ramadan OT Payable', formate_2)
            sheet.write(row, col+35, 'Total Allowance', formate_1)
            # sheet.write(row, col+36, 'Salary Advance Deduction', formate_2)
            # sheet.write(row, col+37, 'Cash Advance Deduction', formate_2)
            # sheet.write(row, col+38, 'Telephone excess usage', formate_2)
            # sheet.write(row, col+39, 'Immigration Fine', formate_2)
            # sheet.write(row, col+40, 'Other Deduction', formate_2)
            # sheet.write(row, col+41, 'Change Status', formate_2)
            # sheet.write(row, col+42, 'Traffic Fine', formate_2)
            # sheet.write(row, col+43, 'Emirates ID Deduction', formate_2)
            # sheet.write(row, col+44, 'Warning Letter Ded', formate_2)
            # sheet.write(row, col+45, 'Total Deduction', formate_2)
            sheet.write(row, col+36, 'Net Salary', formate_2)
            sheet.write(row, col+37, 'Remarks', formate_2)
            sheet.write(row, col+38, 'Special Site Allowances', formate_2)
            sheet.write(row, col+39, 'Idle Days', formate_2)
            sheet.write(row, col+40, 'Idle Days Payable amount', formate_2)
            sheet.write(row, col+41, 'Sick leaves', formate_2)
            sheet.write(row, col+42, 'Sick leaves Payable Amount', formate_2)
            sheet.write(row, col+43, 'Firstcry SOT(paid by client)', formate_2)
            sheet.write(row, col+44, 'Momz world Adjustments (Additions)', formate_2)
            sheet.write(row, col+45, 'OJT And Reliever Salary', formate_2)
            sheet.write(row, col+46, 'Transcorp addition (TS addition & 26 & 30 days payroll variation amount) & Client bonus', formate_2)
            sheet.write(row, col+47, 'ADHOC  HEP Allowances', formate_2)
            sheet.write(row, col+48, 'First Cry Helper Incentive-Feb - As per TS', formate_2)
            sheet.write(row, col+49, 'Tropoverse Salary Adjustment &  incentive', formate_2)
            sheet.write(row, col+50, 'Client Provided -Staff Bonus/ Allowance', formate_2)
            sheet.write(row, col+51, 'Kuber Purpleberry Kitchen Staff - 26 & 30 Calander Days Variation', formate_2)
            sheet.write(row, col+52, 'Happly Plater- Resigned Staff Com off Adjustment', formate_2)
            sheet.write(row, col+53, 'Kanee Driver OT- CLient Provide', formate_2)
            sheet.write(row, col+54, 'Mumz world & GLS -BACK OFFICE DRIVER- Telephone Allowance', formate_2)
            sheet.write(row, col+55, 'Eyewa TS -Adjustment(Site Allowance & OT)', formate_2)
            sheet.write(row, col+56, 'Motion Gate Staff - 26 & 30 Calander Days Variation', formate_2)
            sheet.write(row, col+57, 'CAFU Labor Staff - Jan-24 Incentive - provided by CAFU', formate_2)
            sheet.write(row, col+58, 'Mumz world Staff - Leave salary & Attendance based salary Adjustment', formate_2)
            sheet.write(row, col+59, 'RMA HRA & 26 & 30 calendar days Variation amount- FEB-', formate_2)
            sheet.write(row, col+60, 'Total Addition', formate_2)
            sheet.write(row, col+61, 'Mumz World Adjustments (Deduction)', formate_2)
            sheet.write(row, col+62, 'Transcorp Deduction (TS Deduction & 26 & 30 days payroll variation amount) '
                                     '& Client TS deduction', formate_2)
            sheet.write(row, col+63, 'First Cry Deduction - Absentism', formate_2)
            sheet.write(row, col+64, 'CAFU Labor Staff Timesheet mentioned-  Deduction ', formate_2)
            sheet.write(row, col+65, 'Food Deduction - Soft Services Staff -Securiguard Middle East', formate_2)
            sheet.write(row, col+66, 'Total Deduction', formate_2)
            sheet.write(row, col+67, 'Net Payable', formate_2)
            sheet.write(row, col+68, 'Remarks', formate_2)
            sheet.write(row, col+69, 'Remarks2', formate_2)
            sheet.write(row, col+70, 'WPS%', formate_2)
            sheet.write(row, col+71, 'Joining Date', formate_2)

            timesheets = self.env['employee.timesheet'].search([('monthly_timesheet_id', '=', self.id)])
            sorted_reader = sorted(timesheets, key=lambda d: (d['employee_id'], d['product_id']))
            groups = itertools.groupby(sorted_reader, key=lambda d: (d['employee_id'], d['date'], d['product_id'], d['hour_prefix'], d['hour_spent']))
            employees = []
            for rec in groups:
                if rec[0][0].id not in employees:
                    employees.append(rec[0][0].id)

            if self.company_id.holidays:
                holidays = self.get_dates_by_day_name(int(self.year), int(self.month),
                                                      self.company_id.holidays.capitalize())
                if holidays:
                    total_working_calendar_days = holidays[1] - len(holidays[0])
                    normal_total_overtime = holiday_days = friday_ot_hours = idle_days = idle_hours = on_job_training_hours = shift_days \
                        = payable_gross_amount = reliever_hours = holiday_ot_hours = ramadan_ot_hours = \
                        special_site_allowance = total_working_hours = except_ot = total_allowance = \
                        non_performed_ph_ot_days = sick_leave = j12 = r12 = j9 = r9 = j12_days = r12_days = r9_days = \
                        j9_days = worked_days = total_addition = total_deduction = 0

                    for emp in employees:
                        employee = self.env['hr.employee'].browse(emp)
                        employee_timesheet = self.env['employee.timesheet'].search(
                            [('monthly_timesheet_id', '=', self.id), ('employee_id', '=', emp)])
                        for time in employee_timesheet:
                            if time.hour_prefix:
                                if time.hour_prefix == 'o':
                                    # friday ot hours
                                    friday_ot_hours += time.hour_spent
                                elif time.hour_prefix == 'e' and time.hour_spent != 0.0:
                                    idle_hours += time.hour_spent
                                elif time.hour_prefix == 'e':
                                    idle_days += 1
                                elif time.hour_prefix == 'j' and time.hour_spent == 12:
                                    j12 += time.hour_spent
                                    j12_days += 1
                                elif time.hour_prefix == 'j' and time.hour_spent == 9:
                                    j9 += time.hour_spent
                                    j9_days += 1
                                elif time.hour_prefix == 's' and time.hour_spent != 0.0:
                                    sick_leave += time.hour_spent
                                elif time.hour_prefix == 's':
                                    shift_days += 1
                                elif time.hour_prefix == 'r' and time.hour_spent == 12:
                                    r12 += time.hour_spent
                                    r12_days += 1
                                elif time.hour_prefix == 'r' and time.hour_spent == 9:
                                    r9 += time.hour_spent
                                    r9_days += 1
                                elif time.hour_prefix == 'h' and time.hour_spent != 0.0:
                                    holiday_ot_hours += time.hour_spent
                                    holiday_days += 1
                                elif time.hour_prefix == 'h':
                                    non_performed_ph_ot_days += 1
                            else:
                                if time.hour_spent > self.company_id.normal_working_hours:
                                    normal_ot_hours = time.hour_spent - self.company_id.normal_working_hours
                                    normal_total_overtime += normal_ot_hours
                                    except_ot += (time.hour_spent - normal_ot_hours)
                                else:
                                    except_ot += time.hour_spent
                            if time.hour_spent != 0 or time.hour_prefix:
                                worked_days += 1
                        # ADHOC HEP ALLOWANCE
                        adhoc_hep_allowance = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(lambda x: x.allowance_type_id.code=='ADHEP' and x.employee_id.id == employee.id)]
                        total_adhoc_hep_al = 0
                        if adhoc_hep_allowance:
                            per_day_adhoc_al = sum(adhoc_hep_allowance)/total_working_calendar_days
                            total_adhoc_hep_al = per_day_adhoc_al * worked_days

                        # FIRSTCRY SOT ALLOWANCE PAID BY CLIENT
                        firstcry_allowance = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(lambda x: x.allowance_type_id.code=='FCA' and x.employee_id.id == employee.id)]
                        total_first_cry_allowance = sum(firstcry_allowance)

                        # FiRST CRY HELPER INCENTIVE
                        firstcry_helper_allowance = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(lambda x: x.allowance_type_id.code=='FCHI' and x.employee_id.id == employee.id)]
                        total_first_cry_helper_al = sum(firstcry_helper_allowance)

                        # Tropoverse Salary Adjustment &  incentive
                        tropoverse_salary_adjustment = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(lambda x: x.allowance_type_id.code=='TSAI' and x.employee_id.id == employee.id)]
                        total_tsai_al = sum(tropoverse_salary_adjustment)

                        # Client Provided -Staff Bonus/ Allowance
                        client_provided_al = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                            lambda x: x.allowance_type_id.code == 'CPSBA' and x.employee_id.id == employee.id)]
                        total_cpsba_al = sum(client_provided_al)

                        # Happly Plater- Resigned Staff Com off Adjustment
                        happly_platter_al = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                            lambda x: x.allowance_type_id.code == 'HPRS' and x.employee_id.id == employee.id)]
                        total_hprs_al = sum(happly_platter_al)

                        # Kanee Driver OT- CLient Provide
                        kanee_driver_ot = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                            lambda x: x.allowance_type_id.code == 'KDOT' and x.employee_id.id == employee.id)]
                        total_kdot_al = sum(kanee_driver_ot)

                        # Mumz world & GLS -BACK OFFICE DRIVER-Telephone Allowance
                        back_office_driver_al = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                            lambda x: x.allowance_type_id.code == 'BODA' and x.employee_id.id == employee.id)]
                        total_boda_al = 0
                        if back_office_driver_al:
                            per_day_boda_al = sum(back_office_driver_al) / total_working_calendar_days
                            total_boda_al = per_day_boda_al * worked_days

                        # Eyewa TS -Adjustment(Site Allowance & OT)
                        eyewa_ts_adjustment = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                            lambda x: x.allowance_type_id.code == 'ETSA' and x.employee_id.id == employee.id)]
                        total_etsa_al = sum(eyewa_ts_adjustment)

                        # CAFU Labor Staff - Jan-24 Incentive - provided by CAFU
                        cafu_labour_staff = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                            lambda x: x.allowance_type_id.code == 'CAFULS' and x.employee_id.id == employee.id)]
                        total_cafuls_al = sum(cafu_labour_staff)

                        # Mumz World Adjustments (Deduction)
                        mumz_world_adjustment_deduction = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                            lambda x: x.allowance_type_id.code == 'MWAD' and x.employee_id.id == employee.id)]
                        total_mwad = sum(mumz_world_adjustment_deduction)

                        # First Cry Deduction - Absentism
                        first_cry_deduction = [i.amount for i in
                                                           self.mapped('monthly_allowance_ids').filtered(
                                                               lambda
                                                                   x: x.allowance_type_id.code == 'FCD' and x.employee_id.id == employee.id)]
                        total_first_cry_deduction = sum(first_cry_deduction)

                        # CAFU Labor Staff Timesheet mentioned-Deduction
                        cafu_labour_staff_deduction = [i.amount for i in
                                               self.mapped('monthly_allowance_ids').filtered(
                                                   lambda
                                                       x: x.allowance_type_id.code == 'CAFULSD' and x.employee_id.id == employee.id)]
                        total_cafulsd = sum(cafu_labour_staff_deduction)

                        # Food Dedcution - Soft Services Staff -Securiguard Middle East
                        food_deduction_ss = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                            lambda x: x.allowance_type_id.code == 'FDSS' and x.employee_id.id == employee.id)]
                        total_fdss_al = 0
                        if food_deduction_ss:
                            per_day_fdss_al = sum(food_deduction_ss) / total_working_calendar_days
                            total_fdss_al = per_day_fdss_al * worked_days


                        sheet.write(data_row, data_col, employee.employee_sequence)
                        sheet.write(data_row, data_col + 1, employee.name)
                        sheet.write(data_row, data_col + 2, except_ot)
                        # Basic Salary
                        sheet.write(data_row, data_col + 3, employee.designation_id.wage)
                        # Computed Basic Salary
                        sheet.write(data_row, data_col + 4, employee.designation_id.wage * (
                                    (except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.wage * (
                                    (except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # HRA
                        sheet.write(data_row, data_col + 5,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'HRA').amount)
                        # Computed HRA
                        sheet.write(data_row, data_col + 6,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'HRA').amount * ((
                                                                                         except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'HRA').amount * ((
                                                                                         except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # OA
                        sheet.write(data_row, data_col + 7,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'OA').amount)
                        # Computed Other Allowance
                        sheet.write(data_row, data_col + 8,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'OA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))

                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'OA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # FA
                        sheet.write(data_row, data_col + 9,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FA').amount)

                        # Computed Food Allowance
                        sheet.write(data_row, data_col + 10,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # Site Allowance
                        sheet.write(data_row, data_col + 11,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SA').amount)

                        # Computed Site Allowance
                        sheet.write(data_row, data_col + 12,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # Supervisory Allowance
                        sheet.write(data_row, data_col + 13,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SVA').amount)

                        # Computed SVA
                        sheet.write(data_row, data_col + 14,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SVA').amount * ((
                                                                                     except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SVA').amount * ((
                                                                                     except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # TA
                        sheet.write(data_row, data_col + 15,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'TA').amount)

                        # Computed TA
                        sheet.write(data_row, data_col + 16,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'TA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'TA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # Fixed OT Allowance
                        sheet.write(data_row, data_col + 17,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FOTA').amount)

                        # Computed Fixed OT Allowance
                        sheet.write(data_row, data_col + 18,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FOTA').amount * ((
                                                                                      except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))

                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FOTA').amount * ((
                                                                                      except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # Monthly air ticket allowance
                        sheet.write(data_row, data_col + 19, 0.0)
                        # Total Gross amount
                        total_gross_amount = employee.designation_id.wage + sum(
                            [i.amount for i in employee.designation_id.mapped('allowance_type_ids')])
                        sheet.write(data_row, data_col + 20, total_gross_amount)
                        sheet.write(data_row, data_col + 21, payable_gross_amount)
                        sheet.write(data_row, data_col + 22, 0)
                        sheet.write(data_row, data_col + 23, 0)
                        sheet.write(data_row, data_col + 24, normal_total_overtime)
                        sheet.write(data_row, data_col + 25, (employee.designation_id.wage/(total_working_calendar_days))* (normal_total_overtime/self.company_id.normal_working_hours) * (employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PNOT').amount))
                        total_allowance += (employee.designation_id.wage/(total_working_calendar_days))* (normal_total_overtime/self.company_id.normal_working_hours) * (employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PNOT').amount)
                        sheet.write(data_row, data_col + 26, holiday_ot_hours)
                        sheet.write(data_row, data_col + 27, ((employee.designation_id.wage + employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'OA').amount)/total_working_calendar_days/self.company_id.normal_working_hours*(employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PHOT').amount)*holiday_ot_hours))
                        total_allowance += ((employee.designation_id.wage + employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'OA').amount)/total_working_calendar_days/self.company_id.normal_working_hours*(employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PHOT').amount)*holiday_ot_hours)
                        # newly added raw
                        sheet.write(data_row, data_col + 28, ((total_gross_amount/total_working_calendar_days)*holiday_days)+(employee.designation_id.wage/total_working_calendar_days/self.company_id.normal_working_hours*(employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PHOT').amount)*holiday_ot_hours))
                        sheet.write(data_row, data_col + 29, friday_ot_hours)
                        sheet.write(data_row, data_col + 30, (employee.designation_id.wage/(total_working_calendar_days* self.company_id.normal_working_hours))*(friday_ot_hours/self.company_id.normal_working_hours) * (employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PFOT').amount))
                        total_allowance += (employee.designation_id.wage/(total_working_calendar_days* self.company_id.normal_working_hours))*(friday_ot_hours/self.company_id.normal_working_hours) * (employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PFOT').amount)
                        # sheet.write(data_row, data_col + 31, holiday_ot_hours/self.company_id.normal_working_hours)
                        sheet.write(data_row, data_col + 31, non_performed_ph_ot_days)
                        sheet.write(data_row, data_col + 32, (employee.designation_id.wage/total_working_calendar_days)*non_performed_ph_ot_days)
                        total_allowance += (employee.designation_id.wage/total_working_calendar_days) * non_performed_ph_ot_days
                        ramadan_ot_hours = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(lambda x: x.allowance_type_id.code=='ROH' and x.employee_id.id == employee.id)]
                        sheet.write(data_row, data_col+33, sum(ramadan_ot_hours))

                        # In the below line ramadan ot hours can be calculated by (basic salary + original other
                        # allowance)/ (total_working_calendar_days / normal_working_hours) * payable
                        # ramadan ot fraction amount
                        sheet.write(data_row, data_col+34, (employee.designation_id.wage+employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'OA').amount)/(total_working_calendar_days/self.company_id.normal_working_hours) * (employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PROT').amount)*sum(ramadan_ot_hours))

                        total_allowance += (employee.designation_id.wage+employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'OA').amount)/(total_working_calendar_days/self.company_id.normal_working_hours) * (employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PROT').amount)*sum(ramadan_ot_hours)
                        sheet.write(data_row, data_col + 35, total_allowance)

                        # After Deduction Part Start with Remarks

                        sheet.write(data_row, data_col + 36, total_allowance)
                        sheet.write(data_row, data_col + 37, employee.department_id.name)
                        special_site_allowance = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(lambda x: x.allowance_type_id.code=='SSA' and x.employee_id.id == employee.id)]
                        sheet.write(data_row, data_col + 38, sum(special_site_allowance)*(except_ot/self.company_id.normal_working_hours))
                        total_addition += sum(special_site_allowance)*(except_ot/self.company_id.normal_working_hours)
                        sheet.write(data_row, data_col + 39, idle_days + idle_hours/(self.company_id.normal_working_hours))
                        sheet.write(data_row, data_col + 40, ((employee.designation_id.wage)/total_working_calendar_days)*idle_days + (total_gross_amount/total_working_calendar_days)*(idle_hours/self.company_id.normal_working_hours))
                        total_addition += ((employee.designation_id.wage)/total_working_calendar_days)*idle_days + (total_gross_amount/total_working_calendar_days)*(idle_hours/self.company_id.normal_working_hours)
                        sheet.write(data_row, data_col + 41, shift_days + (sick_leave/self.company_id.normal_working_hours))
                        sheet.write(data_row, data_col + 42,
                                    ((employee.designation_id.wage) / total_working_calendar_days) * shift_days + (total_gross_amount/total_working_calendar_days)*(sick_leave/self.company_id.normal_working_hours))
                        total_addition += ((employee.designation_id.wage) / total_working_calendar_days) * shift_days + (total_gross_amount/total_working_calendar_days)*(sick_leave/self.company_id.normal_working_hours)
                        # First CRY ALLOWANCE PAID BY CLIENT
                        sheet.write(data_row, data_col+43, total_first_cry_allowance)
                        total_addition += total_first_cry_allowance
                        # Momz world adjustments value need to compute
                        sheet.write(data_row, data_col+44, 0)
                        # OJT AND RELIEVER SALARY
                        j12 += (total_gross_amount+(employee.designation_id.wage/(total_working_calendar_days))* (normal_total_overtime/self.company_id.normal_working_hours) * (employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PNOT').amount))/total_working_calendar_days * j12_days
                        r12 += (total_gross_amount+(employee.designation_id.wage/(total_working_calendar_days))* (normal_total_overtime/self.company_id.normal_working_hours) * (employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PNOT').amount))/total_working_calendar_days * r12_days
                        j9 += (total_gross_amount/total_working_calendar_days * j9_days)
                        r9 += (total_gross_amount/total_working_calendar_days * r9_days)

                        sheet.write(data_row, data_col+45, j12+r12+j9+r9)
                        total_addition += j12+r12+r9+r12
                        # Clarification Required
                        sheet.write(data_row, data_col+46, 0)
                        # ADHOC HEP ALLOWANCE
                        sheet.write(data_row, data_col+47, total_adhoc_hep_al)
                        total_addition += total_adhoc_hep_al
                        # First CRY HELPER INCENTIVES
                        sheet.write(data_row, data_col+48, total_first_cry_helper_al)
                        total_addition += total_first_cry_helper_al
                        # Tropoverse Salary Adjustment &  incentive
                        sheet.write(data_row, data_col+49, total_tsai_al)
                        total_addition += total_tsai_al
                        # Client provided staff bonus/allowance
                        sheet.write(data_row, data_col+50, total_cpsba_al)
                        total_addition += total_cpsba_al
                        # Clarification required
                        sheet.write(data_row, data_col+51, 0)
                        # Happly Plater- Resigned Staff Com off Adjustment
                        sheet.write(data_row, data_col+52, total_hprs_al)
                        total_addition += total_hprs_al
                        # Kanee Driver OT- CLient Provide
                        sheet.write(data_row, data_col+53, total_kdot_al)
                        total_addition += total_kdot_al
                        # Mumz world & GLS -BACK OFFICE DRIVER-Telephone Allowance
                        sheet.write(data_row, data_col+54, total_boda_al)
                        total_addition += total_boda_al
                        # Eyewa TS -Adjustment(Site Allowance & OT)
                        sheet.write(data_row, data_col+55, total_etsa_al)
                        total_addition += total_etsa_al
                        # Motion Gate Staff - 26 & 30 Calendar Days Variation (Clarification required)
                        sheet.write(data_row, data_col+56, 0)
                        # CAFU Labor Staff - Jan-24 Incentive - provided by CAFU
                        sheet.write(data_row, data_col+57, total_cafuls_al)
                        total_addition += total_cafuls_al
                        # Mumz world Staff - Leave salary & Attendance based salary Adjustment(Clarification required)
                        sheet.write(data_row, data_col+58, 0)

                        # RMA HRA & 26 & 30 calendar days Variation amount- FEB-(Clarification required)
                        sheet.write(data_row, data_col+59, 0)
                        # Total Addition
                        sheet.write(data_row, data_col+60, total_addition)
                        # Mumz World Adjustments (Deduction)
                        sheet.write(data_row, data_col+61, total_mwad)
                        total_deduction += total_mwad
                        # Trans corp Deduction (TS Deduction & 26 & 30 days payroll variation amount) &
                        # Client TS deduction Clarification required
                        sheet.write(data_row, data_col+62, 0)
                        # First Cry Deduction - Absentism
                        sheet.write(data_row, data_col+63, total_first_cry_deduction)
                        total_deduction += total_first_cry_deduction
                        # CAFU Labor Staff Timesheet mentioned-Deduction
                        sheet.write(data_row, data_col+64, total_cafulsd)
                        total_deduction += total_cafulsd
                        # Food Deduction - Soft Services Staff -Securiguard Middle East
                        sheet.write(data_row, data_col+65, total_fdss_al)
                        total_deduction += total_fdss_al
                        # Total Deduction
                        sheet.write(data_row, data_col+66, total_deduction)

                        sheet.write(data_row, data_col+67, 'Net Payable')
                        if employee.active:
                            sheet.write(data_row, data_col + 68, 'Active')
                        else:
                            sheet.write(data_row, data_col + 68, 'Inactive')
                        sheet.write(data_row, data_col + 69, 'Remarks2')
                        sheet.write(data_row, data_col + 70, 'WPS%')
                        sheet.write(data_row, data_col + 71, employee.date_of_joining if employee.date_of_joining else '', date_style)

                        data_row += 1
                        normal_total_overtime = holiday_days = friday_ot_hours = idle_days = idle_hours = on_job_training_hours = shift_days \
                            = payable_gross_amount = reliever_hours = holiday_ot_hours = ramadan_ot_hours = \
                            special_site_allowance = total_working_hours = except_ot = total_allowance = \
                            non_performed_ph_ot_days = sick_leave = j12 = r12 = j9 = r9 = j12_days = r12_days = r9_days = \
                            j9_days = worked_days = total_addition = total_deduction = 0
            workbook.close()
            ex_report = base64.b64encode(open('/tmp/' + file_path, 'rb+').read())
            excel_report_id = self.env['save.excel.report'].create({"document_frame": file_path,
                                                                        "file_name": ex_report})

            return {
                'res_id': excel_report_id.id,
                'name': 'Files to Download',
                'view_type': 'form',
                "view_mode": 'form',
                'view_id': False,
                'res_model': 'save.excel.report',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
        else:
            data_row = 1
            data_col = 0
            row = col = 0
            sheet.write(row, col, 'Employee ID', formate_2)
            sheet.write(row, col + 1, 'CAFU ID', formate_2)
            sheet.write(row, col + 2, 'Employee Name', formate_2)
            sheet.write(row, col + 3, 'Project Normal Working Hours', formate_2)
            sheet.write(row, col + 4, 'Original Basic', formate_2)
            sheet.write(row, col + 5, 'Payable Basic', formate_2)
            sheet.write(row, col + 6, 'Original HRA', formate_2)
            sheet.write(row, col + 7, 'Payable HRA', formate_2)
            sheet.write(row, col + 8, 'Original Other Allowance', formate_2)
            sheet.write(row, col + 9, 'Payable Other Allowance', formate_2)
            sheet.write(row, col + 10, 'Original Food Allowance', formate_2)
            sheet.write(row, col + 11, 'Payable Food Allowance', formate_2)
            sheet.write(row, col + 12, 'Original Site Allowance', formate_2)
            sheet.write(row, col + 13, 'Payable Site Allowance', formate_2)
            sheet.write(row, col + 14, 'Original Supervisory Allowance', formate_2)
            sheet.write(row, col + 15, 'Payable Supervisory Allowance', formate_2)
            sheet.write(row, col + 16, 'Original Transport Allowance', formate_2)
            sheet.write(row, col + 17, 'Payable Transport Allowance', formate_2)
            sheet.write(row, col + 18, 'Fixed OT Allowance', formate_2)
            sheet.write(row, col + 19, 'Monthly Fixed OT Allowance', formate_2)
            sheet.write(row, col + 20, 'Monthly Air ticket', formate_2)
            sheet.write(row, col + 21, 'Gross Salary', formate_1)
            sheet.write(row, col + 22, 'Gross Payable', formate_1)
            sheet.write(row, col + 23, 'Arrears', formate_2)
            sheet.write(row, col + 24, 'Other Salary', formate_2)
            sheet.write(row, col + 25, 'Final OT1 (@ 1.25) - Hours', formate_2)
            sheet.write(row, col + 26, 'Final OT1 (@ 1.25) - Payable', formate_2)
            sheet.write(row, col + 27, 'Final OT2 (@ 1.50) - Hours', formate_2)
            sheet.write(row, col + 28, 'Final OT2 (@ 1.50) - Payable', formate_2)
            sheet.write(row, col + 29, 'PH OT (@ 1.50) - Hours', formate_2)
            sheet.write(row, col + 30, 'PH OT (@ 1.50)- Payable', formate_2)
            sheet.write(row, col + 31, 'Day Off OT @  2.00 - Hours', formate_2)
            sheet.write(row, col + 32, 'Day Off OT @  2.00 - Payable', formate_2)
            sheet.write(row, col + 33, 'W/P OT @  2.00 - Hours for Pilot', formate_2)
            sheet.write(row, col + 34, 'W/P OT @ 2.00  for Pilot- Payable', formate_2)
            sheet.write(row, col + 35, 'W/P OT @  1.50 - Hours Non Pilot', formate_2)
            sheet.write(row, col + 36, 'W/P OT @ 1.50  for Non Pilot - Payable', formate_2)
            sheet.write(row, col + 37, 'Total OT Payable', formate_2)
            sheet.write(row, col + 38, 'Total Allowance', formate_1)

            # sheet.write(row, col + 40, 'Previous Month Pending OT', formate_2)
            # sheet.write(row, col + 41, 'CAFU-Adjustment', formate_2)
            # sheet.write(row, col + 42, 'Incentives (PBP-Jan 2024)', formate_2)
            # sheet.write(row, col + 43, 'EFS RIDER SICK LEAVE', formate_2)
            # sheet.write(row, col + 44, 'Cafu- Bike Riders -Salik Reimbursement', formate_2)
            # sheet.write(row, col + 45, 'Total Additions', formate_2)
            # sheet.write(row, col + 46, 'CAFU-Deductions- (Jan-24)- Timesheet', formate_2)
            # sheet.write(row, col + 47, 'CAFU-Absenteeism Deductions', formate_2)
            # sheet.write(row, col + 48, 'Other Deductions (Previous month outstanding- EMI )', formate_2)
            # sheet.write(row, col + 40, 'Total Deduction', formate_2)
            # sheet.write(row, col + 41, 'Net Payable', formate_2)
            # sheet.write(row, col + 42, 'Remarks', formate_2)
            # sheet.write(row, col + 43, 'Remarks - Deductions Category (Timesheet)', formate_2)
            # sheet.write(row, col + 44, 'Adjustment  Remarks- Timesheet', formate_2)
            # sheet.write(row, col + 45, 'WPS %', formate_2)
            # sheet.write(row, col + 46, 'Joining Date', formate_2)

            timesheets = self.env['employee.timesheet'].search([('monthly_timesheet_id', '=', self.id)])
            sorted_reader = sorted(timesheets, key=lambda d: (d['employee_id'], d['product_id']))
            groups = itertools.groupby(sorted_reader, key=lambda d: (
            d['employee_id'], d['date'], d['product_id'], d['hour_prefix'], d['hour_spent']))
            employees = []
            for rec in groups:
                if rec[0][0].id not in employees:
                    employees.append(rec[0][0].id)

            final_ot_one = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                lambda x: x.allowance_type_id.code == 'FOT1' and x.employee_id.id == employee.id)]
            total_final_ot_one = sum(final_ot_one)

            final_ot_two = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                lambda x: x.allowance_type_id.code == 'FOT2' and x.employee_id.id == employee.id)]
            total_final_ot_two = sum(final_ot_two)

            phot_hours = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                lambda x: x.allowance_type_id.code == 'PHOTCAFU' and x.employee_id.id == employee.id)]

            total_phot_hours = sum(phot_hours)

            day_off_ot_hours = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                lambda x: x.allowance_type_id.code == 'DOFFOT' and x.employee_id.id == employee.id)]
            total_day_off_ot = sum(day_off_ot_hours)

            wp_ot_hours_pilot = [i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                lambda x: x.allowance_type_id.code == 'WOTHP' and x.employee_id.id == employee.id)]
            total_wp_ot_hours = sum(wp_ot_hours_pilot)

            wp_nonpilot_ot_hours =[i.amount for i in self.mapped('monthly_allowance_ids').filtered(
                lambda x: x.allowance_type_id.code == 'WPNPOT' and x.employee_id.id == employee.id)]
            total_wp_non_pilot_hours = sum(wp_nonpilot_ot_hours)

            if self.company_id.holidays:
                holidays = self.get_dates_by_day_name(int(self.year), int(self.month),
                                                      self.company_id.holidays.capitalize())
                if holidays:
                    total_working_calendar_days = holidays[1] - len(holidays[0])
                    normal_total_overtime = holiday_days = friday_ot_hours = idle_days = idle_hours = on_job_training_hours = shift_days \
                        = payable_gross_amount = reliever_hours = holiday_ot_hours = ramadan_ot_hours = \
                        special_site_allowance = total_working_hours = except_ot = total_allowance = \
                        non_performed_ph_ot_days = sick_leave = j12 = r12 = j9 = r9 = j12_days = r12_days = r9_days = \
                        j9_days = worked_days = total_addition = total_deduction = 0

                    for emp in employees:
                        employee = self.env['hr.employee'].browse(emp)
                        employee_timesheet = self.env['employee.timesheet'].search(
                            [('monthly_timesheet_id', '=', self.id), ('employee_id', '=', emp)])
                        for time in employee_timesheet:
                            if time.hour_prefix:
                                if time.hour_prefix == 'o':
                                    # friday ot hours
                                    friday_ot_hours += time.hour_spent
                                elif time.hour_prefix == 'e' and time.hour_spent != 0.0:
                                    idle_hours += time.hour_spent
                                elif time.hour_prefix == 'e':
                                    idle_days += 1
                                elif time.hour_prefix == 'j' and time.hour_spent == 12:
                                    j12 += time.hour_spent
                                    j12_days += 1
                                elif time.hour_prefix == 'j' and time.hour_spent == 9:
                                    j9 += time.hour_spent
                                    j9_days += 1
                                elif time.hour_prefix == 's' and time.hour_spent != 0.0:
                                    sick_leave += time.hour_spent
                                elif time.hour_prefix == 's':
                                    shift_days += 1
                                elif time.hour_prefix == 'r' and time.hour_spent == 12:
                                    r12 += time.hour_spent
                                    r12_days += 1
                                elif time.hour_prefix == 'r' and time.hour_spent == 9:
                                    r9 += time.hour_spent
                                    r9_days += 1
                                elif time.hour_prefix == 'h' and time.hour_spent != 0.0:
                                    holiday_ot_hours += time.hour_spent
                                    holiday_days += 1
                                elif time.hour_prefix == 'h':
                                    non_performed_ph_ot_days += 1
                            else:
                                if time.hour_spent > self.company_id.normal_working_hours:
                                    normal_ot_hours = time.hour_spent - self.company_id.normal_working_hours
                                    normal_total_overtime += normal_ot_hours
                                    except_ot += (time.hour_spent - normal_ot_hours)
                                else:
                                    except_ot += time.hour_spent
                            if time.hour_spent != 0 or time.hour_prefix:
                                worked_days += 1
                        sheet.write(data_row, data_col, employee.employee_sequence)
                        sheet.write(data_row, data_col + 1, time.cafu_id)
                        sheet.write(data_row, data_col + 2, employee.name)
                        sheet.write(data_row, data_col + 3, except_ot)
                        # Basic Salary
                        sheet.write(data_row, data_col + 4, employee.designation_id.wage)
                        # Computed Basic Salary
                        sheet.write(data_row, data_col + 5,  employee.designation_id.wage * (
                                    (except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.wage * (
                                (except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # HRA
                        sheet.write(data_row, data_col + 6,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'HRA').amount)
                        # Computed HRA
                        sheet.write(data_row, data_col + 7,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'HRA').amount * ((
                                                                                     except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                            lambda x: x.code == 'HRA').amount * ((
                                                                         except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # OA
                        sheet.write(data_row, data_col + 8,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'OA').amount)
                        # Computed Other Allowance
                        sheet.write(data_row, data_col + 9,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'OA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))

                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                            lambda x: x.code == 'OA').amount * ((
                                                                        except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # FA
                        sheet.write(data_row, data_col + 10,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FA').amount)

                        # Computed Food Allowance
                        sheet.write(data_row, data_col + 11,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                            lambda x: x.code == 'FA').amount * ((
                                                                        except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # Site Allowance
                        sheet.write(data_row, data_col + 12,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SA').amount)

                        # Computed Site Allowance
                        sheet.write(data_row, data_col + 13,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                            lambda x: x.code == 'SA').amount * ((
                                                                        except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # Supervisory Allowance
                        sheet.write(data_row, data_col + 14,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SVA').amount)

                        # Computed SVA
                        sheet.write(data_row, data_col + 15,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'SVA').amount * ((
                                                                                     except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                            lambda x: x.code == 'SVA').amount * ((
                                                                         except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # TA
                        sheet.write(data_row, data_col + 16,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'TA').amount)

                        # Computed TA
                        sheet.write(data_row, data_col + 17,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'TA').amount * ((
                                                                                    except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))
                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                            lambda x: x.code == 'TA').amount * ((
                                                                        except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # Fixed OT Allowance
                        sheet.write(data_row, data_col + 18,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FOTA').amount)

                        # Computed Fixed OT Allowance
                        sheet.write(data_row, data_col + 19,
                                    employee.designation_id.mapped('allowance_type_ids').filtered(
                                        lambda x: x.code == 'FOTA').amount * ((
                                                                                      except_ot / self.company_id.normal_working_hours) / total_working_calendar_days))

                        payable_gross_amount += employee.designation_id.mapped('allowance_type_ids').filtered(
                            lambda x: x.code == 'FOTA').amount * ((
                                                                          except_ot / self.company_id.normal_working_hours) / total_working_calendar_days)
                        # Monthly air ticket allowance
                        sheet.write(data_row, data_col + 20, 0.0)
                        # Total Gross amount
                        total_gross_amount = employee.designation_id.wage + sum(
                            [i.amount for i in employee.designation_id.mapped('allowance_type_ids')])

                        sheet.write(data_row, data_col + 21, total_gross_amount)
                        sheet.write(data_row, data_col + 22, payable_gross_amount)
                        # Arrears and other salary
                        sheet.write(data_row, data_col + 23, 0)
                        sheet.write(data_row, data_col + 24, 0)
                        # Final OT1
                        sheet.write(data_row, data_col + 25, total_final_ot_one)

                        # Payable Final OT1
                        sheet.write(data_row, data_col + 26, employee.designation_id.wage/total_working_calendar_days/except_ot*(employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PFOT1').amount)*total_final_ot_one)

                        # Final OT2
                        sheet.write(data_row, data_col + 27, total_final_ot_two)

                        # Payable Final OT2
                        sheet.write(data_row, data_col + 28, employee.designation_id.wage/total_working_calendar_days/except_ot*(employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PFOT2').amount)*total_final_ot_two)

                        # PHOT @1.50
                        sheet.write(data_row, data_col + 29, total_phot_hours)

                        # PHOT @1.50 Payable
                        sheet.write(data_row, data_col + 30, employee.designation_id.wage/total_working_calendar_days/except_ot*(employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='PHOTCAFU').amount)*total_phot_hours)

                        # DAY OFF OT HOURS
                        sheet.write(data_row, data_col + 31, total_day_off_ot)
                        # Day off OT Payable
                        sheet.write(data_row, data_col + 32, employee.designation_id.wage/total_working_calendar_days/except_ot*(employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='DOFFOT').amount)*total_day_off_ot)

                        # W/P OT@ Hours for Pilot
                        sheet.write(data_row, data_col + 33, total_wp_ot_hours)

                        # W/P OT@ Hours for Pilot Payable
                        sheet.write(data_row, data_col + 34, employee.designation_id.wage/total_working_calendar_days/except_ot*(employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='WOTHP').amount)*total_wp_ot_hours)

                        # W/P OT @1.50 Hours NonPilot
                        sheet.write(data_row, data_col + 35, total_wp_non_pilot_hours)

                        # Payable Non pilot W/P OT
                        sheet.write(data_row, data_col + 36, employee.designation_id.wage/total_working_calendar_days/except_ot*(employee.designation_id.mapped('payable_allowance_ids').filtered(lambda x: x.code=='WPNPOT').amount)*total_wp_non_pilot_hours)

                        # Total Payable
                        sheet.write(data_row, data_col + 37, 'Total Payable')

                        # Total Allowance
                        sheet.write(data_row, data_col + 38, 'Total Allowance')

                        # sheet.write(data_row, data_col + 39, 'Net Salary')
                        # sheet.write(data_row, data_col + 40, 'Previous Month OT')
                        # sheet.write(data_row, data_col + 41, 'CAFU-Adjustment')
                        # sheet.write(data_row, data_col + 42, 'Incentives (PBP-Jan 2024)')
                        # sheet.write(data_row, data_col + 43, 'EFS RIDER SICK LEAVE')
                        # sheet.write(data_row, data_col + 44, 'Cafu- Bike Riders -Salik Reimbursement')
                        # sheet.write(data_row, data_col + 45, 'Total Additions')
                        # sheet.write(data_row, data_col + 46, 'CAFU-Deductions- (Jan-24)- Timesheet')
                        # sheet.write(data_row, data_col + 47, 'CAFU-Absenteeism Deductions')
                        # sheet.write(data_row, data_col + 48, 'Other Deductions (Previous month outstanding- EMI )')


                        data_row += 1
                        normal_total_overtime = holiday_days = friday_ot_hours = idle_days = idle_hours = on_job_training_hours = shift_days \
                            = payable_gross_amount = reliever_hours = holiday_ot_hours = ramadan_ot_hours = \
                            special_site_allowance = total_working_hours = except_ot = total_allowance = \
                            non_performed_ph_ot_days = sick_leave = j12 = r12 = j9 = r9 = j12_days = r12_days = r9_days = \
                            j9_days = worked_days = total_addition = total_deduction = 0
            workbook.close()
            ex_report = base64.b64encode(open('/tmp/' + file_path, 'rb+').read())
            excel_report_id = self.env['save.excel.report'].create({"document_frame": file_path,
                                                                    "file_name": ex_report})
            return {
                'res_id': excel_report_id.id,
                'name': 'Files to Download',
                'view_type': 'form',
                "view_mode": 'form',
                'view_id': False,
                'res_model': 'save.excel.report',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
