# -*- coding: utf-8 -*-

from . import hr_payslip
from . import res_company
from . import hr_contract
from . import monthly_timesheet
