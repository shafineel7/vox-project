# -*- coding: utf-8 -*-

from odoo import models, fields, _
import calendar
from datetime import datetime


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    normal_total_overtime = fields.Float('Normal OT Hours')
    total_worked_days = fields.Integer('Total Worked Days')
    friday_ot_hours = fields.Float('Friday OT Hours')
    idle_work_hours = fields.Float('Idle Work Hours')
    on_job_training_hours = fields.Float('On Job Training Hours')
    shift_hours = fields.Float('Shift Hours')
    reliever_hours = fields.Float('Reliever Hours')
    holiday_ot_hours = fields.Float('Holiday OT Hours')
    ramadan_ot_hours = fields.Float('Ramadan OT Hours')
    normal_working_hours = fields.Float('Normal Working Hours')
    total_working_calendar_days = fields.Float('Working Days')
    monthly_timesheet_id = fields.Many2one('monthly.timesheet', 'Employee Monthly Timesheet')
    normal_ot_days = fields.Float('Normal OT Days')
    normal_company_wrkg_hrs = fields.Float('Normal Company working Hours')

    def get_dates_by_day_name(self, year, month, day_name):
        # Get the day number corresponding to the given day name (0 = Monday, 1 = Tuesday, ..., 6 = Sunday)
        target_day = list(calendar.day_name).index(day_name)

        # Initialize a list to store dates
        dates = []

        # Get the total number of days in the given month
        num_days_in_month = calendar.monthrange(year, month)[1]

        # Iterate through each day in the month
        for day in range(1, num_days_in_month + 1):
            # Create a datetime object for the current day
            current_date = datetime(year, month, day)
            # Check if the current day is the desired day of the week
            if current_date.weekday() == target_day:
                dates.append(current_date)

        return dates, num_days_in_month

    def _compute_worked_days_line_ids(self):
        res = super(HrPayslip, self)._compute_worked_days_line_ids()
        for rec in self:
            if rec.company_id.holidays:
                holidays = self.get_dates_by_day_name(rec.date_from.year, rec.date_from.month,
                                                      rec.company_id.holidays.capitalize())
                if holidays:
                    total_working_calendar_days = holidays[1] - len(holidays[0])
                    employee_timesheet = self.env['monthly.timesheet'].search([('id', '=', rec.monthly_timesheet_id.id), ('month', '=', rec.date_from.month),
                                                                               ('year', '=', rec.date_from.year)]).mapped(
                        'employee_timesheet_ids')
                    normal_total_overtime = friday_ot_hours = idle_work_hours = on_job_training_hours = shift_hours \
                        = reliever_hours = holiday_ot_hours = ramadan_ot_hours = total_working_hours = except_ot = 0
                    for time in employee_timesheet:
                        if time.employee_id.id == rec.employee_id.id:
                            if time.hour_prefix:
                                if time.hour_prefix == 'o':
                                    # friday ot hours
                                    friday_ot_hours += time.hour_spent
                                elif time.hour_prefix == 'i':
                                    idle_work_hours += time.hour_spent
                                elif time.hour_prefix == 'j':
                                    on_job_training_hours += time.hour_spent
                                elif time.hour_prefix == 's':
                                    shift_hours += time.hour_spent
                                elif time.hour_prefix == 'r':
                                    reliever_hours += time.hour_spent
                                elif time.hour_prefix == 'h':
                                    holiday_ot_hours += time.hour_spent
                                else:
                                    ramadan_ot_hours += time.hour_spent
                            else:
                                if time.hour_spent > rec.company_id.normal_working_hours:
                                    normal_ot_hours = time.hour_spent - rec.company_id.normal_working_hours
                                    normal_total_overtime += normal_ot_hours
                                    except_ot += (time.hour_spent - normal_ot_hours)
                                else:
                                    except_ot += time.hour_spent
                    rec.write({'total_worked_days': except_ot / rec.company_id.normal_working_hours,
                               'friday_ot_hours': friday_ot_hours,
                               'idle_work_hours': idle_work_hours,
                               'on_job_training_hours': on_job_training_hours,
                               'shift_hours': shift_hours,
                               'reliever_hours': reliever_hours,
                               'ramadan_ot_hours': ramadan_ot_hours,
                               'holiday_ot_hours': holiday_ot_hours,
                               'normal_working_hours': except_ot,
                               'normal_total_overtime': normal_total_overtime,
                               'total_working_calendar_days': total_working_calendar_days,
                               'normal_company_wrkg_hrs': rec.company_id.normal_working_hours
                               })
        return res
