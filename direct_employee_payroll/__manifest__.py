# -*- coding: utf-8 -*-

{
    'name': 'Direct Employee Payroll',
    'version': '17.0.1.0.1',
    'category': 'Human Resources',
    'summary': 'Direct Employee Payroll',
    'description': """
        This module allows you to process payroll for direct employees.
    """,
    'author': 'Jishnu K',
    'website': 'https://www.voxtronme.com',
    'depends': ['employee_timesheet'],
    'external_dependencies': {
        'python': [
            'xlsxwriter',
        ],
    },
    'data': [
        'security/ir.model.access.csv',
        'data/hr_salary_rule_data.xml',
        'wizard/res_config_settings_views.xml',
        'views/hr_payslip_views.xml',
        'views/monthly_timesheet_views.xml',
        'views/hr_contract_views.xml',
        'wizard/save_excel_report_views.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,

}
