# -*- coding: utf-8 -*-

from odoo import models, fields, _


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    normal_working_hours = fields.Float('Normal Working Hours', related='company_id.normal_working_hours',
                                        readonly=False)
    holidays = fields.Selection([('sunday', 'Sunday'), ('monday', 'Monday'), ('tuesday', 'Tuesday'),
                                 ('wednesday', 'Wednesday'), ('thursday', 'Thursday'), ('friday', 'Friday'),
                                 ('saturday', 'Saturday')], related='company_id.holidays',
                                        readonly=False, string='Holiday')
    max_working_hr_per_day = fields.Float('Maximum Working Hr/Day', readonly=False, related='company_id.max_working_hr_per_day')

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        company = self.env.user.company_id
        if self.normal_working_hours:
            company.normal_working_hours = self.normal_working_hours
        if self.holidays:
            company.holidays = self.holidays
        if self.max_working_hr_per_day:
            company.max_working_hr_per_day = self.max_working_hr_per_day

