# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class SaveExcelReport(models.TransientModel):
    _name = 'save.excel.report'
    _description = "Save generated excel"

    file_name = fields.Binary('Excel Report File')
    document_frame = fields.Char('File To Download')
