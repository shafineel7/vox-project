# -*- coding: utf-8 -*-

from odoo import api, fields, models, Command, _
import calendar
from odoo.exceptions import UserError


class HrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'

    designation_id = fields.Many2one('designation', 'Designation')

    def default_get(self, fields_list):
        res = super(HrPayslipEmployees, self).default_get(fields_list)
        if self.env.context.get('active_model') == 'monthly.timesheet':
            active_id = self.env.context.get('active_id')
            employees = self.env['employee.timesheet'].search([('monthly_timesheet_id', '=', active_id)]).mapped('employee_id')
            res['employee_ids'] = [Command.link(emp.id) for emp in employees]
        return res

    def compute_sheet(self):
        if self.env.context.get('active_model') == 'monthly.timesheet':
            active_id = self.env.context.get('active_id')
            monthly_timesheet = self.env['monthly.timesheet'].browse(active_id)
            year = monthly_timesheet.year
            month = monthly_timesheet.month
            location = monthly_timesheet.location_id.name
            days_in_month = calendar.monthrange(int(year), int(month))[1]
            payslip_run = self.env['hr.payslip.run'].create({
                'name': 'Payslip for %s / %s-%s' % (month, year, location),
                'date_start': '{}-{}-01'.format(year, month),
                'date_end': '{}-{}-{}'.format(year, month, days_in_month),
            })
            employees = self.with_context(active_test=False).employee_ids
            if not employees:
                raise UserError(_("You must select employee(s) to generate payslip(s)."))
            # Prevent a payslip_run from having multiple payslips for the same employee
            employees -= payslip_run.slip_ids.employee_id
            success_result = {
                'type': 'ir.actions.act_window',
                'res_model': 'hr.payslip.run',
                'views': [[False, 'form']],
                'res_id': payslip_run.id,
            }
            if not employees:
                return success_result
            Payslip = self.env['hr.payslip']
            default_values = Payslip.default_get(Payslip.fields_get())
            payslips_vals = []
            contracts = employees._get_contracts(
                payslip_run.date_start, payslip_run.date_end, states=['open', 'close']
            ).filtered(lambda c: c.active)
            for contract in self._filter_contracts(contracts):
                values = dict(default_values, **{
                    'name': _('New Payslip'),
                    'employee_id': contract.employee_id.id,
                    'payslip_run_id': payslip_run.id,
                    'date_from': payslip_run.date_start,
                    'date_to': payslip_run.date_end,
                    'contract_id': contract.id,
                    'monthly_timesheet_id': self.env.context.get('active_id'),
                    'struct_id': self.structure_id.id or contract.employee_id.designation_id.salary_structure_id.id,
                })
                payslips_vals.append(values)
            if Payslip.search([('monthly_timesheet_id', '=', self.env.context.get('active_id'))]):
                Payslip.unlink()

            payslips = Payslip.with_context(tracking_disable=True).create(payslips_vals)
            payslips._compute_name()
            payslips.compute_sheet()
            payslip_run.state = 'verify'

            return success_result

        else:
            res = super(HrPayslipEmployees, self).compute_sheet()
            return res
