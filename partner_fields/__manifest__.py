{
    "name": "Partner Fields",
    "version": "17.0.1.0.0",
    "author": "aswin",
    "website": "https://www.voxtronme.com",
    "sequence": 80,
    'summary': 'Enhance Odoo Contacts with Fields',
    "description": """
        This module extends the functionality of the Odoo Contacts module by allowing to add fields
        to odoo contacts.
                    """,
    "depends": ["base"],
    'data': [
        'views/res_partner_views.xml',
    ],
    "installable": "True",
    'license': 'LGPL-3',
    'application': 'True'

}
