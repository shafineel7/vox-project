from odoo import models, fields


class ResPartner(models.Model):
    _inherit = "res.partner"

    is_account = fields.Boolean(string="Is an account ?")
