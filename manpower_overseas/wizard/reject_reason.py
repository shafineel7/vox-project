from odoo import models, fields
from odoo.exceptions import ValidationError


class RejectReason(models.TransientModel):
    _inherit = "reject.reason"

    def confirm_action(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model == 'hr.applicant' and active_id:
            application_request = self.env[active_model].browse(active_id)
            rejected = self.env.ref('manpower_overseas.stage_job22')
            # rejected = self.env['hr.recruitment.stage'].search([('sequence', '=', 22)], limit=1)
            if rejected:
                application_request.write({'stage_id': rejected.id,
                                           'is_self_assign': False,
                                           'reject_reason': self.name

                                           })
            else:
                raise ValidationError("Rejected stage not found!")
            application_request.message_post(body=f"Reason for Rejection is {application_request.reject_reason}")
            user_email = application_request.user_id.email
            mail_template = self.env.ref('manpower_overseas.mail_template_hr_application_request')
            mail_template.write({
                'email_to': user_email,
                'email_from': self.env.user.email,
            })
            mail_template.send_mail(application_request.id, force_send=True)

        return super().confirm_action()
