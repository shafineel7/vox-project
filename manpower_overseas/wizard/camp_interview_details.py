# -*- coding: utf-8 -*-
from odoo import models, fields


class CampInterviewDetails(models.TransientModel):
    _name = 'camp.interview.details'
    _description = 'managing information related to camps'

    hiring_location = fields.Char(string='Camp Location')
    hiring_date = fields.Datetime(string='Scheduled Date')
    interviewer_ids = fields.Many2many('res.users', string='Interviewer')
    agent_details_id = fields.Many2one('job.agent.details', )
    hr_job_id = fields.Many2one('hr.job', 'Job Position')

    def action_submit_camp_details(self):
        for record in self:
            camp_details = self.env['camp.details'].create({
                'hiring_location': record.hiring_location,
                'hiring_date': record.hiring_date,
                'hr_job_id': record.hr_job_id.id,
                'agent_details_id': record.agent_details_id.id,
                'interviewer_ids': [(6, 0, record.interviewer_ids.ids)]
            })

            template_id = self.env.ref(
                'manpower_overseas.mail_template_interviewer_assigning_template')
            if template_id:
                template_id.send_mail(record.id, force_send=True)
