# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions
from odoo.exceptions import ValidationError



class JobAgentDetails(models.Model):
    _name = 'job.agent.details'
    _description = 'The Job Agent Master serves as a comprehensive database of information about job agents'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _rec_name = 'agent_id'

    agent_id = fields.Many2one('agent.agent', string='Agent', required=True)
    hr_job_id = fields.Many2one('hr.job', string='Job Position')
    user_id = fields.Many2one('res.users',string="Users",compute='_compute_agent_users',store=True)

    # @api.onchange('agent_id','user_id')
    # def _onchange_agent_id(self):
    #
    #     agent_list = []
    #     for rec in self:
    #         if rec.agent_id:
    #             acc_team_users = self.env['res.users'].search([('agent_id', 'in',[rec.agent_id.id])], limit=1)
    #             agent_list.append(acc_team_users.id)
    #             agent_list = list(set(agent_list))
    #         rec.hr_job_id.agent_user_ids = [(6, 0, agent_list)]


    # is_agent = fields.Boolean(string="Agent",compute='_compute_agent_talent_user',default=False)
    # is_talent_user = fields.Boolean(string="Agent",compute='_compute_agent_talent_user',default=False)
    #
    # @api.depends('agent_id','hr_job_id','user_id')
    # def _compute_agent_talent_user(self):
    #     for rec in self:
    #         if not rec.user_has_groups('base.group_system'):
    #             if rec.user_has_groups('department_groups.group_agent_agent'):
    #                 rec.is_agent = True
    #                 rec.is_talent_user = False
    #             elif rec.user_has_groups('department_groups.group_hr_talent_acquisition_exec'):
    #                 rec.is_talent_user = True
    #                 rec.is_agent = False
    #             else:
    #                 rec.is_agent = False
    #                 rec.is_talent_user = False

    @api.depends('agent_id')
    def _compute_agent_users(self):
        agent_list = []
        for rec in self:
            acc_team_users = self.env['res.users'].search([('agent_id', 'in',
                                                            [rec.agent_id.id])],limit=1)
            agent_list.append(acc_team_users.id)
            agent_list = list(set(agent_list))
            rec.hr_job_id.agent_user_ids = [(6, 0, agent_list)]
            rec.user_id = acc_team_users.id


    def action_notify_agent(self):
        if self.env.user.user_has_groups('department_groups.group_agent_agent'):
            raise ValidationError("You cannot edit Agent Details")
        for rec in self:
            agent_emails = rec.agent_id.child_ids.mapped('email_address')
            template = rec.env.ref('manpower_overseas.mail_template_job_agent_notification', raise_if_not_found=False)
            ctx = {
                'default_model': 'job.agent.details',
                'default_res_ids': rec.ids,
                'default_template_id': template.id,
                'default_email_layout_xmlid': "mail.mail_notification_light",
                'default_composition_mode': 'comment',
                'force_email': True,
                'default_agent_id': rec.agent_id.id,
            }

            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'mail.compose.message',
                'views': [(False, 'form')],
                'view_id': False,
                'target': 'new',
                'context': ctx,
            }

    def open_camp_details(self):
        camp_details = self.env['camp.interview.details'].sudo().create({
            'agent_details_id': self.id,
            'hr_job_id': self.hr_job_id.id,
        })
        return {
            'action': 'camp_details_action',
            'name': 'Camp Details',
            'type': 'ir.actions.act_window',
            'res_model': 'camp.interview.details',
            'view_mode': 'form',
            'target': 'new',
            'res_id': camp_details.id
        }
