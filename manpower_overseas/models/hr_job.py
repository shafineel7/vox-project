# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError




class HrJob(models.Model):
    _inherit = 'hr.job'

    camp_details_ids = fields.One2many('camp.details', inverse_name='hr_job_id')
    job_agent_details_ids = fields.One2many('job.agent.details', inverse_name='hr_job_id')

    agent_user_ids = fields.Many2many('res.users', 'user_hr_rel', 'user_id', 'leave_hr_rel_req_id',string="Agent Users",default=lambda self: self.env.user.ids)



    def write(self, values):
        for vals in values:
            if not self.env.user.user_has_groups('base.group_system'):
                if self.env.user.user_has_groups('department_groups.group_agent_agent'):
                    if not 'camp_details_ids' in vals:
                        raise ValidationError("You cannot edit the Details")
                    if 'camp_details_ids' in vals:
                        if not ('hiring_location' in values['camp_details_ids'][0][2] and values['camp_details_ids'][0][2].get('hiring_location')):
                            raise ValidationError("You cannot edit the Details")
                        if not ('hiring_date' in values['camp_details_ids'][0][2] and values['camp_details_ids'][0][2].get('hiring_date')):
                            raise ValidationError("You cannot edit the Details")
                elif 'job_agent_details_ids' in vals:
                    if self.env.user.user_has_groups('department_groups.group_agent_agent'):
                        if 'agent_id' in values['job_agent_details_ids'][0][2] and values['job_agent_details_ids'][0][2].get('agent_id')  != False:
                            raise ValidationError("You cannot edit Agent Details")
                elif 'camp_details_ids' in vals:
                    if not self.env.user.user_has_groups('department_groups.group_agent_agent'):
                        if 'hiring_location' in values['camp_details_ids'][0][2] and values['camp_details_ids'][0][2].get('hiring_location') != False:
                            raise ValidationError("You cannot edit Camp Details")
                        if 'hiring_date' in values['camp_details_ids'][0][2] and values['camp_details_ids'][0][2].get('hiring_date') != False:
                            raise ValidationError("You cannot edit Camp Details")
                    elif not self.env.user.user_has_groups('department_groups.group_hr_talent_acquisition_exec'):
                        if 'interviewer_ids' in values['camp_details_ids'][0][2] and values['camp_details_ids'][0][2].get('interviewer_ids') != False:
                            raise ValidationError("You cannot edit Camp Details")
                elif 'camp_details_ids' in vals:
                    if not self.env.user.user_has_groups('department_groups.group_agent_agent'):
                        if 'hiring_location' in values['camp_details_ids'][0][2] and values['camp_details_ids'][0][2].get('hiring_location'):
                            raise ValidationError("You cannot edit Camp Details")
                        if 'hiring_date' in values['camp_details_ids'][0][2] and values['camp_details_ids'][0][2].get('hiring_date'):
                            raise ValidationError("You cannot edit Camp Details")
                    elif not self.env.user.user_has_groups('department_groups.group_hr_talent_acquisition_exec'):
                        if 'interviewer_ids' in values['camp_details_ids'][0][2] and values['camp_details_ids'][0][2].get('interviewer_ids'):
                            raise ValidationError("You cannot edit Camp Details")
        return super(HrJob, self).write(values)

