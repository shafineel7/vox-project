# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class AgentAgent(models.Model):
    _inherit = 'agent.agent'


    def action_agent_recruitment(self):

        agent_details_table= self.env['job.agent.details'].search([('agent_id','=',self.id)])
        hr_job = []
        for rec in agent_details_table:
            hr_job.append(rec.hr_job_id.id)
        return {
            "type": "ir.actions.act_window",
            "name": "Job Positions",
            "res_model": "hr.job",
            "view_mode": "tree,form",
            "domain": [("id", "in", hr_job)],
        }
