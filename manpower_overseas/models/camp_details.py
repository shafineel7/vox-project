# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions
from odoo.exceptions import ValidationError


class CampDetails(models.Model):
    _name = 'camp.details'
    _description = 'managing information related to camps'
    _rec_name = 'hiring_location'

    hiring_location = fields.Char(string='Hiring Location')
    hiring_date = fields.Datetime(string='Hiring Date')
    interviewer_ids = fields.Many2many('res.users', string='Interviewer')
    agent_details_id = fields.Many2one('job.agent.details', )
    hr_job_id = fields.Many2one('hr.job', 'Job Position')
    user_id = fields.Many2one('res.users', string="Users", store=True,related='agent_details_id.user_id')
    # is_agent = fields.Boolean(string="Agent", compute='_compute_agent_talent_user', default=False)
    # is_talent_user = fields.Boolean(string="Agent", compute='_compute_agent_talent_user', default=False)
    #
    #
    # @api.depends('hiring_location','hiring_date','interviewer_ids','agent_details_id','hr_job_id')
    # def _compute_agent_talent_user(self):
    #     for rec in self:
    #         if not rec.user_has_groups('base.group_system'):
    #             if rec.user_has_groups('department_groups.group_agent_agent'):
    #                 rec.is_agent = True
    #                 rec.is_talent_user = False
    #             elif rec.user_has_groups('department_groups.group_hr_talent_acquisition_exec'):
    #                 rec.is_talent_user = True
    #                 rec.is_agent = False
    #             else:
    #                 rec.is_agent = False
    #                 rec.is_talent_user = False

    @api.depends('interviewer_ids')
    def _onchange_interviewer_ids(self):
        interview_list = []
        for rec in self:
            for record in rec.interviewer_ids:
                interview_list.append(record.id)
            interview_list = list(set(interview_list))
            rec.hr_job_id.interview_user_ids = [(6, 0, interview_list)]

    def write(self, vals):
        for rec in self:
            res = super(CampDetails, rec).write(vals)
            template_id = rec.env.ref('manpower_overseas.mail_template_interviewer_assignment')
            if template_id and rec.ids:
                template_id.send_mail(rec.ids[0], force_send=True)
        return res
