# -*- coding: utf-8 -*-

from odoo import api, models, fields,_,Command,exceptions
from odoo.exceptions import ValidationError
from datetime import date



class HrApplicant(models.Model):
    _inherit = 'hr.applicant'

    document = fields.Binary(string="Supporting Documents")
    documents_name = fields.Char(string="Supporting Documents Name")
    status = fields.Selection([
        ('accept', 'Accept'),
        ('reject', 'Reject'),
        ('may-be', 'May-be'),
    ], string='Status')
    remarks = fields.Char(string='Remarks')
    is_new_stage = fields.Boolean(string="Is New Stage", compute='_compute_is_new_stage')
    is_letter_issued = fields.Boolean(string="Is Letter Issued", compute='_compute_is_letter_issued')
    is_onboarding = fields.Boolean(string="Is Onboarding", compute='_compute_is_onboarding')
    fitness_certificate = fields.Binary(string='Fitness Certificate')
    certificate_name = fields.Char(string="Certificate Name")
    # agent_id = fields.Many2one('res.users', string='Agent', default=lambda self: self.env.user.agent_id.id)
    agent_id = fields.Many2one('res.users', string='Agent', default=lambda self: self.env.user.agent_id.id,domain=lambda self: [
        ("groups_id", "=", self.env.ref("department_groups.group_agent_agent").id)])
    onhold_reason_note = fields.Char(string="On Hold Reason")
    reject_reason = fields.Char(string="Reject Reason")
    salary_proposed_extra = fields.Char("Proposed Salary Extra", help="Salary Proposed by the Organisation, extra advantages", tracking=True, groups="hr_recruitment.group_hr_recruitment_user,department_groups.group_agent_agent")
    salary_expected_extra = fields.Char("Expected Salary Extra", help="Salary Expected by Applicant, extra advantages", tracking=True, groups="hr_recruitment.group_hr_recruitment_user,department_groups.group_agent_agent")
    salary_proposed = fields.Float("Proposed Salary", group_operator="avg", help="Salary Proposed by the Organisation", tracking=True, groups="hr_recruitment.group_hr_recruitment_user,department_groups.group_agent_agent")
    salary_expected = fields.Float("Expected Salary", group_operator="avg", help="Salary Expected by Applicant", tracking=True, groups="hr_recruitment.group_hr_recruitment_user,department_groups.group_agent_agent")
    signed_offer_letter = fields.Binary(string='Signed offer letter', attachment=True)
    signed_offer_letter_file_name = fields.Char('Signed offer letter Filename')
    cv = fields.Binary(string='CV', attachment=True)
    cv_file_name = fields.Char('CV Filename')
    educational_certificates = fields.Binary(string='Educational certificates', attachment=True)
    educational_certificates_file_name = fields.Char('Educational certificates Filename')


    def action_offer_letter_signed(self):
        res = super().action_offer_letter_signed()
        for rec in self:
            if not rec.signed_offer_letter:
                raise ValidationError("Please Upload the Signed Offer letter")
        return res


    @api.model
    def default_get(self, fields):
        result = super(HrApplicant, self).default_get(fields)
        purchase_lines = []
        active_ids = self.env.context.get('active_id')
        active_ids = active_ids or False
        interview = []
        if self.env.context.get('active_id'):
            hr_job = self.env['hr.job'].browse(active_ids)
            if self.env.user.has_group('department_groups.group_agent_agent'):
                result['agent_id'] = self.env.user.id

            if self.env.user.has_group('department_groups.group_hr_talent_acquisition_exec') or (
                    hr_job.recruitment_type == 'overseas' and self.env.user.has_group(
                'department_groups.group_agent_agent')):
                result = super(HrApplicant, self).default_get(fields)
                i = 1
                if self.env.context.get('active_id'):
                    hr_job = self.env['hr.job'].browse(active_ids)
                    result['name'] = hr_job.name
                    for camp in hr_job.camp_details_ids:
                        if camp.interviewer_ids:
                            for users in camp.interviewer_ids:
                                interview.append(users.id)

                result['interview_user_ids'] = [(6, 0, interview)],

                return result
            else:
                raise exceptions.ValidationError("You cannot Create Applications")

        return result

    # rejected = self.env['hr.recruitment.stage'].search([('sequence', '=', 22)], limit=1)

    def _offer_letter_reminder(self):
        date_now = date.today()
        match = self.search([])
        rejected = self.env.ref('manpower_overseas.stage_job22')
        for rec in match:
            edit_date = rec.write_date.date()
            if (edit_date - rec.offer_letter_issue_date).day>45:
                rec.write({'stage_id': rejected.id,
                        'is_self_assign': False,
                        'reject_reason': rec.name

                        })
            if (edit_date - rec.send_offer_letter_date).day > 30 and rec.status =='may-be':
                template_id = self.env.ref('manpower_overseas.email_template_agent_notification')
                acc_team_users = self.env['res.users'].search([('id', 'in',[rec.aent_id.id])])
                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)
                if template_id and rec.id and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}). \
                        send_mail(rec.id, force_send=True, raise_exception=False)



    #
    # def write(self, vals):
    #
    #     if self.env.user.user_has_groups('department_groups.group_agent_agent') and ('status' in vals or 'remarks' in vals):
    #         raise ValidationError(_("You are not allowed to edit the data."))
    #     else:
    #         return super().write(vals)

    def action_self_assign(self):
        for rec in self:
            rec.self_assignee_ids = False
            rec.assigned_user_ids = False
            rec.assigned_user_ids = [Command.link(rec.env.user.id)]
            rec.write({'is_self_assign': True, 'self_assignee_ids': [Command.link(rec.env.user.id)], })

    @api.depends('stage_id.sequence')
    def _compute_is_new_stage(self):
        for rec in self:
            rec.is_new_stage = ((rec.stage_id.sequence == 0))

    def action_approved(self):
        for rec in self:
            approved = self.env.ref('manpower_overseas.stage_job21')
            # approved = rec.env['hr.recruitment.stage'].search([('sequence', '=', 21)], limit=1)
            if approved:
                rec.write({'stage_id': approved.id,
                           'is_self_assign': False,

                           })
            else:
                raise ValidationError("Approved stage not found!")


    def action_rejected(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_onhold_submit(self):
        for rec in self:
            onhold = self.env.ref('manpower_overseas.stage_job23')
            # onhold = rec.env['hr.recruitment.stage'].search([('sequence', '=', 23)], limit=1)
            if onhold:
                rec.write({'stage_id': onhold.id,
                           'is_self_assign': False})
            else:
                raise ValidationError("Onhold stage not found!")




    def action_ta_approved(self):
        for rec in self:
            approved = self.env.ref('recruitment.stage_job28')
            # approved = rec.env['hr.recruitment.stage'].search([('sequence', '=', 21)], limit=1)
            if approved:
                rec.write({'stage_id': approved.id,
                           'is_self_assign': False,
                           })
            else:
                raise ValidationError("Approved stage not found!")


    def action_ta_rejected(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }

    def action_ta_onhold(self):

        view_id = self.env.ref('manpower_overseas.manpower_overseas_wizard_view_form')
        ctx = self.env.context.copy()
        return {
            'name': _('On Hold Reason'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'hr.applicant',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx,
        }

    def action_onhold(self):

        view_id = self.env.ref('manpower_overseas.manpower_overseas_wizard_view_form')
        ctx = self.env.context.copy()
        return {
            'name': _('On Hold Reason'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'hr.applicant',
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
            'target': 'new',
            'res_id': self.id,
            'context': ctx,
        }


    @api.depends('stage_id.sequence')
    def _compute_is_letter_issued(self):
        for rec in self:
            rec.is_letter_issued = (rec.stage_id.sequence == 11)

    def action_accept(self):
        for rec in self:
            accept_stage = self.env.ref('manpower_overseas.stage_job26')
            # accept_stage = rec.env['hr.recruitment.stage'].search([('sequence', '=', 26)], limit=1)
            if not accept_stage:
                raise ValidationError("Candidate Confirmed stage not found!")

            if not rec.fitness_certificate:
                raise ValidationError("Please provide a fitness certificate before accepting the candidate.")

            rec.write({'stage_id': accept_stage.id,
                       'is_self_assign': False,
                       'status': 'accept',

                       })

    def action_reject(self):
        for rec in self:
            reject = self.env.ref('manpower_overseas.stage_job25')
            # reject = rec.env['hr.recruitment.stage'].search([('sequence', '=', 25)], limit=1)
            if reject:
                rec.write({'stage_id': reject.id,
                           'is_self_assign': False,
                           'status': 'reject',

                           })
            else:
                raise ValidationError("Candidate Rejection stage not found!")

    def action_maybe(self):
        for rec in self:
            rec.write({
                'status': 'may-be',
                       })

        pass

    @api.depends('stage_id.sequence')
    def _compute_is_onboarding(self):
        for rec in self:
            rec.is_onboarding = (rec.stage_id.sequence == 26)

    def action_onboarding(self):
        for rec in self:
            onboarding = self.env.ref('manpower_overseas.stage_job27')
            # onboarding = rec.env['hr.recruitment.stage'].search([('sequence', '=', 27)], limit=1)
            if onboarding:
                rec.write({'stage_id': onboarding.id,
                           'is_self_assign': False})
            else:
                raise ValidationError("On Boarding stage not found!")
