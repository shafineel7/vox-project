# -*- coding: utf-8 -*-
{
    'name': 'Overseas Manpower Management',
    'version': '17.0.1.0.1',
    'category': 'Human Resources',
    'summary': 'Manage overseas manpower recruitment and deployment',
    'description': 'Manage overseas manpower recruitment and deployment',
    'author': 'Shafi PK' 'Abhilash c',
    'website': 'http://www.voxtronme.com',
    'depends': ['recruitment','reject_reason','hr_contract_salary'],
    'data': [
        'security/ir.model.access.csv',
        'security/manpower_overseas_security.xml',
        'data/hr_applicant_cron.xml',
        'data/mail_template_data.xml',
        'data/hr_recruitment_stage_demo.xml',
        'wizard/camp_interview_details_views.xml',
        'views/hr_job_views.xml',
        'views/hr_applicant_views.xml',
        'views/agent_agent_views.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
