{
    "name": "reject_reason",
    "version": "17.0.2.0.0",
    "author": "neema",
    "website": 'https://www.voxtronme.com',
    "sequence": 80,
    "summary": 'Reject Reason module for handling reject reason',
    "description": """
    This module is for handling the reject reasons, this can be inherited to other modules for providing reject reasons.
    """,
    "depends": ['base'],
    'data': ['security/ir.model.access.csv',
             'wizard/reject_reason_views.xml',
             ],
    "installable": "True",
    'license': 'LGPL-3',
    'application': 'True'
}
