from odoo import models, fields


class RejectReason(models.TransientModel):
    _name = "reject.reason"
    _description = "Reject Reason"

    name = fields.Text(string='Reject Reason')

    def confirm_action(self):
        return True
