from odoo import api, fields, models, _
from dateutil.relativedelta import relativedelta
import calendar
from datetime import date,datetime


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    leave_validity_start = fields.Date()
    leave_validity_end = fields.Date()
    next_update_date = fields.Date(compute='_compute_next_update_date', store=True)
    last_updated_date = fields.Date()


    @api.depends('date_of_joining')
    def _compute_next_update_date(self):
        for rec in self:
            rec.leave_validity_start = rec.date_of_joining
            leave_validity_start = rec.date_of_joining
            date_of_joining = rec.date_of_joining
            today = datetime.now()
            next_year = datetime(year=today.year + 1, month=1, day=1)
            if date_of_joining:
                test_month = date_of_joining + relativedelta(months=+1)
                if date_of_joining.day > calendar.monthrange(test_month.year, test_month.month)[1]:
                    next_update = date_of_joining + relativedelta(months=+2)
                    rec.next_update_date = date(next_update.year, next_update.month, 1)
                else:
                    rec.next_update_date = test_month

                test_year = leave_validity_start + relativedelta(years=+1)
                if date_of_joining.day > calendar.monthrange(test_year.year, test_year.month)[1]:
                    next_update = test_year + relativedelta(months=+1)
                    rec.leave_validity_end = date(next_update.year, next_update.month, 1)
                else:
                    rec.leave_validity_end = test_year
                next_test_month = date(date_of_joining.year, date_of_joining.month+1, 20)
                rec.next_update_date = next_test_month

                test_year = leave_validity_start + relativedelta(years=+1)
                if date_of_joining.day > calendar.monthrange(test_year.year, test_year.month)[1]:
                    next_update = test_year + relativedelta(months=+1)
                    # self.leave_validity_end = date(next_update.year, next_update.month, 1)
                    rec.leave_validity_end = datetime(year=today.year, month=1, day=1)
                else:
                    rec.leave_validity_end = test_year

                    date(today.year, today.month, 20)

