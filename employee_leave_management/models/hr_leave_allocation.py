# -*- coding: utf-8 -*-

from odoo import api, fields, models
import datetime
from datetime import date, datetime,timedelta
from dateutil.relativedelta import relativedelta
import calendar


class HolidaysAllocation(models.Model):
    _inherit = 'hr.leave.allocation'

    @api.model
    def add_leaves(self):
        leave_type_tab = self.env['hr.leave.type']
        leave_alloc_tab = self.env['hr.leave.allocation']
        leave_tab = self.env['hr.leave']
        emp_tab = self.env['hr.employee']
        today = date.today()
        leave_exceptions = []
        today = datetime.now()
        next_year = datetime(year=today.year + 1, month=1, day=1)
        next_schedule = datetime(year=today.year, month=today.month, day=20)
        today = date.today()
        # if today.day == 20:
        if today.day == 6:
            last_day_of_prev_month = date.today().replace(day=1) - timedelta(days=1)
            start_day_of_prev_month = date.today().replace(day=1) - timedelta(days=last_day_of_prev_month.day)
            date_list = []
            for days in range((last_day_of_prev_month - start_day_of_prev_month).days + 1):
                date_list.append(start_day_of_prev_month + timedelta(days=days))
            for r in emp_tab.search([]):
                if r.date_of_joining and not r.next_update_date:
                    next_test_month = date(r.date_of_joining.year, r.date_of_joining.month + 1, 20)
                    r.next_update_date = next_test_month
                if r.date_of_joining and r.last_updated_date != date.today() and r.next_update_date:
                    if r.type=='indirect':
                        emp_leave_day = r.next_update_date
                        holiday_status = leave_type_tab.search([('employee_type','=','indirect'),('is_casual_leave', '=', True)])
                        holiday_status_id = holiday_status.id
                        # previous_month_start = emp_leave_day + relativedelta(months=-1)
                        previous_month_start =  start_day_of_prev_month
                        entitled_leave_type = leave_type_tab.search([('employee_type','=','indirect'),('is_allow_entitlement', '=', True)])
                        if entitled_leave_type:
                            for ent_leave in entitled_leave_type:
                                leave_exceptions.append(ent_leave.id)

                                leaves = self.env["hr.leave"].search(
                                    [
                                        ("employee_id", "=", r.id),
                                        "|",
                                        ("date_from", ">=", start_day_of_prev_month),
                                        ("date_to", ">=", last_day_of_prev_month),
                                    ]
                                )

                        leaves = leave_tab.search([('employee_id', '=', r.id),
                                                   ('request_date_to', '<=', last_day_of_prev_month), ('state', '=', 'validate'),
                                                   ('request_date_from', '>=', previous_month_start),
                                                   ('holiday_status_id', 'not in', leave_exceptions)
                                                   ])
                        number_of_leave_days = 0
                        for leave in leaves:
                            number_of_leave_days = number_of_leave_days + leave.number_of_days
                        work_data = r._get_work_days_data_batch(datetime(year=previous_month_start.year, month=previous_month_start.month, day=previous_month_start.day,
                        ), datetime(year=last_day_of_prev_month.year, month=last_day_of_prev_month.month, day=last_day_of_prev_month.day),compute_leaves=False)[r.id]['days']
                        last_month_total_days = work_data
                        # last_month_total_days = 22
                        to_add_days = holiday_status.entitlement_limit
                        if number_of_leave_days:
                            to_add_days = (last_month_total_days - number_of_leave_days) * (to_add_days / last_month_total_days)
                    else:
                        emp_leave_day = r.next_update_date
                        holiday_status = leave_type_tab.search(
                            [('employee_type', '=', 'direct'), ('is_casual_leave', '=', True)])
                        holiday_status_id = holiday_status.id
                        previous_month_start = emp_leave_day + relativedelta(months=-1)
                        entitled_leave_type = leave_type_tab.search(
                            [('employee_type', '=', 'direct'), ('is_allow_entitlement', '=', True)])
                        if entitled_leave_type:
                            for ent_leave in entitled_leave_type:
                                leave_exceptions.append(ent_leave.id)
                        leaves = leave_tab.search([('employee_id', '=', r.id),
                                                   ('request_date_to', '<=', emp_leave_day), ('state', '=', 'validate'),
                                                   ('request_date_from', '>=', previous_month_start),
                                                   ('holiday_status_id', 'not in', leave_exceptions)
                                                   ])
                        number_of_leave_days = 0
                        for leave in leaves:
                            number_of_leave_days = number_of_leave_days + leave.number_of_days
                        work_data = r._get_work_days_data_batch(
                            datetime(year=previous_month_start.year, month=previous_month_start.month,
                                     day=previous_month_start.day,
                                     ), datetime(year=emp_leave_day.year, month=emp_leave_day.month, day=emp_leave_day.day),
                            compute_leaves=False)[r.id]['days']
                        last_month_total_days = work_data
                        to_add_days = holiday_status.entitlement_limit
                        if number_of_leave_days:
                            to_add_days = (last_month_total_days - number_of_leave_days) * (
                                        to_add_days / last_month_total_days)
                    if (today == emp_leave_day) and (emp_leave_day < r.leave_validity_end):
                        if r.date_of_joining + relativedelta(months=+6) < date.today():
                            entitlements = leave_alloc_tab.search(
                                [('employee_id', '=', r.id), ('holiday_status_id', '=', holiday_status_id),
                                 ('state', '=', 'validate'), ('active', '=', True)])
                            leaves = leave_tab.search(
                                [('employee_id', '=', r.id), ('holiday_status_id', '=', holiday_status_id),
                                 ('state', '=', 'validate'), ('active', '=', True)])
                            # Allocate Leaves
                            leave_alloc_tab.create(
                                {'holiday_status_id': holiday_status_id, 'employee_id': r.id, 'number_of_days': to_add_days,
                                 'state': 'validate'})

                            # Calculate the remaining leaves could be taken
                            total_entitlement = 0
                            for ent in entitlements:
                                total_entitlement += ent.number_of_days

                            total_leaves = 0
                            for lev in leaves:
                                total_leaves += lev.number_of_days

                            total_entitlement = total_entitlement - total_leaves

                        r.last_updated_date = date.today()
                        r.next_update_date = date(today.year, today.month, 20)

                    elif today != r.leave_validity_end:
                        if r.type == 'indirect':
                            flag_1, flag_2 = 0, 0
                            # carry forwarding earned leaves
                            flag_1 = self.allocate_carry_forward(r.id, holiday_status_id, to_add_days)
                            # carry forwarding compensatory leaves
                            compensatory_holiday_status_id = leave_type_tab.search([('employee_type','=','indirect'),('is_compensatory', '=', True)]).id
                            flag_2 = self.allocate_carry_forward(r.id, compensatory_holiday_status_id, to_add_days)
                            # updating next leave validity start and validity end dates
                            if (flag_1 == 1) or (flag_2 == 1):
                                r.date_of_joining = r.leave_validity_end
                                r.leave_validity_end = date(r.leave_validity_end.year + 1, r.leave_validity_end.month,
                                                            r.leave_validity_end.day)
                                r.last_updated_date = date.today()
                                r.next_update_date = date(today.year, today.month, 20)
                        elif r.type == 'direct':
                            flag_1 = 0, 0
                            # carry forwarding earned leaves
                            flag_1 = self.allocate_carry_forward(r.id, holiday_status_id, to_add_days)
                            r.date_of_joining = r.leave_validity_end
                            r.leave_validity_end = date(r.leave_validity_end.year + 1, r.leave_validity_end.month,
                                                        r.leave_validity_end.day)
                            r.last_updated_date = date.today()
                            r.next_update_date = date(today.year, today.month, 20)

    @api.model
    def allocate_carry_forward(self, employee_id, holiday_status_id, to_add_days):
        flag = 0
        leave_type_tab = self.env['hr.leave.type']
        leave_alloc_tab = self.env['hr.leave.allocation']
        holiday_status = leave_type_tab.search([('id', '=', holiday_status_id)])
        allocated_days = 0
        emp_allocation = leave_alloc_tab.search(
            [('employee_id', '=', employee_id), ('holiday_status_id', '=', holiday_status_id),
             ('active', '=', True), ('state', 'in', ['confirm', 'validate1', 'validate'])])

        if emp_allocation:
            flag = 1
            for alloc in emp_allocation:
                allocated_days += alloc.number_of_days
                alloc.write({'active': False})

            leaves_taken = self.env['hr.leave'].search(
                [('employee_id', '=', employee_id), ('holiday_status_id', '=', holiday_status_id),
                 ('state', '=', 'validate'), ('active', '=', True)])
            leave_count = 0
            for leave in leaves_taken:
                leave_count += leave.number_of_days
                leave.write({'active': False})

            allocated_days = (allocated_days - leave_count) + to_add_days
            if allocated_days > 0:
                if allocated_days > holiday_status.carry_forward_leaves:
                    remaining_leaves = allocated_days - holiday_status.carry_forward_leaves
                    leave_alloc_tab.create(
                        {'holiday_status_id': holiday_status_id, 'employee_id': employee_id,
                         'number_of_days': holiday_status.carry_forward_leaves,
                         'name': 'Carry Forwarded Leaves',
                         'state': 'validate'})
                    leave_alloc_tab.create(
                        {'holiday_status_id': holiday_status_id, 'employee_id': employee_id,
                         'number_of_days': remaining_leaves,
                         'name': 'Remaining Leaves after Carry Forward',
                         'state': 'validate'})
                    emp_data = self.env['hr.employee'].search([('id', '=', employee_id)])

                else:
                    # alloc_days = allocated_days + leave_count
                    leave_alloc_tab.create(
                        {'holiday_status_id': holiday_status_id, 'employee_id': employee_id,
                         'number_of_days': allocated_days, 'name': 'carry forwarded leaves are approved',
                         'state': 'validate'})
        return flag
