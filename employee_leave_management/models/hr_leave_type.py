# -*- coding: utf-8 -*-

from odoo import api, fields, models


class HrLeaveType(models.Model):
    _inherit = 'hr.leave.type'

    is_multi_level_validation = fields.Boolean(
        string='Multiple Level Approval',
        help="If checked then multi-level approval is necessary",
        compute='_compute_is_multi_level_validation')
    leave_validation_type = fields.Selection(
        selection_add=[('multi', 'Multi Level Approval')])
    validator_ids = fields.Many2many('hr.holidays.validators',
                                     string='Leave Validators',
                                     help="Indicates the leave validators")

    employee_type = fields.Selection([
        ('direct', 'Direct'),
        ('indirect', 'Indirect'),
    ], string='Type')

    no_of_days_after_joining = fields.Integer(string='No of days after Joining', help='No of days after Joining', default=0)
    is_sick_leave = fields.Boolean(string='Sick leave')

    is_carry_forward = fields.Boolean('Carry Forward?')
    carry_forward_leaves = fields.Float('Carry Forward Leaves')

    is_allow_entitlement = fields.Boolean(default=False, string="Allow Entitlement")
    entitlement_limit = fields.Float('Monthly Entitlement Limit')

    is_compensatory = fields.Boolean(default=False)
    is_casual_leave = fields.Boolean(default=False)
    is_unpaid_leave = fields.Boolean(default=False)

    @api.depends('leave_validation_type')
    def _compute_is_multi_level_validation(self):
        """Method for validating the value of multi_level_validation"""
        for rec in self:
            rec.is_multi_level_validation = True if (
                    rec.leave_validation_type == 'multi') else False
