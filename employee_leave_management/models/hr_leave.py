# -*- coding: utf-8 -*-

from odoo import api, fields, models, _,Command
from odoo.exceptions import UserError,ValidationError
import pytz
from pytz import timezone
from dateutil import relativedelta
from dateutil.relativedelta import relativedelta
from datetime import date, datetime, time, timedelta, timezone




class HrLeave(models.Model):
    _inherit = 'hr.leave'


    no_of_days_after_joining = fields.Integer(string='No of days after Joining', help='No of days after Joining',store=True)
    reject_reason = fields.Char(string="Reject Reason")



    def indirect_add_unpaid_leaves(self):
        today = date.today()
        if today.day == 2:

            indirect_leave_type = self.env['hr.leave.type'].search(
                [('is_unpaid_leave', '=', True), ('employee_type', '=', 'indirect')], limit=1, order='sequence')

            employee= self.env['hr.employee']
            # attendance_obj = self.env['hr.attendance']
            working_days = 0

            # today = datetime.now()

            last_day_of_prev_month = date.today().replace(day=1) - timedelta(days=1)
            start_day_of_prev_month = date.today().replace(day=1) - timedelta(days=last_day_of_prev_month.day)
            # last_date = fields.Datetime.from_string(last_day_of_prev_month)
            # start_date = fields.Datetime.from_string(start_day_of_prev_month)
            date_list = []
            for days in range((last_day_of_prev_month - start_day_of_prev_month).days + 1):
                date_list.append(start_day_of_prev_month + timedelta(days=days))


            for rec in employee.search([]):
                if rec.type == 'indirect':

                    # Get attendances - excluding the ones with check_out missing
                    attendances = self.env["hr.attendance"].search(
                        [
                            ("employee_id", "=", rec.id),
                            ("check_in", ">=", start_day_of_prev_month),
                            ("check_out", "!=", False),
                        ]
                    )
                    attendance_dates = attendances.mapped("check_in") + attendances.mapped(
                        "check_out"
                    )
                    attendance_dates = [dt.date() for dt in attendance_dates]

                    # Get leaves
                    leaves = self.env["hr.leave"].search(
                        [
                            ("employee_id", "=", rec.id),
                            "|",
                            ("date_from", ">=", start_day_of_prev_month),
                            ("date_to", ">=", last_day_of_prev_month),
                        ]
                    )

                    # Get public holidays
                    calendar_leaves = self.env["resource.calendar.leaves"].search(
                        [
                            "&",
                            "&",
                            "&",
                            "|",
                            ("resource_id", "=", False),
                            ("company_id", "=", rec.company_id.id),
                            ("calendar_id", "=", rec.resource_calendar_id.id),
                            ("calendar_id", "=", False),
                            "|",
                            ("date_from", ">=", start_day_of_prev_month),
                            ("date_to", "<=", last_day_of_prev_month),
                        ]
                    )

                    # Check every date in range
                    for check_date in date_list:
                        min_check_date = datetime.combine(check_date, datetime.min.time())
                        max_check_date = datetime.combine(check_date, datetime.max.time())

                        # Get working hours
                        work_hours = rec.resource_calendar_id.get_work_hours_count(
                            min_check_date, max_check_date
                        )
                        is_attendance = check_date in attendance_dates
                        is_leave = leaves.filtered(
                            lambda l: l.date_from.date() <= check_date <= l.date_to.date()
                                      or min_check_date.date() <= l.date_from.date() <= max_check_date.date()
                                      or min_check_date.date() <= l.date_to.date() <= max_check_date.date()
                        )
                        is_calendar_leave = calendar_leaves.filtered(
                            lambda l: l.date_from.date() <= check_date <= l.date_to.date()
                                      or min_check_date.date() <= l.date_from.date() <= max_check_date.date()
                                      or min_check_date.date() <= l.date_to.date() <= max_check_date.date()
                        )



                        if (
                                work_hours
                                and work_hours > 0
                                and not is_attendance
                                and not is_leave
                                and not is_calendar_leave
                        ):
                            leave = self.env["hr.leave"].sudo().create(
                                {
                                    'name':'Unpaid Time Off',
                                    'holiday_status_id': indirect_leave_type.id,
                                    'department_id': rec.department_id.id,
                                    'employee_id': rec.id,
                                    'request_date_from': check_date,
                                    'request_date_to': check_date,
                                    # 'request_date_to': check_date + relativedelta(days=1),


                                }
                            )

                    return

    def direct_add_unpaid_leaves(self):
        today = date.today()
        if today.day == 19:

            indirect_leave_type = self.env['hr.leave.type'].search(
                [('is_unpaid_leave', '=', True), ('employee_type', '=', 'direct')], limit=1, order='sequence')

            employee= self.env['hr.employee']
            working_days = 0
            today = date.today()

            last_day_of_prev_month = date.today().replace(day=1) - timedelta(days=1)
            start_day_of_prev_month = date.today().replace(day=1) - timedelta(days=last_day_of_prev_month.day)
            date_list = []
            for days in range((last_day_of_prev_month - start_day_of_prev_month).days + 1):
                date_list.append(start_day_of_prev_month + timedelta(days=days))


            for rec in employee.search([]):
                if rec.type=='direct':

                    # Get attendances - excluding the ones with check_out missing
                    attendances = self.env["hr.attendance"].search(
                        [
                            ("employee_id", "=", rec.id),
                            ("check_in", ">=", start_day_of_prev_month),
                            ("check_out", "!=", False),
                        ]
                    )
                    attendance_dates = attendances.mapped("check_in") + attendances.mapped(
                        "check_out"
                    )
                    attendance_dates = [dt.date() for dt in attendance_dates]

                    # Get leaves
                    leaves = self.env["hr.leave"].search(
                        [
                            ("employee_id", "=", rec.id),
                            "|",
                            ("date_from", ">=", start_day_of_prev_month),
                            ("date_to", ">=", last_day_of_prev_month),
                        ]
                    )
                    #
                    # # Get public holidays
                    # calendar_leaves = self.env["resource.calendar.leaves"].search(
                    #     [
                    #         "&",
                    #         "&",
                    #         "&",
                    #         "|",
                    #         ("resource_id", "=", False),
                    #         ("company_id", "=", rec.company_id.id),
                    #         ("calendar_id", "=", rec.resource_calendar_id.id),
                    #         ("calendar_id", "=", False),
                    #         "|",
                    #         ("date_from", ">=", start_day_of_prev_month),
                    #         ("date_to", "<=", last_day_of_prev_month),
                    #     ]
                    # )


                    for check_date in date_list:
                        min_check_date = datetime.combine(check_date, datetime.min.time())
                        max_check_date = datetime.combine(check_date, datetime.max.time())

                        # Get working hours
                        work_hours = rec.resource_calendar_id.get_work_hours_count(
                            min_check_date, max_check_date
                        )
                        is_attendance = check_date in attendance_dates
                        is_leave = leaves.filtered(
                            lambda l: l.date_from.date() <= check_date <= l.date_to.date()
                                      or min_check_date.date() <= l.date_from.date() <= max_check_date.date()
                                      or min_check_date.date() <= l.date_to.date() <= max_check_date.date()
                        )


                        # is_calendar_leave = calendar_leaves.filtered(
                        #     lambda l: l.date_from <= check_date <= l.date_to
                        #               or min_check_date <= l.date_from <= max_check_date
                        #               or min_check_date <= l.date_to <= max_check_date
                        # )



                        if (
                                work_hours
                                and work_hours > 0
                                and not is_attendance
                                and not is_leave
                                # and not is_calendar_leave
                        ):
                            leave = self.env["hr.leave"].sudo().create(
                                {
                                    'name':'Unpaid Time Off',
                                    'holiday_status_id': indirect_leave_type.id,
                                    'department_id': rec.department_id.id,
                                    'employee_id': rec.id,
                                    'request_date_from': check_date,
                                    'request_date_to': check_date + relativedelta(days=1),


                                }
                            )

                    return

    @api.depends('request_date_from','employee_id')
    def _compute_holiday_status_type_ids(self):
        domain = []
        leave_type_list = []
        some_model = self.env['hr.leave.type'].search([])
        for rec in self:
            for each in some_model:
                if each.employee_type == rec.env.user.employee_id.type and rec.employee_id.date_of_joining:
                    if (rec.employee_id.date_of_joining - rec.request_date_from).days < each.no_of_days_after_joining:
                        leave_type_list.append(each.id)
            rec.holiday_status_type_ids = [(4,leave) for leave in leave_type_list]


    holiday_status_type_ids = fields.Many2many('hr.leave.type',compute=_compute_holiday_status_type_ids)
    holiday_status_id = fields.Many2one(domain="[('id','in',holiday_status_type_ids)]")
    local_contact_number = fields.Char(string="Local Contact Number")
    overseas_contact_number = fields.Char(string="Overseas Contact Number")




    @api.onchange('holiday_status_id')
    def _onchange_holiday_status_id(self):
        for rec in self:
            rec.no_of_days_after_joining = rec.holiday_status_id.no_of_days_after_joining


    is_multi_level_validation = fields.Boolean(
        string='Multiple Level Approval',
        related='holiday_status_id.is_multi_level_validation')
    user_id = fields.Many2one('res.users', string='User',
                              help='Current user',
                              default=lambda self: self.env.user)

    state = fields.Selection(
        selection_add=[('asst_mgr_approval', 'Assistant Operations Manager Approval'),('department_head_approval', 'Department Head Approval'),('hr_manager_approval', 'HR manager Approval')])

    employee_name = fields.Char(string='Employee Name')
    department_ids = fields.Many2many('hr.department', string='Department')

    validation_status = fields.Boolean(string='Approve Status', readonly=True,
                                       track_visibility='always',
                                       help="Status of leave approval")

    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user)

    dept_head_employee_ids = fields.Many2many('hr.employee', related='employee_id.department_id.department_head_ids')
    dept_head_user_ids = fields.Many2many('res.users', 'user_leave_rel', 'user_id', 'leave_rel_req_id',
                                          compute='_compute_dept_head_user_ids', store=True)

    assigned_hr_ids = fields.Many2many('res.users','user_leave_hr_rel', 'user_id', 'leave_hr_leave_rel_req_id', string='Assigned HR User', tracking=True)
    assigned_sr_ids = fields.Many2many('res.users', 'user_sr_rel', 'user_id', 'leave_sr_rel_req_id',string='Assigned SR User',tracking=True)
    assigned_asst_mgr_ids = fields.Many2many('res.users', 'user_asst_mgr_rel', 'user_id', 'leave_asst_mgr_rel_req_id',string='Assigned assistatnt Operation Mnagers User', tracking=True)
    assigned_dp_head_ids = fields.Many2many('res.users', 'user_dep_head_rel', 'user_id', 'leave_user_dep_head_rel_rel_req_id',string='Department Head User',tracking=True)

    approval_dept_head_id = fields.Many2one('res.users', string="Approval Dept Head")
    approval_hr_id = fields.Many2one('res.users', string="Approval HR")
    approval_asst_hr_id = fields.Many2one('res.users', string="Approval Assistant HR")
    approval_op_super_id = fields.Many2one('res.users', string="Approval Operation Supervisor")
    employee_type = fields.Selection(related='employee_id.type', readonly=False, related_sudo=False)
    is_asst_mgr = fields.Boolean(string="Asst Manager")
    is_dep_mgr = fields.Boolean(string="Dep Head")
    refusal_user_id = fields.Many2one('res.users', string="Refused User")
    is_sick_leave_attachment = fields.Boolean(string='Sick leave')
    is_self_assign = fields.Boolean(string="Self Assign")
    reject_reason = fields.Char(string="Reject Reason")

    @api.constrains('request_date_from', 'employee_id')
    def _check_leave_request(self):
        for leave in self:
            if leave.employee_id.date_of_joining:
                probation_end_date = leave.employee_id.date_of_joining + relativedelta(months=6)
                if leave.request_date_from < probation_end_date:
                    if leave.holiday_status_id.is_unpaid_leave == False:
                        raise ValidationError("You cannot apply the leave during probation period.")

    @api.onchange('holiday_status_id','request_date_from','request_date_to')
    def _onchange_holiday_status_id(self):
        for rec in self:
            if rec.holiday_status_id.is_sick_leave == True and rec.number_of_days_display>3:
                rec.is_sick_leave_attachment = True
            else:
                rec.is_sick_leave_attachment = False


    @api.onchange('employee_ids')
    def _onchange_employee_ids(self):
        project_ids=[]
        name_list=[]
        for record in self:
            for rec in record.employee_ids:
                if rec.department_id:
                    project_ids.append(rec.department_id.id)
                name_list.append(rec.name)
            if len(record.employee_ids) == 1:
                record.employee_id = record.employee_ids[0]._origin
            else:
                record.employee_id = False
            record.update({'department_ids': [(6, 0, project_ids)],'employee_name': ','.join(name_list)})


    @api.depends('employee_id', 'employee_id.department_id.department_head_ids','employee_id.type')
    def _compute_dept_head_user_ids(self):
        user_list = []
        for rec in self:
            if rec.employee_id.type=='indirect':
                if rec.dept_head_employee_ids:
                    for head in rec.dept_head_employee_ids:
                        if head.user_id:
                            user_list.append(head.user_id.id)
            rec.dept_head_user_ids = user_list


    def create(self,vals):
        templates = super(HrLeave, self).create(vals)
        current_employee = self.env.user.employee_id
        for user_obj in templates:

            for user in user_obj.employee_ids:
                if user.type == 'indirect':

                    email_template = self.env.ref('employee_leave_management.email_template_dp_head_approval',
                                                  raise_if_not_found=False)
                    hr_manager_emails = [manager.login for manager in user_obj.dept_head_user_ids]
                    if hr_manager_emails:
                        email_values = {'email_to': ','.join(hr_manager_emails)}
                        email_template.write({'email_to': ','.join(hr_manager_emails)})
                        email_template.with_context(email_values).send_mail(user_obj.id, force_send=True)

                else:

                    if not (self.env.user.has_group(
                            'department_groups.group_construction_operations_coord') or self.env.user.has_group
                        ('department_groups.group_staff_operations_coord') or self.env.user.has_group('department_groups.group_soft_ser_operations_coord') or
                            self.env.user.has_group('department_groups.group_hr_asst')):
                        raise UserError(_('You cannot create records. Please contact Administrator!'))

                    construction_op_super = self.env.ref('department_groups.group_construction_operations_super')
                    soft_ser_op_super = self.env.ref('department_groups.group_soft_ser_operations_super')
                    staff_op_super = self.env.ref('department_groups.group_staff_operations_super')
                    super_users = self.env['res.users'].search(
                        [('groups_id', 'in',
                          [construction_op_super.id, soft_ser_op_super.id, staff_op_super.id,
                           ])])
                    super_groups = self.env['res.groups'].browse(
                        (
                            construction_op_super.id, soft_ser_op_super.id,
                            staff_op_super.id))
                    user_obj.assigned_sr_ids = [Command.link(user.id) for user in super_users]
                    # user_obj.assigned_group_ids = [Command.link(group.id) for group in super_groups]
                    email_template = self.env.ref('employee_leave_management.email_template_sr_approval',
                                                  raise_if_not_found=False)
                    super_emp_emails = [manager.login for manager in super_users]
                    if super_emp_emails:
                        email_values = {'email_to': ','.join(super_emp_emails)}
                        email_template.write({'email_to': ','.join(super_emp_emails)})
                        email_template.with_context(email_values).send_mail(user_obj.id, force_send=True)

            user_obj.refusal_user_id = self.env.user.id
        return templates



    def action_approve(self):
        """ Override action_approve to add multi level approval """
        if any(holiday.state != 'confirm' for holiday in self):
            raise UserError(_(
                'Leave request must be confirmed ("To Approve")'
                ' in order to approve it.'))
        return self.approval_check()

    def approval_check(self):
        # current_employee = self.env['hr.employee'].search(
        #     [('user_id', '=', self.env.uid)], limit=1)
        current_employee = self.env.user.employee_id
        active_id = self.env.context.get('active_id') if self.env.context.get(
            'active_id') else self.id
        user = self.env['hr.leave'].browse(active_id)
        if self.env.uid in self.dept_head_user_ids.ids:
            self.validation_status = True
        approval_flag = True
        for user_obj in self:
            if not user_obj.validation_status:
                approval_flag = False
        # if approval_flag:
        for employee in user.employee_ids:
            if employee.type=='indirect':
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                user_obj.assigned_hr_ids = [Command.link(user.id) for user in hr_manager_users]
                # user_obj.assigned_group_ids = [Command.link(hr_manager_group.id)]
                email_template = self.env.ref('employee_leave_management.email_template_hr_manager_approval',
                                              raise_if_not_found=False)
                hr_manager_emails = [manager.login for manager in hr_manager_users]
                if hr_manager_emails:
                    email_values = {'email_to': ','.join(hr_manager_emails)}
                    email_template.write({'email_to': ','.join(hr_manager_emails)})
                    email_template.with_context(email_values).send_mail(user_obj.id, force_send=True)
                user.filtered(lambda hol: hol.validation_type in ['both','multi']).sudo().write({'state': 'validate1','first_approver_id': current_employee.id,'assigned_hr_ids':hr_manager_users.ids})
                for holiday in self.filtered(lambda holiday: holiday.employee_id.user_id):
                    # user_tz = timezone(holiday.tz)
                    user_tz = pytz.timezone(self.env.user.tz or 'UTC')
                    utc_tz = pytz.utc.localize(holiday.date_from).astimezone(user_tz)
                    # Do not notify the employee by mail, in case if the time off still needs Officer's approval
                    # notify_partner_ids = holiday.employee_id.user_id.partner_id.ids if holiday.validation_type not in ['both','multi'] else []
                    # holiday.message_post(
                    #     body=_(
                    #         'Your %(leave_type)s planned on %(date)s has been accepted',
                    #         leave_type=holiday.holiday_status_id.display_name,
                    #         date=utc_tz.replace(tzinfo=None)
                    #     ),
                    #     partner_ids=notify_partner_ids)

                user.filtered(lambda hol: not hol.validation_type in ['both','multi']).sudo().action_validate()
                if not user.env.context.get('leave_fast_create'):
                    user.activity_update()
                # self.dept_head_user_ids = [(4, 0, self.env.uid)]
                # self.dept_head_user_ids = [Command.link(self.env.uid)]
                # self.approval_hr_id = [Command.link(self.env.uid)]
            else:
                construction_mgr_super = self.env.ref('department_groups.group_construction_mgr_operations')
                soft_ser_mgr_super = self.env.ref('department_groups.group_soft_ser_mgr_operations')
                staff_mgr_super = self.env.ref('department_groups.group_staff_mgr_operations')
                super_users = self.env['res.users'].search(
                    [('groups_id', 'in',
                      [construction_mgr_super.id, soft_ser_mgr_super.id, staff_mgr_super.id,
                       ])])
                super_groups = self.env['res.groups'].browse(
                    (
                        construction_mgr_super.id, soft_ser_mgr_super.id,
                        staff_mgr_super.id))
                user_obj.assigned_asst_mgr_ids = [Command.link(user.id) for user in super_users]
                user_obj.is_asst_mgr = True

                # user_obj.assigned_group_ids = [Command.link(group.id) for group in super_groups]
                email_template = self.env.ref('employee_leave_management.email_template_asst_mgr_approval',
                                              raise_if_not_found=False)
                super_emp_emails = [manager.login for manager in super_users]
                if super_emp_emails:
                    email_values = {'email_to': ','.join(super_emp_emails)}
                    email_template.write({'email_to': ','.join(super_emp_emails)})
                    email_template.with_context(email_values).send_mail(user_obj.id, force_send=True)
                user.filtered(lambda hol: hol.validation_type in ['both', 'multi']).sudo().write(
                    {'state': 'asst_mgr_approval', 'first_approver_id': current_employee.id,
                     'assigned_asst_mgr_ids': super_users.ids})
                for holiday in self.filtered(lambda holiday: holiday.employee_id.user_id):
                    user_tz = pytz.timezone(self.env.user.tz or 'UTC')
                    utc_tz = pytz.utc.localize(holiday.date_from).astimezone(user_tz)
                    # user_tz = timezone(holiday.tz)
                    # utc_tz = pytz.utc.localize(holiday.date_from).astimezone(user_tz)
                    # Do not notify the employee by mail, in case if the time off still needs Officer's approval
                    # notify_partner_ids = holiday.employee_id.user_id.partner_id.ids if holiday.validation_type not in [
                    #     'both', 'multi'] else []
                    # holiday.message_post(
                    #     body=_(
                    #         'Your %(leave_type)s planned on %(date)s has been accepted',
                    #         leave_type=holiday.holiday_status_id.display_name,
                    #         date=utc_tz.replace(tzinfo=None)
                    #     ),
                    #     partner_ids=notify_partner_ids)

                user.filtered(lambda hol: not hol.validation_type in ['both', 'multi']).sudo().action_validate()
                if not user.env.context.get('leave_fast_create'):
                    user.activity_update()

            user_obj.refusal_user_id = self.env.user.id
            user_obj.sudo().is_self_assign = False

            return True
        return False

    def action_asst_mgr_approval(self):
        current_employee = self.env.user.employee_id
        for rec in self:
            rec.sudo().is_self_assign = False
            construction_mgr_super = self.env.ref('department_groups.group_staff_dept_head')
            soft_ser_mgr_super = self.env.ref('department_groups.group_soft_ser_dept_head')
            staff_mgr_super = self.env.ref('department_groups.group_construction_dept_head')
            super_users = self.env['res.users'].search(
                [('groups_id', 'in',
                  [construction_mgr_super.id, soft_ser_mgr_super.id, staff_mgr_super.id,
                   ])])
            super_groups = self.env['res.groups'].browse(
                (
                    construction_mgr_super.id, soft_ser_mgr_super.id,
                    staff_mgr_super.id))
            rec.assigned_dp_head_ids = [Command.link(user.id) for user in super_users]
            rec.is_dep_mgr = True
            # user_obj.assigned_group_ids = [Command.link(group.id) for group in super_groups]
            email_template = self.env.ref('employee_leave_management.email_template_dep_head_approval',
                                          raise_if_not_found=False)
            super_emp_emails = [manager.login for manager in super_users]
            if super_emp_emails:
                email_values = {'email_to': ','.join(super_emp_emails)}
                email_template.write({'email_to': ','.join(super_emp_emails)})
                email_template.with_context(email_values).send_mail(rec.id, force_send=True)

            rec.write({
                'state': 'department_head_approval',
                'first_approver_id': current_employee.id,
                'refusal_user_id' : self.env.user.id
            })

    def action_department_approval_approval(self):

        current_employee = self.env.user.employee_id
        for rec in self:
            rec.sudo().is_self_assign = False
            hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
            hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
            rec.assigned_hr_ids = [Command.link(user.id) for user in hr_manager_users]
            # user_obj.assigned_group_ids = [Command.link(hr_manager_group.id)]
            email_template = self.env.ref('employee_leave_management.email_template_hr_manager_approval',
                                          raise_if_not_found=False)
            hr_manager_emails = [manager.login for manager in hr_manager_users]
            if hr_manager_emails:
                email_values = {'email_to': ','.join(hr_manager_emails)}
                email_template.write({'email_to': ','.join(hr_manager_emails)})
                email_template.with_context(email_values).send_mail(rec.id, force_send=True)
            rec.write({
                'state': 'validate1',
                'first_approver_id': current_employee.id,
                'refusal_user_id': self.env.user.id
            })

    def action_rejected(self):
        """ Override to refuse the leave request if the current user is in
        validators list """

        for rec in self:
            rec.sudo().is_self_assign = False

            email_template = self.env.ref('employee_leave_management.email_template_leave_refusal',
                                          raise_if_not_found=False)
            if rec.refusal_user_id:
                hr_manager_emails = [manager.login for manager in rec.refusal_user_id]
                email_values = {'email_to': ','.join(hr_manager_emails)}
                email_template.write({'email_to': ','.join(hr_manager_emails)})
                email_template.with_context(email_values).send_mail(rec.id, force_send=True)


        current_employee = self.env['hr.employee'].search(
            [('user_id', '=', self.env.uid)], limit=1)
        approval_access = False
        for user in self.dept_head_user_ids:
            if user.id == self.env.uid:
                approval_access = True
        for holiday in self:
            if holiday.state not in ['confirm', 'validate', 'validate1','asst_mgr_approval','department_head_approval']:
                raise UserError(_(
                    'Leave request must be confirmed '
                    'or validated in order to refuse it.'))

            if holiday.state == 'validate1':
                holiday.write({'state': 'refuse',
                               'first_approver_id': current_employee.id})
            else:
                holiday.write({'state': 'refuse',
                               'second_approver_id': current_employee.id})
            # Delete the meeting
            if holiday.meeting_id:
                holiday.meeting_id.unlink()
            # If a category that created several holidays,
            # cancel all related
            holiday.linked_request_ids.action_refuse()
        self._remove_resource_leave()
        self.activity_update()
        return True

    def action_draft(self):
        """ Reset all validation status to false when leave request
        set to draft stage"""
        for user in self:
            user.validation_status = False
        return super().action_draft()


    def action_self_assign(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in hr_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'assigned_hr_ids': rec.env.user.ids})
            else:
                hr_asst_group = self.env.ref('department_groups.group_hr_asst')
                hr_asst_users = self.env['res.users'].search([('groups_id', 'in', hr_asst_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in hr_asst_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_asst_hr_id': rec.env.user.id})



    def action_refuse(self):
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'res_model': 'reject.reason',
            'view_mode': 'form',
            'target': 'new',
            'context': {'active_id': self.id},
        }



    def action_submit(self):
        self.ensure_one()
        self.action_refuse()
        return




