# -*- coding: utf-8 -*-
{
    'name': 'Employee Leave management',
    'version': '17.0.1.0.0',
    'category': 'Human Resources',
    'author': 'Hilsha P H',
    'summary': 'Employee Leave management',
    'description': """
       Employee Leave management
    """,
# hr_work_entry_holidays
    'depends': ['hr_holidays', 'employee','department_groups','reject_reason'],
    'data': [

        'security/employee_leave_management_security.xml',
        'data/ir_cron_data.xml',
        'data/mail_template_data.xml',
        'views/employee_views.xml',
        'views/hr_leave_type_views.xml',
        'views/hr_leave_views.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
