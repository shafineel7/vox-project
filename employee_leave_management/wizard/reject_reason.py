from odoo import models, fields
from odoo.exceptions import ValidationError


class RejectReason(models.TransientModel):
    _inherit = "reject.reason"

    def confirm_action(self):
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model == 'hr.leave' and active_id:
            leave_request = self.env[active_model].browse(active_id)

            leave_request.write({
                                       'is_self_assign': False,
                                       'reject_reason': self.name

                                       })
            leave_request.action_rejected()


        return super().confirm_action()
