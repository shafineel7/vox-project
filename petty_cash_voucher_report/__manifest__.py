{
    'name': 'Petty Cash Payment Voucher Document',
    'author': 'Sourav',
    'summary': 'Module for Downloading Petty Cash Payment Voucher PDF report',
    'version': '17.0.1.0.0',
    'category': 'Inventory/Purchase',
    'website': 'https://www.voxtronme.com',
    'depends': ['expense_management'],
    'description': """This module is for creating Petty Cash Payment Voucher PDF report""",
    'data': [
        'reports/petty_cash_voucher_template.xml',
        'reports/expense_report_template.xml',
        'reports/expense_booking_template.xml',
        'reports/ir_actions_report.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
