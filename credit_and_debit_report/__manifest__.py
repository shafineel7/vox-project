# -*- coding: utf-8 -*-

{
    'name': 'Credit Debit Note Document',
    'author': 'Devika p',
    'version': '17.0.1.0.0',
    'summary': 'Module for Downloading Credit Note and debit note  PDF report',
    'category': 'Report',
    'depends': ['account_accountant', 'purchase', 'document_layout'],
    'description': """This module is for creating Credit Note And Debit Note PDF report""",
    'data': [
        'report/credit_note_report_template.xml',
        'report/accounting_tax_debit_header_template.xml',
        'report/accounting_report.xml',
        'report/accounting_tax_debit_note.xml',
        'views/account_move_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
