# -*- coding: utf-8 -*-
from odoo import models, fields, api


class AccountMoveReversal(models.TransientModel):
    _inherit = 'account.move.reversal'

    def _prepare_default_reversal(self, move):
        values = super()._prepare_default_reversal(move)
        values.update({
            'cust_ref_no': move.name,
        })
        return values
