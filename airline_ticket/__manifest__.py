# -*- coding: utf-8 -*-

{
    'name': 'Airline Ticket ',
    'version': '17.0.1.0.0',
    'summary': 'A comprehensive solution for managing airline tickets .',
    'description': """
        This module provides a robust solution for managing airline tickets in Odoo. It allows users to efficiently 
        create, view, edit, and delete airline tickets with ease. The module includes features such as ticket 
        number generation, passenger information capture, departure and arrival details, seat allocation, and more.
    """,
    'author': 'Shafi PK',
    'category': 'Human Resources',
    'depends': ['employee', 'purchase', 'account_accountant', 'department_groups'],
    'data': [
        'security/ir.model.access.csv',
        'security/air_line_ticket_security.xml',
        'data/ir_sequence_data.xml',
        'data/product_data.xml',
        'views/air_ticket_views.xml',
        'views/purchase_views.xml',
        'views/hr_employee_views.xml',
        'views/account_move_views.xml',
        'views/airport_destination_views.xml',
        'views/air_ticket_type_views.xml',
        'views/air_ticket_menus.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,

}
