# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, exceptions
from odoo.exceptions import ValidationError


class AirlineTicket(models.Model):
    _name = 'airline.ticket'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Airline Ticket'

    name = fields.Char(string='Ticket Request Number', readonly=True, default='New')
    date = fields.Date(string='Date', default=fields.Date.today)
    partner_id = fields.Many2one('res.partner', string='Supplier Name')
    ticket_details_ids = fields.One2many('airline.ticket.details', 'airline_ticket_id')
    grand_total = fields.Float(string='Grand Total', compute='_compute_grand_total')
    status = fields.Selection([('booked', 'Booked'), ('rfq', 'RFQ')], string='status',
                              track_visibility='onchange', default='booked')
    company_id = fields.Many2one('res.company', required=True, default=lambda self: self.env.company)

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if not vals.get('name') or vals['name'] == _('New'):
                vals['name'] = self.env['ir.sequence'].next_by_code('air_ticket_sequence') or _('New')
        return super().create(vals_list)

    @api.depends('ticket_details_ids')
    def _compute_grand_total(self):
        for rec in self:
            if rec.ticket_details_ids:
                rec.grand_total = sum(rec.ticket_details_ids.mapped('total'))
            else:
                rec.grand_total = 0.00

    @api.model
    def default_get(self, fields):
        if self.env.user.has_group('department_groups.group_hr_mgr') or self.env.user.has_group(
                'base.group_system') or self.env.user.has_group('department_groups.group_hr_asst'):
            return super(AirlineTicket, self).default_get(fields)
        else:
            raise exceptions.ValidationError("You cannot Create Air Port Ticket")

    def unlink(self):
        for ticket in self:
            purchase_rfq = self.env['purchase.order'].search([('airline_ticket_id', '=', ticket.id)])
            if purchase_rfq:
                raise exceptions.ValidationError(_('Cannot delete this record as it has associated purchase RFQs.'))
        return super(AirlineTicket, self).unlink()

    def action_create_rfq(self):
        if not (self.env.user.has_group('department_groups.group_hr_mgr') or self.env.user.has_group(
                'department_groups.group_hr_asst') or self.env.user.has_group('base.group_system')):
            raise ValidationError('You are not authorized to approve this request.')

        for record in self:
            if not record.partner_id:
                raise ValidationError('Missing Supplier Name')
            if any(line.unit_price <= 0 for line in record.ticket_details_ids):
                raise ValidationError(_('Unit price must be greater than zero.'))

            product_template = self.env['product.product'].browse(
                self.env.ref('airline_ticket.product_air_ticket').id)

            rfq = self.env['purchase.order'].sudo().create({
                'partner_id': record.partner_id.id,
                'airline_ticket_id': record.id,
                'origin': record.name
            })

            for line in record.ticket_details_ids:
                analytic_name = line.employee_id.department_id.name
                account_analytic = self.env['account.analytic.account'].search([('name', '=', analytic_name)], limit=1)
                if not account_analytic and analytic_name:
                    analytic_plan = self.env['account.analytic.plan'].sudo().create({
                        'name': 'Employee Department',
                    })
                    account_account = self.env['account.analytic.account'].sudo().create({
                        'plan_id': analytic_plan.id,
                        'name': analytic_name
                    })
                    analytic_distribution = self.env['account.analytic.distribution.model'].sudo().create({
                        'partner_id': record.partner_id.id,
                        'product_id': product_template.id,
                        "partner_category_id": rfq.partner_id.category_id.ids,
                        "product_categ_id": product_template.categ_id.id,
                        'analytic_distribution': {account_account.id: 100.0}
                    })
                else:
                    account_account = self.env['account.analytic.account'].search([('name', '=', analytic_name)],
                                                                                  limit=1)
                purchase_order_line = self.env['purchase.order.line'].sudo().create({
                    'order_id': rfq.id,
                    'product_id': product_template.id,
                    'product_qty': line.quantity,
                    'name': line.description,
                    'price_unit': line.unit_price,
                    'taxes_id': line.tax_ids,
                    'employee_id': line.employee_id.id,
                    'employee_name': line.employee_id.name,
                    'analytic_distribution': {account_account.id: 100.0}
                })
            record.write({'status': 'rfq'})

        return {
            'effect': {
                'fadeout': 'slow',
                'message': 'RFQ Created Successfully',
                'type': 'rainbow_man',
            }
        }
