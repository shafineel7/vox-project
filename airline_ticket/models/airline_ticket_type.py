# -*- coding: utf-8 -*-
from odoo import models, fields


class AirlineTicketType(models.Model):
    _name = 'airline.ticket.type'
    _description = 'Airline Ticket Type'

    name = fields.Char(string='Name')
    is_exit = fields.Boolean(default=False, string="Is Exit?")
