# -*- coding: utf-8 -*-
from odoo import models


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    def action_purchase_rfq_view(self):
        return {
            "type": "ir.actions.act_window",
            "name": "Purchase Order",
            "res_model": "purchase.order.line",
            "view_mode": "tree,form",
            "domain": [("employee_id", "=", self.id)],
        }
