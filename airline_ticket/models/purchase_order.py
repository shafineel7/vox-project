# -*- coding: utf-8 -*-
from odoo import models, fields


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    airline_ticket_id = fields.Many2one('airline.ticket', 'AirLine Ticket')
