# -*- coding: utf-8 -*-
from odoo import models, fields, api


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    employee_id = fields.Many2one('hr.employee', 'Employee', compute='_compute_employee_id', store=True)

    @api.depends('purchase_line_id')
    def _compute_employee_id(self):
        for record in self:
            if record.purchase_line_id:
                record.employee_id = record.purchase_line_id.employee_id.id
            else:
                record.employee_id = False

