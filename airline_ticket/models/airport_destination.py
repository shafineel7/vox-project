# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, _


class AirportDestination(models.Model):
    _inherit = 'airport.destination'

    default_amount = fields.Float(string="Default Amount")
    maximum_allowed_amount = fields.Float(string="Maximum Allowed Amount")
    source = fields.Char(string="Source")
    destination = fields.Char(string="Destination")

    @api.constrains('default_amount', 'maximum_allowed_amount')
    def _check_default_amount(self):
        for record in self:
            if record.maximum_allowed_amount != 0 and record.default_amount > record.maximum_allowed_amount:
                raise exceptions.ValidationError("Default Amount cannot be greater than Maximum Allowed Amount!")

    @api.depends('source', 'destination')
    def _compute_display_name(self):
        for record in self:
            if record.source and record.destination:
                record.display_name = f"{record.source} -> {record.destination}"
            else:
                record.display_name = record.name

    def unlink(self):
        for ticket in self:
            purchase_rfq = self.env['airline.ticket.details'].search([('airport_destination_id', '=', ticket.id)])
            if purchase_rfq:
                raise exceptions.ValidationError(_('Cannot delete this record as it has associated AirPort Ticket.'))
        return super(AirportDestination, self).unlink()
