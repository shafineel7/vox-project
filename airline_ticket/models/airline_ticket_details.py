# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions


class AirlineTicketDetails(models.Model):
    _name = 'airline.ticket.details'
    _description = 'Airline Ticket Details'

    description = fields.Char(string='Description')
    employee_id = fields.Many2one('hr.employee', string='Staff ID')
    employee_name = fields.Char(string='Staff Name', readonly=True)
    department_id = fields.Many2one('hr.department', string='Department', readonly=True)
    trip_type = fields.Selection([('one_way', 'One Way'), ('two_way', 'Two Way')],
                                 string="Trip Type")
    airport_destination_id = fields.Many2one('airport.destination', string='Air Port Destination')
    ticket_type_id = fields.Many2one('airline.ticket.type', string='Ticket Type')
    quantity = fields.Integer(string='Quantity', default=1)
    unit_price = fields.Float(string='Unit Price')
    tax_ids = fields.Many2many('account.tax', 'air_ticket_tax', 'airline_ticket_id', 'tax_id',
                               string='Tax', check_company=True, domain="[('id', 'in', suitable_tax_ids)]")
    suitable_tax_ids = fields.Many2many(
        'account.tax', 'airt_ticket_booking_suitable_tax', 'airline_ticket_id', 'suit_tax_id',
        compute='_compute_suitable_tax_ids',
    )
    total = fields.Float(string='Total', compute='_compute_total')
    grand_total = fields.Float(string='Grand Total', compute='_compute_grand_total')
    airline_ticket_id = fields.Many2one('airline.ticket', string='Airline Ticket')
    company_id = fields.Many2one(related="airline_ticket_id.company_id")

    @api.depends('quantity', 'unit_price', 'tax_ids')
    def _compute_total(self):
        for record in self:
            if record.quantity and record.unit_price:
                total_without_tax = record.unit_price * record.quantity
                tax_amount = 0.0
                for tax in record.tax_ids:
                    tax_amount += (tax.amount / 100) * total_without_tax
                total_amount = total_without_tax + tax_amount
                record.total = total_amount
            else:
                record.total = 0.0

    @api.onchange('airport_destination_id')
    def _onchange_airport_destination_id(self):
        for record in self:
            if record.airport_destination_id and record.airport_destination_id.default_amount:
                record.unit_price = record.airport_destination_id.default_amount
            else:
                record.unit_price = False

    @api.onchange('trip_type')
    def _onchange_trip_type(self):
        for record in self:
            if record.trip_type == 'two_way':
                record.quantity = 2
            else:
                record.quantity = 1

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for record in self:
            if record.employee_id and record.employee_id.department_id:
                record.department_id = record.employee_id.department_id.id
                record.employee_name = record.employee_id.name
            else:
                record.department_id = False

    @api.depends('total')
    def _compute_grand_total(self):
        for rec in self:
            if rec.total:
                rec.grand_total = sum(rec.mapped('total'))
            else:
                rec.grand_total = 0.00

    @api.constrains('unit_price')
    def _check_unit_price(self):
        for record in self:
            allowed_amount = record.airport_destination_id.maximum_allowed_amount
            if record.unit_price <= 0 and record.airline_ticket_id.status == 'rfq':
                raise exceptions.ValidationError('Unit price must be greater than zero.')
            if allowed_amount != 0 and record.unit_price > allowed_amount:
                raise exceptions.ValidationError(
                    'Unit price should not exceed the allowed amount of %s.' % allowed_amount
                )

    @api.depends('airline_ticket_id')
    def _compute_suitable_tax_ids(self):
        for record in self:
            if record.airline_ticket_id:
                record.suitable_tax_ids = self.env['account.tax'].search([('type_tax_use', '=', 'purchase'),
                                                                          ('company_id', '=', record.company_id.id)])
            else:
                raise exceptions.ValidationError('Error On Tax Calculation')

