# -*- coding: utf-8 -*-
from odoo import models, fields


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    employee_name = fields.Char(string='Employee Name')
