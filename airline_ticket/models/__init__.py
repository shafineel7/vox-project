# -*- coding: utf-8 -*-
from . import air_ticket
from . import purchase_order_line
from . import hr_employee
from . import account_move_line
from . import airline_ticket_details
from . import airline_ticket_type
from . import account_analytic_account
from . import airport_destination
from . import purchase_order
from . import account_move