# -*- coding: utf-8 -*-
from odoo import models, fields, api


class AccountMove(models.Model):
    _inherit = 'account.move'

    is_airticket = fields.Boolean(string='Is AirTicket', default=False, compute='_compute_is_airticket')

    @api.depends('purchase_id')
    def _compute_is_airticket(self):
        for record in self:
            source_orders = record.line_ids.purchase_line_id.order_id
            if source_orders.airline_ticket_id:
                record.is_airticket = True
            else:
                record.is_airticket = False
