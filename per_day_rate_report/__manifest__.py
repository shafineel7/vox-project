# -*- coding: utf-8 -*-
{
    'name': 'Per Day Rate Invoice',
    'version': '17.0.1.0.0',
    'category': 'Accounting/Accounting',
    'summary': 'Module for Downloading Per Day Rate Report',
    'description': """This module is for creating per day rate invoice PDF report.""",
    'author': 'Anoopa',
    'website': 'https://www.voxtronme.com',
    'depends': ['account_accountant', 'purchase', 'credit_and_debit_report', 'document_layout'],
    'data': [
        'reports/per_day_rate_invoice_report.xml',
        'reports/per_day_rate_report.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'AGPL-3',
}
