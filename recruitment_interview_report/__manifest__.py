# -*- coding: utf-8 -*-

{

    'name': 'Interview Assessment Form',
    'version': '17.0.1.0.0',
    'category': 'Human Resources/Recruitment',
    'summary': 'Module for recruitment interview assessment report',
    'description': """This module is for creating recruitment interview assessment report""",
    'author': 'Abhilash.c',
    'website': 'https://www.voxtronme.com',
    'depends': ['hr_recruitment'],
    'data': [
            'reports/interview_assessment_form.xml',
            'reports/recruitment_report.xml',
            'reports/interview_assessment_header_footer.xml',

    ],

    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'AGPL-3',

}
