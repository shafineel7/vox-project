from odoo import models, fields, Command
from docxtpl import DocxTemplate, RichText
import base64
import os


class ContractContracts(models.Model):
    _inherit = 'contract.contracts'

    def calculate_working_days(self, working_days_per_week):
        days_in_month = [28, 29, 30, 31]
        possible_working_days = set()

        for days in days_in_month:
            weeks = days // 7
            extra_days = days % 7
            working_days = weeks * working_days_per_week + min(extra_days, working_days_per_week)
            possible_working_days.add(working_days)

        return sorted(possible_working_days)

    def generate_note(self, working_days_per_week):
        possible_working_days = self.calculate_working_days(working_days_per_week)
        possible_working_days_str = "/".join(map(str, possible_working_days))
        note = f"Monthly billing calculation method: Monthly rate/{possible_working_days_str} calendar days (excluding off days & Public/national holidays)"
        return note

    def generate_agreement(self):
        module_path = os.path.dirname(os.path.abspath(__file__))
        template_path = os.path.join(module_path, '..', 'contract_agreement_template', 'service_agreement.docx')

        document = DocxTemplate(template_path)

        create_date = self.create_date.strftime('%d %m %Y')

        if self.contract_start_date:
            start_date = self.contract_start_date.strftime('%d %m %Y')
        else:
            start_date = False

        if self.contract_end_date:
            end_date = self.contract_end_date.strftime('%d %m %Y')
        else:
            end_date = False

        owner_mail = RichText()
        owner_mail.add(self.user_id.login, url_id=document.build_url_id(f'mailto:{self.user_id.login}'), font='Arial',
                       underline=True, color='#0563c1')

        company_mail = RichText()
        company_mail.add(self.company_id.email, url_id=document.build_url_id(f'mailto:{self.company_id.email}'),
                         font='Arial', underline=True, color='#0563c1')

        company_data = {
            'name': RichText(self.company_id.name, bold=True, font='Arial'),
            'street': self.company_id.street,
            'city': self.company_id.city,
            'country': self.company_id.country_id.name,
            'phone': self.company_id.phone
        }

        partner_data = {
            'name': RichText(self.partner_id.name, bold=True, font='Arial'),
            'street': self.partner_id.street,
            'city': self.partner_id.city,
            'country': self.partner_id.country_id.name,
            'phone': self.partner_id.phone
        }

        lines = []

        for line in self.contract_lines_ids:
            line_dict = {
                'product': RichText(line.product_id.name, bold=True, font='Arial'),
                'day_count': line.day_count,
                'qty': line.qty,
                'unit_price': line.unit_price,
                'note': self.generate_note(line.day_count),
                'invoice_hours': line.invoice_hours
            }

            lines.append(line_dict)

        context = {
            'code': RichText(self.code, bold=True, font='Arial'),
            'create_date': RichText(create_date, bold=True, font='Arial'),
            'company_data': company_data,
            'partner_data': partner_data,
            'start_date': start_date,
            'end_date': end_date,
            'owner_mail': owner_mail,
            'company_mail': company_mail,
            'contract_lines': lines,
        }

        document.render(context, autoescape=True)

        temp_dir = os.path.join(module_path, '..', 'temp')
        os.makedirs(temp_dir, exist_ok=True)
        temp_file_path = os.path.join(temp_dir, f'{self.name}.docx')

        document.save(temp_file_path)

        with open(temp_file_path, 'rb') as temp_file:
            docx_content = temp_file.read()

        docx_base64 = base64.b64encode(docx_content)

        attachment = self.env['ir.attachment'].sudo().create({
            'name': f"{self.name}.docx",
            'datas': docx_base64,
            'type': 'binary',
            'res_model': 'contract.contracts',
            'res_id': self.id,
            'store_fname': f"{self.name}.docx"
        })

        os.remove(temp_file_path)

        return attachment

    def action_download_agreement(self):
        attachment = self.generate_agreement()
        url = '/download_sa/docx/%d' % attachment.id

        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self'
        }

    def send_contract(self):
        res = super(ContractContracts, self).send_contract()
        if 'context' in res:
            ctx = res['context']
            attachment = self.generate_agreement()
            if attachment:
                ctx['default_attachment_ids'] = [Command.link(attachment.id)]
                res['context'] = ctx
        return res
