{
    'name': 'Service Agreement',
    'author': 'Sourav',
    'summary': 'Module for Service Agreement docx download',
    'version': '17.0.1.0.0',
    'category': 'Technical/Technical',
    'website': 'https://www.voxtronme.com',
    'depends': ['crm_contracts'],
    'description': """This module is for downloading word document of service agreement.""",
    'data': [
        'views/contract_contracts_views.xml'
    ],
    'external_dependencies': {
        'python': [
            'docxtpl',
        ],
    },
    'installable': True,
    'auto_install': False,
    'application': True,
}
