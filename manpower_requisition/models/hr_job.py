# -*- coding: utf-8 -*-
from odoo import models, fields


class HrJob(models.Model):
    _inherit = 'hr.job'

    type = fields.Selection([('direct', 'Direct'), ('indirect', 'Indirect')], string='Type', required=True)
    recruitment_type = fields.Selection([('overseas', 'Overseas'), ('local', 'Local')], required=True,
                                        string='Recruitment From',
                                        default='local')
    branch_id = fields.Many2one('res.company', string='Branch', required=True, domain="[('parent_id', '=', company_id)]")
    salary_upto = fields.Float(string='Salary Up to', required=True)
    preferred_nationality_id = fields.Many2one('res.country', string='Preferred Nationality')
    qualification_ids = fields.Many2many('qualification', 'job_qualification_relation', 'qualification_id',
                                         'job_position_id')
    skills_ids = fields.Many2many('hr.skill', string='Skills', required=True)
    user_id = fields.Many2one("res.users", default=lambda self: self.env.user, string="User")
    source_id = fields.Many2one('manpower.requisition', string='Manpower Requisition')

    def manpower_job_positions(self):
        return {
            'action': 'action_job_position',
            "type": "ir.actions.act_window",
            "name": "Job Position",
            "res_model": "manpower.requisition",
            "view_mode": "tree,form",
            "domain": [("job_id", "=", self.id)],
        }
