# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, _, Command
from odoo.exceptions import ValidationError


class ManpowerRequisition(models.Model):
    _name = 'manpower.requisition'
    _description = 'Manpower requisition used  to request the hiring of new employees'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    status_selection = [('new', 'New'), ('submit_for_hr_approval', 'Submit for HR Approval'),
                        ('submit_for_ta_approval', 'Submit for TA Approval'),
                        ('confirmed', 'Confirmed'),
                        ('on_hold', 'On Hold'),
                        ('rejected', 'Rejected'), ]


    name = fields.Char('Name', readonly=True, default='New')
    type = fields.Selection([('direct', 'Direct'), ('indirect', 'Indirect')], string='Type', required=True)
    recruitment_type = fields.Selection([('overseas', 'Overseas'), ('local', 'Local')], string='Recruitment From',
                                        required=True, default='local')
    branch_id = fields.Many2one('res.company', string='Branch', required=True)
    department_id = fields.Many2one('hr.department', string='Department')
    salary_upto = fields.Float(string='Salary Up to', required=True)
    role = fields.Char(string='Role', required=True)
    number_of_positions = fields.Integer(string='No. of positions', required=True,default=1)
    preferred_nationality_ids = fields.Many2many('res.country', string='Preferred Nationality')
    qualification_ids = fields.Many2many('qualification', 'qualification_relation', 'manpower_id', 'qualification_id')
    skills_ids = fields.Many2many('hr.skill', string='Skills', required=True)
    description = fields.Text(string='Job Description', required=True)
    status = fields.Selection(status_selection, string='Status', default='new', track_visibility='onchange',group_expand='_group_expand_states')
    def _group_expand_states(self, states, domain, order):
        return [key for key, val in self._fields['status'].selection]

    user_id = fields.Many2one("res.users", default=lambda self: self.env.user, string="User")
    team_dp_id = fields.Many2one('hr.department', string="User Department",default=lambda self: self.env.user.department_id.id)
    hr_user_id = fields.Many2one("res.users", string="Hr User", )
    reject_reason = fields.Char(string='Reject Reason')
    reject_user_id = fields.Many2one('res.users', string='Rejected By', readonly=True)
    reject_date = fields.Datetime(string='Rejected On', readonly=True)
    onhold_reason = fields.Char(string='OnHold Reason', )
    temp_onhold_reason = fields.Char(string='OnHold Reason')
    onhold_user_id = fields.Many2one('res.users', string='OnHold By', readonly=True)
    onhold_date = fields.Datetime(string='OnHold Date', readonly=True)
    confirmed_user_id = fields.Many2one('res.users', string='Confirmed User', readonly=True)
    confirmed_date = fields.Datetime(string='Confirmed Date', readonly=True)
    job_id = fields.Many2one('hr.job', 'Job Positions')
    is_onhold = fields.Boolean()
    is_approve = fields.Boolean()
    ta_onhold_reason = fields.Char('Talent Acquisition Onhold Reason', readonly=True)



    def write(self, vals):
        if any(state == 'confirmed' for state in set(self.mapped('status'))):
            raise ValidationError(_("No edit in Confirmed state"))
        else:
            return super().write(vals)


    @api.constrains('number_of_positions')
    def available_positions(self):
        for rec in self:
            if rec.number_of_positions==0:
                raise ValidationError("Add the Number of Positions")

    def _get_default_groups(self):
        dep_head = self.env.ref('department_groups.group_staff_dept_head').id
        con_dep_head = self.env.ref('department_groups.group_construction_dept_head').id
        soft_dep_head = self.env.ref('department_groups.group_soft_ser_dept_head').id
        admin_settings = self.env.ref('base.group_system').id
        contract_mgr = self.env.ref('department_groups.group_contarct_bids_exec').id
        cn_emp_mgr = self.env.ref('department_groups.group_contarct_bde').id
        return [Command.link(dep_head),Command.link(con_dep_head),Command.link(soft_dep_head),Command.link(admin_settings),Command.link(contract_mgr),Command.link(cn_emp_mgr)]



    assigned_user_ids = fields.Many2many(
        'res.users', string='Assigned User', default=lambda self: self.env.user, tracking=True)
    assigned_group_ids = fields.Many2many('res.groups', string="Assigned Role", default=_get_default_groups)
    is_assign = fields.Boolean(string="Self Assign")
    is_logged_user = fields.Boolean(string="Logged USer", compute='_compute_logged_user')
    is_logged_group = fields.Boolean(string="Logged Group", compute='_compute_logged_user')
    is_self_assign = fields.Boolean(string="Self Assign")
    self_assignee_ids = fields.Many2many('res.users', 'manpower_requisition_self_assign_rel',
                                         'requisition_id', 'self_assign_id', string="Assignees")


    approval_hr_id = fields.Many2one('res.users', string="Approval HR")
    approval_talent_id = fields.Many2one('res.users', string="Approval Talent acquisition")
    dept_head_user_ids = fields.Many2many('res.users', 'user_manpower_rel', 'user_id', 'manpower_rel_req_id',
                                          compute='_compute_dept_head_user_ids', store=True)
    approval_create_user_ids = fields.Many2many('res.users', 'user_manpower_create_rel', 'user_id', 'manpower_create_rel_req_id',
                                          compute='_compute_dept_head_user_ids', store=True)


    @api.depends('user_id.employee_id', 'user_id.employee_id.department_id.department_head_ids')
    def _compute_dept_head_user_ids(self):
        group = self.env['res.groups']
        dep_head = self.env.ref('department_groups.group_staff_dept_head').id
        con_dep_head = self.env.ref('department_groups.group_construction_dept_head').id
        soft_dep_head = self.env.ref('department_groups.group_soft_ser_dept_head').id
        admin_settings = self.env.ref('base.group_system').id
        contract_mgr = self.env.ref('department_groups.group_contarct_bids_exec').id
        cn_emp_mgr = self.env.ref('department_groups.group_contarct_bde').id
        all_users = self.env['res.users'].search([('groups_id', 'in', [dep_head,con_dep_head,soft_dep_head,admin_settings,contract_mgr,cn_emp_mgr])])
        user_list = []
        departments = self.env['hr.department'].search([])
        for rec in self:
            if departments.department_head_ids:
                for head in departments.department_head_ids:
                    if head.user_id:
                        user_list.append(head.user_id.id)
            rec.dept_head_user_ids = user_list
            rec.approval_create_user_ids = user_list + all_users.ids

    def action_self_assign(self):
        for rec in self:
            if rec.env.user.has_group('department_groups.group_hr_mgr'):
                hr_manager_group = self.env.ref('department_groups.group_hr_mgr')
                hr_manager_users = self.env['res.users'].search([('groups_id', 'in', hr_manager_group.id)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in hr_manager_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_hr_id': rec.env.user.id})
            elif rec.user_has_groups('department_groups.group_hr_talent_acquisition_exec'):

                talent_user = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
                dept_head_groups = self.env['res.groups'].browse((talent_user.id))
                dept_head_users = self.env['res.users'].search(
                    [('groups_id', 'in', dept_head_groups.ids)])
                rec.assigned_user_ids = [Command.unlink(user.id) for user in dept_head_users]
                rec.assigned_user_ids = [Command.link(rec.env.user.id)]
                rec.write({'is_self_assign': True, 'approval_talent_id': rec.env.user.id})



    # def action_self_assign(self):
    #     for rec in self:
    #         rec.self_assignee_ids = False
    #         rec.assigned_user_ids = False
    #         rec.assigned_user_ids = [Command.link(rec.env.user.id)]
    #         rec.write({'is_self_assign': True, 'self_assignee_ids': [Command.link(rec.env.user.id)], })


    @api.depends('assigned_user_ids', 'assigned_group_ids')
    def _compute_logged_user(self):
        for vals in self:
            vals.is_logged_user = False
            vals.is_logged_group = False
            if vals.env.user in vals.assigned_user_ids:
                vals.is_logged_user = True
            if vals.env.user in vals.assigned_group_ids.users:
                vals.is_logged_group = True


    @api.model
    def default_get(self, fields):
        user_list = []
        departments = self.env['hr.department'].search([])
        if departments.department_head_ids:
            for head in departments.department_head_ids:
                if head.user_id:
                    user_list.append(head.user_id.id)
        if self.env.user.user_has_groups(
                'department_groups.group_staff_dept_head,'
                'department_groups.group_construction_dept_head,'
                'department_groups.group_soft_ser_dept_head,'
                'department_groups.group_contarct_bids_exec,'
                'base.group_system,'
                'department_groups.group_contarct_bde',) or self.env.user.id in user_list:
            return super(ManpowerRequisition, self).default_get(fields)
        else:
            raise exceptions.ValidationError("You cannot Create Manpower Requisition")

    @api.onchange('user_id')
    def onchange_user_id(self):
        for rec in self:
            if rec.user_id and rec.user_id.department_id:
                rec.team_dp_id = rec.user_id.department_id.id
            else:
                rec.team_dp_id = False

    @api.depends('status')
    def action_submit_for_hr_approval(self):
        group = self.env['res.groups']
        head = self.env.ref('department_groups.group_hr_mgr')
        hr_users = self.env['res.users'].search([('groups_id', 'in', [head.id])])
        acc_team_users = self.env['res.users'].search([('groups_id', 'in', [head.id])])
        acc_team_groups = self.env['res.groups'].browse((head.id))
        for rec in self:
            rec.sudo().write({
                'assigned_user_ids': [(6, 0, acc_team_users.ids)],
                'assigned_group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False,

            })

            try:
                rec.sudo().status = 'submit_for_hr_approval'
                template_id = rec.env.ref('manpower_requisition.mail_template_user_approval', raise_if_not_found=False)

                hr_users = self.env.ref('department_groups.group_hr_mgr')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [hr_users.id])])
                acc_team_groups = self.env['res.groups'].browse((hr_users.id))

                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (rec.id or rec._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(rec.id or rec._origin.id,
                                                                                          force_send=True,
                                                                                          raise_exception=False)
                # if template_id:
                #     template = rec.env['mail.template'].browse(template_id.id)
                #     template.send_mail(rec.id, force_send=True)
                else:
                    raise exceptions.ValidationError('Email template not found. Cannot proceed.')
            except Exception as e:
                raise exceptions.ValidationError('An error occurred while submitting for HR approval: %s' % str(e))

    def action_submit_for_ta_approval(self):
        group = self.env['res.groups']
        ta_group = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
        acc_team_users = self.env['res.users'].search([('groups_id', 'in', [ta_group.id])])
        acc_team_groups = self.env['res.groups'].browse((ta_group.id))
        for rec in self:
            try:
                rec.sudo().write({
                    'is_approve': False,
                    'status': 'submit_for_ta_approval',
                    'assigned_user_ids': [(6, 0, acc_team_users.ids)],
                    'assigned_group_ids': [(6, 0, acc_team_groups.ids)],
                    'is_self_assign': False,
                })
                template_id = rec.env.ref('manpower_requisition.mail_template_hr_approval')


                ta_user = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
                acc_team_users = self.env['res.users'].search([('groups_id', 'in',
                                                                [ta_user.id])])
                acc_team_groups = self.env['res.groups'].browse((ta_user.id))

                mail_to = []
                for user in acc_team_users:
                    if user.partner_id.email:
                        mail_to.append(user.partner_id.email)
                reorder_qty_mail_to = ",".join(mail_to)

                if template_id and (rec.id or rec._origin.id) and reorder_qty_mail_to:
                    template_id.write({'email_to': reorder_qty_mail_to})
                    template_id.with_context({'email_to': reorder_qty_mail_to}).send_mail(rec.id or rec._origin.id,
                                                                                          force_send=True,
                                                                                          raise_exception=False)


                # if template_id:
                #     template = rec.env['mail.template'].browse(template_id)
                #     template.send_mail(rec.id, force_send=True)
            except Exception as e:
                raise exceptions.ValidationError(f"An error occurred: {e}")

    def action_confirm(self):
        group = self.env['res.groups']
        ta_group = self.env.ref('department_groups.group_hr_talent_acquisition_exec')
        acc_team_users = self.env['res.users'].search([('groups_id', 'in', [ta_group.id])])
        acc_team_groups = self.env['res.groups'].browse((ta_group.id))
        for record in self:
            hr_job = self.env['hr.job'].create({
                'name': record.role,
                'department_id': record.department_id.id,
                'type': record.type,
                'recruitment_type': record.recruitment_type,
                'no_of_recruitment': record.number_of_positions,
                'user_id': record.user_id.id,
                'branch_id': record.branch_id.id,
                'salary_upto': record.salary_upto,
                'skills_ids': [(6, 0, record.skills_ids.ids)],
                'qualification_ids': [(6, 0, record.qualification_ids.ids)],
                'source_id': record.id,
                'description': record.description
            })
            record.sudo().write({
                'status': 'confirmed',
                'confirmed_user_id': self.env.user.id,
                'confirmed_date': fields.datetime.today(),
                'job_id': hr_job.id,
                'assigned_user_ids': [(6, 0, acc_team_users.ids)],
                'assigned_group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False,

            })

            return {
                'effect': {
                    'fadeout': 'slow',
                    'message': 'Job Position Created Successfully',
                    'type': 'rainbow_man',
                }
            }

    def action_reject_form(self):
        reject_reason = self.env.ref('manpower_requisition.reject_reason_view_form')
        return {
            'name': 'Reject Reason',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'manpower.requisition',
            'views': [(reject_reason.id, 'form')],
            'view_id': reject_reason.id,
            'target': 'new',
            'res_id': self.id,
        }

    def action_on_hold_form(self):
        hold_reason = self.env.ref('manpower_requisition.hold_reason_view_form')
        return {
            'name': 'On Hold Reason',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'manpower.requisition',
            'views': [(hold_reason.id, 'form')],
            'view_id': hold_reason.id,
            'target': 'new',
            'res_id': self.id,
        }

    @api.model_create_multi
    def create(self, vals_list):
        user_list = []
        departments = self.env['hr.department'].search([])
        if departments.department_head_ids:
            for head in departments.department_head_ids:
                if head.user_id:
                    user_list.append(head.user_id.id)
        if self.env.user.user_has_groups(
                'department_groups.group_staff_dept_head,'
                'department_groups.group_construction_dept_head,'
                'department_groups.group_soft_ser_dept_head,'
                'department_groups.group_contarct_bids_exec,'
                'base.group_system,'
                'department_groups.group_contarct_bde', ) or self.env.user.id in user_list:
            for vals in vals_list:
                if not vals.get('name') or vals['name'] == _('New'):
                    vals['name'] = self.env['ir.sequence'].next_by_code('manpower_requisition_sequence') or _('New')
        else:
            raise exceptions.ValidationError("You cannot Create Manpower Requisition")


        return super().create(vals_list)

    def action_set_on_hold(self):
        for record in self:
            if record.status == 'submit_for_hr_approval':
                record.write({
                    'is_onhold': False,
                    'is_approve': True,
                    'onhold_reason': record.temp_onhold_reason,
                    'onhold_user_id': record.env.user.id,
                    'onhold_date': fields.datetime.today()
                })
            elif record.status == 'submit_for_ta_approval':
                record.write({
                    'is_onhold': True,
                    'ta_onhold_reason': record.temp_onhold_reason,
                    'onhold_user_id': record.env.user.id,
                    'onhold_date': fields.datetime.today()
                })

            record.write({
                'status': 'on_hold',
                'temp_onhold_reason': False,
                # 'assigned_user_ids': [(6, 0, acc_team_users.ids)],
                # 'assigned_group_ids': [(6, 0, acc_team_groups.ids)],
                'is_self_assign': False,
            })

    def action_reject(self):
        for record in self:
            record.write({
                'reject_reason': record.reject_reason,
                'reject_user_id': record.env.user.id,
                'reject_date': fields.datetime.today(),
                'status': 'rejected',
                'is_self_assign': False,
            })

    def action_job_positions(self):
        return {
            "type": "ir.actions.act_window",
            "name": "Job Position",
            "res_model": "hr.job",
            "view_mode": "tree,form",
            "domain": [("source_id", "=", self.ids)],
        }
