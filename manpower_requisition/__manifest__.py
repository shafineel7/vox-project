# -*- coding: utf-8 -*-

{
    'name': 'Manpower Requisition',
    'version': '17.0.1.0.1',
    'summary': 'Manage manpower requisitions for job positions',
    'description': 'This module allows you to create and manage manpower requisitions for various job positions within your organization.',
    'category': 'Human Resources',
    'author': 'Shafi PK',
    'website': 'https://www.voxtronme.com',
    'depends': ['employee', 'hr_recruitment', 'department_groups'],
    'data': [
        'security/ir.model.access.csv',
        'security/manpower_requisition_security.xml',
        'data/ir_sequence_data.xml',
        'data/mail_template_data.xml',
        'views/manpower_requisition_views.xml',
        'views/manpower_requisition_menus.xml',
        'views/reject_reason_views.xml',
        'views/hold_reason_views.xml',
        'views/hr_job_position_views.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
