from odoo import models, fields, api, _


class AccountJournal(models.Model):
    _inherit = "account.journal"

    is_cheque = fields.Boolean(string='Cheque',default=False)