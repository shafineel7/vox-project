# -*- coding: utf-8 -*-
from odoo import fields, models, _,api
import datetime
from datetime import datetime,date, timedelta
# import tkinter
# from tkinter import messagebox

class AccountPayment(models.Model):
    _inherit = "account.payment"
    _inherits = {'account.move': 'move_id'}


    cheque_tracker_status_id = fields.Many2one('cheque.tracker.status',string="Cheque Tracker")
    is_mail_sent = fields.Boolean("Send Mail")
    alert_required = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Alert Required')
    is_check_tracker = fields.Boolean("Check Tracker")
    is_cheque = fields.Boolean(string='Cheque',related="journal_id.is_cheque")




    @api.onchange('cheque_tracker_status_id')
    def _onchange_cheque_tracker_status_id(self):
        for rec in self:
            if rec.cheque_tracker_status_id:
                rec.alert_required = rec.cheque_tracker_status_id.alert_required

    def action_send_email_checque_tracker(self):
        payments = self.env['account.payment'].sudo().search([])
        # date_now = fields.datetime.today()
        date_now = date.today()
        for rec in payments:
            if rec.date and rec.cheque_tracker_status_id:
                date_before_2_days = rec.date - timedelta(rec.cheque_tracker_status_id.no_of_days)
                if date_now >= date_before_2_days and rec.alert_required == 'yes':
                    rec.is_check_tracker = True
                    if rec.is_mail_sent == False:
                        account_user = self.env.ref('account.group_account_user')
                        account_manager = self.env.ref('account.group_account_manager')
                        account_manager_users = self.env['res.users'].search(
                            [('groups_id', 'in', [account_user.id,account_manager.id])])
                        email_template = self.env.ref('cheque_tracker_status.email_template_check_tracker_status')
                        account_manager_emails = [manager.email for manager in account_manager_users if manager.email]
                        if account_manager_emails:
                            email_values = {'email_to': ','.join(account_manager_emails)}
                            email_template.with_context(email_values).send_mail(rec.id, force_send=True,email_values=email_values)
                            rec.is_mail_sent = True

                    # messagebox.showinfo(message='Please Check the Cheque Status Payments')
                    # messagebox.showinfo("Title", "Please Check the Cheque Tracker Status of %s" % rec.name)

                else:
                    rec.is_check_tracker = False


