# -*- coding: utf-8 -*-
from odoo import models, fields, api, Command, _
from odoo.exceptions import ValidationError, UserError
import datetime
from dateutil.relativedelta import relativedelta


class EmployeeExitManagement(models.Model):
    _name = 'cheque.tracker.status'
    _description = 'Cheque Tracker Status '


    name = fields.Char(string='Status')
    alert_required = fields.Selection([('yes', 'Yes'), ('no', 'NO'), ], string='Alert Required')
    no_of_days = fields.Integer(string="Number of Days")
    is_completed = fields.Boolean(string="Completed")
