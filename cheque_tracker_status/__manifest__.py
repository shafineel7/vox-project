# -*- coding: utf-8 -*-
{
    'name': ' Cheque Tracker Status ',
    'version': '17.0.1.0.0',
    'category': 'Human Resources',
    'author': 'Hilsha P H',
    'summary': ' Cheque Tracker Status ',
    'description': """
       Cheque Tracker Status 
    """,
    'depends': ['account'],
    'data': [
        'security/ir.model.access.csv',
        'data/cheque_tracker_status_cron.xml',
        'data/mail_template_data.xml',
        'views/account_journal_views.xml',
        'views/cheque_tracker_status_views.xml',
        'views/account_payment_views.xml',
        'wizard/cheque_tracker_report.xml',
        'report/cheque_tracker_status_templates.xml',
        'report/check_tracker_status_report.xml',
        'views/cheque_tracker_status_menus.xml',

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
