{
    'name': 'Employee Attendance',
    'author': 'Sourav',
    'summary': 'Module for showing employee details on attendance list and form views.',
    'version': '17.0.1.0.0',
    'category': 'Human Resources/Attendances',
    'depends': ['hr_attendance', 'employee'],
    'description': """Extra information about employee, such as company, branch, department will be available
    in employee attendance views.""",
    'data': [
        'views/hr_attendance_views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}