# -*- coding: utf-8 -*-
from odoo import models, fields


class CoverSheetContractDetails(models.Model):
    _name = 'cover.sheet.con.details'
    _description = 'Cover Sheet Contract Details'

    desi_product_id = fields.Many2one('product.product', string="Role")
    rate = fields.Float(string="Rate")
    work_days_week = fields.Integer(string="Working Days/Week")
    cover_sheet_id = fields.Many2one('cover.sheet', string="Cover Sheet")
