# -*- coding: utf-8 -*-
from odoo import models, fields


class CoverSheetLine(models.Model):
    _name = 'cover.sheet.line'
    _description = 'Cover Sheet Line'

    date = fields.Date(string="Date")
    desi_product_id = fields.Many2one('product.product', string="Designation Product")
    no_of_days = fields.Integer(string="No.of Days")
    rate_per_day = fields.Float(string="Rate/Day")
    normal_ot_rate = fields.Float(string="Normal OT Rate")
    holiday_ot_rate = fields.Float(string="Holiday OT Rate")
    ramdan_ot_rate = fields.Float(string="Ramdan OT Rate")
    weekend_ot_rate = fields.Float(string="Weekend OT Rate")
    total_value = fields.Float(string="Total Value")
    normal_ot_hours = fields.Float(string="Normal OT Hours")
    holiday_ot_hours = fields.Float(string="Holiday OT Hours")
    ramdan_ot_hours = fields.Float(string="Ramdan OT Hours")
    weekend_ot_hours = fields.Float(string="Weekend OT Hours")
    cover_sheet_id = fields.Many2one('cover.sheet', string="Cover Sheet")
