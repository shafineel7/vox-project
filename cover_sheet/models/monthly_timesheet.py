# -*- coding: utf-8 -*-

from odoo import models, fields, _
import itertools
import calendar
from odoo.exceptions import ValidationError


class MonthlyTimesheet(models.Model):
    _inherit = 'monthly.timesheet'

    cover_sheet_id = fields.Many2one('cover.sheet', string="Cover Sheet")

    def action_generate_cover_sheet(self):
        con_details_lines_list = []
        timesheet_products = list(set(self.employee_timesheet_ids.mapped('product_id')))
        contract_products = list(set(self.customer_contract_id.contract_lines_ids.mapped('product_id')))
        time_not_con_products = []

        for tp in timesheet_products:
            if tp not in contract_products:
                time_not_con_products.append(tp.name)

        if time_not_con_products:
            raise ValidationError(_('The designations not found in contract {}').format(time_not_con_products))

        for rec in self:
            type_description = dict(self._fields['month']._description_selection(self.env))
            month_name = type_description[self.month]
            no_of_days_time_month = calendar._monthlen(rec.year, int(rec.month))

            existing_cover_sheet = self.env['cover.sheet'].search([('monthly_timesheet_id', '=', rec.id)])
            existing_cover_sheet.sudo().unlink()

            cover_sheet = self.env['cover.sheet'].create(
                {'partner_id': rec.customer_contract_id.partner_id.id, 'contract_id': rec.customer_contract_id.id,
                 'location_id': rec.location_id.id, 'con_start_date': rec.customer_contract_id.contract_start_date,
                 'monthly_timesheet_id': rec.id, 'inv_for': month_name + ' ' + rec.year,
                 'no_of_days_month': no_of_days_time_month})
            rec.cover_sheet_id = cover_sheet.id

            for line in rec.customer_contract_id.contract_lines_ids:
                con_details_lines = {'desi_product_id': line.product_id.id, 'work_days_week': line.day_count,
                                     'rate': line.unit_price, 'cover_sheet_id': cover_sheet.id}
                con_details_lines_list.append(con_details_lines)

        cs_timehseet_lines = self.employee_timesheet_ids.filtered(lambda s: s.hour_prefix not in ('j', 'r', 's', 'i'))
        sorted_correctC = sorted(cs_timehseet_lines, key=lambda x: (x.product_id.name), reverse=True)
        sorted_correctK = sorted(sorted_correctC, key=lambda x: (x.date))
        lst = []
        for rec in sorted_correctK:
            lst.append([rec.date, rec.product_id, rec])
        cover_sheet_lines_list = []
        no_of_days = 0
        for k, g in itertools.groupby(sorted(lst, key=lambda x: x[0]), lambda x: x[1]):
            l = list(g)
            weekend_time = weekend_ot_rate = holiday_time = holiday_ot_rate = ramdan_time = ramdan_ot_rate = normal_time = normal_ot_rate = no_of_days = rate_per_day = total_amount = 0.0
            for d in l:
                desi = d[1]
                date = d[0]
                contract_line = self.env['contract.lines'].search(
                    [('contract_id', '=', self.customer_contract_id.id), ('product_id', '=', d[1].id)], limit=1)

                if d[2].hour_prefix not in ('o', 'e', 'h'):
                    no_of_days += 1
                if contract_line.day_count == 7:
                    no_of_days_month = calendar._monthlen(date.year, date.month)
                    rate_per_day = contract_line.unit_price / no_of_days_month if no_of_days != 0 else 0.0
                    total_amount = rate_per_day * no_of_days if no_of_days != 0 else 0.0

                    if d[2].hour_prefix == 'o':
                        for ot in contract_line.contract_inv_line_ids:
                            if ot.type == 'overtime' and ot.overtime_type_id.code == 'o' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                weekend_time += d[2].hour_spent
                                weekend_ot_rate = ot.inv_amount

                    if d[2].hour_prefix == 'h':
                        for ot in contract_line.contract_inv_line_ids:
                            if ot.type == 'overtime' and ot.overtime_type_id.code == 'h' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                holiday_time += d[2].hour_spent
                                holiday_ot_rate = ot.inv_amount

                    if d[2].hour_prefix == 'e':
                        for ot in contract_line.contract_inv_line_ids:
                            if ot.type == 'overtime' and ot.overtime_type_id.code == 'e' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                ramdan_time += d[2].hour_spent
                                ramdan_ot_rate = ot.inv_amount

                    if d[2].hour_prefix not in ('e', 'h', 'o'):
                        if contract_line.invoice_hours < d[2].hour_spent:
                            time_diff = d[2].hour_spent - contract_line.invoice_hours
                            for ot in contract_line.contract_inv_line_ids:
                                if ot.type == 'overtime' and ot.overtime_type_id.code == 'n' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                    normal_time += time_diff
                                    normal_ot_rate = ot.inv_amount



                elif contract_line.day_count == 6:
                    no_of_days_month = calendar._monthlen(date.year, date.month)
                    matrix = calendar.monthcalendar(date.year, date.month)
                    num_sunday = sum(1 for x in matrix if x[calendar.SUNDAY] != 0)
                    rate_per_day = contract_line.unit_price / (
                            no_of_days_month - num_sunday) if no_of_days != 0 else 0.0
                    total_amount = rate_per_day * no_of_days if no_of_days != 0 else 0.0

                    if d[2].hour_prefix == 'o':
                        for ot in contract_line.contract_inv_line_ids:
                            if ot.type == 'overtime' and ot.overtime_type_id.code == 'o' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                weekend_time += d[2].hour_spent
                                weekend_ot_rate = ot.inv_amount

                    if d[2].hour_prefix == 'h':
                        for ot in contract_line.contract_inv_line_ids:
                            if ot.type == 'overtime' and ot.overtime_type_id.code == 'h' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                holiday_time += d[2].hour_spent
                                holiday_ot_rate = ot.inv_amount

                    if d[2].hour_prefix == 'e':
                        for ot in contract_line.contract_inv_line_ids:
                            if ot.type == 'overtime' and ot.overtime_type_id.code == 'e' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                ramdan_time += d[2].hour_spent
                                ramdan_ot_rate = ot.inv_amount

                    if d[2].hour_prefix not in ('e', 'h', 'o'):
                        if contract_line.invoice_hours < d[2].hour_spent:
                            time_diff = d[2].hour_spent - contract_line.invoice_hours
                            for ot in contract_line.contract_inv_line_ids:
                                if ot.type == 'overtime' and ot.overtime_type_id.code == 'n' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                    normal_time += time_diff
                                    normal_ot_rate = ot.inv_amount



                elif contract_line.day_count == 5:
                    no_of_days_month = calendar._monthlen(date.year, date.month)
                    matrix = calendar.monthcalendar(date.year, date.month)
                    num_sunday = sum(1 for x in matrix if x[calendar.SUNDAY] != 0)
                    num_sat = sum(1 for x in matrix if x[calendar.SATURDAY] != 0)
                    rate_per_day = contract_line.unit_price / (
                            no_of_days_month - num_sunday - num_sat) if no_of_days != 0 else 0.0
                    total_amount = rate_per_day * no_of_days if no_of_days != 0 else 0.0

                    if d[2].hour_prefix == 'o':
                        for ot in contract_line.contract_inv_line_ids:
                            if ot.type == 'overtime' and ot.overtime_type_id.code == 'o' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                weekend_time += d[2].hour_spent
                                weekend_ot_rate = ot.inv_amount

                    if d[2].hour_prefix == 'h':
                        for ot in contract_line.contract_inv_line_ids:
                            if ot.type == 'overtime' and ot.overtime_type_id.code == 'h' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                holiday_time += d[2].hour_spent
                                holiday_ot_rate = ot.inv_amount

                    if d[2].hour_prefix == 'e':
                        for ot in contract_line.contract_inv_line_ids:
                            if ot.type == 'overtime' and ot.overtime_type_id.code == 'e' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                ramdan_time += d[2].hour_spent
                                ramdan_ot_rate = ot.inv_amount

                    if d[2].hour_prefix not in ('e', 'h', 'o'):
                        if contract_line.invoice_hours < d[2].hour_spent:
                            time_diff = d[2].hour_spent - contract_line.invoice_hours
                            for ot in contract_line.contract_inv_line_ids:
                                if ot.type == 'overtime' and ot.overtime_type_id.code == 'n' and ot.inv_based_on == 'hours' and ot.inv_mode == 'fixed' and ot.inv_billed_to == 'customer':
                                    normal_time += time_diff
                                    normal_ot_rate = ot.inv_amount

                total_amount = (rate_per_day * no_of_days) + (weekend_time * weekend_ot_rate) + (
                        holiday_time * holiday_ot_rate) + (ramdan_time * ramdan_ot_rate) + (
                                       normal_time * normal_ot_rate)

            cover_sheet_lines = {'date': date, 'desi_product_id': desi.id, 'no_of_days': no_of_days,
                                 'rate_per_day': rate_per_day, 'total_value': total_amount,
                                 'cover_sheet_id': cover_sheet.id, 'weekend_ot_hours': weekend_time,
                                 'weekend_ot_rate': weekend_ot_rate, 'holiday_ot_hours': holiday_time,
                                 'holiday_ot_rate': holiday_ot_rate,
                                 'ramdan_ot_hours': ramdan_time, 'ramdan_ot_rate': ramdan_ot_rate,
                                 'normal_ot_hours': normal_time, 'normal_ot_rate': normal_ot_rate}
            cover_sheet_lines_list.append(cover_sheet_lines)

        self.env['cover.sheet.line'].create(cover_sheet_lines_list)
        self.env['cover.sheet.con.details'].create(con_details_lines_list)
