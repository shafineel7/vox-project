# -*- coding: utf-8 -*-
from odoo import models, fields


class CoverSheet(models.Model):
    _name = 'cover.sheet'
    _description = 'Cover Sheet'
    _rec_name = 'contract_id'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    partner_id = fields.Many2one('res.partner', string="Client Name")
    contract_id = fields.Many2one('contract.contracts', string="Contract")
    location_id = fields.Many2one('hr.work.location', string="Location")
    con_start_date = fields.Date(string="Contract Start Date")
    inv_date = fields.Date(string="Invoice Date")
    inv_for = fields.Char(string="Invoice For")
    no_of_days_month = fields.Integer(string="No.of days in invoice month")
    cover_sheet_line_ids = fields.One2many('cover.sheet.line', 'cover_sheet_id', string="Cover Sheet Line")
    monthly_timesheet_id = fields.Many2one('monthly.timesheet', string="Monthly Timesheet")
    cover_sheet_contract_details_ids = fields.One2many('cover.sheet.con.details', 'cover_sheet_id',
                                                       string="Contract Details")
    company_id = fields.Many2one('res.company', string='Company', readonly=False,
                                 default=lambda self: self.env.company)
