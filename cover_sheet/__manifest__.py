# -*- coding: utf-8 -*-
{
    'name': 'Cover Sheet',
    'version': '17.0.1.0.0',
    'category': 'Accounting',
    'author': 'Muhammed Aslam',
    'summary': 'Cover Sheet module for generate coversheet from timesheet',
    'description': """
        This module used to generate coversheet based on timesheet and contract 
    """,
    'depends': ['crm_contracts', 'employee',  'account_accountant', 'mail', 'direct_employee_payroll', 'employee_timesheet', 'document_layout'],
    'data': [
        'security/ir.model.access.csv',
        'views/cover_sheet_views.xml',
        'views/monthly_timesheet_views.xml',
        'views/cover_sheet_menus.xml',
        'report/cover_sheet_templates.xml',
        'report/cover_sheet_report.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
